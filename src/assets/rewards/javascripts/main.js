function modifyIconClass(t) {}
function isScrolledIntoView(t) {
    var e = jQuery(window).scrollTop()
      , n = e + jQuery(window).height()
      , i = jQuery(t).offset().top;
    return i + jQuery(t).height() <= n && i >= e
}
function Utils() {}
function circleAnimation() {
    var t = jQuery("#path-circle").data("path");
    6 === ++counter && (counter = 5,
    window.clearInterval(animationInterval));
    var e = document.getElementById("img-circle")
      , n = document.getElementById("img-circle2");
    e && n && (e.src = t + "test" + counter + ".png",
    n.src = t + "test" + counter + ".png")
}
function resetIconsClass(t) {
    for (var e = 1; e < 9; e++) {
        var n = "icon-" + e
          , i = document.getElementById(n);
        i && (i.className = t === n ? "active" : "")
    }
}
function changeImageRewards(t) {
    var e = RepoCoinConstants.templateDirectoryUri + "/dist/images/how-it-works/rewards/" + t + ".png"
      , n = document.getElementById("place-img");
    n && (n.src = e)
}
function iconClick(t, e) {
    window.clearInterval(phoneInterval);
    var n = t.id;
    changeImageRewards(n),
    resetIconsClass(n)
}
if (function(t, e) {
    "use strict";
    "object" == typeof module && "object" == typeof module.exports ? module.exports = t.document ? e(t, !0) : function(t) {
        if (!t.document)
            throw new Error("jQuery requires a window with a document");
        return e(t)
    }
    : e(t)
}("undefined" != typeof window ? window : this, function(t, e) {
    "use strict";
    function n(t, e) {
        e = e || nt;
        var n = e.createElement("script");
        n.text = t,
        e.head.appendChild(n).parentNode.removeChild(n)
    }
    function i(t) {
        var e = !!t && "length"in t && t.length
          , n = ht.type(t);
        return "function" !== n && !ht.isWindow(t) && ("array" === n || 0 === e || "number" == typeof e && e > 0 && e - 1 in t)
    }
    function o(t, e) {
        return t.nodeName && t.nodeName.toLowerCase() === e.toLowerCase()
    }
    function r(t, e, n) {
        return ht.isFunction(e) ? ht.grep(t, function(t, i) {
            return !!e.call(t, i, t) !== n
        }) : e.nodeType ? ht.grep(t, function(t) {
            return t === e !== n
        }) : "string" != typeof e ? ht.grep(t, function(t) {
            return at.call(e, t) > -1 !== n
        }) : Tt.test(e) ? ht.filter(e, t, n) : (e = ht.filter(e, t),
        ht.grep(t, function(t) {
            return at.call(e, t) > -1 !== n && 1 === t.nodeType
        }))
    }
    function s(t, e) {
        for (; (t = t[e]) && 1 !== t.nodeType; )
            ;
        return t
    }
    function a(t) {
        var e = {};
        return ht.each(t.match(Pt) || [], function(t, n) {
            e[n] = !0
        }),
        e
    }
    function l(t) {
        return t
    }
    function c(t) {
        throw t
    }
    function u(t, e, n, i) {
        var o;
        try {
            t && ht.isFunction(o = t.promise) ? o.call(t).done(e).fail(n) : t && ht.isFunction(o = t.then) ? o.call(t, e, n) : e.apply(void 0, [t].slice(i))
        } catch (t) {
            n.apply(void 0, [t])
        }
    }
    function d() {
        nt.removeEventListener("DOMContentLoaded", d),
        t.removeEventListener("load", d),
        ht.ready()
    }
    function f() {
        this.expando = ht.expando + f.uid++
    }
    function p(t) {
        return "true" === t || "false" !== t && ("null" === t ? null : t === +t + "" ? +t : Mt.test(t) ? JSON.parse(t) : t)
    }
    function h(t, e, n) {
        var i;
        if (void 0 === n && 1 === t.nodeType)
            if (i = "data-" + e.replace(Nt, "-$&").toLowerCase(),
            "string" == typeof (n = t.getAttribute(i))) {
                try {
                    n = p(n)
                } catch (t) {}
                Rt.set(t, e, n)
            } else
                n = void 0;
        return n
    }
    function g(t, e, n, i) {
        var o, r = 1, s = 20, a = i ? function() {
            return i.cur()
        }
        : function() {
            return ht.css(t, e, "")
        }
        , l = a(), c = n && n[3] || (ht.cssNumber[e] ? "" : "px"), u = (ht.cssNumber[e] || "px" !== c && +l) && Ft.exec(ht.css(t, e));
        if (u && u[3] !== c) {
            c = c || u[3],
            n = n || [],
            u = +l || 1;
            do {
                r = r || ".5",
                u /= r,
                ht.style(t, e, u + c)
            } while (r !== (r = a() / l) && 1 !== r && --s)
        }
        return n && (u = +u || +l || 0,
        o = n[1] ? u + (n[1] + 1) * n[2] : +n[2],
        i && (i.unit = c,
        i.start = u,
        i.end = o)),
        o
    }
    function v(t) {
        var e, n = t.ownerDocument, i = t.nodeName, o = Bt[i];
        return o || (e = n.body.appendChild(n.createElement(i)),
        o = ht.css(e, "display"),
        e.parentNode.removeChild(e),
        "none" === o && (o = "block"),
        Bt[i] = o,
        o)
    }
    function m(t, e) {
        for (var n, i, o = [], r = 0, s = t.length; r < s; r++)
            i = t[r],
            i.style && (n = i.style.display,
            e ? ("none" === n && (o[r] = Dt.get(i, "display") || null,
            o[r] || (i.style.display = "")),
            "" === i.style.display && Ht(i) && (o[r] = v(i))) : "none" !== n && (o[r] = "none",
            Dt.set(i, "display", n)));
        for (r = 0; r < s; r++)
            null != o[r] && (t[r].style.display = o[r]);
        return t
    }
    function y(t, e) {
        var n;
        return n = void 0 !== t.getElementsByTagName ? t.getElementsByTagName(e || "*") : void 0 !== t.querySelectorAll ? t.querySelectorAll(e || "*") : [],
        void 0 === e || e && o(t, e) ? ht.merge([t], n) : n
    }
    function b(t, e) {
        for (var n = 0, i = t.length; n < i; n++)
            Dt.set(t[n], "globalEval", !e || Dt.get(e[n], "globalEval"))
    }
    function w(t, e, n, i, o) {
        for (var r, s, a, l, c, u, d = e.createDocumentFragment(), f = [], p = 0, h = t.length; p < h; p++)
            if ((r = t[p]) || 0 === r)
                if ("object" === ht.type(r))
                    ht.merge(f, r.nodeType ? [r] : r);
                else if (Xt.test(r)) {
                    for (s = s || d.appendChild(e.createElement("div")),
                    a = (qt.exec(r) || ["", ""])[1].toLowerCase(),
                    l = Vt[a] || Vt._default,
                    s.innerHTML = l[1] + ht.htmlPrefilter(r) + l[2],
                    u = l[0]; u--; )
                        s = s.lastChild;
                    ht.merge(f, s.childNodes),
                    s = d.firstChild,
                    s.textContent = ""
                } else
                    f.push(e.createTextNode(r));
        for (d.textContent = "",
        p = 0; r = f[p++]; )
            if (i && ht.inArray(r, i) > -1)
                o && o.push(r);
            else if (c = ht.contains(r.ownerDocument, r),
            s = y(d.appendChild(r), "script"),
            c && b(s),
            n)
                for (u = 0; r = s[u++]; )
                    zt.test(r.type || "") && n.push(r);
        return d
    }
    function E() {
        return !0
    }
    function C() {
        return !1
    }
    function x() {
        try {
            return nt.activeElement
        } catch (t) {}
    }
    function T(t, e, n, i, o, r) {
        var s, a;
        if ("object" == typeof e) {
            "string" != typeof n && (i = i || n,
            n = void 0);
            for (a in e)
                T(t, a, n, i, e[a], r);
            return t
        }
        if (null == i && null == o ? (o = n,
        i = n = void 0) : null == o && ("string" == typeof n ? (o = i,
        i = void 0) : (o = i,
        i = n,
        n = void 0)),
        !1 === o)
            o = C;
        else if (!o)
            return t;
        return 1 === r && (s = o,
        o = function(t) {
            return ht().off(t),
            s.apply(this, arguments)
        }
        ,
        o.guid = s.guid || (s.guid = ht.guid++)),
        t.each(function() {
            ht.event.add(this, e, o, i, n)
        })
    }
    function k(t, e) {
        return o(t, "table") && o(11 !== e.nodeType ? e : e.firstChild, "tr") ? ht(">tbody", t)[0] || t : t
    }
    function S(t) {
        return t.type = (null !== t.getAttribute("type")) + "/" + t.type,
        t
    }
    function _(t) {
        var e = ne.exec(t.type);
        return e ? t.type = e[1] : t.removeAttribute("type"),
        t
    }
    function A(t, e) {
        var n, i, o, r, s, a, l, c;
        if (1 === e.nodeType) {
            if (Dt.hasData(t) && (r = Dt.access(t),
            s = Dt.set(e, r),
            c = r.events)) {
                delete s.handle,
                s.events = {};
                for (o in c)
                    for (n = 0,
                    i = c[o].length; n < i; n++)
                        ht.event.add(e, o, c[o][n])
            }
            Rt.hasData(t) && (a = Rt.access(t),
            l = ht.extend({}, a),
            Rt.set(e, l))
        }
    }
    function P(t, e) {
        var n = e.nodeName.toLowerCase();
        "input" === n && Ut.test(t.type) ? e.checked = t.checked : "input" !== n && "textarea" !== n || (e.defaultValue = t.defaultValue)
    }
    function L(t, e, i, o) {
        e = rt.apply([], e);
        var r, s, a, l, c, u, d = 0, f = t.length, p = f - 1, h = e[0], g = ht.isFunction(h);
        if (g || f > 1 && "string" == typeof h && !pt.checkClone && ee.test(h))
            return t.each(function(n) {
                var r = t.eq(n);
                g && (e[0] = h.call(this, n, r.html())),
                L(r, e, i, o)
            });
        if (f && (r = w(e, t[0].ownerDocument, !1, t, o),
        s = r.firstChild,
        1 === r.childNodes.length && (r = s),
        s || o)) {
            for (a = ht.map(y(r, "script"), S),
            l = a.length; d < f; d++)
                c = r,
                d !== p && (c = ht.clone(c, !0, !0),
                l && ht.merge(a, y(c, "script"))),
                i.call(t[d], c, d);
            if (l)
                for (u = a[a.length - 1].ownerDocument,
                ht.map(a, _),
                d = 0; d < l; d++)
                    c = a[d],
                    zt.test(c.type || "") && !Dt.access(c, "globalEval") && ht.contains(u, c) && (c.src ? ht._evalUrl && ht._evalUrl(c.src) : n(c.textContent.replace(ie, ""), u))
        }
        return t
    }
    function I(t, e, n) {
        for (var i, o = e ? ht.filter(e, t) : t, r = 0; null != (i = o[r]); r++)
            n || 1 !== i.nodeType || ht.cleanData(y(i)),
            i.parentNode && (n && ht.contains(i.ownerDocument, i) && b(y(i, "script")),
            i.parentNode.removeChild(i));
        return t
    }
    function O(t, e, n) {
        var i, o, r, s, a = t.style;
        return n = n || se(t),
        n && (s = n.getPropertyValue(e) || n[e],
        "" !== s || ht.contains(t.ownerDocument, t) || (s = ht.style(t, e)),
        !pt.pixelMarginRight() && re.test(s) && oe.test(e) && (i = a.width,
        o = a.minWidth,
        r = a.maxWidth,
        a.minWidth = a.maxWidth = a.width = s,
        s = n.width,
        a.width = i,
        a.minWidth = o,
        a.maxWidth = r)),
        void 0 !== s ? s + "" : s
    }
    function j(t, e) {
        return {
            get: function() {
                return t() ? void delete this.get : (this.get = e).apply(this, arguments)
            }
        }
    }
    function D(t) {
        if (t in fe)
            return t;
        for (var e = t[0].toUpperCase() + t.slice(1), n = de.length; n--; )
            if ((t = de[n] + e)in fe)
                return t
    }
    function R(t) {
        var e = ht.cssProps[t];
        return e || (e = ht.cssProps[t] = D(t) || t),
        e
    }
    function M(t, e, n) {
        var i = Ft.exec(e);
        return i ? Math.max(0, i[2] - (n || 0)) + (i[3] || "px") : e
    }
    function N(t, e, n, i, o) {
        var r, s = 0;
        for (r = n === (i ? "border" : "content") ? 4 : "width" === e ? 1 : 0; r < 4; r += 2)
            "margin" === n && (s += ht.css(t, n + Wt[r], !0, o)),
            i ? ("content" === n && (s -= ht.css(t, "padding" + Wt[r], !0, o)),
            "margin" !== n && (s -= ht.css(t, "border" + Wt[r] + "Width", !0, o))) : (s += ht.css(t, "padding" + Wt[r], !0, o),
            "padding" !== n && (s += ht.css(t, "border" + Wt[r] + "Width", !0, o)));
        return s
    }
    function $(t, e, n) {
        var i, o = se(t), r = O(t, e, o), s = "border-box" === ht.css(t, "boxSizing", !1, o);
        return re.test(r) ? r : (i = s && (pt.boxSizingReliable() || r === t.style[e]),
        "auto" === r && (r = t["offset" + e[0].toUpperCase() + e.slice(1)]),
        (r = parseFloat(r) || 0) + N(t, e, n || (s ? "border" : "content"), i, o) + "px")
    }
    function F(t, e, n, i, o) {
        return new F.prototype.init(t,e,n,i,o)
    }
    function W() {
        he && (!1 === nt.hidden && t.requestAnimationFrame ? t.requestAnimationFrame(W) : t.setTimeout(W, ht.fx.interval),
        ht.fx.tick())
    }
    function H() {
        return t.setTimeout(function() {
            pe = void 0
        }),
        pe = ht.now()
    }
    function Y(t, e) {
        var n, i = 0, o = {
            height: t
        };
        for (e = e ? 1 : 0; i < 4; i += 2 - e)
            n = Wt[i],
            o["margin" + n] = o["padding" + n] = t;
        return e && (o.opacity = o.width = t),
        o
    }
    function B(t, e, n) {
        for (var i, o = (z.tweeners[e] || []).concat(z.tweeners["*"]), r = 0, s = o.length; r < s; r++)
            if (i = o[r].call(n, e, t))
                return i
    }
    function U(t, e, n) {
        var i, o, r, s, a, l, c, u, d = "width"in e || "height"in e, f = this, p = {}, h = t.style, g = t.nodeType && Ht(t), v = Dt.get(t, "fxshow");
        n.queue || (s = ht._queueHooks(t, "fx"),
        null == s.unqueued && (s.unqueued = 0,
        a = s.empty.fire,
        s.empty.fire = function() {
            s.unqueued || a()
        }
        ),
        s.unqueued++,
        f.always(function() {
            f.always(function() {
                s.unqueued--,
                ht.queue(t, "fx").length || s.empty.fire()
            })
        }));
        for (i in e)
            if (o = e[i],
            ge.test(o)) {
                if (delete e[i],
                r = r || "toggle" === o,
                o === (g ? "hide" : "show")) {
                    if ("show" !== o || !v || void 0 === v[i])
                        continue;
                    g = !0
                }
                p[i] = v && v[i] || ht.style(t, i)
            }
        if ((l = !ht.isEmptyObject(e)) || !ht.isEmptyObject(p)) {
            d && 1 === t.nodeType && (n.overflow = [h.overflow, h.overflowX, h.overflowY],
            c = v && v.display,
            null == c && (c = Dt.get(t, "display")),
            u = ht.css(t, "display"),
            "none" === u && (c ? u = c : (m([t], !0),
            c = t.style.display || c,
            u = ht.css(t, "display"),
            m([t]))),
            ("inline" === u || "inline-block" === u && null != c) && "none" === ht.css(t, "float") && (l || (f.done(function() {
                h.display = c
            }),
            null == c && (u = h.display,
            c = "none" === u ? "" : u)),
            h.display = "inline-block")),
            n.overflow && (h.overflow = "hidden",
            f.always(function() {
                h.overflow = n.overflow[0],
                h.overflowX = n.overflow[1],
                h.overflowY = n.overflow[2]
            })),
            l = !1;
            for (i in p)
                l || (v ? "hidden"in v && (g = v.hidden) : v = Dt.access(t, "fxshow", {
                    display: c
                }),
                r && (v.hidden = !g),
                g && m([t], !0),
                f.done(function() {
                    g || m([t]),
                    Dt.remove(t, "fxshow");
                    for (i in p)
                        ht.style(t, i, p[i])
                })),
                l = B(g ? v[i] : 0, i, f),
                i in v || (v[i] = l.start,
                g && (l.end = l.start,
                l.start = 0))
        }
    }
    function q(t, e) {
        var n, i, o, r, s;
        for (n in t)
            if (i = ht.camelCase(n),
            o = e[i],
            r = t[n],
            Array.isArray(r) && (o = r[1],
            r = t[n] = r[0]),
            n !== i && (t[i] = r,
            delete t[n]),
            (s = ht.cssHooks[i]) && "expand"in s) {
                r = s.expand(r),
                delete t[i];
                for (n in r)
                    n in t || (t[n] = r[n],
                    e[n] = o)
            } else
                e[i] = o
    }
    function z(t, e, n) {
        var i, o, r = 0, s = z.prefilters.length, a = ht.Deferred().always(function() {
            delete l.elem
        }), l = function() {
            if (o)
                return !1;
            for (var e = pe || H(), n = Math.max(0, c.startTime + c.duration - e), i = n / c.duration || 0, r = 1 - i, s = 0, l = c.tweens.length; s < l; s++)
                c.tweens[s].run(r);
            return a.notifyWith(t, [c, r, n]),
            r < 1 && l ? n : (l || a.notifyWith(t, [c, 1, 0]),
            a.resolveWith(t, [c]),
            !1)
        }, c = a.promise({
            elem: t,
            props: ht.extend({}, e),
            opts: ht.extend(!0, {
                specialEasing: {},
                easing: ht.easing._default
            }, n),
            originalProperties: e,
            originalOptions: n,
            startTime: pe || H(),
            duration: n.duration,
            tweens: [],
            createTween: function(e, n) {
                var i = ht.Tween(t, c.opts, e, n, c.opts.specialEasing[e] || c.opts.easing);
                return c.tweens.push(i),
                i
            },
            stop: function(e) {
                var n = 0
                  , i = e ? c.tweens.length : 0;
                if (o)
                    return this;
                for (o = !0; n < i; n++)
                    c.tweens[n].run(1);
                return e ? (a.notifyWith(t, [c, 1, 0]),
                a.resolveWith(t, [c, e])) : a.rejectWith(t, [c, e]),
                this
            }
        }), u = c.props;
        for (q(u, c.opts.specialEasing); r < s; r++)
            if (i = z.prefilters[r].call(c, t, u, c.opts))
                return ht.isFunction(i.stop) && (ht._queueHooks(c.elem, c.opts.queue).stop = ht.proxy(i.stop, i)),
                i;
        return ht.map(u, B, c),
        ht.isFunction(c.opts.start) && c.opts.start.call(t, c),
        c.progress(c.opts.progress).done(c.opts.done, c.opts.complete).fail(c.opts.fail).always(c.opts.always),
        ht.fx.timer(ht.extend(l, {
            elem: t,
            anim: c,
            queue: c.opts.queue
        })),
        c
    }
    function V(t) {
        return (t.match(Pt) || []).join(" ")
    }
    function X(t) {
        return t.getAttribute && t.getAttribute("class") || ""
    }
    function J(t, e, n, i) {
        var o;
        if (Array.isArray(e))
            ht.each(e, function(e, o) {
                n || Se.test(t) ? i(t, o) : J(t + "[" + ("object" == typeof o && null != o ? e : "") + "]", o, n, i)
            });
        else if (n || "object" !== ht.type(e))
            i(t, e);
        else
            for (o in e)
                J(t + "[" + o + "]", e[o], n, i)
    }
    function Q(t) {
        return function(e, n) {
            "string" != typeof e && (n = e,
            e = "*");
            var i, o = 0, r = e.toLowerCase().match(Pt) || [];
            if (ht.isFunction(n))
                for (; i = r[o++]; )
                    "+" === i[0] ? (i = i.slice(1) || "*",
                    (t[i] = t[i] || []).unshift(n)) : (t[i] = t[i] || []).push(n)
        }
    }
    function G(t, e, n, i) {
        function o(a) {
            var l;
            return r[a] = !0,
            ht.each(t[a] || [], function(t, a) {
                var c = a(e, n, i);
                return "string" != typeof c || s || r[c] ? s ? !(l = c) : void 0 : (e.dataTypes.unshift(c),
                o(c),
                !1)
            }),
            l
        }
        var r = {}
          , s = t === $e;
        return o(e.dataTypes[0]) || !r["*"] && o("*")
    }
    function K(t, e) {
        var n, i, o = ht.ajaxSettings.flatOptions || {};
        for (n in e)
            void 0 !== e[n] && ((o[n] ? t : i || (i = {}))[n] = e[n]);
        return i && ht.extend(!0, t, i),
        t
    }
    function Z(t, e, n) {
        for (var i, o, r, s, a = t.contents, l = t.dataTypes; "*" === l[0]; )
            l.shift(),
            void 0 === i && (i = t.mimeType || e.getResponseHeader("Content-Type"));
        if (i)
            for (o in a)
                if (a[o] && a[o].test(i)) {
                    l.unshift(o);
                    break
                }
        if (l[0]in n)
            r = l[0];
        else {
            for (o in n) {
                if (!l[0] || t.converters[o + " " + l[0]]) {
                    r = o;
                    break
                }
                s || (s = o)
            }
            r = r || s
        }
        if (r)
            return r !== l[0] && l.unshift(r),
            n[r]
    }
    function tt(t, e, n, i) {
        var o, r, s, a, l, c = {}, u = t.dataTypes.slice();
        if (u[1])
            for (s in t.converters)
                c[s.toLowerCase()] = t.converters[s];
        for (r = u.shift(); r; )
            if (t.responseFields[r] && (n[t.responseFields[r]] = e),
            !l && i && t.dataFilter && (e = t.dataFilter(e, t.dataType)),
            l = r,
            r = u.shift())
                if ("*" === r)
                    r = l;
                else if ("*" !== l && l !== r) {
                    if (!(s = c[l + " " + r] || c["* " + r]))
                        for (o in c)
                            if (a = o.split(" "),
                            a[1] === r && (s = c[l + " " + a[0]] || c["* " + a[0]])) {
                                !0 === s ? s = c[o] : !0 !== c[o] && (r = a[0],
                                u.unshift(a[1]));
                                break
                            }
                    if (!0 !== s)
                        if (s && t.throws)
                            e = s(e);
                        else
                            try {
                                e = s(e)
                            } catch (t) {
                                return {
                                    state: "parsererror",
                                    error: s ? t : "No conversion from " + l + " to " + r
                                }
                            }
                }
        return {
            state: "success",
            data: e
        }
    }
    var et = []
      , nt = t.document
      , it = Object.getPrototypeOf
      , ot = et.slice
      , rt = et.concat
      , st = et.push
      , at = et.indexOf
      , lt = {}
      , ct = lt.toString
      , ut = lt.hasOwnProperty
      , dt = ut.toString
      , ft = dt.call(Object)
      , pt = {}
      , ht = function(t, e) {
        return new ht.fn.init(t,e)
    }
      , gt = /^[\s\uFEFF\xA0]+|[\s\uFEFF\xA0]+$/g
      , vt = /^-ms-/
      , mt = /-([a-z])/g
      , yt = function(t, e) {
        return e.toUpperCase()
    };
    ht.fn = ht.prototype = {
        jquery: "3.2.1",
        constructor: ht,
        length: 0,
        toArray: function() {
            return ot.call(this)
        },
        get: function(t) {
            return null == t ? ot.call(this) : t < 0 ? this[t + this.length] : this[t]
        },
        pushStack: function(t) {
            var e = ht.merge(this.constructor(), t);
            return e.prevObject = this,
            e
        },
        each: function(t) {
            return ht.each(this, t)
        },
        map: function(t) {
            return this.pushStack(ht.map(this, function(e, n) {
                return t.call(e, n, e)
            }))
        },
        slice: function() {
            return this.pushStack(ot.apply(this, arguments))
        },
        first: function() {
            return this.eq(0)
        },
        last: function() {
            return this.eq(-1)
        },
        eq: function(t) {
            var e = this.length
              , n = +t + (t < 0 ? e : 0);
            return this.pushStack(n >= 0 && n < e ? [this[n]] : [])
        },
        end: function() {
            return this.prevObject || this.constructor()
        },
        push: st,
        sort: et.sort,
        splice: et.splice
    },
    ht.extend = ht.fn.extend = function() {
        var t, e, n, i, o, r, s = arguments[0] || {}, a = 1, l = arguments.length, c = !1;
        for ("boolean" == typeof s && (c = s,
        s = arguments[a] || {},
        a++),
        "object" == typeof s || ht.isFunction(s) || (s = {}),
        a === l && (s = this,
        a--); a < l; a++)
            if (null != (t = arguments[a]))
                for (e in t)
                    n = s[e],
                    i = t[e],
                    s !== i && (c && i && (ht.isPlainObject(i) || (o = Array.isArray(i))) ? (o ? (o = !1,
                    r = n && Array.isArray(n) ? n : []) : r = n && ht.isPlainObject(n) ? n : {},
                    s[e] = ht.extend(c, r, i)) : void 0 !== i && (s[e] = i));
        return s
    }
    ,
    ht.extend({
        expando: "jQuery" + ("3.2.1" + Math.random()).replace(/\D/g, ""),
        isReady: !0,
        error: function(t) {
            throw new Error(t)
        },
        noop: function() {},
        isFunction: function(t) {
            return "function" === ht.type(t)
        },
        isWindow: function(t) {
            return null != t && t === t.window
        },
        isNumeric: function(t) {
            var e = ht.type(t);
            return ("number" === e || "string" === e) && !isNaN(t - parseFloat(t))
        },
        isPlainObject: function(t) {
            var e, n;
            return !(!t || "[object Object]" !== ct.call(t)) && (!(e = it(t)) || "function" == typeof (n = ut.call(e, "constructor") && e.constructor) && dt.call(n) === ft)
        },
        isEmptyObject: function(t) {
            var e;
            for (e in t)
                return !1;
            return !0
        },
        type: function(t) {
            return null == t ? t + "" : "object" == typeof t || "function" == typeof t ? lt[ct.call(t)] || "object" : typeof t
        },
        globalEval: function(t) {
            n(t)
        },
        camelCase: function(t) {
            return t.replace(vt, "ms-").replace(mt, yt)
        },
        each: function(t, e) {
            var n, o = 0;
            if (i(t))
                for (n = t.length; o < n && !1 !== e.call(t[o], o, t[o]); o++)
                    ;
            else
                for (o in t)
                    if (!1 === e.call(t[o], o, t[o]))
                        break;
            return t
        },
        trim: function(t) {
            return null == t ? "" : (t + "").replace(gt, "")
        },
        makeArray: function(t, e) {
            var n = e || [];
            return null != t && (i(Object(t)) ? ht.merge(n, "string" == typeof t ? [t] : t) : st.call(n, t)),
            n
        },
        inArray: function(t, e, n) {
            return null == e ? -1 : at.call(e, t, n)
        },
        merge: function(t, e) {
            for (var n = +e.length, i = 0, o = t.length; i < n; i++)
                t[o++] = e[i];
            return t.length = o,
            t
        },
        grep: function(t, e, n) {
            for (var i = [], o = 0, r = t.length, s = !n; o < r; o++)
                !e(t[o], o) !== s && i.push(t[o]);
            return i
        },
        map: function(t, e, n) {
            var o, r, s = 0, a = [];
            if (i(t))
                for (o = t.length; s < o; s++)
                    null != (r = e(t[s], s, n)) && a.push(r);
            else
                for (s in t)
                    null != (r = e(t[s], s, n)) && a.push(r);
            return rt.apply([], a)
        },
        guid: 1,
        proxy: function(t, e) {
            var n, i, o;
            if ("string" == typeof e && (n = t[e],
            e = t,
            t = n),
            ht.isFunction(t))
                return i = ot.call(arguments, 2),
                o = function() {
                    return t.apply(e || this, i.concat(ot.call(arguments)))
                }
                ,
                o.guid = t.guid = t.guid || ht.guid++,
                o
        },
        now: Date.now,
        support: pt
    }),
    "function" == typeof Symbol && (ht.fn[Symbol.iterator] = et[Symbol.iterator]),
    ht.each("Boolean Number String Function Array Date RegExp Object Error Symbol".split(" "), function(t, e) {
        lt["[object " + e + "]"] = e.toLowerCase()
    });
    var bt = function(t) {
        function e(t, e, n, i) {
            var o, r, s, a, l, u, f, p = e && e.ownerDocument, h = e ? e.nodeType : 9;
            if (n = n || [],
            "string" != typeof t || !t || 1 !== h && 9 !== h && 11 !== h)
                return n;
            if (!i && ((e ? e.ownerDocument || e : F) !== I && L(e),
            e = e || I,
            j)) {
                if (11 !== h && (l = gt.exec(t)))
                    if (o = l[1]) {
                        if (9 === h) {
                            if (!(s = e.getElementById(o)))
                                return n;
                            if (s.id === o)
                                return n.push(s),
                                n
                        } else if (p && (s = p.getElementById(o)) && N(e, s) && s.id === o)
                            return n.push(s),
                            n
                    } else {
                        if (l[2])
                            return Q.apply(n, e.getElementsByTagName(t)),
                            n;
                        if ((o = l[3]) && w.getElementsByClassName && e.getElementsByClassName)
                            return Q.apply(n, e.getElementsByClassName(o)),
                            n
                    }
                if (w.qsa && !U[t + " "] && (!D || !D.test(t))) {
                    if (1 !== h)
                        p = e,
                        f = t;
                    else if ("object" !== e.nodeName.toLowerCase()) {
                        for ((a = e.getAttribute("id")) ? a = a.replace(bt, wt) : e.setAttribute("id", a = $),
                        u = T(t),
                        r = u.length; r--; )
                            u[r] = "#" + a + " " + d(u[r]);
                        f = u.join(","),
                        p = vt.test(t) && c(e.parentNode) || e
                    }
                    if (f)
                        try {
                            return Q.apply(n, p.querySelectorAll(f)),
                            n
                        } catch (t) {} finally {
                            a === $ && e.removeAttribute("id")
                        }
                }
            }
            return S(t.replace(rt, "$1"), e, n, i)
        }
        function n() {
            function t(n, i) {
                return e.push(n + " ") > E.cacheLength && delete t[e.shift()],
                t[n + " "] = i
            }
            var e = [];
            return t
        }
        function i(t) {
            return t[$] = !0,
            t
        }
        function o(t) {
            var e = I.createElement("fieldset");
            try {
                return !!t(e)
            } catch (t) {
                return !1
            } finally {
                e.parentNode && e.parentNode.removeChild(e),
                e = null
            }
        }
        function r(t, e) {
            for (var n = t.split("|"), i = n.length; i--; )
                E.attrHandle[n[i]] = e
        }
        function s(t, e) {
            var n = e && t
              , i = n && 1 === t.nodeType && 1 === e.nodeType && t.sourceIndex - e.sourceIndex;
            if (i)
                return i;
            if (n)
                for (; n = n.nextSibling; )
                    if (n === e)
                        return -1;
            return t ? 1 : -1
        }
        function a(t) {
            return function(e) {
                return "form"in e ? e.parentNode && !1 === e.disabled ? "label"in e ? "label"in e.parentNode ? e.parentNode.disabled === t : e.disabled === t : e.isDisabled === t || e.isDisabled !== !t && Ct(e) === t : e.disabled === t : "label"in e && e.disabled === t
            }
        }
        function l(t) {
            return i(function(e) {
                return e = +e,
                i(function(n, i) {
                    for (var o, r = t([], n.length, e), s = r.length; s--; )
                        n[o = r[s]] && (n[o] = !(i[o] = n[o]))
                })
            })
        }
        function c(t) {
            return t && void 0 !== t.getElementsByTagName && t
        }
        function u() {}
        function d(t) {
            for (var e = 0, n = t.length, i = ""; e < n; e++)
                i += t[e].value;
            return i
        }
        function f(t, e, n) {
            var i = e.dir
              , o = e.next
              , r = o || i
              , s = n && "parentNode" === r
              , a = H++;
            return e.first ? function(e, n, o) {
                for (; e = e[i]; )
                    if (1 === e.nodeType || s)
                        return t(e, n, o);
                return !1
            }
            : function(e, n, l) {
                var c, u, d, f = [W, a];
                if (l) {
                    for (; e = e[i]; )
                        if ((1 === e.nodeType || s) && t(e, n, l))
                            return !0
                } else
                    for (; e = e[i]; )
                        if (1 === e.nodeType || s)
                            if (d = e[$] || (e[$] = {}),
                            u = d[e.uniqueID] || (d[e.uniqueID] = {}),
                            o && o === e.nodeName.toLowerCase())
                                e = e[i] || e;
                            else {
                                if ((c = u[r]) && c[0] === W && c[1] === a)
                                    return f[2] = c[2];
                                if (u[r] = f,
                                f[2] = t(e, n, l))
                                    return !0
                            }
                return !1
            }
        }
        function p(t) {
            return t.length > 1 ? function(e, n, i) {
                for (var o = t.length; o--; )
                    if (!t[o](e, n, i))
                        return !1;
                return !0
            }
            : t[0]
        }
        function h(t, n, i) {
            for (var o = 0, r = n.length; o < r; o++)
                e(t, n[o], i);
            return i
        }
        function g(t, e, n, i, o) {
            for (var r, s = [], a = 0, l = t.length, c = null != e; a < l; a++)
                (r = t[a]) && (n && !n(r, i, o) || (s.push(r),
                c && e.push(a)));
            return s
        }
        function v(t, e, n, o, r, s) {
            return o && !o[$] && (o = v(o)),
            r && !r[$] && (r = v(r, s)),
            i(function(i, s, a, l) {
                var c, u, d, f = [], p = [], v = s.length, m = i || h(e || "*", a.nodeType ? [a] : a, []), y = !t || !i && e ? m : g(m, f, t, a, l), b = n ? r || (i ? t : v || o) ? [] : s : y;
                if (n && n(y, b, a, l),
                o)
                    for (c = g(b, p),
                    o(c, [], a, l),
                    u = c.length; u--; )
                        (d = c[u]) && (b[p[u]] = !(y[p[u]] = d));
                if (i) {
                    if (r || t) {
                        if (r) {
                            for (c = [],
                            u = b.length; u--; )
                                (d = b[u]) && c.push(y[u] = d);
                            r(null, b = [], c, l)
                        }
                        for (u = b.length; u--; )
                            (d = b[u]) && (c = r ? K(i, d) : f[u]) > -1 && (i[c] = !(s[c] = d))
                    }
                } else
                    b = g(b === s ? b.splice(v, b.length) : b),
                    r ? r(null, s, b, l) : Q.apply(s, b)
            })
        }
        function m(t) {
            for (var e, n, i, o = t.length, r = E.relative[t[0].type], s = r || E.relative[" "], a = r ? 1 : 0, l = f(function(t) {
                return t === e
            }, s, !0), c = f(function(t) {
                return K(e, t) > -1
            }, s, !0), u = [function(t, n, i) {
                var o = !r && (i || n !== _) || ((e = n).nodeType ? l(t, n, i) : c(t, n, i));
                return e = null,
                o
            }
            ]; a < o; a++)
                if (n = E.relative[t[a].type])
                    u = [f(p(u), n)];
                else {
                    if (n = E.filter[t[a].type].apply(null, t[a].matches),
                    n[$]) {
                        for (i = ++a; i < o && !E.relative[t[i].type]; i++)
                            ;
                        return v(a > 1 && p(u), a > 1 && d(t.slice(0, a - 1).concat({
                            value: " " === t[a - 2].type ? "*" : ""
                        })).replace(rt, "$1"), n, a < i && m(t.slice(a, i)), i < o && m(t = t.slice(i)), i < o && d(t))
                    }
                    u.push(n)
                }
            return p(u)
        }
        function y(t, n) {
            var o = n.length > 0
              , r = t.length > 0
              , s = function(i, s, a, l, c) {
                var u, d, f, p = 0, h = "0", v = i && [], m = [], y = _, b = i || r && E.find.TAG("*", c), w = W += null == y ? 1 : Math.random() || .1, C = b.length;
                for (c && (_ = s === I || s || c); h !== C && null != (u = b[h]); h++) {
                    if (r && u) {
                        for (d = 0,
                        s || u.ownerDocument === I || (L(u),
                        a = !j); f = t[d++]; )
                            if (f(u, s || I, a)) {
                                l.push(u);
                                break
                            }
                        c && (W = w)
                    }
                    o && ((u = !f && u) && p--,
                    i && v.push(u))
                }
                if (p += h,
                o && h !== p) {
                    for (d = 0; f = n[d++]; )
                        f(v, m, s, a);
                    if (i) {
                        if (p > 0)
                            for (; h--; )
                                v[h] || m[h] || (m[h] = X.call(l));
                        m = g(m)
                    }
                    Q.apply(l, m),
                    c && !i && m.length > 0 && p + n.length > 1 && e.uniqueSort(l)
                }
                return c && (W = w,
                _ = y),
                v
            };
            return o ? i(s) : s
        }
        var b, w, E, C, x, T, k, S, _, A, P, L, I, O, j, D, R, M, N, $ = "sizzle" + 1 * new Date, F = t.document, W = 0, H = 0, Y = n(), B = n(), U = n(), q = function(t, e) {
            return t === e && (P = !0),
            0
        }, z = {}.hasOwnProperty, V = [], X = V.pop, J = V.push, Q = V.push, G = V.slice, K = function(t, e) {
            for (var n = 0, i = t.length; n < i; n++)
                if (t[n] === e)
                    return n;
            return -1
        }, Z = "checked|selected|async|autofocus|autoplay|controls|defer|disabled|hidden|ismap|loop|multiple|open|readonly|required|scoped", tt = "[\\x20\\t\\r\\n\\f]", et = "(?:\\\\.|[\\w-]|[^\0-\\xa0])+", nt = "\\[" + tt + "*(" + et + ")(?:" + tt + "*([*^$|!~]?=)" + tt + "*(?:'((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\"|(" + et + "))|)" + tt + "*\\]", it = ":(" + et + ")(?:\\((('((?:\\\\.|[^\\\\'])*)'|\"((?:\\\\.|[^\\\\\"])*)\")|((?:\\\\.|[^\\\\()[\\]]|" + nt + ")*)|.*)\\)|)", ot = new RegExp(tt + "+","g"), rt = new RegExp("^" + tt + "+|((?:^|[^\\\\])(?:\\\\.)*)" + tt + "+$","g"), st = new RegExp("^" + tt + "*," + tt + "*"), at = new RegExp("^" + tt + "*([>+~]|" + tt + ")" + tt + "*"), lt = new RegExp("=" + tt + "*([^\\]'\"]*?)" + tt + "*\\]","g"), ct = new RegExp(it), ut = new RegExp("^" + et + "$"), dt = {
            ID: new RegExp("^#(" + et + ")"),
            CLASS: new RegExp("^\\.(" + et + ")"),
            TAG: new RegExp("^(" + et + "|[*])"),
            ATTR: new RegExp("^" + nt),
            PSEUDO: new RegExp("^" + it),
            CHILD: new RegExp("^:(only|first|last|nth|nth-last)-(child|of-type)(?:\\(" + tt + "*(even|odd|(([+-]|)(\\d*)n|)" + tt + "*(?:([+-]|)" + tt + "*(\\d+)|))" + tt + "*\\)|)","i"),
            bool: new RegExp("^(?:" + Z + ")$","i"),
            needsContext: new RegExp("^" + tt + "*[>+~]|:(even|odd|eq|gt|lt|nth|first|last)(?:\\(" + tt + "*((?:-\\d)?\\d*)" + tt + "*\\)|)(?=[^-]|$)","i")
        }, ft = /^(?:input|select|textarea|button)$/i, pt = /^h\d$/i, ht = /^[^{]+\{\s*\[native \w/, gt = /^(?:#([\w-]+)|(\w+)|\.([\w-]+))$/, vt = /[+~]/, mt = new RegExp("\\\\([\\da-f]{1,6}" + tt + "?|(" + tt + ")|.)","ig"), yt = function(t, e, n) {
            var i = "0x" + e - 65536;
            return i !== i || n ? e : i < 0 ? String.fromCharCode(i + 65536) : String.fromCharCode(i >> 10 | 55296, 1023 & i | 56320)
        }, bt = /([\0-\x1f\x7f]|^-?\d)|^-$|[^\0-\x1f\x7f-\uFFFF\w-]/g, wt = function(t, e) {
            return e ? "\0" === t ? "�" : t.slice(0, -1) + "\\" + t.charCodeAt(t.length - 1).toString(16) + " " : "\\" + t
        }, Et = function() {
            L()
        }, Ct = f(function(t) {
            return !0 === t.disabled && ("form"in t || "label"in t)
        }, {
            dir: "parentNode",
            next: "legend"
        });
        try {
            Q.apply(V = G.call(F.childNodes), F.childNodes),
            V[F.childNodes.length].nodeType
        } catch (t) {
            Q = {
                apply: V.length ? function(t, e) {
                    J.apply(t, G.call(e))
                }
                : function(t, e) {
                    for (var n = t.length, i = 0; t[n++] = e[i++]; )
                        ;
                    t.length = n - 1
                }
            }
        }
        w = e.support = {},
        x = e.isXML = function(t) {
            var e = t && (t.ownerDocument || t).documentElement;
            return !!e && "HTML" !== e.nodeName
        }
        ,
        L = e.setDocument = function(t) {
            var e, n, i = t ? t.ownerDocument || t : F;
            return i !== I && 9 === i.nodeType && i.documentElement ? (I = i,
            O = I.documentElement,
            j = !x(I),
            F !== I && (n = I.defaultView) && n.top !== n && (n.addEventListener ? n.addEventListener("unload", Et, !1) : n.attachEvent && n.attachEvent("onunload", Et)),
            w.attributes = o(function(t) {
                return t.className = "i",
                !t.getAttribute("className")
            }),
            w.getElementsByTagName = o(function(t) {
                return t.appendChild(I.createComment("")),
                !t.getElementsByTagName("*").length
            }),
            w.getElementsByClassName = ht.test(I.getElementsByClassName),
            w.getById = o(function(t) {
                return O.appendChild(t).id = $,
                !I.getElementsByName || !I.getElementsByName($).length
            }),
            w.getById ? (E.filter.ID = function(t) {
                var e = t.replace(mt, yt);
                return function(t) {
                    return t.getAttribute("id") === e
                }
            }
            ,
            E.find.ID = function(t, e) {
                if (void 0 !== e.getElementById && j) {
                    var n = e.getElementById(t);
                    return n ? [n] : []
                }
            }
            ) : (E.filter.ID = function(t) {
                var e = t.replace(mt, yt);
                return function(t) {
                    var n = void 0 !== t.getAttributeNode && t.getAttributeNode("id");
                    return n && n.value === e
                }
            }
            ,
            E.find.ID = function(t, e) {
                if (void 0 !== e.getElementById && j) {
                    var n, i, o, r = e.getElementById(t);
                    if (r) {
                        if ((n = r.getAttributeNode("id")) && n.value === t)
                            return [r];
                        for (o = e.getElementsByName(t),
                        i = 0; r = o[i++]; )
                            if ((n = r.getAttributeNode("id")) && n.value === t)
                                return [r]
                    }
                    return []
                }
            }
            ),
            E.find.TAG = w.getElementsByTagName ? function(t, e) {
                return void 0 !== e.getElementsByTagName ? e.getElementsByTagName(t) : w.qsa ? e.querySelectorAll(t) : void 0
            }
            : function(t, e) {
                var n, i = [], o = 0, r = e.getElementsByTagName(t);
                if ("*" === t) {
                    for (; n = r[o++]; )
                        1 === n.nodeType && i.push(n);
                    return i
                }
                return r
            }
            ,
            E.find.CLASS = w.getElementsByClassName && function(t, e) {
                if (void 0 !== e.getElementsByClassName && j)
                    return e.getElementsByClassName(t)
            }
            ,
            R = [],
            D = [],
            (w.qsa = ht.test(I.querySelectorAll)) && (o(function(t) {
                O.appendChild(t).innerHTML = "<a id='" + $ + "'></a><select id='" + $ + "-\r\\' msallowcapture=''><option selected=''></option></select>",
                t.querySelectorAll("[msallowcapture^='']").length && D.push("[*^$]=" + tt + "*(?:''|\"\")"),
                t.querySelectorAll("[selected]").length || D.push("\\[" + tt + "*(?:value|" + Z + ")"),
                t.querySelectorAll("[id~=" + $ + "-]").length || D.push("~="),
                t.querySelectorAll(":checked").length || D.push(":checked"),
                t.querySelectorAll("a#" + $ + "+*").length || D.push(".#.+[+~]")
            }),
            o(function(t) {
                t.innerHTML = "<a href='' disabled='disabled'></a><select disabled='disabled'><option/></select>";
                var e = I.createElement("input");
                e.setAttribute("type", "hidden"),
                t.appendChild(e).setAttribute("name", "D"),
                t.querySelectorAll("[name=d]").length && D.push("name" + tt + "*[*^$|!~]?="),
                2 !== t.querySelectorAll(":enabled").length && D.push(":enabled", ":disabled"),
                O.appendChild(t).disabled = !0,
                2 !== t.querySelectorAll(":disabled").length && D.push(":enabled", ":disabled"),
                t.querySelectorAll("*,:x"),
                D.push(",.*:")
            })),
            (w.matchesSelector = ht.test(M = O.matches || O.webkitMatchesSelector || O.mozMatchesSelector || O.oMatchesSelector || O.msMatchesSelector)) && o(function(t) {
                w.disconnectedMatch = M.call(t, "*"),
                M.call(t, "[s!='']:x"),
                R.push("!=", it)
            }),
            D = D.length && new RegExp(D.join("|")),
            R = R.length && new RegExp(R.join("|")),
            e = ht.test(O.compareDocumentPosition),
            N = e || ht.test(O.contains) ? function(t, e) {
                var n = 9 === t.nodeType ? t.documentElement : t
                  , i = e && e.parentNode;
                return t === i || !(!i || 1 !== i.nodeType || !(n.contains ? n.contains(i) : t.compareDocumentPosition && 16 & t.compareDocumentPosition(i)))
            }
            : function(t, e) {
                if (e)
                    for (; e = e.parentNode; )
                        if (e === t)
                            return !0;
                return !1
            }
            ,
            q = e ? function(t, e) {
                if (t === e)
                    return P = !0,
                    0;
                var n = !t.compareDocumentPosition - !e.compareDocumentPosition;
                return n || (n = (t.ownerDocument || t) === (e.ownerDocument || e) ? t.compareDocumentPosition(e) : 1,
                1 & n || !w.sortDetached && e.compareDocumentPosition(t) === n ? t === I || t.ownerDocument === F && N(F, t) ? -1 : e === I || e.ownerDocument === F && N(F, e) ? 1 : A ? K(A, t) - K(A, e) : 0 : 4 & n ? -1 : 1)
            }
            : function(t, e) {
                if (t === e)
                    return P = !0,
                    0;
                var n, i = 0, o = t.parentNode, r = e.parentNode, a = [t], l = [e];
                if (!o || !r)
                    return t === I ? -1 : e === I ? 1 : o ? -1 : r ? 1 : A ? K(A, t) - K(A, e) : 0;
                if (o === r)
                    return s(t, e);
                for (n = t; n = n.parentNode; )
                    a.unshift(n);
                for (n = e; n = n.parentNode; )
                    l.unshift(n);
                for (; a[i] === l[i]; )
                    i++;
                return i ? s(a[i], l[i]) : a[i] === F ? -1 : l[i] === F ? 1 : 0
            }
            ,
            I) : I
        }
        ,
        e.matches = function(t, n) {
            return e(t, null, null, n)
        }
        ,
        e.matchesSelector = function(t, n) {
            if ((t.ownerDocument || t) !== I && L(t),
            n = n.replace(lt, "='$1']"),
            w.matchesSelector && j && !U[n + " "] && (!R || !R.test(n)) && (!D || !D.test(n)))
                try {
                    var i = M.call(t, n);
                    if (i || w.disconnectedMatch || t.document && 11 !== t.document.nodeType)
                        return i
                } catch (t) {}
            return e(n, I, null, [t]).length > 0
        }
        ,
        e.contains = function(t, e) {
            return (t.ownerDocument || t) !== I && L(t),
            N(t, e)
        }
        ,
        e.attr = function(t, e) {
            (t.ownerDocument || t) !== I && L(t);
            var n = E.attrHandle[e.toLowerCase()]
              , i = n && z.call(E.attrHandle, e.toLowerCase()) ? n(t, e, !j) : void 0;
            return void 0 !== i ? i : w.attributes || !j ? t.getAttribute(e) : (i = t.getAttributeNode(e)) && i.specified ? i.value : null
        }
        ,
        e.escape = function(t) {
            return (t + "").replace(bt, wt)
        }
        ,
        e.error = function(t) {
            throw new Error("Syntax error, unrecognized expression: " + t)
        }
        ,
        e.uniqueSort = function(t) {
            var e, n = [], i = 0, o = 0;
            if (P = !w.detectDuplicates,
            A = !w.sortStable && t.slice(0),
            t.sort(q),
            P) {
                for (; e = t[o++]; )
                    e === t[o] && (i = n.push(o));
                for (; i--; )
                    t.splice(n[i], 1)
            }
            return A = null,
            t
        }
        ,
        C = e.getText = function(t) {
            var e, n = "", i = 0, o = t.nodeType;
            if (o) {
                if (1 === o || 9 === o || 11 === o) {
                    if ("string" == typeof t.textContent)
                        return t.textContent;
                    for (t = t.firstChild; t; t = t.nextSibling)
                        n += C(t)
                } else if (3 === o || 4 === o)
                    return t.nodeValue
            } else
                for (; e = t[i++]; )
                    n += C(e);
            return n
        }
        ,
        E = e.selectors = {
            cacheLength: 50,
            createPseudo: i,
            match: dt,
            attrHandle: {},
            find: {},
            relative: {
                ">": {
                    dir: "parentNode",
                    first: !0
                },
                " ": {
                    dir: "parentNode"
                },
                "+": {
                    dir: "previousSibling",
                    first: !0
                },
                "~": {
                    dir: "previousSibling"
                }
            },
            preFilter: {
                ATTR: function(t) {
                    return t[1] = t[1].replace(mt, yt),
                    t[3] = (t[3] || t[4] || t[5] || "").replace(mt, yt),
                    "~=" === t[2] && (t[3] = " " + t[3] + " "),
                    t.slice(0, 4)
                },
                CHILD: function(t) {
                    return t[1] = t[1].toLowerCase(),
                    "nth" === t[1].slice(0, 3) ? (t[3] || e.error(t[0]),
                    t[4] = +(t[4] ? t[5] + (t[6] || 1) : 2 * ("even" === t[3] || "odd" === t[3])),
                    t[5] = +(t[7] + t[8] || "odd" === t[3])) : t[3] && e.error(t[0]),
                    t
                },
                PSEUDO: function(t) {
                    var e, n = !t[6] && t[2];
                    return dt.CHILD.test(t[0]) ? null : (t[3] ? t[2] = t[4] || t[5] || "" : n && ct.test(n) && (e = T(n, !0)) && (e = n.indexOf(")", n.length - e) - n.length) && (t[0] = t[0].slice(0, e),
                    t[2] = n.slice(0, e)),
                    t.slice(0, 3))
                }
            },
            filter: {
                TAG: function(t) {
                    var e = t.replace(mt, yt).toLowerCase();
                    return "*" === t ? function() {
                        return !0
                    }
                    : function(t) {
                        return t.nodeName && t.nodeName.toLowerCase() === e
                    }
                },
                CLASS: function(t) {
                    var e = Y[t + " "];
                    return e || (e = new RegExp("(^|" + tt + ")" + t + "(" + tt + "|$)")) && Y(t, function(t) {
                        return e.test("string" == typeof t.className && t.className || void 0 !== t.getAttribute && t.getAttribute("class") || "")
                    })
                },
                ATTR: function(t, n, i) {
                    return function(o) {
                        var r = e.attr(o, t);
                        return null == r ? "!=" === n : !n || (r += "",
                        "=" === n ? r === i : "!=" === n ? r !== i : "^=" === n ? i && 0 === r.indexOf(i) : "*=" === n ? i && r.indexOf(i) > -1 : "$=" === n ? i && r.slice(-i.length) === i : "~=" === n ? (" " + r.replace(ot, " ") + " ").indexOf(i) > -1 : "|=" === n && (r === i || r.slice(0, i.length + 1) === i + "-"))
                    }
                },
                CHILD: function(t, e, n, i, o) {
                    var r = "nth" !== t.slice(0, 3)
                      , s = "last" !== t.slice(-4)
                      , a = "of-type" === e;
                    return 1 === i && 0 === o ? function(t) {
                        return !!t.parentNode
                    }
                    : function(e, n, l) {
                        var c, u, d, f, p, h, g = r !== s ? "nextSibling" : "previousSibling", v = e.parentNode, m = a && e.nodeName.toLowerCase(), y = !l && !a, b = !1;
                        if (v) {
                            if (r) {
                                for (; g; ) {
                                    for (f = e; f = f[g]; )
                                        if (a ? f.nodeName.toLowerCase() === m : 1 === f.nodeType)
                                            return !1;
                                    h = g = "only" === t && !h && "nextSibling"
                                }
                                return !0
                            }
                            if (h = [s ? v.firstChild : v.lastChild],
                            s && y) {
                                for (f = v,
                                d = f[$] || (f[$] = {}),
                                u = d[f.uniqueID] || (d[f.uniqueID] = {}),
                                c = u[t] || [],
                                p = c[0] === W && c[1],
                                b = p && c[2],
                                f = p && v.childNodes[p]; f = ++p && f && f[g] || (b = p = 0) || h.pop(); )
                                    if (1 === f.nodeType && ++b && f === e) {
                                        u[t] = [W, p, b];
                                        break
                                    }
                            } else if (y && (f = e,
                            d = f[$] || (f[$] = {}),
                            u = d[f.uniqueID] || (d[f.uniqueID] = {}),
                            c = u[t] || [],
                            p = c[0] === W && c[1],
                            b = p),
                            !1 === b)
                                for (; (f = ++p && f && f[g] || (b = p = 0) || h.pop()) && ((a ? f.nodeName.toLowerCase() !== m : 1 !== f.nodeType) || !++b || (y && (d = f[$] || (f[$] = {}),
                                u = d[f.uniqueID] || (d[f.uniqueID] = {}),
                                u[t] = [W, b]),
                                f !== e)); )
                                    ;
                            return (b -= o) === i || b % i == 0 && b / i >= 0
                        }
                    }
                },
                PSEUDO: function(t, n) {
                    var o, r = E.pseudos[t] || E.setFilters[t.toLowerCase()] || e.error("unsupported pseudo: " + t);
                    return r[$] ? r(n) : r.length > 1 ? (o = [t, t, "", n],
                    E.setFilters.hasOwnProperty(t.toLowerCase()) ? i(function(t, e) {
                        for (var i, o = r(t, n), s = o.length; s--; )
                            i = K(t, o[s]),
                            t[i] = !(e[i] = o[s])
                    }) : function(t) {
                        return r(t, 0, o)
                    }
                    ) : r
                }
            },
            pseudos: {
                not: i(function(t) {
                    var e = []
                      , n = []
                      , o = k(t.replace(rt, "$1"));
                    return o[$] ? i(function(t, e, n, i) {
                        for (var r, s = o(t, null, i, []), a = t.length; a--; )
                            (r = s[a]) && (t[a] = !(e[a] = r))
                    }) : function(t, i, r) {
                        return e[0] = t,
                        o(e, null, r, n),
                        e[0] = null,
                        !n.pop()
                    }
                }),
                has: i(function(t) {
                    return function(n) {
                        return e(t, n).length > 0
                    }
                }),
                contains: i(function(t) {
                    return t = t.replace(mt, yt),
                    function(e) {
                        return (e.textContent || e.innerText || C(e)).indexOf(t) > -1
                    }
                }),
                lang: i(function(t) {
                    return ut.test(t || "") || e.error("unsupported lang: " + t),
                    t = t.replace(mt, yt).toLowerCase(),
                    function(e) {
                        var n;
                        do {
                            if (n = j ? e.lang : e.getAttribute("xml:lang") || e.getAttribute("lang"))
                                return (n = n.toLowerCase()) === t || 0 === n.indexOf(t + "-")
                        } while ((e = e.parentNode) && 1 === e.nodeType);return !1
                    }
                }),
                target: function(e) {
                    var n = t.location && t.location.hash;
                    return n && n.slice(1) === e.id
                },
                root: function(t) {
                    return t === O
                },
                focus: function(t) {
                    return t === I.activeElement && (!I.hasFocus || I.hasFocus()) && !!(t.type || t.href || ~t.tabIndex)
                },
                enabled: a(!1),
                disabled: a(!0),
                checked: function(t) {
                    var e = t.nodeName.toLowerCase();
                    return "input" === e && !!t.checked || "option" === e && !!t.selected
                },
                selected: function(t) {
                    return t.parentNode && t.parentNode.selectedIndex,
                    !0 === t.selected
                },
                empty: function(t) {
                    for (t = t.firstChild; t; t = t.nextSibling)
                        if (t.nodeType < 6)
                            return !1;
                    return !0
                },
                parent: function(t) {
                    return !E.pseudos.empty(t)
                },
                header: function(t) {
                    return pt.test(t.nodeName)
                },
                input: function(t) {
                    return ft.test(t.nodeName)
                },
                button: function(t) {
                    var e = t.nodeName.toLowerCase();
                    return "input" === e && "button" === t.type || "button" === e
                },
                text: function(t) {
                    var e;
                    return "input" === t.nodeName.toLowerCase() && "text" === t.type && (null == (e = t.getAttribute("type")) || "text" === e.toLowerCase())
                },
                first: l(function() {
                    return [0]
                }),
                last: l(function(t, e) {
                    return [e - 1]
                }),
                eq: l(function(t, e, n) {
                    return [n < 0 ? n + e : n]
                }),
                even: l(function(t, e) {
                    for (var n = 0; n < e; n += 2)
                        t.push(n);
                    return t
                }),
                odd: l(function(t, e) {
                    for (var n = 1; n < e; n += 2)
                        t.push(n);
                    return t
                }),
                lt: l(function(t, e, n) {
                    for (var i = n < 0 ? n + e : n; --i >= 0; )
                        t.push(i);
                    return t
                }),
                gt: l(function(t, e, n) {
                    for (var i = n < 0 ? n + e : n; ++i < e; )
                        t.push(i);
                    return t
                })
            }
        },
        E.pseudos.nth = E.pseudos.eq;
        for (b in {
            radio: !0,
            checkbox: !0,
            file: !0,
            password: !0,
            image: !0
        })
            E.pseudos[b] = function(t) {
                return function(e) {
                    return "input" === e.nodeName.toLowerCase() && e.type === t
                }
            }(b);
        for (b in {
            submit: !0,
            reset: !0
        })
            E.pseudos[b] = function(t) {
                return function(e) {
                    var n = e.nodeName.toLowerCase();
                    return ("input" === n || "button" === n) && e.type === t
                }
            }(b);
        return u.prototype = E.filters = E.pseudos,
        E.setFilters = new u,
        T = e.tokenize = function(t, n) {
            var i, o, r, s, a, l, c, u = B[t + " "];
            if (u)
                return n ? 0 : u.slice(0);
            for (a = t,
            l = [],
            c = E.preFilter; a; ) {
                i && !(o = st.exec(a)) || (o && (a = a.slice(o[0].length) || a),
                l.push(r = [])),
                i = !1,
                (o = at.exec(a)) && (i = o.shift(),
                r.push({
                    value: i,
                    type: o[0].replace(rt, " ")
                }),
                a = a.slice(i.length));
                for (s in E.filter)
                    !(o = dt[s].exec(a)) || c[s] && !(o = c[s](o)) || (i = o.shift(),
                    r.push({
                        value: i,
                        type: s,
                        matches: o
                    }),
                    a = a.slice(i.length));
                if (!i)
                    break
            }
            return n ? a.length : a ? e.error(t) : B(t, l).slice(0)
        }
        ,
        k = e.compile = function(t, e) {
            var n, i = [], o = [], r = U[t + " "];
            if (!r) {
                for (e || (e = T(t)),
                n = e.length; n--; )
                    r = m(e[n]),
                    r[$] ? i.push(r) : o.push(r);
                r = U(t, y(o, i)),
                r.selector = t
            }
            return r
        }
        ,
        S = e.select = function(t, e, n, i) {
            var o, r, s, a, l, u = "function" == typeof t && t, f = !i && T(t = u.selector || t);
            if (n = n || [],
            1 === f.length) {
                if (r = f[0] = f[0].slice(0),
                r.length > 2 && "ID" === (s = r[0]).type && 9 === e.nodeType && j && E.relative[r[1].type]) {
                    if (!(e = (E.find.ID(s.matches[0].replace(mt, yt), e) || [])[0]))
                        return n;
                    u && (e = e.parentNode),
                    t = t.slice(r.shift().value.length)
                }
                for (o = dt.needsContext.test(t) ? 0 : r.length; o-- && (s = r[o],
                !E.relative[a = s.type]); )
                    if ((l = E.find[a]) && (i = l(s.matches[0].replace(mt, yt), vt.test(r[0].type) && c(e.parentNode) || e))) {
                        if (r.splice(o, 1),
                        !(t = i.length && d(r)))
                            return Q.apply(n, i),
                            n;
                        break
                    }
            }
            return (u || k(t, f))(i, e, !j, n, !e || vt.test(t) && c(e.parentNode) || e),
            n
        }
        ,
        w.sortStable = $.split("").sort(q).join("") === $,
        w.detectDuplicates = !!P,
        L(),
        w.sortDetached = o(function(t) {
            return 1 & t.compareDocumentPosition(I.createElement("fieldset"))
        }),
        o(function(t) {
            return t.innerHTML = "<a href='#'></a>",
            "#" === t.firstChild.getAttribute("href")
        }) || r("type|href|height|width", function(t, e, n) {
            if (!n)
                return t.getAttribute(e, "type" === e.toLowerCase() ? 1 : 2)
        }),
        w.attributes && o(function(t) {
            return t.innerHTML = "<input/>",
            t.firstChild.setAttribute("value", ""),
            "" === t.firstChild.getAttribute("value")
        }) || r("value", function(t, e, n) {
            if (!n && "input" === t.nodeName.toLowerCase())
                return t.defaultValue
        }),
        o(function(t) {
            return null == t.getAttribute("disabled")
        }) || r(Z, function(t, e, n) {
            var i;
            if (!n)
                return !0 === t[e] ? e.toLowerCase() : (i = t.getAttributeNode(e)) && i.specified ? i.value : null
        }),
        e
    }(t);
    ht.find = bt,
    ht.expr = bt.selectors,
    ht.expr[":"] = ht.expr.pseudos,
    ht.uniqueSort = ht.unique = bt.uniqueSort,
    ht.text = bt.getText,
    ht.isXMLDoc = bt.isXML,
    ht.contains = bt.contains,
    ht.escapeSelector = bt.escape;
    var wt = function(t, e, n) {
        for (var i = [], o = void 0 !== n; (t = t[e]) && 9 !== t.nodeType; )
            if (1 === t.nodeType) {
                if (o && ht(t).is(n))
                    break;
                i.push(t)
            }
        return i
    }
      , Et = function(t, e) {
        for (var n = []; t; t = t.nextSibling)
            1 === t.nodeType && t !== e && n.push(t);
        return n
    }
      , Ct = ht.expr.match.needsContext
      , xt = /^<([a-z][^\/\0>:\x20\t\r\n\f]*)[\x20\t\r\n\f]*\/?>(?:<\/\1>|)$/i
      , Tt = /^.[^:#\[\.,]*$/;
    ht.filter = function(t, e, n) {
        var i = e[0];
        return n && (t = ":not(" + t + ")"),
        1 === e.length && 1 === i.nodeType ? ht.find.matchesSelector(i, t) ? [i] : [] : ht.find.matches(t, ht.grep(e, function(t) {
            return 1 === t.nodeType
        }))
    }
    ,
    ht.fn.extend({
        find: function(t) {
            var e, n, i = this.length, o = this;
            if ("string" != typeof t)
                return this.pushStack(ht(t).filter(function() {
                    for (e = 0; e < i; e++)
                        if (ht.contains(o[e], this))
                            return !0
                }));
            for (n = this.pushStack([]),
            e = 0; e < i; e++)
                ht.find(t, o[e], n);
            return i > 1 ? ht.uniqueSort(n) : n
        },
        filter: function(t) {
            return this.pushStack(r(this, t || [], !1))
        },
        not: function(t) {
            return this.pushStack(r(this, t || [], !0))
        },
        is: function(t) {
            return !!r(this, "string" == typeof t && Ct.test(t) ? ht(t) : t || [], !1).length
        }
    });
    var kt, St = /^(?:\s*(<[\w\W]+>)[^>]*|#([\w-]+))$/;
    (ht.fn.init = function(t, e, n) {
        var i, o;
        if (!t)
            return this;
        if (n = n || kt,
        "string" == typeof t) {
            if (!(i = "<" === t[0] && ">" === t[t.length - 1] && t.length >= 3 ? [null, t, null] : St.exec(t)) || !i[1] && e)
                return !e || e.jquery ? (e || n).find(t) : this.constructor(e).find(t);
            if (i[1]) {
                if (e = e instanceof ht ? e[0] : e,
                ht.merge(this, ht.parseHTML(i[1], e && e.nodeType ? e.ownerDocument || e : nt, !0)),
                xt.test(i[1]) && ht.isPlainObject(e))
                    for (i in e)
                        ht.isFunction(this[i]) ? this[i](e[i]) : this.attr(i, e[i]);
                return this
            }
            return o = nt.getElementById(i[2]),
            o && (this[0] = o,
            this.length = 1),
            this
        }
        return t.nodeType ? (this[0] = t,
        this.length = 1,
        this) : ht.isFunction(t) ? void 0 !== n.ready ? n.ready(t) : t(ht) : ht.makeArray(t, this)
    }
    ).prototype = ht.fn,
    kt = ht(nt);
    var _t = /^(?:parents|prev(?:Until|All))/
      , At = {
        children: !0,
        contents: !0,
        next: !0,
        prev: !0
    };
    ht.fn.extend({
        has: function(t) {
            var e = ht(t, this)
              , n = e.length;
            return this.filter(function() {
                for (var t = 0; t < n; t++)
                    if (ht.contains(this, e[t]))
                        return !0
            })
        },
        closest: function(t, e) {
            var n, i = 0, o = this.length, r = [], s = "string" != typeof t && ht(t);
            if (!Ct.test(t))
                for (; i < o; i++)
                    for (n = this[i]; n && n !== e; n = n.parentNode)
                        if (n.nodeType < 11 && (s ? s.index(n) > -1 : 1 === n.nodeType && ht.find.matchesSelector(n, t))) {
                            r.push(n);
                            break
                        }
            return this.pushStack(r.length > 1 ? ht.uniqueSort(r) : r)
        },
        index: function(t) {
            return t ? "string" == typeof t ? at.call(ht(t), this[0]) : at.call(this, t.jquery ? t[0] : t) : this[0] && this[0].parentNode ? this.first().prevAll().length : -1
        },
        add: function(t, e) {
            return this.pushStack(ht.uniqueSort(ht.merge(this.get(), ht(t, e))))
        },
        addBack: function(t) {
            return this.add(null == t ? this.prevObject : this.prevObject.filter(t))
        }
    }),
    ht.each({
        parent: function(t) {
            var e = t.parentNode;
            return e && 11 !== e.nodeType ? e : null
        },
        parents: function(t) {
            return wt(t, "parentNode")
        },
        parentsUntil: function(t, e, n) {
            return wt(t, "parentNode", n)
        },
        next: function(t) {
            return s(t, "nextSibling")
        },
        prev: function(t) {
            return s(t, "previousSibling")
        },
        nextAll: function(t) {
            return wt(t, "nextSibling")
        },
        prevAll: function(t) {
            return wt(t, "previousSibling")
        },
        nextUntil: function(t, e, n) {
            return wt(t, "nextSibling", n)
        },
        prevUntil: function(t, e, n) {
            return wt(t, "previousSibling", n)
        },
        siblings: function(t) {
            return Et((t.parentNode || {}).firstChild, t)
        },
        children: function(t) {
            return Et(t.firstChild)
        },
        contents: function(t) {
            return o(t, "iframe") ? t.contentDocument : (o(t, "template") && (t = t.content || t),
            ht.merge([], t.childNodes))
        }
    }, function(t, e) {
        ht.fn[t] = function(n, i) {
            var o = ht.map(this, e, n);
            return "Until" !== t.slice(-5) && (i = n),
            i && "string" == typeof i && (o = ht.filter(i, o)),
            this.length > 1 && (At[t] || ht.uniqueSort(o),
            _t.test(t) && o.reverse()),
            this.pushStack(o)
        }
    });
    var Pt = /[^\x20\t\r\n\f]+/g;
    ht.Callbacks = function(t) {
        t = "string" == typeof t ? a(t) : ht.extend({}, t);
        var e, n, i, o, r = [], s = [], l = -1, c = function() {
            for (o = o || t.once,
            i = e = !0; s.length; l = -1)
                for (n = s.shift(); ++l < r.length; )
                    !1 === r[l].apply(n[0], n[1]) && t.stopOnFalse && (l = r.length,
                    n = !1);
            t.memory || (n = !1),
            e = !1,
            o && (r = n ? [] : "")
        }, u = {
            add: function() {
                return r && (n && !e && (l = r.length - 1,
                s.push(n)),
                function e(n) {
                    ht.each(n, function(n, i) {
                        ht.isFunction(i) ? t.unique && u.has(i) || r.push(i) : i && i.length && "string" !== ht.type(i) && e(i)
                    })
                }(arguments),
                n && !e && c()),
                this
            },
            remove: function() {
                return ht.each(arguments, function(t, e) {
                    for (var n; (n = ht.inArray(e, r, n)) > -1; )
                        r.splice(n, 1),
                        n <= l && l--
                }),
                this
            },
            has: function(t) {
                return t ? ht.inArray(t, r) > -1 : r.length > 0
            },
            empty: function() {
                return r && (r = []),
                this
            },
            disable: function() {
                return o = s = [],
                r = n = "",
                this
            },
            disabled: function() {
                return !r
            },
            lock: function() {
                return o = s = [],
                n || e || (r = n = ""),
                this
            },
            locked: function() {
                return !!o
            },
            fireWith: function(t, n) {
                return o || (n = n || [],
                n = [t, n.slice ? n.slice() : n],
                s.push(n),
                e || c()),
                this
            },
            fire: function() {
                return u.fireWith(this, arguments),
                this
            },
            fired: function() {
                return !!i
            }
        };
        return u
    }
    ,
    ht.extend({
        Deferred: function(e) {
            var n = [["notify", "progress", ht.Callbacks("memory"), ht.Callbacks("memory"), 2], ["resolve", "done", ht.Callbacks("once memory"), ht.Callbacks("once memory"), 0, "resolved"], ["reject", "fail", ht.Callbacks("once memory"), ht.Callbacks("once memory"), 1, "rejected"]]
              , i = "pending"
              , o = {
                state: function() {
                    return i
                },
                always: function() {
                    return r.done(arguments).fail(arguments),
                    this
                },
                catch: function(t) {
                    return o.then(null, t)
                },
                pipe: function() {
                    var t = arguments;
                    return ht.Deferred(function(e) {
                        ht.each(n, function(n, i) {
                            var o = ht.isFunction(t[i[4]]) && t[i[4]];
                            r[i[1]](function() {
                                var t = o && o.apply(this, arguments);
                                t && ht.isFunction(t.promise) ? t.promise().progress(e.notify).done(e.resolve).fail(e.reject) : e[i[0] + "With"](this, o ? [t] : arguments)
                            })
                        }),
                        t = null
                    }).promise()
                },
                then: function(e, i, o) {
                    function r(e, n, i, o) {
                        return function() {
                            var a = this
                              , u = arguments
                              , d = function() {
                                var t, d;
                                if (!(e < s)) {
                                    if ((t = i.apply(a, u)) === n.promise())
                                        throw new TypeError("Thenable self-resolution");
                                    d = t && ("object" == typeof t || "function" == typeof t) && t.then,
                                    ht.isFunction(d) ? o ? d.call(t, r(s, n, l, o), r(s, n, c, o)) : (s++,
                                    d.call(t, r(s, n, l, o), r(s, n, c, o), r(s, n, l, n.notifyWith))) : (i !== l && (a = void 0,
                                    u = [t]),
                                    (o || n.resolveWith)(a, u))
                                }
                            }
                              , f = o ? d : function() {
                                try {
                                    d()
                                } catch (t) {
                                    ht.Deferred.exceptionHook && ht.Deferred.exceptionHook(t, f.stackTrace),
                                    e + 1 >= s && (i !== c && (a = void 0,
                                    u = [t]),
                                    n.rejectWith(a, u))
                                }
                            }
                            ;
                            e ? f() : (ht.Deferred.getStackHook && (f.stackTrace = ht.Deferred.getStackHook()),
                            t.setTimeout(f))
                        }
                    }
                    var s = 0;
                    return ht.Deferred(function(t) {
                        n[0][3].add(r(0, t, ht.isFunction(o) ? o : l, t.notifyWith)),
                        n[1][3].add(r(0, t, ht.isFunction(e) ? e : l)),
                        n[2][3].add(r(0, t, ht.isFunction(i) ? i : c))
                    }).promise()
                },
                promise: function(t) {
                    return null != t ? ht.extend(t, o) : o
                }
            }
              , r = {};
            return ht.each(n, function(t, e) {
                var s = e[2]
                  , a = e[5];
                o[e[1]] = s.add,
                a && s.add(function() {
                    i = a
                }, n[3 - t][2].disable, n[0][2].lock),
                s.add(e[3].fire),
                r[e[0]] = function() {
                    return r[e[0] + "With"](this === r ? void 0 : this, arguments),
                    this
                }
                ,
                r[e[0] + "With"] = s.fireWith
            }),
            o.promise(r),
            e && e.call(r, r),
            r
        },
        when: function(t) {
            var e = arguments.length
              , n = e
              , i = Array(n)
              , o = ot.call(arguments)
              , r = ht.Deferred()
              , s = function(t) {
                return function(n) {
                    i[t] = this,
                    o[t] = arguments.length > 1 ? ot.call(arguments) : n,
                    --e || r.resolveWith(i, o)
                }
            };
            if (e <= 1 && (u(t, r.done(s(n)).resolve, r.reject, !e),
            "pending" === r.state() || ht.isFunction(o[n] && o[n].then)))
                return r.then();
            for (; n--; )
                u(o[n], s(n), r.reject);
            return r.promise()
        }
    });
    var Lt = /^(Eval|Internal|Range|Reference|Syntax|Type|URI)Error$/;
    ht.Deferred.exceptionHook = function(e, n) {
        t.console && t.console.warn && e && Lt.test(e.name) && t.console.warn("jQuery.Deferred exception: " + e.message, e.stack, n)
    }
    ,
    ht.readyException = function(e) {
        t.setTimeout(function() {
            throw e
        })
    }
    ;
    var It = ht.Deferred();
    ht.fn.ready = function(t) {
        return It.then(t).catch(function(t) {
            ht.readyException(t)
        }),
        this
    }
    ,
    ht.extend({
        isReady: !1,
        readyWait: 1,
        ready: function(t) {
            (!0 === t ? --ht.readyWait : ht.isReady) || (ht.isReady = !0,
            !0 !== t && --ht.readyWait > 0 || It.resolveWith(nt, [ht]))
        }
    }),
    ht.ready.then = It.then,
    "complete" === nt.readyState || "loading" !== nt.readyState && !nt.documentElement.doScroll ? t.setTimeout(ht.ready) : (nt.addEventListener("DOMContentLoaded", d),
    t.addEventListener("load", d));
    var Ot = function(t, e, n, i, o, r, s) {
        var a = 0
          , l = t.length
          , c = null == n;
        if ("object" === ht.type(n)) {
            o = !0;
            for (a in n)
                Ot(t, e, a, n[a], !0, r, s)
        } else if (void 0 !== i && (o = !0,
        ht.isFunction(i) || (s = !0),
        c && (s ? (e.call(t, i),
        e = null) : (c = e,
        e = function(t, e, n) {
            return c.call(ht(t), n)
        }
        )),
        e))
            for (; a < l; a++)
                e(t[a], n, s ? i : i.call(t[a], a, e(t[a], n)));
        return o ? t : c ? e.call(t) : l ? e(t[0], n) : r
    }
      , jt = function(t) {
        return 1 === t.nodeType || 9 === t.nodeType || !+t.nodeType
    };
    f.uid = 1,
    f.prototype = {
        cache: function(t) {
            var e = t[this.expando];
            return e || (e = {},
            jt(t) && (t.nodeType ? t[this.expando] = e : Object.defineProperty(t, this.expando, {
                value: e,
                configurable: !0
            }))),
            e
        },
        set: function(t, e, n) {
            var i, o = this.cache(t);
            if ("string" == typeof e)
                o[ht.camelCase(e)] = n;
            else
                for (i in e)
                    o[ht.camelCase(i)] = e[i];
            return o
        },
        get: function(t, e) {
            return void 0 === e ? this.cache(t) : t[this.expando] && t[this.expando][ht.camelCase(e)]
        },
        access: function(t, e, n) {
            return void 0 === e || e && "string" == typeof e && void 0 === n ? this.get(t, e) : (this.set(t, e, n),
            void 0 !== n ? n : e)
        },
        remove: function(t, e) {
            var n, i = t[this.expando];
            if (void 0 !== i) {
                if (void 0 !== e) {
                    Array.isArray(e) ? e = e.map(ht.camelCase) : (e = ht.camelCase(e),
                    e = e in i ? [e] : e.match(Pt) || []),
                    n = e.length;
                    for (; n--; )
                        delete i[e[n]]
                }
                (void 0 === e || ht.isEmptyObject(i)) && (t.nodeType ? t[this.expando] = void 0 : delete t[this.expando])
            }
        },
        hasData: function(t) {
            var e = t[this.expando];
            return void 0 !== e && !ht.isEmptyObject(e)
        }
    };
    var Dt = new f
      , Rt = new f
      , Mt = /^(?:\{[\w\W]*\}|\[[\w\W]*\])$/
      , Nt = /[A-Z]/g;
    ht.extend({
        hasData: function(t) {
            return Rt.hasData(t) || Dt.hasData(t)
        },
        data: function(t, e, n) {
            return Rt.access(t, e, n)
        },
        removeData: function(t, e) {
            Rt.remove(t, e)
        },
        _data: function(t, e, n) {
            return Dt.access(t, e, n)
        },
        _removeData: function(t, e) {
            Dt.remove(t, e)
        }
    }),
    ht.fn.extend({
        data: function(t, e) {
            var n, i, o, r = this[0], s = r && r.attributes;
            if (void 0 === t) {
                if (this.length && (o = Rt.get(r),
                1 === r.nodeType && !Dt.get(r, "hasDataAttrs"))) {
                    for (n = s.length; n--; )
                        s[n] && (i = s[n].name,
                        0 === i.indexOf("data-") && (i = ht.camelCase(i.slice(5)),
                        h(r, i, o[i])));
                    Dt.set(r, "hasDataAttrs", !0)
                }
                return o
            }
            return "object" == typeof t ? this.each(function() {
                Rt.set(this, t)
            }) : Ot(this, function(e) {
                var n;
                if (r && void 0 === e) {
                    if (void 0 !== (n = Rt.get(r, t)))
                        return n;
                    if (void 0 !== (n = h(r, t)))
                        return n
                } else
                    this.each(function() {
                        Rt.set(this, t, e)
                    })
            }, null, e, arguments.length > 1, null, !0)
        },
        removeData: function(t) {
            return this.each(function() {
                Rt.remove(this, t)
            })
        }
    }),
    ht.extend({
        queue: function(t, e, n) {
            var i;
            if (t)
                return e = (e || "fx") + "queue",
                i = Dt.get(t, e),
                n && (!i || Array.isArray(n) ? i = Dt.access(t, e, ht.makeArray(n)) : i.push(n)),
                i || []
        },
        dequeue: function(t, e) {
            e = e || "fx";
            var n = ht.queue(t, e)
              , i = n.length
              , o = n.shift()
              , r = ht._queueHooks(t, e)
              , s = function() {
                ht.dequeue(t, e)
            };
            "inprogress" === o && (o = n.shift(),
            i--),
            o && ("fx" === e && n.unshift("inprogress"),
            delete r.stop,
            o.call(t, s, r)),
            !i && r && r.empty.fire()
        },
        _queueHooks: function(t, e) {
            var n = e + "queueHooks";
            return Dt.get(t, n) || Dt.access(t, n, {
                empty: ht.Callbacks("once memory").add(function() {
                    Dt.remove(t, [e + "queue", n])
                })
            })
        }
    }),
    ht.fn.extend({
        queue: function(t, e) {
            var n = 2;
            return "string" != typeof t && (e = t,
            t = "fx",
            n--),
            arguments.length < n ? ht.queue(this[0], t) : void 0 === e ? this : this.each(function() {
                var n = ht.queue(this, t, e);
                ht._queueHooks(this, t),
                "fx" === t && "inprogress" !== n[0] && ht.dequeue(this, t)
            })
        },
        dequeue: function(t) {
            return this.each(function() {
                ht.dequeue(this, t)
            })
        },
        clearQueue: function(t) {
            return this.queue(t || "fx", [])
        },
        promise: function(t, e) {
            var n, i = 1, o = ht.Deferred(), r = this, s = this.length, a = function() {
                --i || o.resolveWith(r, [r])
            };
            for ("string" != typeof t && (e = t,
            t = void 0),
            t = t || "fx"; s--; )
                (n = Dt.get(r[s], t + "queueHooks")) && n.empty && (i++,
                n.empty.add(a));
            return a(),
            o.promise(e)
        }
    });
    var $t = /[+-]?(?:\d*\.|)\d+(?:[eE][+-]?\d+|)/.source
      , Ft = new RegExp("^(?:([+-])=|)(" + $t + ")([a-z%]*)$","i")
      , Wt = ["Top", "Right", "Bottom", "Left"]
      , Ht = function(t, e) {
        return t = e || t,
        "none" === t.style.display || "" === t.style.display && ht.contains(t.ownerDocument, t) && "none" === ht.css(t, "display")
    }
      , Yt = function(t, e, n, i) {
        var o, r, s = {};
        for (r in e)
            s[r] = t.style[r],
            t.style[r] = e[r];
        o = n.apply(t, i || []);
        for (r in e)
            t.style[r] = s[r];
        return o
    }
      , Bt = {};
    ht.fn.extend({
        show: function() {
            return m(this, !0)
        },
        hide: function() {
            return m(this)
        },
        toggle: function(t) {
            return "boolean" == typeof t ? t ? this.show() : this.hide() : this.each(function() {
                Ht(this) ? ht(this).show() : ht(this).hide()
            })
        }
    });
    var Ut = /^(?:checkbox|radio)$/i
      , qt = /<([a-z][^\/\0>\x20\t\r\n\f]+)/i
      , zt = /^$|\/(?:java|ecma)script/i
      , Vt = {
        option: [1, "<select multiple='multiple'>", "</select>"],
        thead: [1, "<table>", "</table>"],
        col: [2, "<table><colgroup>", "</colgroup></table>"],
        tr: [2, "<table><tbody>", "</tbody></table>"],
        td: [3, "<table><tbody><tr>", "</tr></tbody></table>"],
        _default: [0, "", ""]
    };
    Vt.optgroup = Vt.option,
    Vt.tbody = Vt.tfoot = Vt.colgroup = Vt.caption = Vt.thead,
    Vt.th = Vt.td;
    var Xt = /<|&#?\w+;/;
    !function() {
        var t = nt.createDocumentFragment()
          , e = t.appendChild(nt.createElement("div"))
          , n = nt.createElement("input");
        n.setAttribute("type", "radio"),
        n.setAttribute("checked", "checked"),
        n.setAttribute("name", "t"),
        e.appendChild(n),
        pt.checkClone = e.cloneNode(!0).cloneNode(!0).lastChild.checked,
        e.innerHTML = "<textarea>x</textarea>",
        pt.noCloneChecked = !!e.cloneNode(!0).lastChild.defaultValue
    }();
    var Jt = nt.documentElement
      , Qt = /^key/
      , Gt = /^(?:mouse|pointer|contextmenu|drag|drop)|click/
      , Kt = /^([^.]*)(?:\.(.+)|)/;
    ht.event = {
        global: {},
        add: function(t, e, n, i, o) {
            var r, s, a, l, c, u, d, f, p, h, g, v = Dt.get(t);
            if (v)
                for (n.handler && (r = n,
                n = r.handler,
                o = r.selector),
                o && ht.find.matchesSelector(Jt, o),
                n.guid || (n.guid = ht.guid++),
                (l = v.events) || (l = v.events = {}),
                (s = v.handle) || (s = v.handle = function(e) {
                    return void 0 !== ht && ht.event.triggered !== e.type ? ht.event.dispatch.apply(t, arguments) : void 0
                }
                ),
                e = (e || "").match(Pt) || [""],
                c = e.length; c--; )
                    a = Kt.exec(e[c]) || [],
                    p = g = a[1],
                    h = (a[2] || "").split(".").sort(),
                    p && (d = ht.event.special[p] || {},
                    p = (o ? d.delegateType : d.bindType) || p,
                    d = ht.event.special[p] || {},
                    u = ht.extend({
                        type: p,
                        origType: g,
                        data: i,
                        handler: n,
                        guid: n.guid,
                        selector: o,
                        needsContext: o && ht.expr.match.needsContext.test(o),
                        namespace: h.join(".")
                    }, r),
                    (f = l[p]) || (f = l[p] = [],
                    f.delegateCount = 0,
                    d.setup && !1 !== d.setup.call(t, i, h, s) || t.addEventListener && t.addEventListener(p, s)),
                    d.add && (d.add.call(t, u),
                    u.handler.guid || (u.handler.guid = n.guid)),
                    o ? f.splice(f.delegateCount++, 0, u) : f.push(u),
                    ht.event.global[p] = !0)
        },
        remove: function(t, e, n, i, o) {
            var r, s, a, l, c, u, d, f, p, h, g, v = Dt.hasData(t) && Dt.get(t);
            if (v && (l = v.events)) {
                for (e = (e || "").match(Pt) || [""],
                c = e.length; c--; )
                    if (a = Kt.exec(e[c]) || [],
                    p = g = a[1],
                    h = (a[2] || "").split(".").sort(),
                    p) {
                        for (d = ht.event.special[p] || {},
                        p = (i ? d.delegateType : d.bindType) || p,
                        f = l[p] || [],
                        a = a[2] && new RegExp("(^|\\.)" + h.join("\\.(?:.*\\.|)") + "(\\.|$)"),
                        s = r = f.length; r--; )
                            u = f[r],
                            !o && g !== u.origType || n && n.guid !== u.guid || a && !a.test(u.namespace) || i && i !== u.selector && ("**" !== i || !u.selector) || (f.splice(r, 1),
                            u.selector && f.delegateCount--,
                            d.remove && d.remove.call(t, u));
                        s && !f.length && (d.teardown && !1 !== d.teardown.call(t, h, v.handle) || ht.removeEvent(t, p, v.handle),
                        delete l[p])
                    } else
                        for (p in l)
                            ht.event.remove(t, p + e[c], n, i, !0);
                ht.isEmptyObject(l) && Dt.remove(t, "handle events")
            }
        },
        dispatch: function(t) {
            var e, n, i, o, r, s, a = ht.event.fix(t), l = new Array(arguments.length), c = (Dt.get(this, "events") || {})[a.type] || [], u = ht.event.special[a.type] || {};
            for (l[0] = a,
            e = 1; e < arguments.length; e++)
                l[e] = arguments[e];
            if (a.delegateTarget = this,
            !u.preDispatch || !1 !== u.preDispatch.call(this, a)) {
                for (s = ht.event.handlers.call(this, a, c),
                e = 0; (o = s[e++]) && !a.isPropagationStopped(); )
                    for (a.currentTarget = o.elem,
                    n = 0; (r = o.handlers[n++]) && !a.isImmediatePropagationStopped(); )
                        a.rnamespace && !a.rnamespace.test(r.namespace) || (a.handleObj = r,
                        a.data = r.data,
                        void 0 !== (i = ((ht.event.special[r.origType] || {}).handle || r.handler).apply(o.elem, l)) && !1 === (a.result = i) && (a.preventDefault(),
                        a.stopPropagation()));
                return u.postDispatch && u.postDispatch.call(this, a),
                a.result
            }
        },
        handlers: function(t, e) {
            var n, i, o, r, s, a = [], l = e.delegateCount, c = t.target;
            if (l && c.nodeType && !("click" === t.type && t.button >= 1))
                for (; c !== this; c = c.parentNode || this)
                    if (1 === c.nodeType && ("click" !== t.type || !0 !== c.disabled)) {
                        for (r = [],
                        s = {},
                        n = 0; n < l; n++)
                            i = e[n],
                            o = i.selector + " ",
                            void 0 === s[o] && (s[o] = i.needsContext ? ht(o, this).index(c) > -1 : ht.find(o, this, null, [c]).length),
                            s[o] && r.push(i);
                        r.length && a.push({
                            elem: c,
                            handlers: r
                        })
                    }
            return c = this,
            l < e.length && a.push({
                elem: c,
                handlers: e.slice(l)
            }),
            a
        },
        addProp: function(t, e) {
            Object.defineProperty(ht.Event.prototype, t, {
                enumerable: !0,
                configurable: !0,
                get: ht.isFunction(e) ? function() {
                    if (this.originalEvent)
                        return e(this.originalEvent)
                }
                : function() {
                    if (this.originalEvent)
                        return this.originalEvent[t]
                }
                ,
                set: function(e) {
                    Object.defineProperty(this, t, {
                        enumerable: !0,
                        configurable: !0,
                        writable: !0,
                        value: e
                    })
                }
            })
        },
        fix: function(t) {
            return t[ht.expando] ? t : new ht.Event(t)
        },
        special: {
            load: {
                noBubble: !0
            },
            focus: {
                trigger: function() {
                    if (this !== x() && this.focus)
                        return this.focus(),
                        !1
                },
                delegateType: "focusin"
            },
            blur: {
                trigger: function() {
                    if (this === x() && this.blur)
                        return this.blur(),
                        !1
                },
                delegateType: "focusout"
            },
            click: {
                trigger: function() {
                    if ("checkbox" === this.type && this.click && o(this, "input"))
                        return this.click(),
                        !1
                },
                _default: function(t) {
                    return o(t.target, "a")
                }
            },
            beforeunload: {
                postDispatch: function(t) {
                    void 0 !== t.result && t.originalEvent && (t.originalEvent.returnValue = t.result)
                }
            }
        }
    },
    ht.removeEvent = function(t, e, n) {
        t.removeEventListener && t.removeEventListener(e, n)
    }
    ,
    ht.Event = function(t, e) {
        if (!(this instanceof ht.Event))
            return new ht.Event(t,e);
        t && t.type ? (this.originalEvent = t,
        this.type = t.type,
        this.isDefaultPrevented = t.defaultPrevented || void 0 === t.defaultPrevented && !1 === t.returnValue ? E : C,
        this.target = t.target && 3 === t.target.nodeType ? t.target.parentNode : t.target,
        this.currentTarget = t.currentTarget,
        this.relatedTarget = t.relatedTarget) : this.type = t,
        e && ht.extend(this, e),
        this.timeStamp = t && t.timeStamp || ht.now(),
        this[ht.expando] = !0
    }
    ,
    ht.Event.prototype = {
        constructor: ht.Event,
        isDefaultPrevented: C,
        isPropagationStopped: C,
        isImmediatePropagationStopped: C,
        isSimulated: !1,
        preventDefault: function() {
            var t = this.originalEvent;
            this.isDefaultPrevented = E,
            t && !this.isSimulated && t.preventDefault()
        },
        stopPropagation: function() {
            var t = this.originalEvent;
            this.isPropagationStopped = E,
            t && !this.isSimulated && t.stopPropagation()
        },
        stopImmediatePropagation: function() {
            var t = this.originalEvent;
            this.isImmediatePropagationStopped = E,
            t && !this.isSimulated && t.stopImmediatePropagation(),
            this.stopPropagation()
        }
    },
    ht.each({
        altKey: !0,
        bubbles: !0,
        cancelable: !0,
        changedTouches: !0,
        ctrlKey: !0,
        detail: !0,
        eventPhase: !0,
        metaKey: !0,
        pageX: !0,
        pageY: !0,
        shiftKey: !0,
        view: !0,
        char: !0,
        charCode: !0,
        key: !0,
        keyCode: !0,
        button: !0,
        buttons: !0,
        clientX: !0,
        clientY: !0,
        offsetX: !0,
        offsetY: !0,
        pointerId: !0,
        pointerType: !0,
        screenX: !0,
        screenY: !0,
        targetTouches: !0,
        toElement: !0,
        touches: !0,
        which: function(t) {
            var e = t.button;
            return null == t.which && Qt.test(t.type) ? null != t.charCode ? t.charCode : t.keyCode : !t.which && void 0 !== e && Gt.test(t.type) ? 1 & e ? 1 : 2 & e ? 3 : 4 & e ? 2 : 0 : t.which
        }
    }, ht.event.addProp),
    ht.each({
        mouseenter: "mouseover",
        mouseleave: "mouseout",
        pointerenter: "pointerover",
        pointerleave: "pointerout"
    }, function(t, e) {
        ht.event.special[t] = {
            delegateType: e,
            bindType: e,
            handle: function(t) {
                var n, i = this, o = t.relatedTarget, r = t.handleObj;
                return o && (o === i || ht.contains(i, o)) || (t.type = r.origType,
                n = r.handler.apply(this, arguments),
                t.type = e),
                n
            }
        }
    }),
    ht.fn.extend({
        on: function(t, e, n, i) {
            return T(this, t, e, n, i)
        },
        one: function(t, e, n, i) {
            return T(this, t, e, n, i, 1)
        },
        off: function(t, e, n) {
            var i, o;
            if (t && t.preventDefault && t.handleObj)
                return i = t.handleObj,
                ht(t.delegateTarget).off(i.namespace ? i.origType + "." + i.namespace : i.origType, i.selector, i.handler),
                this;
            if ("object" == typeof t) {
                for (o in t)
                    this.off(o, e, t[o]);
                return this
            }
            return !1 !== e && "function" != typeof e || (n = e,
            e = void 0),
            !1 === n && (n = C),
            this.each(function() {
                ht.event.remove(this, t, n, e)
            })
        }
    });
    var Zt = /<(?!area|br|col|embed|hr|img|input|link|meta|param)(([a-z][^\/\0>\x20\t\r\n\f]*)[^>]*)\/>/gi
      , te = /<script|<style|<link/i
      , ee = /checked\s*(?:[^=]|=\s*.checked.)/i
      , ne = /^true\/(.*)/
      , ie = /^\s*<!(?:\[CDATA\[|--)|(?:\]\]|--)>\s*$/g;
    ht.extend({
        htmlPrefilter: function(t) {
            return t.replace(Zt, "<$1></$2>")
        },
        clone: function(t, e, n) {
            var i, o, r, s, a = t.cloneNode(!0), l = ht.contains(t.ownerDocument, t);
            if (!(pt.noCloneChecked || 1 !== t.nodeType && 11 !== t.nodeType || ht.isXMLDoc(t)))
                for (s = y(a),
                r = y(t),
                i = 0,
                o = r.length; i < o; i++)
                    P(r[i], s[i]);
            if (e)
                if (n)
                    for (r = r || y(t),
                    s = s || y(a),
                    i = 0,
                    o = r.length; i < o; i++)
                        A(r[i], s[i]);
                else
                    A(t, a);
            return s = y(a, "script"),
            s.length > 0 && b(s, !l && y(t, "script")),
            a
        },
        cleanData: function(t) {
            for (var e, n, i, o = ht.event.special, r = 0; void 0 !== (n = t[r]); r++)
                if (jt(n)) {
                    if (e = n[Dt.expando]) {
                        if (e.events)
                            for (i in e.events)
                                o[i] ? ht.event.remove(n, i) : ht.removeEvent(n, i, e.handle);
                        n[Dt.expando] = void 0
                    }
                    n[Rt.expando] && (n[Rt.expando] = void 0)
                }
        }
    }),
    ht.fn.extend({
        detach: function(t) {
            return I(this, t, !0)
        },
        remove: function(t) {
            return I(this, t)
        },
        text: function(t) {
            return Ot(this, function(t) {
                return void 0 === t ? ht.text(this) : this.empty().each(function() {
                    1 !== this.nodeType && 11 !== this.nodeType && 9 !== this.nodeType || (this.textContent = t)
                })
            }, null, t, arguments.length)
        },
        append: function() {
            return L(this, arguments, function(t) {
                if (1 === this.nodeType || 11 === this.nodeType || 9 === this.nodeType) {
                    k(this, t).appendChild(t)
                }
            })
        },
        prepend: function() {
            return L(this, arguments, function(t) {
                if (1 === this.nodeType || 11 === this.nodeType || 9 === this.nodeType) {
                    var e = k(this, t);
                    e.insertBefore(t, e.firstChild)
                }
            })
        },
        before: function() {
            return L(this, arguments, function(t) {
                this.parentNode && this.parentNode.insertBefore(t, this)
            })
        },
        after: function() {
            return L(this, arguments, function(t) {
                this.parentNode && this.parentNode.insertBefore(t, this.nextSibling)
            })
        },
        empty: function() {
            for (var t, e = 0; null != (t = this[e]); e++)
                1 === t.nodeType && (ht.cleanData(y(t, !1)),
                t.textContent = "");
            return this
        },
        clone: function(t, e) {
            return t = null != t && t,
            e = null == e ? t : e,
            this.map(function() {
                return ht.clone(this, t, e)
            })
        },
        html: function(t) {
            return Ot(this, function(t) {
                var e = this[0] || {}
                  , n = 0
                  , i = this.length;
                if (void 0 === t && 1 === e.nodeType)
                    return e.innerHTML;
                if ("string" == typeof t && !te.test(t) && !Vt[(qt.exec(t) || ["", ""])[1].toLowerCase()]) {
                    t = ht.htmlPrefilter(t);
                    try {
                        for (; n < i; n++)
                            e = this[n] || {},
                            1 === e.nodeType && (ht.cleanData(y(e, !1)),
                            e.innerHTML = t);
                        e = 0
                    } catch (t) {}
                }
                e && this.empty().append(t)
            }, null, t, arguments.length)
        },
        replaceWith: function() {
            var t = [];
            return L(this, arguments, function(e) {
                var n = this.parentNode;
                ht.inArray(this, t) < 0 && (ht.cleanData(y(this)),
                n && n.replaceChild(e, this))
            }, t)
        }
    }),
    ht.each({
        appendTo: "append",
        prependTo: "prepend",
        insertBefore: "before",
        insertAfter: "after",
        replaceAll: "replaceWith"
    }, function(t, e) {
        ht.fn[t] = function(t) {
            for (var n, i = [], o = ht(t), r = o.length - 1, s = 0; s <= r; s++)
                n = s === r ? this : this.clone(!0),
                ht(o[s])[e](n),
                st.apply(i, n.get());
            return this.pushStack(i)
        }
    });
    var oe = /^margin/
      , re = new RegExp("^(" + $t + ")(?!px)[a-z%]+$","i")
      , se = function(e) {
        var n = e.ownerDocument.defaultView;
        return n && n.opener || (n = t),
        n.getComputedStyle(e)
    };
    !function() {
        function e() {
            if (a) {
                a.style.cssText = "box-sizing:border-box;position:relative;display:block;margin:auto;border:1px;padding:1px;top:1%;width:50%",
                a.innerHTML = "",
                Jt.appendChild(s);
                var e = t.getComputedStyle(a);
                n = "1%" !== e.top,
                r = "2px" === e.marginLeft,
                i = "4px" === e.width,
                a.style.marginRight = "50%",
                o = "4px" === e.marginRight,
                Jt.removeChild(s),
                a = null
            }
        }
        var n, i, o, r, s = nt.createElement("div"), a = nt.createElement("div");
        a.style && (a.style.backgroundClip = "content-box",
        a.cloneNode(!0).style.backgroundClip = "",
        pt.clearCloneStyle = "content-box" === a.style.backgroundClip,
        s.style.cssText = "border:0;width:8px;height:0;top:0;left:-9999px;padding:0;margin-top:1px;position:absolute",
        s.appendChild(a),
        ht.extend(pt, {
            pixelPosition: function() {
                return e(),
                n
            },
            boxSizingReliable: function() {
                return e(),
                i
            },
            pixelMarginRight: function() {
                return e(),
                o
            },
            reliableMarginLeft: function() {
                return e(),
                r
            }
        }))
    }();
    var ae = /^(none|table(?!-c[ea]).+)/
      , le = /^--/
      , ce = {
        position: "absolute",
        visibility: "hidden",
        display: "block"
    }
      , ue = {
        letterSpacing: "0",
        fontWeight: "400"
    }
      , de = ["Webkit", "Moz", "ms"]
      , fe = nt.createElement("div").style;
    ht.extend({
        cssHooks: {
            opacity: {
                get: function(t, e) {
                    if (e) {
                        var n = O(t, "opacity");
                        return "" === n ? "1" : n
                    }
                }
            }
        },
        cssNumber: {
            animationIterationCount: !0,
            columnCount: !0,
            fillOpacity: !0,
            flexGrow: !0,
            flexShrink: !0,
            fontWeight: !0,
            lineHeight: !0,
            opacity: !0,
            order: !0,
            orphans: !0,
            widows: !0,
            zIndex: !0,
            zoom: !0
        },
        cssProps: {
            float: "cssFloat"
        },
        style: function(t, e, n, i) {
            if (t && 3 !== t.nodeType && 8 !== t.nodeType && t.style) {
                var o, r, s, a = ht.camelCase(e), l = le.test(e), c = t.style;
                if (l || (e = R(a)),
                s = ht.cssHooks[e] || ht.cssHooks[a],
                void 0 === n)
                    return s && "get"in s && void 0 !== (o = s.get(t, !1, i)) ? o : c[e];
                r = typeof n,
                "string" === r && (o = Ft.exec(n)) && o[1] && (n = g(t, e, o),
                r = "number"),
                null != n && n === n && ("number" === r && (n += o && o[3] || (ht.cssNumber[a] ? "" : "px")),
                pt.clearCloneStyle || "" !== n || 0 !== e.indexOf("background") || (c[e] = "inherit"),
                s && "set"in s && void 0 === (n = s.set(t, n, i)) || (l ? c.setProperty(e, n) : c[e] = n))
            }
        },
        css: function(t, e, n, i) {
            var o, r, s, a = ht.camelCase(e);
            return le.test(e) || (e = R(a)),
            s = ht.cssHooks[e] || ht.cssHooks[a],
            s && "get"in s && (o = s.get(t, !0, n)),
            void 0 === o && (o = O(t, e, i)),
            "normal" === o && e in ue && (o = ue[e]),
            "" === n || n ? (r = parseFloat(o),
            !0 === n || isFinite(r) ? r || 0 : o) : o
        }
    }),
    ht.each(["height", "width"], function(t, e) {
        ht.cssHooks[e] = {
            get: function(t, n, i) {
                if (n)
                    return !ae.test(ht.css(t, "display")) || t.getClientRects().length && t.getBoundingClientRect().width ? $(t, e, i) : Yt(t, ce, function() {
                        return $(t, e, i)
                    })
            },
            set: function(t, n, i) {
                var o, r = i && se(t), s = i && N(t, e, i, "border-box" === ht.css(t, "boxSizing", !1, r), r);
                return s && (o = Ft.exec(n)) && "px" !== (o[3] || "px") && (t.style[e] = n,
                n = ht.css(t, e)),
                M(t, n, s)
            }
        }
    }),
    ht.cssHooks.marginLeft = j(pt.reliableMarginLeft, function(t, e) {
        if (e)
            return (parseFloat(O(t, "marginLeft")) || t.getBoundingClientRect().left - Yt(t, {
                marginLeft: 0
            }, function() {
                return t.getBoundingClientRect().left
            })) + "px"
    }),
    ht.each({
        margin: "",
        padding: "",
        border: "Width"
    }, function(t, e) {
        ht.cssHooks[t + e] = {
            expand: function(n) {
                for (var i = 0, o = {}, r = "string" == typeof n ? n.split(" ") : [n]; i < 4; i++)
                    o[t + Wt[i] + e] = r[i] || r[i - 2] || r[0];
                return o
            }
        },
        oe.test(t) || (ht.cssHooks[t + e].set = M)
    }),
    ht.fn.extend({
        css: function(t, e) {
            return Ot(this, function(t, e, n) {
                var i, o, r = {}, s = 0;
                if (Array.isArray(e)) {
                    for (i = se(t),
                    o = e.length; s < o; s++)
                        r[e[s]] = ht.css(t, e[s], !1, i);
                    return r
                }
                return void 0 !== n ? ht.style(t, e, n) : ht.css(t, e)
            }, t, e, arguments.length > 1)
        }
    }),
    ht.Tween = F,
    F.prototype = {
        constructor: F,
        init: function(t, e, n, i, o, r) {
            this.elem = t,
            this.prop = n,
            this.easing = o || ht.easing._default,
            this.options = e,
            this.start = this.now = this.cur(),
            this.end = i,
            this.unit = r || (ht.cssNumber[n] ? "" : "px")
        },
        cur: function() {
            var t = F.propHooks[this.prop];
            return t && t.get ? t.get(this) : F.propHooks._default.get(this)
        },
        run: function(t) {
            var e, n = F.propHooks[this.prop];
            return this.options.duration ? this.pos = e = ht.easing[this.easing](t, this.options.duration * t, 0, 1, this.options.duration) : this.pos = e = t,
            this.now = (this.end - this.start) * e + this.start,
            this.options.step && this.options.step.call(this.elem, this.now, this),
            n && n.set ? n.set(this) : F.propHooks._default.set(this),
            this
        }
    },
    F.prototype.init.prototype = F.prototype,
    F.propHooks = {
        _default: {
            get: function(t) {
                var e;
                return 1 !== t.elem.nodeType || null != t.elem[t.prop] && null == t.elem.style[t.prop] ? t.elem[t.prop] : (e = ht.css(t.elem, t.prop, ""),
                e && "auto" !== e ? e : 0)
            },
            set: function(t) {
                ht.fx.step[t.prop] ? ht.fx.step[t.prop](t) : 1 !== t.elem.nodeType || null == t.elem.style[ht.cssProps[t.prop]] && !ht.cssHooks[t.prop] ? t.elem[t.prop] = t.now : ht.style(t.elem, t.prop, t.now + t.unit)
            }
        }
    },
    F.propHooks.scrollTop = F.propHooks.scrollLeft = {
        set: function(t) {
            t.elem.nodeType && t.elem.parentNode && (t.elem[t.prop] = t.now)
        }
    },
    ht.easing = {
        linear: function(t) {
            return t
        },
        swing: function(t) {
            return .5 - Math.cos(t * Math.PI) / 2
        },
        _default: "swing"
    },
    ht.fx = F.prototype.init,
    ht.fx.step = {};
    var pe, he, ge = /^(?:toggle|show|hide)$/, ve = /queueHooks$/;
    ht.Animation = ht.extend(z, {
        tweeners: {
            "*": [function(t, e) {
                var n = this.createTween(t, e);
                return g(n.elem, t, Ft.exec(e), n),
                n
            }
            ]
        },
        tweener: function(t, e) {
            ht.isFunction(t) ? (e = t,
            t = ["*"]) : t = t.match(Pt);
            for (var n, i = 0, o = t.length; i < o; i++)
                n = t[i],
                z.tweeners[n] = z.tweeners[n] || [],
                z.tweeners[n].unshift(e)
        },
        prefilters: [U],
        prefilter: function(t, e) {
            e ? z.prefilters.unshift(t) : z.prefilters.push(t)
        }
    }),
    ht.speed = function(t, e, n) {
        var i = t && "object" == typeof t ? ht.extend({}, t) : {
            complete: n || !n && e || ht.isFunction(t) && t,
            duration: t,
            easing: n && e || e && !ht.isFunction(e) && e
        };
        return ht.fx.off ? i.duration = 0 : "number" != typeof i.duration && (i.duration in ht.fx.speeds ? i.duration = ht.fx.speeds[i.duration] : i.duration = ht.fx.speeds._default),
        null != i.queue && !0 !== i.queue || (i.queue = "fx"),
        i.old = i.complete,
        i.complete = function() {
            ht.isFunction(i.old) && i.old.call(this),
            i.queue && ht.dequeue(this, i.queue)
        }
        ,
        i
    }
    ,
    ht.fn.extend({
        fadeTo: function(t, e, n, i) {
            return this.filter(Ht).css("opacity", 0).show().end().animate({
                opacity: e
            }, t, n, i)
        },
        animate: function(t, e, n, i) {
            var o = ht.isEmptyObject(t)
              , r = ht.speed(e, n, i)
              , s = function() {
                var e = z(this, ht.extend({}, t), r);
                (o || Dt.get(this, "finish")) && e.stop(!0)
            };
            return s.finish = s,
            o || !1 === r.queue ? this.each(s) : this.queue(r.queue, s)
        },
        stop: function(t, e, n) {
            var i = function(t) {
                var e = t.stop;
                delete t.stop,
                e(n)
            };
            return "string" != typeof t && (n = e,
            e = t,
            t = void 0),
            e && !1 !== t && this.queue(t || "fx", []),
            this.each(function() {
                var e = !0
                  , o = null != t && t + "queueHooks"
                  , r = ht.timers
                  , s = Dt.get(this);
                if (o)
                    s[o] && s[o].stop && i(s[o]);
                else
                    for (o in s)
                        s[o] && s[o].stop && ve.test(o) && i(s[o]);
                for (o = r.length; o--; )
                    r[o].elem !== this || null != t && r[o].queue !== t || (r[o].anim.stop(n),
                    e = !1,
                    r.splice(o, 1));
                !e && n || ht.dequeue(this, t)
            })
        },
        finish: function(t) {
            return !1 !== t && (t = t || "fx"),
            this.each(function() {
                var e, n = Dt.get(this), i = n[t + "queue"], o = n[t + "queueHooks"], r = ht.timers, s = i ? i.length : 0;
                for (n.finish = !0,
                ht.queue(this, t, []),
                o && o.stop && o.stop.call(this, !0),
                e = r.length; e--; )
                    r[e].elem === this && r[e].queue === t && (r[e].anim.stop(!0),
                    r.splice(e, 1));
                for (e = 0; e < s; e++)
                    i[e] && i[e].finish && i[e].finish.call(this);
                delete n.finish
            })
        }
    }),
    ht.each(["toggle", "show", "hide"], function(t, e) {
        var n = ht.fn[e];
        ht.fn[e] = function(t, i, o) {
            return null == t || "boolean" == typeof t ? n.apply(this, arguments) : this.animate(Y(e, !0), t, i, o)
        }
    }),
    ht.each({
        slideDown: Y("show"),
        slideUp: Y("hide"),
        slideToggle: Y("toggle"),
        fadeIn: {
            opacity: "show"
        },
        fadeOut: {
            opacity: "hide"
        },
        fadeToggle: {
            opacity: "toggle"
        }
    }, function(t, e) {
        ht.fn[t] = function(t, n, i) {
            return this.animate(e, t, n, i)
        }
    }),
    ht.timers = [],
    ht.fx.tick = function() {
        var t, e = 0, n = ht.timers;
        for (pe = ht.now(); e < n.length; e++)
            (t = n[e])() || n[e] !== t || n.splice(e--, 1);
        n.length || ht.fx.stop(),
        pe = void 0
    }
    ,
    ht.fx.timer = function(t) {
        ht.timers.push(t),
        ht.fx.start()
    }
    ,
    ht.fx.interval = 13,
    ht.fx.start = function() {
        he || (he = !0,
        W())
    }
    ,
    ht.fx.stop = function() {
        he = null
    }
    ,
    ht.fx.speeds = {
        slow: 600,
        fast: 200,
        _default: 400
    },
    ht.fn.delay = function(e, n) {
        return e = ht.fx ? ht.fx.speeds[e] || e : e,
        n = n || "fx",
        this.queue(n, function(n, i) {
            var o = t.setTimeout(n, e);
            i.stop = function() {
                t.clearTimeout(o)
            }
        })
    }
    ,
    function() {
        var t = nt.createElement("input")
          , e = nt.createElement("select")
          , n = e.appendChild(nt.createElement("option"));
        t.type = "checkbox",
        pt.checkOn = "" !== t.value,
        pt.optSelected = n.selected,
        t = nt.createElement("input"),
        t.value = "t",
        t.type = "radio",
        pt.radioValue = "t" === t.value
    }();
    var me, ye = ht.expr.attrHandle;
    ht.fn.extend({
        attr: function(t, e) {
            return Ot(this, ht.attr, t, e, arguments.length > 1)
        },
        removeAttr: function(t) {
            return this.each(function() {
                ht.removeAttr(this, t)
            })
        }
    }),
    ht.extend({
        attr: function(t, e, n) {
            var i, o, r = t.nodeType;
            if (3 !== r && 8 !== r && 2 !== r)
                return void 0 === t.getAttribute ? ht.prop(t, e, n) : (1 === r && ht.isXMLDoc(t) || (o = ht.attrHooks[e.toLowerCase()] || (ht.expr.match.bool.test(e) ? me : void 0)),
                void 0 !== n ? null === n ? void ht.removeAttr(t, e) : o && "set"in o && void 0 !== (i = o.set(t, n, e)) ? i : (t.setAttribute(e, n + ""),
                n) : o && "get"in o && null !== (i = o.get(t, e)) ? i : (i = ht.find.attr(t, e),
                null == i ? void 0 : i))
        },
        attrHooks: {
            type: {
                set: function(t, e) {
                    if (!pt.radioValue && "radio" === e && o(t, "input")) {
                        var n = t.value;
                        return t.setAttribute("type", e),
                        n && (t.value = n),
                        e
                    }
                }
            }
        },
        removeAttr: function(t, e) {
            var n, i = 0, o = e && e.match(Pt);
            if (o && 1 === t.nodeType)
                for (; n = o[i++]; )
                    t.removeAttribute(n)
        }
    }),
    me = {
        set: function(t, e, n) {
            return !1 === e ? ht.removeAttr(t, n) : t.setAttribute(n, n),
            n
        }
    },
    ht.each(ht.expr.match.bool.source.match(/\w+/g), function(t, e) {
        var n = ye[e] || ht.find.attr;
        ye[e] = function(t, e, i) {
            var o, r, s = e.toLowerCase();
            return i || (r = ye[s],
            ye[s] = o,
            o = null != n(t, e, i) ? s : null,
            ye[s] = r),
            o
        }
    });
    var be = /^(?:input|select|textarea|button)$/i
      , we = /^(?:a|area)$/i;
    ht.fn.extend({
        prop: function(t, e) {
            return Ot(this, ht.prop, t, e, arguments.length > 1)
        },
        removeProp: function(t) {
            return this.each(function() {
                delete this[ht.propFix[t] || t]
            })
        }
    }),
    ht.extend({
        prop: function(t, e, n) {
            var i, o, r = t.nodeType;
            if (3 !== r && 8 !== r && 2 !== r)
                return 1 === r && ht.isXMLDoc(t) || (e = ht.propFix[e] || e,
                o = ht.propHooks[e]),
                void 0 !== n ? o && "set"in o && void 0 !== (i = o.set(t, n, e)) ? i : t[e] = n : o && "get"in o && null !== (i = o.get(t, e)) ? i : t[e]
        },
        propHooks: {
            tabIndex: {
                get: function(t) {
                    var e = ht.find.attr(t, "tabindex");
                    return e ? parseInt(e, 10) : be.test(t.nodeName) || we.test(t.nodeName) && t.href ? 0 : -1
                }
            }
        },
        propFix: {
            for: "htmlFor",
            class: "className"
        }
    }),
    pt.optSelected || (ht.propHooks.selected = {
        get: function(t) {
            var e = t.parentNode;
            return e && e.parentNode && e.parentNode.selectedIndex,
            null
        },
        set: function(t) {
            var e = t.parentNode;
            e && (e.selectedIndex,
            e.parentNode && e.parentNode.selectedIndex)
        }
    }),
    ht.each(["tabIndex", "readOnly", "maxLength", "cellSpacing", "cellPadding", "rowSpan", "colSpan", "useMap", "frameBorder", "contentEditable"], function() {
        ht.propFix[this.toLowerCase()] = this
    }),
    ht.fn.extend({
        addClass: function(t) {
            var e, n, i, o, r, s, a, l = 0;
            if (ht.isFunction(t))
                return this.each(function(e) {
                    ht(this).addClass(t.call(this, e, X(this)))
                });
            if ("string" == typeof t && t)
                for (e = t.match(Pt) || []; n = this[l++]; )
                    if (o = X(n),
                    i = 1 === n.nodeType && " " + V(o) + " ") {
                        for (s = 0; r = e[s++]; )
                            i.indexOf(" " + r + " ") < 0 && (i += r + " ");
                        a = V(i),
                        o !== a && n.setAttribute("class", a)
                    }
            return this
        },
        removeClass: function(t) {
            var e, n, i, o, r, s, a, l = 0;
            if (ht.isFunction(t))
                return this.each(function(e) {
                    ht(this).removeClass(t.call(this, e, X(this)))
                });
            if (!arguments.length)
                return this.attr("class", "");
            if ("string" == typeof t && t)
                for (e = t.match(Pt) || []; n = this[l++]; )
                    if (o = X(n),
                    i = 1 === n.nodeType && " " + V(o) + " ") {
                        for (s = 0; r = e[s++]; )
                            for (; i.indexOf(" " + r + " ") > -1; )
                                i = i.replace(" " + r + " ", " ");
                        a = V(i),
                        o !== a && n.setAttribute("class", a)
                    }
            return this
        },
        toggleClass: function(t, e) {
            var n = typeof t;
            return "boolean" == typeof e && "string" === n ? e ? this.addClass(t) : this.removeClass(t) : ht.isFunction(t) ? this.each(function(n) {
                ht(this).toggleClass(t.call(this, n, X(this), e), e)
            }) : this.each(function() {
                var e, i, o, r;
                if ("string" === n)
                    for (i = 0,
                    o = ht(this),
                    r = t.match(Pt) || []; e = r[i++]; )
                        o.hasClass(e) ? o.removeClass(e) : o.addClass(e);
                else
                    void 0 !== t && "boolean" !== n || (e = X(this),
                    e && Dt.set(this, "__className__", e),
                    this.setAttribute && this.setAttribute("class", e || !1 === t ? "" : Dt.get(this, "__className__") || ""))
            })
        },
        hasClass: function(t) {
            var e, n, i = 0;
            for (e = " " + t + " "; n = this[i++]; )
                if (1 === n.nodeType && (" " + V(X(n)) + " ").indexOf(e) > -1)
                    return !0;
            return !1
        }
    });
    var Ee = /\r/g;
    ht.fn.extend({
        val: function(t) {
            var e, n, i, o = this[0];
            {
                if (arguments.length)
                    return i = ht.isFunction(t),
                    this.each(function(n) {
                        var o;
                        1 === this.nodeType && (o = i ? t.call(this, n, ht(this).val()) : t,
                        null == o ? o = "" : "number" == typeof o ? o += "" : Array.isArray(o) && (o = ht.map(o, function(t) {
                            return null == t ? "" : t + ""
                        })),
                        (e = ht.valHooks[this.type] || ht.valHooks[this.nodeName.toLowerCase()]) && "set"in e && void 0 !== e.set(this, o, "value") || (this.value = o))
                    });
                if (o)
                    return (e = ht.valHooks[o.type] || ht.valHooks[o.nodeName.toLowerCase()]) && "get"in e && void 0 !== (n = e.get(o, "value")) ? n : (n = o.value,
                    "string" == typeof n ? n.replace(Ee, "") : null == n ? "" : n)
            }
        }
    }),
    ht.extend({
        valHooks: {
            option: {
                get: function(t) {
                    var e = ht.find.attr(t, "value");
                    return null != e ? e : V(ht.text(t))
                }
            },
            select: {
                get: function(t) {
                    var e, n, i, r = t.options, s = t.selectedIndex, a = "select-one" === t.type, l = a ? null : [], c = a ? s + 1 : r.length;
                    for (i = s < 0 ? c : a ? s : 0; i < c; i++)
                        if (n = r[i],
                        (n.selected || i === s) && !n.disabled && (!n.parentNode.disabled || !o(n.parentNode, "optgroup"))) {
                            if (e = ht(n).val(),
                            a)
                                return e;
                            l.push(e)
                        }
                    return l
                },
                set: function(t, e) {
                    for (var n, i, o = t.options, r = ht.makeArray(e), s = o.length; s--; )
                        i = o[s],
                        (i.selected = ht.inArray(ht.valHooks.option.get(i), r) > -1) && (n = !0);
                    return n || (t.selectedIndex = -1),
                    r
                }
            }
        }
    }),
    ht.each(["radio", "checkbox"], function() {
        ht.valHooks[this] = {
            set: function(t, e) {
                if (Array.isArray(e))
                    return t.checked = ht.inArray(ht(t).val(), e) > -1
            }
        },
        pt.checkOn || (ht.valHooks[this].get = function(t) {
            return null === t.getAttribute("value") ? "on" : t.value
        }
        )
    });
    var Ce = /^(?:focusinfocus|focusoutblur)$/;
    ht.extend(ht.event, {
        trigger: function(e, n, i, o) {
            var r, s, a, l, c, u, d, f = [i || nt], p = ut.call(e, "type") ? e.type : e, h = ut.call(e, "namespace") ? e.namespace.split(".") : [];
            if (s = a = i = i || nt,
            3 !== i.nodeType && 8 !== i.nodeType && !Ce.test(p + ht.event.triggered) && (p.indexOf(".") > -1 && (h = p.split("."),
            p = h.shift(),
            h.sort()),
            c = p.indexOf(":") < 0 && "on" + p,
            e = e[ht.expando] ? e : new ht.Event(p,"object" == typeof e && e),
            e.isTrigger = o ? 2 : 3,
            e.namespace = h.join("."),
            e.rnamespace = e.namespace ? new RegExp("(^|\\.)" + h.join("\\.(?:.*\\.|)") + "(\\.|$)") : null,
            e.result = void 0,
            e.target || (e.target = i),
            n = null == n ? [e] : ht.makeArray(n, [e]),
            d = ht.event.special[p] || {},
            o || !d.trigger || !1 !== d.trigger.apply(i, n))) {
                if (!o && !d.noBubble && !ht.isWindow(i)) {
                    for (l = d.delegateType || p,
                    Ce.test(l + p) || (s = s.parentNode); s; s = s.parentNode)
                        f.push(s),
                        a = s;
                    a === (i.ownerDocument || nt) && f.push(a.defaultView || a.parentWindow || t)
                }
                for (r = 0; (s = f[r++]) && !e.isPropagationStopped(); )
                    e.type = r > 1 ? l : d.bindType || p,
                    u = (Dt.get(s, "events") || {})[e.type] && Dt.get(s, "handle"),
                    u && u.apply(s, n),
                    (u = c && s[c]) && u.apply && jt(s) && (e.result = u.apply(s, n),
                    !1 === e.result && e.preventDefault());
                return e.type = p,
                o || e.isDefaultPrevented() || d._default && !1 !== d._default.apply(f.pop(), n) || !jt(i) || c && ht.isFunction(i[p]) && !ht.isWindow(i) && (a = i[c],
                a && (i[c] = null),
                ht.event.triggered = p,
                i[p](),
                ht.event.triggered = void 0,
                a && (i[c] = a)),
                e.result
            }
        },
        simulate: function(t, e, n) {
            var i = ht.extend(new ht.Event, n, {
                type: t,
                isSimulated: !0
            });
            ht.event.trigger(i, null, e)
        }
    }),
    ht.fn.extend({
        trigger: function(t, e) {
            return this.each(function() {
                ht.event.trigger(t, e, this)
            })
        },
        triggerHandler: function(t, e) {
            var n = this[0];
            if (n)
                return ht.event.trigger(t, e, n, !0)
        }
    }),
    ht.each("blur focus focusin focusout resize scroll click dblclick mousedown mouseup mousemove mouseover mouseout mouseenter mouseleave change select submit keydown keypress keyup contextmenu".split(" "), function(t, e) {
        ht.fn[e] = function(t, n) {
            return arguments.length > 0 ? this.on(e, null, t, n) : this.trigger(e)
        }
    }),
    ht.fn.extend({
        hover: function(t, e) {
            return this.mouseenter(t).mouseleave(e || t)
        }
    }),
    pt.focusin = "onfocusin"in t,
    pt.focusin || ht.each({
        focus: "focusin",
        blur: "focusout"
    }, function(t, e) {
        var n = function(t) {
            ht.event.simulate(e, t.target, ht.event.fix(t))
        };
        ht.event.special[e] = {
            setup: function() {
                var i = this.ownerDocument || this
                  , o = Dt.access(i, e);
                o || i.addEventListener(t, n, !0),
                Dt.access(i, e, (o || 0) + 1)
            },
            teardown: function() {
                var i = this.ownerDocument || this
                  , o = Dt.access(i, e) - 1;
                o ? Dt.access(i, e, o) : (i.removeEventListener(t, n, !0),
                Dt.remove(i, e))
            }
        }
    });
    var xe = t.location
      , Te = ht.now()
      , ke = /\?/;
    ht.parseXML = function(e) {
        var n;
        if (!e || "string" != typeof e)
            return null;
        try {
            n = (new t.DOMParser).parseFromString(e, "text/xml")
        } catch (t) {
            n = void 0
        }
        return n && !n.getElementsByTagName("parsererror").length || ht.error("Invalid XML: " + e),
        n
    }
    ;
    var Se = /\[\]$/
      , _e = /\r?\n/g
      , Ae = /^(?:submit|button|image|reset|file)$/i
      , Pe = /^(?:input|select|textarea|keygen)/i;
    ht.param = function(t, e) {
        var n, i = [], o = function(t, e) {
            var n = ht.isFunction(e) ? e() : e;
            i[i.length] = encodeURIComponent(t) + "=" + encodeURIComponent(null == n ? "" : n)
        };
        if (Array.isArray(t) || t.jquery && !ht.isPlainObject(t))
            ht.each(t, function() {
                o(this.name, this.value)
            });
        else
            for (n in t)
                J(n, t[n], e, o);
        return i.join("&")
    }
    ,
    ht.fn.extend({
        serialize: function() {
            return ht.param(this.serializeArray())
        },
        serializeArray: function() {
            return this.map(function() {
                var t = ht.prop(this, "elements");
                return t ? ht.makeArray(t) : this
            }).filter(function() {
                var t = this.type;
                return this.name && !ht(this).is(":disabled") && Pe.test(this.nodeName) && !Ae.test(t) && (this.checked || !Ut.test(t))
            }).map(function(t, e) {
                var n = ht(this).val();
                return null == n ? null : Array.isArray(n) ? ht.map(n, function(t) {
                    return {
                        name: e.name,
                        value: t.replace(_e, "\r\n")
                    }
                }) : {
                    name: e.name,
                    value: n.replace(_e, "\r\n")
                }
            }).get()
        }
    });
    var Le = /%20/g
      , Ie = /#.*$/
      , Oe = /([?&])_=[^&]*/
      , je = /^(.*?):[ \t]*([^\r\n]*)$/gm
      , De = /^(?:about|app|app-storage|.+-extension|file|res|widget):$/
      , Re = /^(?:GET|HEAD)$/
      , Me = /^\/\//
      , Ne = {}
      , $e = {}
      , Fe = "*/".concat("*")
      , We = nt.createElement("a");
    We.href = xe.href,
    ht.extend({
        active: 0,
        lastModified: {},
        etag: {},
        ajaxSettings: {
            url: xe.href,
            type: "GET",
            isLocal: De.test(xe.protocol),
            global: !0,
            processData: !0,
            async: !0,
            contentType: "application/x-www-form-urlencoded; charset=UTF-8",
            accepts: {
                "*": Fe,
                text: "text/plain",
                html: "text/html",
                xml: "application/xml, text/xml",
                json: "application/json, text/javascript"
            },
            contents: {
                xml: /\bxml\b/,
                html: /\bhtml/,
                json: /\bjson\b/
            },
            responseFields: {
                xml: "responseXML",
                text: "responseText",
                json: "responseJSON"
            },
            converters: {
                "* text": String,
                "text html": !0,
                "text json": JSON.parse,
                "text xml": ht.parseXML
            },
            flatOptions: {
                url: !0,
                context: !0
            }
        },
        ajaxSetup: function(t, e) {
            return e ? K(K(t, ht.ajaxSettings), e) : K(ht.ajaxSettings, t)
        },
        ajaxPrefilter: Q(Ne),
        ajaxTransport: Q($e),
        ajax: function(e, n) {
            function i(e, n, i, a) {
                var c, f, p, w, E, C = n;
                u || (u = !0,
                l && t.clearTimeout(l),
                o = void 0,
                s = a || "",
                x.readyState = e > 0 ? 4 : 0,
                c = e >= 200 && e < 300 || 304 === e,
                i && (w = Z(h, x, i)),
                w = tt(h, w, x, c),
                c ? (h.ifModified && (E = x.getResponseHeader("Last-Modified"),
                E && (ht.lastModified[r] = E),
                (E = x.getResponseHeader("etag")) && (ht.etag[r] = E)),
                204 === e || "HEAD" === h.type ? C = "nocontent" : 304 === e ? C = "notmodified" : (C = w.state,
                f = w.data,
                p = w.error,
                c = !p)) : (p = C,
                !e && C || (C = "error",
                e < 0 && (e = 0))),
                x.status = e,
                x.statusText = (n || C) + "",
                c ? m.resolveWith(g, [f, C, x]) : m.rejectWith(g, [x, C, p]),
                x.statusCode(b),
                b = void 0,
                d && v.trigger(c ? "ajaxSuccess" : "ajaxError", [x, h, c ? f : p]),
                y.fireWith(g, [x, C]),
                d && (v.trigger("ajaxComplete", [x, h]),
                --ht.active || ht.event.trigger("ajaxStop")))
            }
            "object" == typeof e && (n = e,
            e = void 0),
            n = n || {};
            var o, r, s, a, l, c, u, d, f, p, h = ht.ajaxSetup({}, n), g = h.context || h, v = h.context && (g.nodeType || g.jquery) ? ht(g) : ht.event, m = ht.Deferred(), y = ht.Callbacks("once memory"), b = h.statusCode || {}, w = {}, E = {}, C = "canceled", x = {
                readyState: 0,
                getResponseHeader: function(t) {
                    var e;
                    if (u) {
                        if (!a)
                            for (a = {}; e = je.exec(s); )
                                a[e[1].toLowerCase()] = e[2];
                        e = a[t.toLowerCase()]
                    }
                    return null == e ? null : e
                },
                getAllResponseHeaders: function() {
                    return u ? s : null
                },
                setRequestHeader: function(t, e) {
                    return null == u && (t = E[t.toLowerCase()] = E[t.toLowerCase()] || t,
                    w[t] = e),
                    this
                },
                overrideMimeType: function(t) {
                    return null == u && (h.mimeType = t),
                    this
                },
                statusCode: function(t) {
                    var e;
                    if (t)
                        if (u)
                            x.always(t[x.status]);
                        else
                            for (e in t)
                                b[e] = [b[e], t[e]];
                    return this
                },
                abort: function(t) {
                    var e = t || C;
                    return o && o.abort(e),
                    i(0, e),
                    this
                }
            };
            if (m.promise(x),
            h.url = ((e || h.url || xe.href) + "").replace(Me, xe.protocol + "//"),
            h.type = n.method || n.type || h.method || h.type,
            h.dataTypes = (h.dataType || "*").toLowerCase().match(Pt) || [""],
            null == h.crossDomain) {
                c = nt.createElement("a");
                try {
                    c.href = h.url,
                    c.href = c.href,
                    h.crossDomain = We.protocol + "//" + We.host != c.protocol + "//" + c.host
                } catch (t) {
                    h.crossDomain = !0
                }
            }
            if (h.data && h.processData && "string" != typeof h.data && (h.data = ht.param(h.data, h.traditional)),
            G(Ne, h, n, x),
            u)
                return x;
            d = ht.event && h.global,
            d && 0 == ht.active++ && ht.event.trigger("ajaxStart"),
            h.type = h.type.toUpperCase(),
            h.hasContent = !Re.test(h.type),
            r = h.url.replace(Ie, ""),
            h.hasContent ? h.data && h.processData && 0 === (h.contentType || "").indexOf("application/x-www-form-urlencoded") && (h.data = h.data.replace(Le, "+")) : (p = h.url.slice(r.length),
            h.data && (r += (ke.test(r) ? "&" : "?") + h.data,
            delete h.data),
            !1 === h.cache && (r = r.replace(Oe, "$1"),
            p = (ke.test(r) ? "&" : "?") + "_=" + Te++ + p),
            h.url = r + p),
            h.ifModified && (ht.lastModified[r] && x.setRequestHeader("If-Modified-Since", ht.lastModified[r]),
            ht.etag[r] && x.setRequestHeader("If-None-Match", ht.etag[r])),
            (h.data && h.hasContent && !1 !== h.contentType || n.contentType) && x.setRequestHeader("Content-Type", h.contentType),
            x.setRequestHeader("Accept", h.dataTypes[0] && h.accepts[h.dataTypes[0]] ? h.accepts[h.dataTypes[0]] + ("*" !== h.dataTypes[0] ? ", " + Fe + "; q=0.01" : "") : h.accepts["*"]);
            for (f in h.headers)
                x.setRequestHeader(f, h.headers[f]);
            if (h.beforeSend && (!1 === h.beforeSend.call(g, x, h) || u))
                return x.abort();
            if (C = "abort",
            y.add(h.complete),
            x.done(h.success),
            x.fail(h.error),
            o = G($e, h, n, x)) {
                if (x.readyState = 1,
                d && v.trigger("ajaxSend", [x, h]),
                u)
                    return x;
                h.async && h.timeout > 0 && (l = t.setTimeout(function() {
                    x.abort("timeout")
                }, h.timeout));
                try {
                    u = !1,
                    o.send(w, i)
                } catch (t) {
                    if (u)
                        throw t;
                    i(-1, t)
                }
            } else
                i(-1, "No Transport");
            return x
        },
        getJSON: function(t, e, n) {
            return ht.get(t, e, n, "json")
        },
        getScript: function(t, e) {
            return ht.get(t, void 0, e, "script")
        }
    }),
    ht.each(["get", "post"], function(t, e) {
        ht[e] = function(t, n, i, o) {
            return ht.isFunction(n) && (o = o || i,
            i = n,
            n = void 0),
            ht.ajax(ht.extend({
                url: t,
                type: e,
                dataType: o,
                data: n,
                success: i
            }, ht.isPlainObject(t) && t))
        }
    }),
    ht._evalUrl = function(t) {
        return ht.ajax({
            url: t,
            type: "GET",
            dataType: "script",
            cache: !0,
            async: !1,
            global: !1,
            throws: !0
        })
    }
    ,
    ht.fn.extend({
        wrapAll: function(t) {
            var e;
            return this[0] && (ht.isFunction(t) && (t = t.call(this[0])),
            e = ht(t, this[0].ownerDocument).eq(0).clone(!0),
            this[0].parentNode && e.insertBefore(this[0]),
            e.map(function() {
                for (var t = this; t.firstElementChild; )
                    t = t.firstElementChild;
                return t
            }).append(this)),
            this
        },
        wrapInner: function(t) {
            return ht.isFunction(t) ? this.each(function(e) {
                ht(this).wrapInner(t.call(this, e))
            }) : this.each(function() {
                var e = ht(this)
                  , n = e.contents();
                n.length ? n.wrapAll(t) : e.append(t)
            })
        },
        wrap: function(t) {
            var e = ht.isFunction(t);
            return this.each(function(n) {
                ht(this).wrapAll(e ? t.call(this, n) : t)
            })
        },
        unwrap: function(t) {
            return this.parent(t).not("body").each(function() {
                ht(this).replaceWith(this.childNodes)
            }),
            this
        }
    }),
    ht.expr.pseudos.hidden = function(t) {
        return !ht.expr.pseudos.visible(t)
    }
    ,
    ht.expr.pseudos.visible = function(t) {
        return !!(t.offsetWidth || t.offsetHeight || t.getClientRects().length)
    }
    ,
    ht.ajaxSettings.xhr = function() {
        try {
            return new t.XMLHttpRequest
        } catch (t) {}
    }
    ;
    var He = {
        0: 200,
        1223: 204
    }
      , Ye = ht.ajaxSettings.xhr();
    pt.cors = !!Ye && "withCredentials"in Ye,
    pt.ajax = Ye = !!Ye,
    ht.ajaxTransport(function(e) {
        var n, i;
        if (pt.cors || Ye && !e.crossDomain)
            return {
                send: function(o, r) {
                    var s, a = e.xhr();
                    if (a.open(e.type, e.url, e.async, e.username, e.password),
                    e.xhrFields)
                        for (s in e.xhrFields)
                            a[s] = e.xhrFields[s];
                    e.mimeType && a.overrideMimeType && a.overrideMimeType(e.mimeType),
                    e.crossDomain || o["X-Requested-With"] || (o["X-Requested-With"] = "XMLHttpRequest");
                    for (s in o)
                        a.setRequestHeader(s, o[s]);
                    n = function(t) {
                        return function() {
                            n && (n = i = a.onload = a.onerror = a.onabort = a.onreadystatechange = null,
                            "abort" === t ? a.abort() : "error" === t ? "number" != typeof a.status ? r(0, "error") : r(a.status, a.statusText) : r(He[a.status] || a.status, a.statusText, "text" !== (a.responseType || "text") || "string" != typeof a.responseText ? {
                                binary: a.response
                            } : {
                                text: a.responseText
                            }, a.getAllResponseHeaders()))
                        }
                    }
                    ,
                    a.onload = n(),
                    i = a.onerror = n("error"),
                    void 0 !== a.onabort ? a.onabort = i : a.onreadystatechange = function() {
                        4 === a.readyState && t.setTimeout(function() {
                            n && i()
                        })
                    }
                    ,
                    n = n("abort");
                    try {
                        a.send(e.hasContent && e.data || null)
                    } catch (t) {
                        if (n)
                            throw t
                    }
                },
                abort: function() {
                    n && n()
                }
            }
    }),
    ht.ajaxPrefilter(function(t) {
        t.crossDomain && (t.contents.script = !1)
    }),
    ht.ajaxSetup({
        accepts: {
            script: "text/javascript, application/javascript, application/ecmascript, application/x-ecmascript"
        },
        contents: {
            script: /\b(?:java|ecma)script\b/
        },
        converters: {
            "text script": function(t) {
                return ht.globalEval(t),
                t
            }
        }
    }),
    ht.ajaxPrefilter("script", function(t) {
        void 0 === t.cache && (t.cache = !1),
        t.crossDomain && (t.type = "GET")
    }),
    ht.ajaxTransport("script", function(t) {
        if (t.crossDomain) {
            var e, n;
            return {
                send: function(i, o) {
                    e = ht("<script>").prop({
                        charset: t.scriptCharset,
                        src: t.url
                    }).on("load error", n = function(t) {
                        e.remove(),
                        n = null,
                        t && o("error" === t.type ? 404 : 200, t.type)
                    }
                    ),
                    nt.head.appendChild(e[0])
                },
                abort: function() {
                    n && n()
                }
            }
        }
    });
    var Be = []
      , Ue = /(=)\?(?=&|$)|\?\?/;
    ht.ajaxSetup({
        jsonp: "callback",
        jsonpCallback: function() {
            var t = Be.pop() || ht.expando + "_" + Te++;
            return this[t] = !0,
            t
        }
    }),
    ht.ajaxPrefilter("json jsonp", function(e, n, i) {
        var o, r, s, a = !1 !== e.jsonp && (Ue.test(e.url) ? "url" : "string" == typeof e.data && 0 === (e.contentType || "").indexOf("application/x-www-form-urlencoded") && Ue.test(e.data) && "data");
        if (a || "jsonp" === e.dataTypes[0])
            return o = e.jsonpCallback = ht.isFunction(e.jsonpCallback) ? e.jsonpCallback() : e.jsonpCallback,
            a ? e[a] = e[a].replace(Ue, "$1" + o) : !1 !== e.jsonp && (e.url += (ke.test(e.url) ? "&" : "?") + e.jsonp + "=" + o),
            e.converters["script json"] = function() {
                return s || ht.error(o + " was not called"),
                s[0]
            }
            ,
            e.dataTypes[0] = "json",
            r = t[o],
            t[o] = function() {
                s = arguments
            }
            ,
            i.always(function() {
                void 0 === r ? ht(t).removeProp(o) : t[o] = r,
                e[o] && (e.jsonpCallback = n.jsonpCallback,
                Be.push(o)),
                s && ht.isFunction(r) && r(s[0]),
                s = r = void 0
            }),
            "script"
    }),
    pt.createHTMLDocument = function() {
        var t = nt.implementation.createHTMLDocument("").body;
        return t.innerHTML = "<form></form><form></form>",
        2 === t.childNodes.length
    }(),
    ht.parseHTML = function(t, e, n) {
        if ("string" != typeof t)
            return [];
        "boolean" == typeof e && (n = e,
        e = !1);
        var i, o, r;
        return e || (pt.createHTMLDocument ? (e = nt.implementation.createHTMLDocument(""),
        i = e.createElement("base"),
        i.href = nt.location.href,
        e.head.appendChild(i)) : e = nt),
        o = xt.exec(t),
        r = !n && [],
        o ? [e.createElement(o[1])] : (o = w([t], e, r),
        r && r.length && ht(r).remove(),
        ht.merge([], o.childNodes))
    }
    ,
    ht.fn.load = function(t, e, n) {
        var i, o, r, s = this, a = t.indexOf(" ");
        return a > -1 && (i = V(t.slice(a)),
        t = t.slice(0, a)),
        ht.isFunction(e) ? (n = e,
        e = void 0) : e && "object" == typeof e && (o = "POST"),
        s.length > 0 && ht.ajax({
            url: t,
            type: o || "GET",
            dataType: "html",
            data: e
        }).done(function(t) {
            r = arguments,
            s.html(i ? ht("<div>").append(ht.parseHTML(t)).find(i) : t)
        }).always(n && function(t, e) {
            s.each(function() {
                n.apply(this, r || [t.responseText, e, t])
            })
        }
        ),
        this
    }
    ,
    ht.each(["ajaxStart", "ajaxStop", "ajaxComplete", "ajaxError", "ajaxSuccess", "ajaxSend"], function(t, e) {
        ht.fn[e] = function(t) {
            return this.on(e, t)
        }
    }),
    ht.expr.pseudos.animated = function(t) {
        return ht.grep(ht.timers, function(e) {
            return t === e.elem
        }).length
    }
    ,
    ht.offset = {
        setOffset: function(t, e, n) {
            var i, o, r, s, a, l, c, u = ht.css(t, "position"), d = ht(t), f = {};
            "static" === u && (t.style.position = "relative"),
            a = d.offset(),
            r = ht.css(t, "top"),
            l = ht.css(t, "left"),
            c = ("absolute" === u || "fixed" === u) && (r + l).indexOf("auto") > -1,
            c ? (i = d.position(),
            s = i.top,
            o = i.left) : (s = parseFloat(r) || 0,
            o = parseFloat(l) || 0),
            ht.isFunction(e) && (e = e.call(t, n, ht.extend({}, a))),
            null != e.top && (f.top = e.top - a.top + s),
            null != e.left && (f.left = e.left - a.left + o),
            "using"in e ? e.using.call(t, f) : d.css(f)
        }
    },
    ht.fn.extend({
        offset: function(t) {
            if (arguments.length)
                return void 0 === t ? this : this.each(function(e) {
                    ht.offset.setOffset(this, t, e)
                });
            var e, n, i, o, r = this[0];
            if (r)
                return r.getClientRects().length ? (i = r.getBoundingClientRect(),
                e = r.ownerDocument,
                n = e.documentElement,
                o = e.defaultView,
                {
                    top: i.top + o.pageYOffset - n.clientTop,
                    left: i.left + o.pageXOffset - n.clientLeft
                }) : {
                    top: 0,
                    left: 0
                }
        },
        position: function() {
            if (this[0]) {
                var t, e, n = this[0], i = {
                    top: 0,
                    left: 0
                };
                return "fixed" === ht.css(n, "position") ? e = n.getBoundingClientRect() : (t = this.offsetParent(),
                e = this.offset(),
                o(t[0], "html") || (i = t.offset()),
                i = {
                    top: i.top + ht.css(t[0], "borderTopWidth", !0),
                    left: i.left + ht.css(t[0], "borderLeftWidth", !0)
                }),
                {
                    top: e.top - i.top - ht.css(n, "marginTop", !0),
                    left: e.left - i.left - ht.css(n, "marginLeft", !0)
                }
            }
        },
        offsetParent: function() {
            return this.map(function() {
                for (var t = this.offsetParent; t && "static" === ht.css(t, "position"); )
                    t = t.offsetParent;
                return t || Jt
            })
        }
    }),
    ht.each({
        scrollLeft: "pageXOffset",
        scrollTop: "pageYOffset"
    }, function(t, e) {
        var n = "pageYOffset" === e;
        ht.fn[t] = function(i) {
            return Ot(this, function(t, i, o) {
                var r;
                if (ht.isWindow(t) ? r = t : 9 === t.nodeType && (r = t.defaultView),
                void 0 === o)
                    return r ? r[e] : t[i];
                r ? r.scrollTo(n ? r.pageXOffset : o, n ? o : r.pageYOffset) : t[i] = o
            }, t, i, arguments.length)
        }
    }),
    ht.each(["top", "left"], function(t, e) {
        ht.cssHooks[e] = j(pt.pixelPosition, function(t, n) {
            if (n)
                return n = O(t, e),
                re.test(n) ? ht(t).position()[e] + "px" : n
        })
    }),
    ht.each({
        Height: "height",
        Width: "width"
    }, function(t, e) {
        ht.each({
            padding: "inner" + t,
            content: e,
            "": "outer" + t
        }, function(n, i) {
            ht.fn[i] = function(o, r) {
                var s = arguments.length && (n || "boolean" != typeof o)
                  , a = n || (!0 === o || !0 === r ? "margin" : "border");
                return Ot(this, function(e, n, o) {
                    var r;
                    return ht.isWindow(e) ? 0 === i.indexOf("outer") ? e["inner" + t] : e.document.documentElement["client" + t] : 9 === e.nodeType ? (r = e.documentElement,
                    Math.max(e.body["scroll" + t], r["scroll" + t], e.body["offset" + t], r["offset" + t], r["client" + t])) : void 0 === o ? ht.css(e, n, a) : ht.style(e, n, o, a)
                }, e, s ? o : void 0, s)
            }
        })
    }),
    ht.fn.extend({
        bind: function(t, e, n) {
            return this.on(t, null, e, n)
        },
        unbind: function(t, e) {
            return this.off(t, null, e)
        },
        delegate: function(t, e, n, i) {
            return this.on(e, t, n, i)
        },
        undelegate: function(t, e, n) {
            return 1 === arguments.length ? this.off(t, "**") : this.off(e, t || "**", n)
        }
    }),
    ht.holdReady = function(t) {
        t ? ht.readyWait++ : ht.ready(!0)
    }
    ,
    ht.isArray = Array.isArray,
    ht.parseJSON = JSON.parse,
    ht.nodeName = o,
    "function" == typeof define && define.amd && define("jquery", [], function() {
        return ht
    });
    var qe = t.jQuery
      , ze = t.$;
    return ht.noConflict = function(e) {
        return t.$ === ht && (t.$ = ze),
        e && t.jQuery === ht && (t.jQuery = qe),
        ht
    }
    ,
    e || (t.jQuery = t.$ = ht),
    ht
}),
function(t, e) {
    "function" == typeof define && define.amd ? define(e) : "object" == typeof exports ? module.exports = e(require, exports, module) : t.Tether = e()
}(this, function(t, e, n) {
    "use strict";
    function i(t, e) {
        if (!(t instanceof e))
            throw new TypeError("Cannot call a class as a function")
    }
    function o(t) {
        var e = t.getBoundingClientRect()
          , n = {};
        for (var i in e)
            n[i] = e[i];
        if (t.ownerDocument !== document) {
            var r = t.ownerDocument.defaultView.frameElement;
            if (r) {
                var s = o(r);
                n.top += s.top,
                n.bottom += s.top,
                n.left += s.left,
                n.right += s.left
            }
        }
        return n
    }
    function r(t) {
        var e = getComputedStyle(t) || {}
          , n = e.position
          , i = [];
        if ("fixed" === n)
            return [t];
        for (var o = t; (o = o.parentNode) && o && 1 === o.nodeType; ) {
            var r = void 0;
            try {
                r = getComputedStyle(o)
            } catch (t) {}
            if (void 0 === r || null === r)
                return i.push(o),
                i;
            var s = r
              , a = s.overflow
              , l = s.overflowX;
            /(auto|scroll)/.test(a + s.overflowY + l) && ("absolute" !== n || ["relative", "absolute", "fixed"].indexOf(r.position) >= 0) && i.push(o)
        }
        return i.push(t.ownerDocument.body),
        t.ownerDocument !== document && i.push(t.ownerDocument.defaultView),
        i
    }
    function s() {
        k && document.body.removeChild(k),
        k = null
    }
    function a(t) {
        var e = void 0;
        t === document ? (e = document,
        t = document.documentElement) : e = t.ownerDocument;
        var n = e.documentElement
          , i = o(t)
          , r = A();
        return i.top -= r.top,
        i.left -= r.left,
        void 0 === i.width && (i.width = document.body.scrollWidth - i.left - i.right),
        void 0 === i.height && (i.height = document.body.scrollHeight - i.top - i.bottom),
        i.top = i.top - n.clientTop,
        i.left = i.left - n.clientLeft,
        i.right = e.body.clientWidth - i.width - i.left,
        i.bottom = e.body.clientHeight - i.height - i.top,
        i
    }
    function l(t) {
        return t.offsetParent || document.documentElement
    }
    function c() {
        if (P)
            return P;
        var t = document.createElement("div");
        t.style.width = "100%",
        t.style.height = "200px";
        var e = document.createElement("div");
        u(e.style, {
            position: "absolute",
            top: 0,
            left: 0,
            pointerEvents: "none",
            visibility: "hidden",
            width: "200px",
            height: "150px",
            overflow: "hidden"
        }),
        e.appendChild(t),
        document.body.appendChild(e);
        var n = t.offsetWidth;
        e.style.overflow = "scroll";
        var i = t.offsetWidth;
        n === i && (i = e.clientWidth),
        document.body.removeChild(e);
        var o = n - i;
        return P = {
            width: o,
            height: o
        }
    }
    function u() {
        var t = arguments.length <= 0 || void 0 === arguments[0] ? {} : arguments[0]
          , e = [];
        return Array.prototype.push.apply(e, arguments),
        e.slice(1).forEach(function(e) {
            if (e)
                for (var n in e)
                    ({}).hasOwnProperty.call(e, n) && (t[n] = e[n])
        }),
        t
    }
    function d(t, e) {
        if (void 0 !== t.classList)
            e.split(" ").forEach(function(e) {
                e.trim() && t.classList.remove(e)
            });
        else {
            var n = new RegExp("(^| )" + e.split(" ").join("|") + "( |$)","gi")
              , i = h(t).replace(n, " ");
            g(t, i)
        }
    }
    function f(t, e) {
        if (void 0 !== t.classList)
            e.split(" ").forEach(function(e) {
                e.trim() && t.classList.add(e)
            });
        else {
            d(t, e);
            var n = h(t) + " " + e;
            g(t, n)
        }
    }
    function p(t, e) {
        if (void 0 !== t.classList)
            return t.classList.contains(e);
        var n = h(t);
        return new RegExp("(^| )" + e + "( |$)","gi").test(n)
    }
    function h(t) {
        return t.className instanceof t.ownerDocument.defaultView.SVGAnimatedString ? t.className.baseVal : t.className
    }
    function g(t, e) {
        t.setAttribute("class", e)
    }
    function v(t, e, n) {
        n.forEach(function(n) {
            -1 === e.indexOf(n) && p(t, n) && d(t, n)
        }),
        e.forEach(function(e) {
            p(t, e) || f(t, e)
        })
    }
    function i(t, e) {
        if (!(t instanceof e))
            throw new TypeError("Cannot call a class as a function")
    }
    function m(t, e) {
        if ("function" != typeof e && null !== e)
            throw new TypeError("Super expression must either be null or a function, not " + typeof e);
        t.prototype = Object.create(e && e.prototype, {
            constructor: {
                value: t,
                enumerable: !1,
                writable: !0,
                configurable: !0
            }
        }),
        e && (Object.setPrototypeOf ? Object.setPrototypeOf(t, e) : t.__proto__ = e)
    }
    function y(t, e) {
        var n = arguments.length <= 2 || void 0 === arguments[2] ? 1 : arguments[2];
        return t + n >= e && e >= t - n
    }
    function b() {
        return "undefined" != typeof performance && void 0 !== performance.now ? performance.now() : +new Date
    }
    function w() {
        for (var t = {
            top: 0,
            left: 0
        }, e = arguments.length, n = Array(e), i = 0; i < e; i++)
            n[i] = arguments[i];
        return n.forEach(function(e) {
            var n = e.top
              , i = e.left;
            "string" == typeof n && (n = parseFloat(n, 10)),
            "string" == typeof i && (i = parseFloat(i, 10)),
            t.top += n,
            t.left += i
        }),
        t
    }
    function E(t, e) {
        return "string" == typeof t.left && -1 !== t.left.indexOf("%") && (t.left = parseFloat(t.left, 10) / 100 * e.width),
        "string" == typeof t.top && -1 !== t.top.indexOf("%") && (t.top = parseFloat(t.top, 10) / 100 * e.height),
        t
    }
    function C(t, e) {
        return "scrollParent" === e ? e = t.scrollParents[0] : "window" === e && (e = [pageXOffset, pageYOffset, innerWidth + pageXOffset, innerHeight + pageYOffset]),
        e === document && (e = e.documentElement),
        void 0 !== e.nodeType && function() {
            var t = e
              , n = a(e)
              , i = n
              , o = getComputedStyle(e);
            if (e = [i.left, i.top, n.width + i.left, n.height + i.top],
            t.ownerDocument !== document) {
                var r = t.ownerDocument.defaultView;
                e[0] += r.pageXOffset,
                e[1] += r.pageYOffset,
                e[2] += r.pageXOffset,
                e[3] += r.pageYOffset
            }
            J.forEach(function(t, n) {
                t = t[0].toUpperCase() + t.substr(1),
                "Top" === t || "Left" === t ? e[n] += parseFloat(o["border" + t + "Width"]) : e[n] -= parseFloat(o["border" + t + "Width"])
            })
        }(),
        e
    }
    var x = function() {
        function t(t, e) {
            for (var n = 0; n < e.length; n++) {
                var i = e[n];
                i.enumerable = i.enumerable || !1,
                i.configurable = !0,
                "value"in i && (i.writable = !0),
                Object.defineProperty(t, i.key, i)
            }
        }
        return function(e, n, i) {
            return n && t(e.prototype, n),
            i && t(e, i),
            e
        }
    }()
      , T = void 0;
    void 0 === T && (T = {
        modules: []
    });
    var k = null
      , S = function() {
        var t = 0;
        return function() {
            return ++t
        }
    }()
      , _ = {}
      , A = function() {
        var t = k;
        t && document.body.contains(t) || (t = document.createElement("div"),
        t.setAttribute("data-tether-id", S()),
        u(t.style, {
            top: 0,
            left: 0,
            position: "absolute"
        }),
        document.body.appendChild(t),
        k = t);
        var e = t.getAttribute("data-tether-id");
        return void 0 === _[e] && (_[e] = o(t),
        I(function() {
            delete _[e]
        })),
        _[e]
    }
      , P = null
      , L = []
      , I = function(t) {
        L.push(t)
    }
      , O = function() {
        for (var t = void 0; t = L.pop(); )
            t()
    }
      , j = function() {
        function t() {
            i(this, t)
        }
        return x(t, [{
            key: "on",
            value: function(t, e, n) {
                var i = !(arguments.length <= 3 || void 0 === arguments[3]) && arguments[3];
                void 0 === this.bindings && (this.bindings = {}),
                void 0 === this.bindings[t] && (this.bindings[t] = []),
                this.bindings[t].push({
                    handler: e,
                    ctx: n,
                    once: i
                })
            }
        }, {
            key: "once",
            value: function(t, e, n) {
                this.on(t, e, n, !0)
            }
        }, {
            key: "off",
            value: function(t, e) {
                if (void 0 !== this.bindings && void 0 !== this.bindings[t])
                    if (void 0 === e)
                        delete this.bindings[t];
                    else
                        for (var n = 0; n < this.bindings[t].length; )
                            this.bindings[t][n].handler === e ? this.bindings[t].splice(n, 1) : ++n
            }
        }, {
            key: "trigger",
            value: function(t) {
                if (void 0 !== this.bindings && this.bindings[t]) {
                    for (var e = 0, n = arguments.length, i = Array(n > 1 ? n - 1 : 0), o = 1; o < n; o++)
                        i[o - 1] = arguments[o];
                    for (; e < this.bindings[t].length; ) {
                        var r = this.bindings[t][e]
                          , s = r.handler
                          , a = r.ctx
                          , l = r.once
                          , c = a;
                        void 0 === c && (c = this),
                        s.apply(c, i),
                        l ? this.bindings[t].splice(e, 1) : ++e
                    }
                }
            }
        }]),
        t
    }();
    T.Utils = {
        getActualBoundingClientRect: o,
        getScrollParents: r,
        getBounds: a,
        getOffsetParent: l,
        extend: u,
        addClass: f,
        removeClass: d,
        hasClass: p,
        updateClasses: v,
        defer: I,
        flush: O,
        uniqueId: S,
        Evented: j,
        getScrollBarSize: c,
        removeUtilElements: s
    };
    var D = function() {
        function t(t, e) {
            var n = []
              , i = !0
              , o = !1
              , r = void 0;
            try {
                for (var s, a = t[Symbol.iterator](); !(i = (s = a.next()).done) && (n.push(s.value),
                !e || n.length !== e); i = !0)
                    ;
            } catch (t) {
                o = !0,
                r = t
            } finally {
                try {
                    !i && a.return && a.return()
                } finally {
                    if (o)
                        throw r
                }
            }
            return n
        }
        return function(e, n) {
            if (Array.isArray(e))
                return e;
            if (Symbol.iterator in Object(e))
                return t(e, n);
            throw new TypeError("Invalid attempt to destructure non-iterable instance")
        }
    }()
      , x = function() {
        function t(t, e) {
            for (var n = 0; n < e.length; n++) {
                var i = e[n];
                i.enumerable = i.enumerable || !1,
                i.configurable = !0,
                "value"in i && (i.writable = !0),
                Object.defineProperty(t, i.key, i)
            }
        }
        return function(e, n, i) {
            return n && t(e.prototype, n),
            i && t(e, i),
            e
        }
    }()
      , R = function(t, e, n) {
        for (var i = !0; i; ) {
            var o = t
              , r = e
              , s = n;
            i = !1,
            null === o && (o = Function.prototype);
            var a = Object.getOwnPropertyDescriptor(o, r);
            if (void 0 !== a) {
                if ("value"in a)
                    return a.value;
                var l = a.get;
                if (void 0 === l)
                    return;
                return l.call(s)
            }
            var c = Object.getPrototypeOf(o);
            if (null === c)
                return;
            t = c,
            e = r,
            n = s,
            i = !0,
            a = c = void 0
        }
    };
    if (void 0 === T)
        throw new Error("You must include the utils.js file before tether.js");
    var M = T.Utils
      , r = M.getScrollParents
      , a = M.getBounds
      , l = M.getOffsetParent
      , u = M.extend
      , f = M.addClass
      , d = M.removeClass
      , v = M.updateClasses
      , I = M.defer
      , O = M.flush
      , c = M.getScrollBarSize
      , s = M.removeUtilElements
      , N = function() {
        if ("undefined" == typeof document)
            return "";
        for (var t = document.createElement("div"), e = ["transform", "WebkitTransform", "OTransform", "MozTransform", "msTransform"], n = 0; n < e.length; ++n) {
            var i = e[n];
            if (void 0 !== t.style[i])
                return i
        }
    }()
      , $ = []
      , F = function() {
        $.forEach(function(t) {
            t.position(!1)
        }),
        O()
    };
    !function() {
        var t = null
          , e = null
          , n = null
          , i = function i() {
            if (void 0 !== e && e > 16)
                return e = Math.min(e - 16, 250),
                void (n = setTimeout(i, 250));
            void 0 !== t && b() - t < 10 || (null != n && (clearTimeout(n),
            n = null),
            t = b(),
            F(),
            e = b() - t)
        };
        "undefined" != typeof window && void 0 !== window.addEventListener && ["resize", "scroll", "touchmove"].forEach(function(t) {
            window.addEventListener(t, i)
        })
    }();
    var W = {
        center: "center",
        left: "right",
        right: "left"
    }
      , H = {
        middle: "middle",
        top: "bottom",
        bottom: "top"
    }
      , Y = {
        top: 0,
        left: 0,
        middle: "50%",
        center: "50%",
        bottom: "100%",
        right: "100%"
    }
      , B = function(t, e) {
        var n = t.left
          , i = t.top;
        return "auto" === n && (n = W[e.left]),
        "auto" === i && (i = H[e.top]),
        {
            left: n,
            top: i
        }
    }
      , U = function(t) {
        var e = t.left
          , n = t.top;
        return void 0 !== Y[t.left] && (e = Y[t.left]),
        void 0 !== Y[t.top] && (n = Y[t.top]),
        {
            left: e,
            top: n
        }
    }
      , q = function(t) {
        var e = t.split(" ")
          , n = D(e, 2);
        return {
            top: n[0],
            left: n[1]
        }
    }
      , z = q
      , V = function(t) {
        function e(t) {
            var n = this;
            i(this, e),
            R(Object.getPrototypeOf(e.prototype), "constructor", this).call(this),
            this.position = this.position.bind(this),
            $.push(this),
            this.history = [],
            this.setOptions(t, !1),
            T.modules.forEach(function(t) {
                void 0 !== t.initialize && t.initialize.call(n)
            }),
            this.position()
        }
        return m(e, t),
        x(e, [{
            key: "getClass",
            value: function() {
                var t = arguments.length <= 0 || void 0 === arguments[0] ? "" : arguments[0]
                  , e = this.options.classes;
                return void 0 !== e && e[t] ? this.options.classes[t] : this.options.classPrefix ? this.options.classPrefix + "-" + t : t
            }
        }, {
            key: "setOptions",
            value: function(t) {
                var e = this
                  , n = arguments.length <= 1 || void 0 === arguments[1] || arguments[1]
                  , i = {
                    offset: "0 0",
                    targetOffset: "0 0",
                    targetAttachment: "auto auto",
                    classPrefix: "tether"
                };
                this.options = u(i, t);
                var o = this.options
                  , s = o.element
                  , a = o.target
                  , l = o.targetModifier;
                if (this.element = s,
                this.target = a,
                this.targetModifier = l,
                "viewport" === this.target ? (this.target = document.body,
                this.targetModifier = "visible") : "scroll-handle" === this.target && (this.target = document.body,
                this.targetModifier = "scroll-handle"),
                ["element", "target"].forEach(function(t) {
                    if (void 0 === e[t])
                        throw new Error("Tether Error: Both element and target must be defined");
                    void 0 !== e[t].jquery ? e[t] = e[t][0] : "string" == typeof e[t] && (e[t] = document.querySelector(e[t]))
                }),
                f(this.element, this.getClass("element")),
                !1 !== this.options.addTargetClasses && f(this.target, this.getClass("target")),
                !this.options.attachment)
                    throw new Error("Tether Error: You must provide an attachment");
                this.targetAttachment = z(this.options.targetAttachment),
                this.attachment = z(this.options.attachment),
                this.offset = q(this.options.offset),
                this.targetOffset = q(this.options.targetOffset),
                void 0 !== this.scrollParents && this.disable(),
                "scroll-handle" === this.targetModifier ? this.scrollParents = [this.target] : this.scrollParents = r(this.target),
                !1 !== this.options.enabled && this.enable(n)
            }
        }, {
            key: "getTargetBounds",
            value: function() {
                if (void 0 === this.targetModifier)
                    return a(this.target);
                if ("visible" === this.targetModifier) {
                    if (this.target === document.body)
                        return {
                            top: pageYOffset,
                            left: pageXOffset,
                            height: innerHeight,
                            width: innerWidth
                        };
                    var t = a(this.target)
                      , e = {
                        height: t.height,
                        width: t.width,
                        top: t.top,
                        left: t.left
                    };
                    return e.height = Math.min(e.height, t.height - (pageYOffset - t.top)),
                    e.height = Math.min(e.height, t.height - (t.top + t.height - (pageYOffset + innerHeight))),
                    e.height = Math.min(innerHeight, e.height),
                    e.height -= 2,
                    e.width = Math.min(e.width, t.width - (pageXOffset - t.left)),
                    e.width = Math.min(e.width, t.width - (t.left + t.width - (pageXOffset + innerWidth))),
                    e.width = Math.min(innerWidth, e.width),
                    e.width -= 2,
                    e.top < pageYOffset && (e.top = pageYOffset),
                    e.left < pageXOffset && (e.left = pageXOffset),
                    e
                }
                if ("scroll-handle" === this.targetModifier) {
                    var t = void 0
                      , n = this.target;
                    n === document.body ? (n = document.documentElement,
                    t = {
                        left: pageXOffset,
                        top: pageYOffset,
                        height: innerHeight,
                        width: innerWidth
                    }) : t = a(n);
                    var i = getComputedStyle(n)
                      , o = n.scrollWidth > n.clientWidth || [i.overflow, i.overflowX].indexOf("scroll") >= 0 || this.target !== document.body
                      , r = 0;
                    o && (r = 15);
                    var s = t.height - parseFloat(i.borderTopWidth) - parseFloat(i.borderBottomWidth) - r
                      , e = {
                        width: 15,
                        height: .975 * s * (s / n.scrollHeight),
                        left: t.left + t.width - parseFloat(i.borderLeftWidth) - 15
                    }
                      , l = 0;
                    s < 408 && this.target === document.body && (l = -11e-5 * Math.pow(s, 2) - .00727 * s + 22.58),
                    this.target !== document.body && (e.height = Math.max(e.height, 24));
                    var c = this.target.scrollTop / (n.scrollHeight - s);
                    return e.top = c * (s - e.height - l) + t.top + parseFloat(i.borderTopWidth),
                    this.target === document.body && (e.height = Math.max(e.height, 24)),
                    e
                }
            }
        }, {
            key: "clearCache",
            value: function() {
                this._cache = {}
            }
        }, {
            key: "cache",
            value: function(t, e) {
                return void 0 === this._cache && (this._cache = {}),
                void 0 === this._cache[t] && (this._cache[t] = e.call(this)),
                this._cache[t]
            }
        }, {
            key: "enable",
            value: function() {
                var t = this
                  , e = arguments.length <= 0 || void 0 === arguments[0] || arguments[0];
                !1 !== this.options.addTargetClasses && f(this.target, this.getClass("enabled")),
                f(this.element, this.getClass("enabled")),
                this.enabled = !0,
                this.scrollParents.forEach(function(e) {
                    e !== t.target.ownerDocument && e.addEventListener("scroll", t.position)
                }),
                e && this.position()
            }
        }, {
            key: "disable",
            value: function() {
                var t = this;
                d(this.target, this.getClass("enabled")),
                d(this.element, this.getClass("enabled")),
                this.enabled = !1,
                void 0 !== this.scrollParents && this.scrollParents.forEach(function(e) {
                    e.removeEventListener("scroll", t.position)
                })
            }
        }, {
            key: "destroy",
            value: function() {
                var t = this;
                this.disable(),
                $.forEach(function(e, n) {
                    e === t && $.splice(n, 1)
                }),
                0 === $.length && s()
            }
        }, {
            key: "updateAttachClasses",
            value: function(t, e) {
                var n = this;
                t = t || this.attachment,
                e = e || this.targetAttachment;
                var i = ["left", "top", "bottom", "right", "middle", "center"];
                void 0 !== this._addAttachClasses && this._addAttachClasses.length && this._addAttachClasses.splice(0, this._addAttachClasses.length),
                void 0 === this._addAttachClasses && (this._addAttachClasses = []);
                var o = this._addAttachClasses;
                t.top && o.push(this.getClass("element-attached") + "-" + t.top),
                t.left && o.push(this.getClass("element-attached") + "-" + t.left),
                e.top && o.push(this.getClass("target-attached") + "-" + e.top),
                e.left && o.push(this.getClass("target-attached") + "-" + e.left);
                var r = [];
                i.forEach(function(t) {
                    r.push(n.getClass("element-attached") + "-" + t),
                    r.push(n.getClass("target-attached") + "-" + t)
                }),
                I(function() {
                    void 0 !== n._addAttachClasses && (v(n.element, n._addAttachClasses, r),
                    !1 !== n.options.addTargetClasses && v(n.target, n._addAttachClasses, r),
                    delete n._addAttachClasses)
                })
            }
        }, {
            key: "position",
            value: function() {
                var t = this
                  , e = arguments.length <= 0 || void 0 === arguments[0] || arguments[0];
                if (this.enabled) {
                    this.clearCache();
                    var n = B(this.targetAttachment, this.attachment);
                    this.updateAttachClasses(this.attachment, n);
                    var i = this.cache("element-bounds", function() {
                        return a(t.element)
                    })
                      , o = i.width
                      , r = i.height;
                    if (0 === o && 0 === r && void 0 !== this.lastSize) {
                        var s = this.lastSize;
                        o = s.width,
                        r = s.height
                    } else
                        this.lastSize = {
                            width: o,
                            height: r
                        };
                    var u = this.cache("target-bounds", function() {
                        return t.getTargetBounds()
                    })
                      , d = u
                      , f = E(U(this.attachment), {
                        width: o,
                        height: r
                    })
                      , p = E(U(n), d)
                      , h = E(this.offset, {
                        width: o,
                        height: r
                    })
                      , g = E(this.targetOffset, d);
                    f = w(f, h),
                    p = w(p, g);
                    for (var v = u.left + p.left - f.left, m = u.top + p.top - f.top, y = 0; y < T.modules.length; ++y) {
                        var b = T.modules[y]
                          , C = b.position.call(this, {
                            left: v,
                            top: m,
                            targetAttachment: n,
                            targetPos: u,
                            elementPos: i,
                            offset: f,
                            targetOffset: p,
                            manualOffset: h,
                            manualTargetOffset: g,
                            scrollbarSize: _,
                            attachment: this.attachment
                        });
                        if (!1 === C)
                            return !1;
                        void 0 !== C && "object" == typeof C && (m = C.top,
                        v = C.left)
                    }
                    var x = {
                        page: {
                            top: m,
                            left: v
                        },
                        viewport: {
                            top: m - pageYOffset,
                            bottom: pageYOffset - m - r + innerHeight,
                            left: v - pageXOffset,
                            right: pageXOffset - v - o + innerWidth
                        }
                    }
                      , k = this.target.ownerDocument
                      , S = k.defaultView
                      , _ = void 0;
                    return S.innerHeight > k.documentElement.clientHeight && (_ = this.cache("scrollbar-size", c),
                    x.viewport.bottom -= _.height),
                    S.innerWidth > k.documentElement.clientWidth && (_ = this.cache("scrollbar-size", c),
                    x.viewport.right -= _.width),
                    -1 !== ["", "static"].indexOf(k.body.style.position) && -1 !== ["", "static"].indexOf(k.body.parentElement.style.position) || (x.page.bottom = k.body.scrollHeight - m - r,
                    x.page.right = k.body.scrollWidth - v - o),
                    void 0 !== this.options.optimizations && !1 !== this.options.optimizations.moveElement && void 0 === this.targetModifier && function() {
                        var e = t.cache("target-offsetparent", function() {
                            return l(t.target)
                        })
                          , n = t.cache("target-offsetparent-bounds", function() {
                            return a(e)
                        })
                          , i = getComputedStyle(e)
                          , o = n
                          , r = {};
                        if (["Top", "Left", "Bottom", "Right"].forEach(function(t) {
                            r[t.toLowerCase()] = parseFloat(i["border" + t + "Width"])
                        }),
                        n.right = k.body.scrollWidth - n.left - o.width + r.right,
                        n.bottom = k.body.scrollHeight - n.top - o.height + r.bottom,
                        x.page.top >= n.top + r.top && x.page.bottom >= n.bottom && x.page.left >= n.left + r.left && x.page.right >= n.right) {
                            var s = e.scrollTop
                              , c = e.scrollLeft;
                            x.offset = {
                                top: x.page.top - n.top + s - r.top,
                                left: x.page.left - n.left + c - r.left
                            }
                        }
                    }(),
                    this.move(x),
                    this.history.unshift(x),
                    this.history.length > 3 && this.history.pop(),
                    e && O(),
                    !0
                }
            }
        }, {
            key: "move",
            value: function(t) {
                var e = this;
                if (void 0 !== this.element.parentNode) {
                    var n = {};
                    for (var i in t) {
                        n[i] = {};
                        for (var o in t[i]) {
                            for (var r = !1, s = 0; s < this.history.length; ++s) {
                                var a = this.history[s];
                                if (void 0 !== a[i] && !y(a[i][o], t[i][o])) {
                                    r = !0;
                                    break
                                }
                            }
                            r || (n[i][o] = !0)
                        }
                    }
                    var c = {
                        top: "",
                        left: "",
                        right: "",
                        bottom: ""
                    }
                      , d = function(t, n) {
                        if (!1 !== (void 0 !== e.options.optimizations ? e.options.optimizations.gpu : null)) {
                            var i = void 0
                              , o = void 0;
                            t.top ? (c.top = 0,
                            i = n.top) : (c.bottom = 0,
                            i = -n.bottom),
                            t.left ? (c.left = 0,
                            o = n.left) : (c.right = 0,
                            o = -n.right),
                            window.matchMedia && (window.matchMedia("only screen and (min-resolution: 1.3dppx)").matches || window.matchMedia("only screen and (-webkit-min-device-pixel-ratio: 1.3)").matches || (o = Math.round(o),
                            i = Math.round(i))),
                            c[N] = "translateX(" + o + "px) translateY(" + i + "px)",
                            "msTransform" !== N && (c[N] += " translateZ(0)")
                        } else
                            t.top ? c.top = n.top + "px" : c.bottom = n.bottom + "px",
                            t.left ? c.left = n.left + "px" : c.right = n.right + "px"
                    }
                      , f = !1;
                    if ((n.page.top || n.page.bottom) && (n.page.left || n.page.right) ? (c.position = "absolute",
                    d(n.page, t.page)) : (n.viewport.top || n.viewport.bottom) && (n.viewport.left || n.viewport.right) ? (c.position = "fixed",
                    d(n.viewport, t.viewport)) : void 0 !== n.offset && n.offset.top && n.offset.left ? function() {
                        c.position = "absolute";
                        var i = e.cache("target-offsetparent", function() {
                            return l(e.target)
                        });
                        l(e.element) !== i && I(function() {
                            e.element.parentNode.removeChild(e.element),
                            i.appendChild(e.element)
                        }),
                        d(n.offset, t.offset),
                        f = !0
                    }() : (c.position = "absolute",
                    d({
                        top: !0,
                        left: !0
                    }, t.page)),
                    !f)
                        if (this.options.bodyElement)
                            this.options.bodyElement.appendChild(this.element);
                        else {
                            for (var p = !0, h = this.element.parentNode; h && 1 === h.nodeType && "BODY" !== h.tagName; ) {
                                if ("static" !== getComputedStyle(h).position) {
                                    p = !1;
                                    break
                                }
                                h = h.parentNode
                            }
                            p || (this.element.parentNode.removeChild(this.element),
                            this.element.ownerDocument.body.appendChild(this.element))
                        }
                    var g = {}
                      , v = !1;
                    for (var o in c) {
                        var m = c[o];
                        this.element.style[o] !== m && (v = !0,
                        g[o] = m)
                    }
                    v && I(function() {
                        u(e.element.style, g),
                        e.trigger("repositioned")
                    })
                }
            }
        }]),
        e
    }(j);
    V.modules = [],
    T.position = F;
    var X = u(V, T)
      , D = function() {
        function t(t, e) {
            var n = []
              , i = !0
              , o = !1
              , r = void 0;
            try {
                for (var s, a = t[Symbol.iterator](); !(i = (s = a.next()).done) && (n.push(s.value),
                !e || n.length !== e); i = !0)
                    ;
            } catch (t) {
                o = !0,
                r = t
            } finally {
                try {
                    !i && a.return && a.return()
                } finally {
                    if (o)
                        throw r
                }
            }
            return n
        }
        return function(e, n) {
            if (Array.isArray(e))
                return e;
            if (Symbol.iterator in Object(e))
                return t(e, n);
            throw new TypeError("Invalid attempt to destructure non-iterable instance")
        }
    }()
      , M = T.Utils
      , a = M.getBounds
      , u = M.extend
      , v = M.updateClasses
      , I = M.defer
      , J = ["left", "top", "right", "bottom"];
    T.modules.push({
        position: function(t) {
            var e = this
              , n = t.top
              , i = t.left
              , o = t.targetAttachment;
            if (!this.options.constraints)
                return !0;
            var r = this.cache("element-bounds", function() {
                return a(e.element)
            })
              , s = r.height
              , l = r.width;
            if (0 === l && 0 === s && void 0 !== this.lastSize) {
                var c = this.lastSize;
                l = c.width,
                s = c.height
            }
            var d = this.cache("target-bounds", function() {
                return e.getTargetBounds()
            })
              , f = d.height
              , p = d.width
              , h = [this.getClass("pinned"), this.getClass("out-of-bounds")];
            this.options.constraints.forEach(function(t) {
                var e = t.outOfBoundsClass
                  , n = t.pinnedClass;
                e && h.push(e),
                n && h.push(n)
            }),
            h.forEach(function(t) {
                ["left", "top", "right", "bottom"].forEach(function(e) {
                    h.push(t + "-" + e)
                })
            });
            var g = []
              , m = u({}, o)
              , y = u({}, this.attachment);
            return this.options.constraints.forEach(function(t) {
                var r = t.to
                  , a = t.attachment
                  , c = t.pin;
                void 0 === a && (a = "");
                var u = void 0
                  , d = void 0;
                if (a.indexOf(" ") >= 0) {
                    var h = a.split(" ")
                      , v = D(h, 2);
                    d = v[0],
                    u = v[1]
                } else
                    u = d = a;
                var b = C(e, r);
                "target" !== d && "both" !== d || (n < b[1] && "top" === m.top && (n += f,
                m.top = "bottom"),
                n + s > b[3] && "bottom" === m.top && (n -= f,
                m.top = "top")),
                "together" === d && ("top" === m.top && ("bottom" === y.top && n < b[1] ? (n += f,
                m.top = "bottom",
                n += s,
                y.top = "top") : "top" === y.top && n + s > b[3] && n - (s - f) >= b[1] && (n -= s - f,
                m.top = "bottom",
                y.top = "bottom")),
                "bottom" === m.top && ("top" === y.top && n + s > b[3] ? (n -= f,
                m.top = "top",
                n -= s,
                y.top = "bottom") : "bottom" === y.top && n < b[1] && n + (2 * s - f) <= b[3] && (n += s - f,
                m.top = "top",
                y.top = "top")),
                "middle" === m.top && (n + s > b[3] && "top" === y.top ? (n -= s,
                y.top = "bottom") : n < b[1] && "bottom" === y.top && (n += s,
                y.top = "top"))),
                "target" !== u && "both" !== u || (i < b[0] && "left" === m.left && (i += p,
                m.left = "right"),
                i + l > b[2] && "right" === m.left && (i -= p,
                m.left = "left")),
                "together" === u && (i < b[0] && "left" === m.left ? "right" === y.left ? (i += p,
                m.left = "right",
                i += l,
                y.left = "left") : "left" === y.left && (i += p,
                m.left = "right",
                i -= l,
                y.left = "right") : i + l > b[2] && "right" === m.left ? "left" === y.left ? (i -= p,
                m.left = "left",
                i -= l,
                y.left = "right") : "right" === y.left && (i -= p,
                m.left = "left",
                i += l,
                y.left = "left") : "center" === m.left && (i + l > b[2] && "left" === y.left ? (i -= l,
                y.left = "right") : i < b[0] && "right" === y.left && (i += l,
                y.left = "left"))),
                "element" !== d && "both" !== d || (n < b[1] && "bottom" === y.top && (n += s,
                y.top = "top"),
                n + s > b[3] && "top" === y.top && (n -= s,
                y.top = "bottom")),
                "element" !== u && "both" !== u || (i < b[0] && ("right" === y.left ? (i += l,
                y.left = "left") : "center" === y.left && (i += l / 2,
                y.left = "left")),
                i + l > b[2] && ("left" === y.left ? (i -= l,
                y.left = "right") : "center" === y.left && (i -= l / 2,
                y.left = "right"))),
                "string" == typeof c ? c = c.split(",").map(function(t) {
                    return t.trim()
                }) : !0 === c && (c = ["top", "left", "right", "bottom"]),
                c = c || [];
                var w = []
                  , E = [];
                n < b[1] && (c.indexOf("top") >= 0 ? (n = b[1],
                w.push("top")) : E.push("top")),
                n + s > b[3] && (c.indexOf("bottom") >= 0 ? (n = b[3] - s,
                w.push("bottom")) : E.push("bottom")),
                i < b[0] && (c.indexOf("left") >= 0 ? (i = b[0],
                w.push("left")) : E.push("left")),
                i + l > b[2] && (c.indexOf("right") >= 0 ? (i = b[2] - l,
                w.push("right")) : E.push("right")),
                w.length && function() {
                    var t = void 0;
                    t = void 0 !== e.options.pinnedClass ? e.options.pinnedClass : e.getClass("pinned"),
                    g.push(t),
                    w.forEach(function(e) {
                        g.push(t + "-" + e)
                    })
                }(),
                E.length && function() {
                    var t = void 0;
                    t = void 0 !== e.options.outOfBoundsClass ? e.options.outOfBoundsClass : e.getClass("out-of-bounds"),
                    g.push(t),
                    E.forEach(function(e) {
                        g.push(t + "-" + e)
                    })
                }(),
                (w.indexOf("left") >= 0 || w.indexOf("right") >= 0) && (y.left = m.left = !1),
                (w.indexOf("top") >= 0 || w.indexOf("bottom") >= 0) && (y.top = m.top = !1),
                m.top === o.top && m.left === o.left && y.top === e.attachment.top && y.left === e.attachment.left || (e.updateAttachClasses(y, m),
                e.trigger("update", {
                    attachment: y,
                    targetAttachment: m
                }))
            }),
            I(function() {
                !1 !== e.options.addTargetClasses && v(e.target, g, h),
                v(e.element, g, h)
            }),
            {
                top: n,
                left: i
            }
        }
    });
    var M = T.Utils
      , a = M.getBounds
      , v = M.updateClasses
      , I = M.defer;
    T.modules.push({
        position: function(t) {
            var e = this
              , n = t.top
              , i = t.left
              , o = this.cache("element-bounds", function() {
                return a(e.element)
            })
              , r = o.height
              , s = o.width
              , l = this.getTargetBounds()
              , c = n + r
              , u = i + s
              , d = [];
            n <= l.bottom && c >= l.top && ["left", "right"].forEach(function(t) {
                var e = l[t];
                e !== i && e !== u || d.push(t)
            }),
            i <= l.right && u >= l.left && ["top", "bottom"].forEach(function(t) {
                var e = l[t];
                e !== n && e !== c || d.push(t)
            });
            var f = []
              , p = []
              , h = ["left", "top", "right", "bottom"];
            return f.push(this.getClass("abutted")),
            h.forEach(function(t) {
                f.push(e.getClass("abutted") + "-" + t)
            }),
            d.length && p.push(this.getClass("abutted")),
            d.forEach(function(t) {
                p.push(e.getClass("abutted") + "-" + t)
            }),
            I(function() {
                !1 !== e.options.addTargetClasses && v(e.target, p, f),
                v(e.element, p, f)
            }),
            !0
        }
    });
    var D = function() {
        function t(t, e) {
            var n = []
              , i = !0
              , o = !1
              , r = void 0;
            try {
                for (var s, a = t[Symbol.iterator](); !(i = (s = a.next()).done) && (n.push(s.value),
                !e || n.length !== e); i = !0)
                    ;
            } catch (t) {
                o = !0,
                r = t
            } finally {
                try {
                    !i && a.return && a.return()
                } finally {
                    if (o)
                        throw r
                }
            }
            return n
        }
        return function(e, n) {
            if (Array.isArray(e))
                return e;
            if (Symbol.iterator in Object(e))
                return t(e, n);
            throw new TypeError("Invalid attempt to destructure non-iterable instance")
        }
    }();
    return T.modules.push({
        position: function(t) {
            var e = t.top
              , n = t.left;
            if (this.options.shift) {
                var i = this.options.shift;
                "function" == typeof this.options.shift && (i = this.options.shift.call(this, {
                    top: e,
                    left: n
                }));
                var o = void 0
                  , r = void 0;
                if ("string" == typeof i) {
                    i = i.split(" "),
                    i[1] = i[1] || i[0];
                    var s = i
                      , a = D(s, 2);
                    o = a[0],
                    r = a[1],
                    o = parseFloat(o, 10),
                    r = parseFloat(r, 10)
                } else
                    o = i.top,
                    r = i.left;
                return e += o,
                n += r,
                {
                    top: e,
                    left: n
                }
            }
        }
    }),
    X
}),
function(t, e, n, i) {
    "use strict";
    function o(t) {
        var e = t.currentTarget
          , i = t.data ? t.data.options : {}
          , o = t.data ? t.data.items : []
          , r = n(e).attr("data-fancybox") || ""
          , s = 0;
        t.preventDefault(),
        t.stopPropagation(),
        r ? (o = o.length ? o.filter('[data-fancybox="' + r + '"]') : n('[data-fancybox="' + r + '"]'),
        (s = o.index(e)) < 0 && (s = 0)) : o = [e],
        n.fancybox.open(o, i, s)
    }
    if (n) {
        if (n.fn.fancybox)
            return void n.error("fancyBox already initialized");
        var r = {
            loop: !1,
            margin: [44, 0],
            gutter: 50,
            keyboard: !0,
            arrows: !0,
            infobar: !1,
            toolbar: !0,
            buttons: ["slideShow", "fullScreen", "thumbs", "close"],
            idleTime: 4,
            smallBtn: "auto",
            protect: !1,
            modal: !1,
            image: {
                preload: "auto"
            },
            ajax: {
                settings: {
                    data: {
                        fancybox: !0
                    }
                }
            },
            iframe: {
                tpl: '<iframe id="fancybox-frame{rnd}" name="fancybox-frame{rnd}" class="fancybox-iframe" frameborder="0" vspace="0" hspace="0" webkitAllowFullScreen mozallowfullscreen allowFullScreen allowtransparency="true" src=""></iframe>',
                preload: !0,
                css: {},
                attr: {
                    scrolling: "auto"
                }
            },
            animationEffect: "zoom",
            animationDuration: 366,
            zoomOpacity: "auto",
            transitionEffect: "fade",
            transitionDuration: 366,
            slideClass: "",
            baseClass: "",
            baseTpl: '<div class="fancybox-container" role="dialog" tabindex="-1"><div class="fancybox-bg"></div><div class="fancybox-inner"><div class="fancybox-infobar"><button data-fancybox-prev title="{{PREV}}" class="fancybox-button fancybox-button--left"></button><div class="fancybox-infobar__body"><span data-fancybox-index></span>&nbsp;/&nbsp;<span data-fancybox-count></span></div><button data-fancybox-next title="{{NEXT}}" class="fancybox-button fancybox-button--right"></button></div><div class="fancybox-toolbar">{{BUTTONS}}</div><div class="fancybox-navigation"><button data-fancybox-prev title="{{PREV}}" class="fancybox-arrow fancybox-arrow--left" /><button data-fancybox-next title="{{NEXT}}" class="fancybox-arrow fancybox-arrow--right" /></div><div class="fancybox-stage"></div><div class="fancybox-caption-wrap"><div class="fancybox-caption"></div></div></div></div>',
            spinnerTpl: '<div class="fancybox-loading"></div>',
            errorTpl: '<div class="fancybox-error"><p>{{ERROR}}<p></div>',
            btnTpl: {
                slideShow: '<button data-fancybox-play class="fancybox-button fancybox-button--play" title="{{PLAY_START}}"></button>',
                fullScreen: '<button data-fancybox-fullscreen class="fancybox-button fancybox-button--fullscreen" title="{{FULL_SCREEN}}"></button>',
                thumbs: '<button data-fancybox-thumbs class="fancybox-button fancybox-button--thumbs" title="{{THUMBS}}"></button>',
                close: '<button data-fancybox-close class="fancybox-button fancybox-button--close" title="{{CLOSE}}"></button>',
                smallBtn: '<button data-fancybox-close class="fancybox-close-small bandClass" title="{{CLOSE}}" ></button>'
            },
            parentEl: "body",
            autoFocus: !0,
            backFocus: !0,
            trapFocus: !0,
            fullScreen: {
                autoStart: !1
            },
            touch: {
                vertical: !0,
                momentum: !0
            },
            hash: null,
            media: {},
            slideShow: {
                autoStart: !1,
                speed: 4e3
            },
            thumbs: {
                autoStart: !1,
                hideOnClose: !0
            },
            onInit: n.noop,
            beforeLoad: n.noop,
            afterLoad: n.noop,
            beforeShow: n.noop,
            afterShow: n.noop,
            beforeClose: n.noop,
            afterClose: n.noop,
            onActivate: n.noop,
            onDeactivate: n.noop,
            clickContent: function(t, e) {
                return "image" === t.type && "zoom"
            },
            clickSlide: "close",
            clickOutside: "close",
            dblclickContent: !1,
            dblclickSlide: !1,
            dblclickOutside: !1,
            mobile: {
                clickContent: function(t, e) {
                    return "image" === t.type && "toggleControls"
                },
                clickSlide: function(t, e) {
                    return "image" === t.type ? "toggleControls" : "close"
                },
                dblclickContent: function(t, e) {
                    return "image" === t.type && "zoom"
                },
                dblclickSlide: function(t, e) {
                    return "image" === t.type && "zoom"
                }
            },
            lang: "en",
            i18n: {
                en: {
                    CLOSE: "Close",
                    NEXT: "Next",
                    PREV: "Previous",
                    ERROR: "The requested content cannot be loaded. <br/> Please try again later.",
                    PLAY_START: "Start slideshow",
                    PLAY_STOP: "Pause slideshow",
                    FULL_SCREEN: "Full screen",
                    THUMBS: "Thumbnails"
                },
                de: {
                    CLOSE: "Schliessen",
                    NEXT: "Weiter",
                    PREV: "Zurück",
                    ERROR: "Die angeforderten Daten konnten nicht geladen werden. <br/> Bitte versuchen Sie es später nochmal.",
                    PLAY_START: "Diaschau starten",
                    PLAY_STOP: "Diaschau beenden",
                    FULL_SCREEN: "Vollbild",
                    THUMBS: "Vorschaubilder"
                }
            }
        }
          , s = n(t)
          , a = n(e)
          , l = 0
          , c = function(t) {
            return t && t.hasOwnProperty && t instanceof n
        }
          , u = function() {
            return t.requestAnimationFrame || t.webkitRequestAnimationFrame || t.mozRequestAnimationFrame || t.oRequestAnimationFrame || function(e) {
                return t.setTimeout(e, 1e3 / 60)
            }
        }()
          , d = function() {
            var t, n = e.createElement("fakeelement"), o = {
                transition: "transitionend",
                OTransition: "oTransitionEnd",
                MozTransition: "transitionend",
                WebkitTransition: "webkitTransitionEnd"
            };
            for (t in o)
                if (n.style[t] !== i)
                    return o[t]
        }()
          , f = function(t) {
            return t && t.length && t[0].offsetHeight
        }
          , p = function(t, i, o) {
            var s = this;
            s.opts = n.extend(!0, {
                index: o
            }, r, i || {}),
            i && n.isArray(i.buttons) && (s.opts.buttons = i.buttons),
            s.id = s.opts.id || ++l,
            s.group = [],
            s.currIndex = parseInt(s.opts.index, 10) || 0,
            s.prevIndex = null,
            s.prevPos = null,
            s.currPos = 0,
            s.firstRun = null,
            s.createGroup(t),
            s.group.length && (s.$lastFocus = n(e.activeElement).blur(),
            s.slides = {},
            s.init(t))
        };
        n.extend(p.prototype, {
            init: function() {
                var t, e, i, o = this, r = o.group[o.currIndex].opts;
                o.scrollTop = a.scrollTop(),
                o.scrollLeft = a.scrollLeft(),
                n.fancybox.getInstance() || n.fancybox.isMobile || "hidden" === n("body").css("overflow") || (t = n("body").width(),
                n("html").addClass("fancybox-enabled"),
                (t = n("body").width() - t) > 1 && n("head").append('<style id="fancybox-style-noscroll" type="text/css">.compensate-for-scrollbar, .fancybox-enabled body { margin-right: ' + t + "px; }</style>")),
                i = "",
                n.each(r.buttons, function(t, e) {
                    i += r.btnTpl[e] || ""
                }),
                e = n(o.translate(o, r.baseTpl.replace("{{BUTTONS}}", i))).addClass("fancybox-is-hidden").attr("id", "fancybox-container-" + o.id).addClass(r.baseClass).data("FancyBox", o).prependTo(r.parentEl),
                o.$refs = {
                    container: e
                },
                ["bg", "inner", "infobar", "toolbar", "stage", "caption"].forEach(function(t) {
                    o.$refs[t] = e.find(".fancybox-" + t)
                }),
                (!r.arrows || o.group.length < 2) && e.find(".fancybox-navigation").remove(),
                r.infobar || o.$refs.infobar.remove(),
                r.toolbar || o.$refs.toolbar.remove(),
                o.trigger("onInit"),
                o.activate(),
                o.jumpTo(o.currIndex)
            },
            translate: function(t, e) {
                var n = t.opts.i18n[t.opts.lang];
                return e.replace(/\{\{(\w+)\}\}/g, function(t, e) {
                    var o = n[e];
                    return o === i ? t : o
                })
            },
            createGroup: function(t) {
                var e = this
                  , o = n.makeArray(t);
                n.each(o, function(t, o) {
                    var r, s, a, l, c = {}, u = {}, d = [];
                    n.isPlainObject(o) ? (c = o,
                    u = o.opts || o) : "object" === n.type(o) && n(o).length ? (r = n(o),
                    d = r.data(),
                    u = "options"in d ? d.options : {},
                    u = "object" === n.type(u) ? u : {},
                    c.src = "src"in d ? d.src : u.src || r.attr("href"),
                    ["width", "height", "thumb", "type", "filter"].forEach(function(t) {
                        t in d && (u[t] = d[t])
                    }),
                    "srcset"in d && (u.image = {
                        srcset: d.srcset
                    }),
                    u.$orig = r,
                    c.type || c.src || (c.type = "inline",
                    c.src = o)) : c = {
                        type: "html",
                        src: o + ""
                    },
                    c.opts = n.extend(!0, {}, e.opts, u),
                    n.fancybox.isMobile && (c.opts = n.extend(!0, {}, c.opts, c.opts.mobile)),
                    s = c.type || c.opts.type,
                    a = c.src || "",
                    !s && a && (a.match(/(^data:image\/[a-z0-9+\/=]*,)|(\.(jp(e|g|eg)|gif|png|bmp|webp|svg|ico)((\?|#).*)?$)/i) ? s = "image" : a.match(/\.(pdf)((\?|#).*)?$/i) ? s = "pdf" : "#" === a.charAt(0) && (s = "inline")),
                    c.type = s,
                    c.index = e.group.length,
                    c.opts.$orig && !c.opts.$orig.length && delete c.opts.$orig,
                    !c.opts.$thumb && c.opts.$orig && (c.opts.$thumb = c.opts.$orig.find("img:first")),
                    c.opts.$thumb && !c.opts.$thumb.length && delete c.opts.$thumb,
                    "function" === n.type(c.opts.caption) ? c.opts.caption = c.opts.caption.apply(o, [e, c]) : "caption"in d && (c.opts.caption = d.caption),
                    c.opts.caption = c.opts.caption === i ? "" : c.opts.caption + "",
                    "ajax" === s && (l = a.split(/\s+/, 2),
                    l.length > 1 && (c.src = l.shift(),
                    c.opts.filter = l.shift())),
                    "auto" == c.opts.smallBtn && (n.inArray(s, ["html", "inline", "ajax"]) > -1 ? (c.opts.toolbar = !1,
                    c.opts.smallBtn = !0) : c.opts.smallBtn = !1),
                    "pdf" === s && (c.type = "iframe",
                    c.opts.iframe.preload = !1),
                    c.opts.modal && (c.opts = n.extend(!0, c.opts, {
                        infobar: 0,
                        toolbar: 0,
                        smallBtn: 0,
                        keyboard: 0,
                        slideShow: 0,
                        fullScreen: 0,
                        thumbs: 0,
                        touch: 0,
                        clickContent: !1,
                        clickSlide: !1,
                        clickOutside: !1,
                        dblclickContent: !1,
                        dblclickSlide: !1,
                        dblclickOutside: !1
                    })),
                    e.group.push(c)
                })
            },
            addEvents: function() {
                var i = this;
                i.removeEvents(),
                i.$refs.container.on("click.fb-close", "[data-fancybox-close]", function(t) {

                    t.stopPropagation(),
                    t.preventDefault(),
                    i.close(t)
                }).on("click.fb-prev touchend.fb-prev", "[data-fancybox-prev]", function(t) {
                    t.stopPropagation(),
                    t.preventDefault(),
                    i.previous()
                }).on("click.fb-next touchend.fb-next", "[data-fancybox-next]", function(t) {
                    t.stopPropagation(),
                    t.preventDefault(),
                    i.next()
                }),
                s.on("orientationchange.fb resize.fb", function(t) {
                    t && t.originalEvent && "resize" === t.originalEvent.type ? u(function() {
                        i.update()
                    }) : (i.$refs.stage.hide(),
                    setTimeout(function() {
                        i.$refs.stage.show(),
                        i.update()
                    }, 500))
                }),
                a.on("focusin.fb", function(t) {
                    var o = n.fancybox ? n.fancybox.getInstance() : null;
                    o.isClosing || !o.current || !o.current.opts.trapFocus || n(t.target).hasClass("fancybox-container") || n(t.target).is(e) || o && "fixed" !== n(t.target).css("position") && !o.$refs.container.has(t.target).length && (t.stopPropagation(),
                    o.focus(),
                    s.scrollTop(i.scrollTop).scrollLeft(i.scrollLeft))
                }),
                a.on("keydown.fb", function(t) {
                    var e = i.current
                      , o = t.keyCode || t.which;
                    if (e && e.opts.keyboard && !n(t.target).is("input") && !n(t.target).is("textarea"))
                        return 8 === o || 27 === o ? (t.preventDefault(),
                        void i.close(t)) : 37 === o || 38 === o ? (t.preventDefault(),
                        void i.previous()) : 39 === o || 40 === o ? (t.preventDefault(),
                        void i.next()) : void i.trigger("afterKeydown", t, o)
                }),
                i.group[i.currIndex].opts.idleTime && (i.idleSecondsCounter = 0,
                a.on("mousemove.fb-idle mouseenter.fb-idle mouseleave.fb-idle mousedown.fb-idle touchstart.fb-idle touchmove.fb-idle scroll.fb-idle keydown.fb-idle", function() {
                    i.idleSecondsCounter = 0,
                    i.isIdle && i.showControls(),
                    i.isIdle = !1
                }),
                i.idleInterval = t.setInterval(function() {
                    ++i.idleSecondsCounter >= i.group[i.currIndex].opts.idleTime && (i.isIdle = !0,
                    i.idleSecondsCounter = 0,
                    i.hideControls())
                }, 1e3))
            },
            removeEvents: function() {
                var e = this;
                s.off("orientationchange.fb resize.fb"),
                a.off("focusin.fb keydown.fb .fb-idle"),
                this.$refs.container.off(".fb-close .fb-prev .fb-next"),
                e.idleInterval && (t.clearInterval(e.idleInterval),
                e.idleInterval = null)
            },
            previous: function(t) {
                return this.jumpTo(this.currPos - 1, t)
            },
            next: function(t) {
                return this.jumpTo(this.currPos + 1, t)
            },
            jumpTo: function(t, e, o) {
                var r, s, a, l, c, u, d, p = this, h = p.group.length;
                if (!(p.isSliding || p.isClosing || p.isAnimating && p.firstRun)) {
                    if (t = parseInt(t, 10),
                    !(s = p.current ? p.current.opts.loop : p.opts.loop) && (t < 0 || t >= h))
                        return !1;
                    if (r = p.firstRun = null === p.firstRun,
                    !(h < 2 && !r && p.isSliding)) {
                        if (l = p.current,
                        p.prevIndex = p.currIndex,
                        p.prevPos = p.currPos,
                        a = p.createSlide(t),
                        h > 1 && ((s || a.index > 0) && p.createSlide(t - 1),
                        (s || a.index < h - 1) && p.createSlide(t + 1)),
                        p.current = a,
                        p.currIndex = a.index,
                        p.currPos = a.pos,
                        p.trigger("beforeShow", r),
                        p.updateControls(),
                        u = n.fancybox.getTranslate(a.$slide),
                        a.isMoved = (0 !== u.left || 0 !== u.top) && !a.$slide.hasClass("fancybox-animated"),
                        a.forcedDuration = i,
                        n.isNumeric(e) ? a.forcedDuration = e : e = a.opts[r ? "animationDuration" : "transitionDuration"],
                        e = parseInt(e, 10),
                        r)
                            return a.opts.animationEffect && e && p.$refs.container.css("transition-duration", e + "ms"),
                            p.$refs.container.removeClass("fancybox-is-hidden"),
                            f(p.$refs.container),
                            p.$refs.container.addClass("fancybox-is-open"),
                            a.$slide.addClass("fancybox-slide--current"),
                            p.loadSlide(a),
                            void p.preload();
                        n.each(p.slides, function(t, e) {
                            n.fancybox.stop(e.$slide)
                        }),
                        a.$slide.removeClass("fancybox-slide--next fancybox-slide--previous").addClass("fancybox-slide--current"),
                        a.isMoved ? (c = Math.round(a.$slide.width()),
                        n.each(p.slides, function(t, i) {
                            var o = i.pos - a.pos;
                            n.fancybox.animate(i.$slide, {
                                top: 0,
                                left: o * c + o * i.opts.gutter
                            }, e, function() {
                                i.$slide.removeAttr("style").removeClass("fancybox-slide--next fancybox-slide--previous"),
                                i.pos === p.currPos && (a.isMoved = !1,
                                p.complete())
                            })
                        })) : p.$refs.stage.children().removeAttr("style"),
                        a.isLoaded ? p.revealContent(a) : p.loadSlide(a),
                        p.preload(),
                        l.pos !== a.pos && (d = "fancybox-slide--" + (l.pos > a.pos ? "next" : "previous"),
                        l.$slide.removeClass("fancybox-slide--complete fancybox-slide--current fancybox-slide--next fancybox-slide--previous"),
                        l.isComplete = !1,
                        e && (a.isMoved || a.opts.transitionEffect) && (a.isMoved ? l.$slide.addClass(d) : (d = "fancybox-animated " + d + " fancybox-fx-" + a.opts.transitionEffect,
                        n.fancybox.animate(l.$slide, d, e, function() {
                            l.$slide.removeClass(d).removeAttr("style")
                        }))))
                    }
                }
            },
            createSlide: function(t) {
                var e, i, o = this;
                return i = t % o.group.length,
                i = i < 0 ? o.group.length + i : i,
                !o.slides[t] && o.group[i] && (e = n('<div class="fancybox-slide"></div>').appendTo(o.$refs.stage),
                o.slides[t] = n.extend(!0, {}, o.group[i], {
                    pos: t,
                    $slide: e,
                    isLoaded: !1
                }),
                o.updateSlide(o.slides[t])),
                o.slides[t]
            },
            scaleToActual: function(t, e, o) {
                var r, s, a, l, c, u = this, d = u.current, f = d.$content, p = parseInt(d.$slide.width(), 10), h = parseInt(d.$slide.height(), 10), g = d.width, v = d.height;
                "image" != d.type || d.hasError || !f || u.isAnimating || (n.fancybox.stop(f),
                u.isAnimating = !0,
                t = t === i ? .5 * p : t,
                e = e === i ? .5 * h : e,
                r = n.fancybox.getTranslate(f),
                l = g / r.width,
                c = v / r.height,
                s = .5 * p - .5 * g,
                a = .5 * h - .5 * v,
                g > p && (s = r.left * l - (t * l - t),
                s > 0 && (s = 0),
                s < p - g && (s = p - g)),
                v > h && (a = r.top * c - (e * c - e),
                a > 0 && (a = 0),
                a < h - v && (a = h - v)),
                u.updateCursor(g, v),
                n.fancybox.animate(f, {
                    top: a,
                    left: s,
                    scaleX: l,
                    scaleY: c
                }, o || 330, function() {
                    u.isAnimating = !1
                }),
                u.SlideShow && u.SlideShow.isActive && u.SlideShow.stop())
            },
            scaleToFit: function(t) {
                var e, i = this, o = i.current, r = o.$content;
                "image" != o.type || o.hasError || !r || i.isAnimating || (n.fancybox.stop(r),
                i.isAnimating = !0,
                e = i.getFitPos(o),
                i.updateCursor(e.width, e.height),
                n.fancybox.animate(r, {
                    top: e.top,
                    left: e.left,
                    scaleX: e.width / r.width(),
                    scaleY: e.height / r.height()
                }, t || 330, function() {
                    i.isAnimating = !1
                }))
            },
            getFitPos: function(t) {
                var e, i, o, r, a, l = this, c = t.$content, u = t.width, d = t.height, f = t.opts.margin;
                return !(!c || !c.length || !u && !d) && ("number" === n.type(f) && (f = [f, f]),
                2 == f.length && (f = [f[0], f[1], f[0], f[1]]),
                s.width() < 800 && (f = [0, 0, 0, 0]),
                e = parseInt(l.$refs.stage.width(), 10) - (f[1] + f[3]),
                i = parseInt(l.$refs.stage.height(), 10) - (f[0] + f[2]),
                o = Math.min(1, e / u, i / d),
                r = Math.floor(o * u),
                a = Math.floor(o * d),
                {
                    top: Math.floor(.5 * (i - a)) + f[0],
                    left: Math.floor(.5 * (e - r)) + f[3],
                    width: r,
                    height: a
                })
            },
            update: function() {
                var t = this;
                n.each(t.slides, function(e, n) {
                    t.updateSlide(n)
                })
            },
            updateSlide: function(t) {
                var e = this
                  , i = t.$content;
                i && (t.width || t.height) && (n.fancybox.stop(i),
                n.fancybox.setTranslate(i, e.getFitPos(t)),
                t.pos === e.currPos && e.updateCursor()),
                t.$slide.trigger("refresh"),
                e.trigger("onUpdate", t)
            },
            updateCursor: function(t, e) {
                var n, o = this, r = o.$refs.container.removeClass("fancybox-is-zoomable fancybox-can-zoomIn fancybox-can-drag fancybox-can-zoomOut");
                o.current && !o.isClosing && (o.isZoomable() ? (r.addClass("fancybox-is-zoomable"),
                n = t !== i && e !== i ? t < o.current.width && e < o.current.height : o.isScaledDown(),
                n ? r.addClass("fancybox-can-zoomIn") : o.current.opts.touch ? r.addClass("fancybox-can-drag") : r.addClass("fancybox-can-zoomOut")) : o.current.opts.touch && r.addClass("fancybox-can-drag"))
            },
            isZoomable: function() {
                var t, e = this, i = e.current;
                if (i && !e.isClosing)
                    return !!("image" === i.type && i.isLoaded && !i.hasError && ("zoom" === i.opts.clickContent || n.isFunction(i.opts.clickContent) && "zoom" === i.opts.clickContent(i)) && (t = e.getFitPos(i),
                    i.width > t.width || i.height > t.height))
            },
            isScaledDown: function() {
                var t = this
                  , e = t.current
                  , i = e.$content
                  , o = !1;
                return i && (o = n.fancybox.getTranslate(i),
                o = o.width < e.width || o.height < e.height),
                o
            },
            canPan: function() {
                var t = this
                  , e = t.current
                  , n = e.$content
                  , i = !1;
                return n && (i = t.getFitPos(e),
                i = Math.abs(n.width() - i.width) > 1 || Math.abs(n.height() - i.height) > 1),
                i
            },
            loadSlide: function(t) {
                var e, i, o, r = this;
                if (!t.isLoading && !t.isLoaded) {
                    switch (t.isLoading = !0,
                    r.trigger("beforeLoad", t),
                    e = t.type,
                    i = t.$slide,
                    i.off("refresh").trigger("onReset").addClass("fancybox-slide--" + (e || "unknown")).addClass(t.opts.slideClass),
                    e) {
                    case "image":
                        r.setImage(t);
                        break;
                    case "iframe":
                        r.setIframe(t);
                        break;
                    case "html":
                        r.setContent(t, t.src || t.content);
                        break;
                    case "inline":
                        n(t.src).length ? r.setContent(t, n(t.src)) : r.setError(t);
                        break;
                    case "ajax":
                        r.showLoading(t),
                        o = n.ajax(n.extend({}, t.opts.ajax.settings, {
                            url: t.src,
                            success: function(e, n) {
                                "success" === n && r.setContent(t, e)
                            },
                            error: function(e, n) {
                                e && "abort" !== n && r.setError(t)
                            }
                        })),
                        i.one("onReset", function() {
                            o.abort()
                        });
                        break;
                    default:
                        r.setError(t)
                    }
                    return !0
                }
            },
            setImage: function(e) {
                var i, o, r, s, a = this, l = e.opts.image.srcset;
                if (l) {
                    r = t.devicePixelRatio || 1,
                    s = t.innerWidth * r,
                    o = l.split(",").map(function(t) {
                        var e = {};
                        return t.trim().split(/\s+/).forEach(function(t, n) {
                            var i = parseInt(t.substring(0, t.length - 1), 10);
                            return 0 === n ? e.url = t : void (i && (e.value = i,
                            e.postfix = t[t.length - 1]))
                        }),
                        e
                    }),
                    o.sort(function(t, e) {
                        return t.value - e.value
                    });
                    for (var c = 0; c < o.length; c++) {
                        var u = o[c];
                        if ("w" === u.postfix && u.value >= s || "x" === u.postfix && u.value >= r) {
                            i = u;
                            break
                        }
                    }
                    !i && o.length && (i = o[o.length - 1]),
                    i && (e.src = i.url,
                    e.width && e.height && "w" == i.postfix && (e.height = e.width / e.height * i.value,
                    e.width = i.value))
                }
                e.$content = n('<div class="fancybox-image-wrap"></div>').addClass("fancybox-is-hidden").appendTo(e.$slide),
                !1 !== e.opts.preload && e.opts.width && e.opts.height && (e.opts.thumb || e.opts.$thumb) ? (e.width = e.opts.width,
                e.height = e.opts.height,
                e.$ghost = n("<img />").one("error", function() {
                    n(this).remove(),
                    e.$ghost = null,
                    a.setBigImage(e)
                }).one("load", function() {
                    a.afterLoad(e),
                    a.setBigImage(e)
                }).addClass("fancybox-image").appendTo(e.$content).attr("src", e.opts.thumb || e.opts.$thumb.attr("src"))) : a.setBigImage(e)
            },
            setBigImage: function(t) {
                var e = this
                  , i = n("<img />");
                t.$image = i.one("error", function() {
                    e.setError(t)
                }).one("load", function() {
                    clearTimeout(t.timouts),
                    t.timouts = null,
                    e.isClosing || (t.width = this.naturalWidth,
                    t.height = this.naturalHeight,
                    t.opts.image.srcset && i.attr("sizes", "100vw").attr("srcset", t.opts.image.srcset),
                    e.hideLoading(t),
                    t.$ghost ? t.timouts = setTimeout(function() {
                        t.timouts = null,
                        t.$ghost.hide()
                    }, Math.min(300, Math.max(1e3, t.height / 1600))) : e.afterLoad(t))
                }).addClass("fancybox-image").attr("src", t.src).appendTo(t.$content),
                i[0].complete ? i.trigger("load") : i[0].error ? i.trigger("error") : t.timouts = setTimeout(function() {
                    i[0].complete || t.hasError || e.showLoading(t)
                }, 100)
            },
            setIframe: function(t) {
                var e, o = this, r = t.opts.iframe, s = t.$slide;
                t.$content = n('<div class="fancybox-content' + (r.preload ? " fancybox-is-hidden" : "") + '"></div>').css(r.css).appendTo(s),
                e = n(r.tpl.replace(/\{rnd\}/g, (new Date).getTime())).attr(r.attr).appendTo(t.$content),
                r.preload ? (o.showLoading(t),
                e.on("load.fb error.fb", function(e) {
                    this.isReady = 1,
                    t.$slide.trigger("refresh"),
                    o.afterLoad(t)
                }),
                s.on("refresh.fb", function() {
                    var n, o, s, a, l, c = t.$content;
                    if (1 === e[0].isReady) {
                        try {
                            n = e.contents(),
                            o = n.find("body")
                        } catch (t) {}
                        o && o.length && (r.css.width === i || r.css.height === i) && (s = e[0].contentWindow.document.documentElement.scrollWidth,
                        a = Math.ceil(o.outerWidth(!0) + (c.width() - s)),
                        l = Math.ceil(o.outerHeight(!0)),
                        c.css({
                            width: r.css.width === i ? a + (c.outerWidth() - c.innerWidth()) : r.css.width,
                            height: r.css.height === i ? l + (c.outerHeight() - c.innerHeight()) : r.css.height
                        })),
                        c.removeClass("fancybox-is-hidden")
                    }
                })) : this.afterLoad(t),
                e.attr("src", t.src),
                !0 === t.opts.smallBtn && t.$content.prepend(o.translate(t, t.opts.btnTpl.smallBtn)),
                s.one("onReset", function() {
                    try {
                        n(this).find("iframe").hide().attr("src", "//about:blank")
                    } catch (t) {}
                    n(this).empty(),
                    t.isLoaded = !1
                })
            },
            setContent: function(t, e) {
                var i = this;
                i.isClosing || (i.hideLoading(t),
                t.$slide.empty(),
                c(e) && e.parent().length ? (e.parent(".fancybox-slide--inline").trigger("onReset"),
                t.$placeholder = n("<div></div>").hide().insertAfter(e),
                e.css("display", "inline-block")) : t.hasError || ("string" === n.type(e) && (e = n("<div>").append(n.trim(e)).contents(),
                3 === e[0].nodeType && (e = n("<div>").html(e))),
                t.opts.filter && (e = n("<div>").html(e).find(t.opts.filter))),
                t.$slide.one("onReset", function() {
                    t.$placeholder && (t.$placeholder.after(e.hide()).remove(),
                    t.$placeholder = null),
                    t.$smallBtn && (t.$smallBtn.remove(),
                    t.$smallBtn = null),
                    t.hasError || (n(this).empty(),
                    t.isLoaded = !1)
                }),
                t.$content = n(e).appendTo(t.$slide),
                t.opts.smallBtn && !t.$smallBtn && (t.$smallBtn = n(i.translate(t, t.opts.btnTpl.smallBtn)).appendTo(t.$content)),
                this.afterLoad(t))
            },
            setError: function(t) {
                t.hasError = !0,
                t.$slide.removeClass("fancybox-slide--" + t.type),
                this.setContent(t, this.translate(t, t.opts.errorTpl))
            },
            showLoading: function(t) {
                var e = this;
                (t = t || e.current) && !t.$spinner && (t.$spinner = n(e.opts.spinnerTpl).appendTo(t.$slide))
            },
            hideLoading: function(t) {
                var e = this;
                (t = t || e.current) && t.$spinner && (t.$spinner.remove(),
                delete t.$spinner)
            },
            afterLoad: function(t) {
                var e = this;
                e.isClosing || (t.isLoading = !1,
                t.isLoaded = !0,
                e.trigger("afterLoad", t),
                e.hideLoading(t),
                t.opts.protect && t.$content && !t.hasError && (t.$content.on("contextmenu.fb", function(t) {
                    return 2 == t.button && t.preventDefault(),
                    !0
                }),
                "image" === t.type && n('<div class="fancybox-spaceball"></div>').appendTo(t.$content)),
                e.revealContent(t))
            },
            revealContent: function(t) {
                var e, o, r, s, a, l = this, c = t.$slide, u = !1;
                return e = t.opts[l.firstRun ? "animationEffect" : "transitionEffect"],
                r = t.opts[l.firstRun ? "animationDuration" : "transitionDuration"],
                r = parseInt(t.forcedDuration === i ? r : t.forcedDuration, 10),
                !t.isMoved && t.pos === l.currPos && r || (e = !1),
                "zoom" !== e || t.pos === l.currPos && r && "image" === t.type && !t.hasError && (u = l.getThumbPos(t)) || (e = "fade"),
                "zoom" === e ? (a = l.getFitPos(t),
                a.scaleX = Math.round(a.width / u.width * 100) / 100,
                a.scaleY = Math.round(a.height / u.height * 100) / 100,
                delete a.width,
                delete a.height,
                s = t.opts.zoomOpacity,
                "auto" == s && (s = Math.abs(t.width / t.height - u.width / u.height) > .1),
                s && (u.opacity = .1,
                a.opacity = 1),
                n.fancybox.setTranslate(t.$content.removeClass("fancybox-is-hidden"), u),
                f(t.$content),
                void n.fancybox.animate(t.$content, a, r, function() {
                    l.complete()
                })) : (l.updateSlide(t),
                e ? (n.fancybox.stop(c),
                o = "fancybox-animated fancybox-slide--" + (t.pos > l.prevPos ? "next" : "previous") + " fancybox-fx-" + e,
                c.removeAttr("style").removeClass("fancybox-slide--current fancybox-slide--next fancybox-slide--previous").addClass(o),
                t.$content.removeClass("fancybox-is-hidden"),
                f(c),
                void n.fancybox.animate(c, "fancybox-slide--current", r, function(e) {
                    c.removeClass(o).removeAttr("style"),
                    t.pos === l.currPos && l.complete()
                }, !0)) : (f(c),
                t.$content.removeClass("fancybox-is-hidden"),
                void (t.pos === l.currPos && l.complete())))
            },
            getThumbPos: function(i) {
                var o, r = this, s = !1, a = i.opts.$thumb, l = a ? a.offset() : 0;
                return l && a[0].ownerDocument === e && function(e) {
                    for (var i = e[0], o = i.getBoundingClientRect(), r = []; null !== i.parentElement; )
                        "hidden" !== n(i.parentElement).css("overflow") && "auto" !== n(i.parentElement).css("overflow") || r.push(i.parentElement.getBoundingClientRect()),
                        i = i.parentElement;
                    return r.every(function(t) {
                        var e = Math.min(o.right, t.right) - Math.max(o.left, t.left)
                          , n = Math.min(o.bottom, t.bottom) - Math.max(o.top, t.top);
                        return e > 0 && n > 0
                    }) && o.bottom > 0 && o.right > 0 && o.left < n(t).width() && o.top < n(t).height()
                }(a) && (o = r.$refs.stage.offset(),
                s = {
                    top: l.top - o.top + parseFloat(a.css("border-top-width") || 0),
                    left: l.left - o.left + parseFloat(a.css("border-left-width") || 0),
                    width: a.width(),
                    height: a.height(),
                    scaleX: 1,
                    scaleY: 1
                }),
                s
            },
            complete: function() {
                var t = this
                  , i = t.current
                  , o = {};
                i.isMoved || !i.isLoaded || i.isComplete || (i.isComplete = !0,
                i.$slide.siblings().trigger("onReset"),
                f(i.$slide),
                i.$slide.addClass("fancybox-slide--complete"),
                n.each(t.slides, function(e, i) {
                    i.pos >= t.currPos - 1 && i.pos <= t.currPos + 1 ? o[i.pos] = i : i && (n.fancybox.stop(i.$slide),
                    i.$slide.unbind().remove())
                }),
                t.slides = o,
                t.updateCursor(),
                t.trigger("afterShow"),
                (n(e.activeElement).is("[disabled]") || i.opts.autoFocus && "image" != i.type && "iframe" !== i.type) && t.focus())
            },
            preload: function() {
                var t, e, n = this;
                n.group.length < 2 || (t = n.slides[n.currPos + 1],
                e = n.slides[n.currPos - 1],
                t && "image" === t.type && n.loadSlide(t),
                e && "image" === e.type && n.loadSlide(e))
            },
            focus: function() {
                var t, e = this.current;
                this.isClosing || (t = e && e.isComplete ? e.$slide.find("button,:input,[tabindex],a").filter(":not([disabled]):visible:first") : null,
                t = t && t.length ? t : this.$refs.container,
                t.focus())
            },
            activate: function() {
                var t = this;
                n(".fancybox-container").each(function() {
                    var e = n(this).data("FancyBox");
                    e && e.uid !== t.uid && !e.isClosing && e.trigger("onDeactivate")
                }),
                t.current && (t.$refs.container.index() > 0 && t.$refs.container.prependTo(e.body),
                t.updateControls()),
                t.trigger("onActivate"),
                t.addEvents()
            },
          close: function (t, e) {
           // alert('dgdsgdsg');
                var i, o, r, s, a, l, c = this, f = c.current, p = function() {
                    c.cleanUp(t)
                };
                return !(c.isClosing || (c.isClosing = !0,
                !1 === c.trigger("beforeClose", t) ? (c.isClosing = !1,
                u(function() {
                    c.update()
                }),
                1) : (c.removeEvents(),
                f.timouts && clearTimeout(f.timouts),
                r = f.$content,
                i = f.opts.animationEffect,
                o = n.isNumeric(e) ? e : i ? f.opts.animationDuration : 0,
                f.$slide.off(d).removeClass("fancybox-slide--complete fancybox-slide--next fancybox-slide--previous fancybox-animated"),
                f.$slide.siblings().trigger("onReset").remove(),
                o && c.$refs.container.removeClass("fancybox-is-open").addClass("fancybox-is-closing"),
                c.hideLoading(f),
                c.hideControls(),
                c.updateCursor(),
                "zoom" !== i || !0 !== t && r && o && "image" === f.type && !f.hasError && (l = c.getThumbPos(f)) || (i = "fade"),
                "zoom" === i ? (n.fancybox.stop(r),
                a = n.fancybox.getTranslate(r),
                a.width = a.width * a.scaleX,
                a.height = a.height * a.scaleY,
                s = f.opts.zoomOpacity,
                "auto" == s && (s = Math.abs(f.width / f.height - l.width / l.height) > .1),
                s && (l.opacity = 0),
                a.scaleX = a.width / l.width,
                a.scaleY = a.height / l.height,
                a.width = l.width,
                a.height = l.height,
                n.fancybox.setTranslate(f.$content, a),
                n.fancybox.animate(f.$content, l, o, p),
                0) : (i && o ? !0 === t ? setTimeout(p, o) : n.fancybox.animate(f.$slide.removeClass("fancybox-slide--current"), "fancybox-animated fancybox-slide--previous fancybox-fx-" + i, o, p) : p(),
                0))))
            },
            cleanUp: function(t) {
                var e, i = this;
                i.current.$slide.trigger("onReset"),
                i.$refs.container.empty().remove(),
                i.trigger("afterClose", t),
                i.$lastFocus && !i.current.focusBack && i.$lastFocus.focus(),
                i.current = null,
                e = n.fancybox.getInstance(),
                e ? e.activate() : (s.scrollTop(i.scrollTop).scrollLeft(i.scrollLeft),
                n("html").removeClass("fancybox-enabled"),
                n("#fancybox-style-noscroll").remove())
            },
            trigger: function(t, e) {
                var i, o = Array.prototype.slice.call(arguments, 1), r = this, s = e && e.opts ? e : r.current;
                return s ? o.unshift(s) : s = r,
                o.unshift(r),
                n.isFunction(s.opts[t]) && (i = s.opts[t].apply(s, o)),
                !1 === i ? i : void ("afterClose" === t ? a.trigger(t + ".fb", o) : r.$refs.container.trigger(t + ".fb", o))
            },
            updateControls: function(t) {
                var e = this
                  , i = e.current
                  , o = i.index
                  , r = i.opts
                  , s = r.caption
                  , a = e.$refs.caption;
                i.$slide.trigger("refresh"),
                e.$caption = s && s.length ? a.html(s) : null,
                e.isHiddenControls || e.showControls(),
                n("[data-fancybox-count]").html(e.group.length),
                n("[data-fancybox-index]").html(o + 1),
                n("[data-fancybox-prev]").prop("disabled", !r.loop && o <= 0),
                n("[data-fancybox-next]").prop("disabled", !r.loop && o >= e.group.length - 1)
            },
            hideControls: function() {
                this.isHiddenControls = !0,
                this.$refs.container.removeClass("fancybox-show-infobar fancybox-show-toolbar fancybox-show-caption fancybox-show-nav")
            },
            showControls: function() {
                var t = this
                  , e = t.current ? t.current.opts : t.opts
                  , n = t.$refs.container;
                t.isHiddenControls = !1,
                t.idleSecondsCounter = 0,
                n.toggleClass("fancybox-show-toolbar", !(!e.toolbar || !e.buttons)).toggleClass("fancybox-show-infobar", !!(e.infobar && t.group.length > 1)).toggleClass("fancybox-show-nav", !!(e.arrows && t.group.length > 1)).toggleClass("fancybox-is-modal", !!e.modal),
                t.$caption ? n.addClass("fancybox-show-caption ") : n.removeClass("fancybox-show-caption")
            },
            toggleControls: function() {
                this.isHiddenControls ? this.showControls() : this.hideControls()
            }
        }),
        n.fancybox = {
            version: "3.1.20",
            defaults: r,
            getInstance: function(t) {
                var e = n('.fancybox-container:not(".fancybox-is-closing"):first').data("FancyBox")
                  , i = Array.prototype.slice.call(arguments, 1);
                return e instanceof p && ("string" === n.type(t) ? e[t].apply(e, i) : "function" === n.type(t) && t.apply(e, i),
                e)
            },
            open: function(t, e, n) {
                return new p(t,e,n)
            },
            close: function(t) {
                var e = this.getInstance();
                e && (e.close(),
                !0 === t && this.close())
            },
            destroy: function() {
                this.close(!0),
                a.off("click.fb-start")
            },
            isMobile: e.createTouch !== i && /Android|webOS|iPhone|iPad|iPod|BlackBerry/i.test(navigator.userAgent),
            use3d: function() {
                var n = e.createElement("div");
                return t.getComputedStyle && t.getComputedStyle(n).getPropertyValue("transform") && !(e.documentMode && e.documentMode < 11)
            }(),
            getTranslate: function(t) {
                var e;
                if (!t || !t.length)
                    return !1;
                if (e = t.eq(0).css("transform"),
                e && -1 !== e.indexOf("matrix") ? (e = e.split("(")[1],
                e = e.split(")")[0],
                e = e.split(",")) : e = [],
                e.length)
                    e = e.length > 10 ? [e[13], e[12], e[0], e[5]] : [e[5], e[4], e[0], e[3]],
                    e = e.map(parseFloat);
                else {
                    e = [0, 0, 1, 1];
                    var n = /\.*translate\((.*)px,(.*)px\)/i
                      , i = n.exec(t.eq(0).attr("style"));
                    i && (e[0] = parseFloat(i[2]),
                    e[1] = parseFloat(i[1]))
                }
                return {
                    top: e[0],
                    left: e[1],
                    scaleX: e[2],
                    scaleY: e[3],
                    opacity: parseFloat(t.css("opacity")),
                    width: t.width(),
                    height: t.height()
                }
            },
            setTranslate: function(t, e) {
                var n = ""
                  , o = {};
                if (t && e)
                    return e.left === i && e.top === i || (n = (e.left === i ? t.position().left : e.left) + "px, " + (e.top === i ? t.position().top : e.top) + "px",
                    n = this.use3d ? "translate3d(" + n + ", 0px)" : "translate(" + n + ")"),
                    e.scaleX !== i && e.scaleY !== i && (n = (n.length ? n + " " : "") + "scale(" + e.scaleX + ", " + e.scaleY + ")"),
                    n.length && (o.transform = n),
                    e.opacity !== i && (o.opacity = e.opacity),
                    e.width !== i && (o.width = e.width),
                    e.height !== i && (o.height = e.height),
                    t.css(o)
            },
            animate: function(t, e, o, r, s) {
                var a = d || "transitionend";
                n.isFunction(o) && (r = o,
                o = null),
                n.isPlainObject(e) || t.removeAttr("style"),
                t.on(a, function(o) {
                    (!o || !o.originalEvent || t.is(o.originalEvent.target) && "z-index" != o.originalEvent.propertyName) && (t.off(a),
                    n.isPlainObject(e) ? e.scaleX !== i && e.scaleY !== i && (t.css("transition-duration", "0ms"),
                    e.width = t.width() * e.scaleX,
                    e.height = t.height() * e.scaleY,
                    e.scaleX = 1,
                    e.scaleY = 1,
                    n.fancybox.setTranslate(t, e)) : !0 !== s && t.removeClass(e),
                    n.isFunction(r) && r(o))
                }),
                n.isNumeric(o) && t.css("transition-duration", o + "ms"),
                n.isPlainObject(e) ? n.fancybox.setTranslate(t, e) : t.addClass(e),
                t.data("timer", setTimeout(function() {
                    t.trigger("transitionend")
                }, o + 16))
            },
            stop: function(t) {
                clearTimeout(t.data("timer")),
                t.off(d)
            }
        },
        n.fn.fancybox = function(t) {
            var e;
            return t = t || {},
            e = t.selector || !1,
            e ? n("body").off("click.fb-start", e).on("click.fb-start", e, {
                items: n(e),
                options: t
            }, o) : this.off("click.fb-start").on("click.fb-start", {
                items: this,
                options: t
            }, o),
            this
        }
        ,
        a.on("click.fb-start", "[data-fancybox]", o)
    }
}(window, document, window.jQuery),
function(t) {
    "use strict";
    var e = function(e, n, i) {
        if (e)
            return i = i || "",
            "object" === t.type(i) && (i = t.param(i, !0)),
            t.each(n, function(t, n) {
                e = e.replace("$" + t, n || "")
            }),
            i.length && (e += (e.indexOf("?") > 0 ? "&" : "?") + i),
            e
    }
      , n = {
        youtube: {
            matcher: /(youtube\.com|youtu\.be|youtube\-nocookie\.com)\/(watch\?(.*&)?v=|v\/|u\/|embed\/?)?(videoseries\?list=(.*)|[\w-]{11}|\?listType=(.*)&list=(.*))(.*)/i,
            params: {
                autoplay: 1,
                autohide: 1,
                fs: 1,
                rel: 0,
                hd: 1,
                wmode: "transparent",
                enablejsapi: 1,
                html5: 1
            },
            paramPlace: 8,
            type: "iframe",
            url: "//www.youtube.com/embed/$4",
            thumb: "//img.youtube.com/vi/$4/hqdefault.jpg"
        },
        vimeo: {
            matcher: /^.+vimeo.com\/(.*\/)?([\d]+)(.*)?/,
            params: {
                autoplay: 1,
                hd: 1,
                show_title: 1,
                show_byline: 1,
                show_portrait: 0,
                fullscreen: 1,
                api: 1
            },
            paramPlace: 3,
            type: "iframe",
            url: "//player.vimeo.com/video/$2"
        },
        metacafe: {
            matcher: /metacafe.com\/watch\/(\d+)\/(.*)?/,
            type: "iframe",
            url: "//www.metacafe.com/embed/$1/?ap=1"
        },
        dailymotion: {
            matcher: /dailymotion.com\/video\/(.*)\/?(.*)/,
            params: {
                additionalInfos: 0,
                autoStart: 1
            },
            type: "iframe",
            url: "//www.dailymotion.com/embed/video/$1"
        },
        vine: {
            matcher: /vine.co\/v\/([a-zA-Z0-9\?\=\-]+)/,
            type: "iframe",
            url: "//vine.co/v/$1/embed/simple"
        },
        instagram: {
            matcher: /(instagr\.am|instagram\.com)\/p\/([a-zA-Z0-9_\-]+)\/?/i,
            type: "image",
            url: "//$1/p/$2/media/?size=l"
        },
        google_maps: {
            matcher: /(maps\.)?google\.([a-z]{2,3}(\.[a-z]{2})?)\/(((maps\/(place\/(.*)\/)?\@(.*),(\d+.?\d+?)z))|(\?ll=))(.*)?/i,
            type: "iframe",
            url: function(t) {
                return "//maps.google." + t[2] + "/?ll=" + (t[9] ? t[9] + "&z=" + Math.floor(t[10]) + (t[12] ? t[12].replace(/^\//, "&") : "") : t[12]) + "&output=" + (t[12] && t[12].indexOf("layer=c") > 0 ? "svembed" : "embed")
            }
        }
    };
    t(document).on("onInit.fb", function(i, o) {
        t.each(o.group, function(i, o) {
            var r, s, a, l, c, u, d, f = o.src || "", p = !1;
            o.type || (r = t.extend(!0, {}, n, o.opts.media),
            t.each(r, function(n, i) {
                if (a = f.match(i.matcher),
                u = {},
                d = n,
                a) {
                    if (p = i.type,
                    i.paramPlace && a[i.paramPlace]) {
                        c = a[i.paramPlace],
                        "?" == c[0] && (c = c.substring(1)),
                        c = c.split("&");
                        for (var r = 0; r < c.length; ++r) {
                            var h = c[r].split("=", 2);
                            2 == h.length && (u[h[0]] = decodeURIComponent(h[1].replace(/\+/g, " ")))
                        }
                    }
                    return l = t.extend(!0, {}, i.params, o.opts[n], u),
                    f = "function" === t.type(i.url) ? i.url.call(this, a, l, o) : e(i.url, a, l),
                    s = "function" === t.type(i.thumb) ? i.thumb.call(this, a, l, o) : e(i.thumb, a),
                    "vimeo" === d && (f = f.replace("&%23", "#")),
                    !1
                }
            }),
            p ? (o.src = f,
            o.type = p,
            o.opts.thumb || o.opts.$thumb && o.opts.$thumb.length || (o.opts.thumb = s),
            "iframe" === p && (t.extend(!0, o.opts, {
                iframe: {
                    preload: !1,
                    attr: {
                        scrolling: "no"
                    }
                }
            }),
            o.contentProvider = d,
            o.opts.slideClass += " fancybox-slide--" + ("google_maps" == d ? "map" : "video"))) : o.type = "image")
        })
    })
}(window.jQuery),
function(t, e, n) {
    "use strict";
    var i = function() {
        return t.requestAnimationFrame || t.webkitRequestAnimationFrame || t.mozRequestAnimationFrame || t.oRequestAnimationFrame || function(e) {
            return t.setTimeout(e, 1e3 / 60)
        }
    }()
      , o = function() {
        return t.cancelAnimationFrame || t.webkitCancelAnimationFrame || t.mozCancelAnimationFrame || t.oCancelAnimationFrame || function(e) {
            t.clearTimeout(e)
        }
    }()
      , r = function(e) {
        var n = [];
        e = e.originalEvent || e || t.e,
        e = e.touches && e.touches.length ? e.touches : e.changedTouches && e.changedTouches.length ? e.changedTouches : [e];
        for (var i in e)
            e[i].pageX ? n.push({
                x: e[i].pageX,
                y: e[i].pageY
            }) : e[i].clientX && n.push({
                x: e[i].clientX,
                y: e[i].clientY
            });
        return n
    }
      , s = function(t, e, n) {
        return e && t ? "x" === n ? t.x - e.x : "y" === n ? t.y - e.y : Math.sqrt(Math.pow(t.x - e.x, 2) + Math.pow(t.y - e.y, 2)) : 0
    }
      , a = function(t) {
        if (t.is("a,button,input,select,textarea") || n.isFunction(t.get(0).onclick))
            return !0;
        for (var e = 0, i = t[0].attributes, o = i.length; e < o; e++)
            if ("data-fancybox-" === i[e].nodeName.substr(0, 14))
                return !0;
        return !1
    }
      , l = function(e) {
        var n = t.getComputedStyle(e)["overflow-y"]
          , i = t.getComputedStyle(e)["overflow-x"]
          , o = ("scroll" === n || "auto" === n) && e.scrollHeight > e.clientHeight
          , r = ("scroll" === i || "auto" === i) && e.scrollWidth > e.clientWidth;
        return o || r
    }
      , c = function(t) {
        for (var e = !1; !(e = l(t.get(0))) && (t = t.parent(),
        t.length && !t.hasClass("fancybox-stage") && !t.is("body")); )
            ;
        return e
    }
      , u = function(t) {
        var e = this;
        e.instance = t,
        e.$bg = t.$refs.bg,
        e.$stage = t.$refs.stage,
        e.$container = t.$refs.container,
        e.destroy(),
        e.$container.on("touchstart.fb.touch mousedown.fb.touch", n.proxy(e, "ontouchstart"))
    };
    u.prototype.destroy = function() {
        this.$container.off(".fb.touch")
    }
    ,
    u.prototype.ontouchstart = function(i) {
        var o = this
          , l = n(i.target)
          , u = o.instance
          , d = u.current
          , f = d.$content
          , p = "touchstart" == i.type;
        if (p && o.$container.off("mousedown.fb.touch"),
        !d || o.instance.isAnimating || o.instance.isClosing)
            return i.stopPropagation(),
            void i.preventDefault();
        if ((!i.originalEvent || 2 != i.originalEvent.button) && l.length && !a(l) && !a(l.parent()) && !(i.originalEvent.clientX > l[0].clientWidth + l.offset().left) && (o.startPoints = r(i),
        o.startPoints && !(o.startPoints.length > 1 && u.isSliding))) {
            if (o.$target = l,
            o.$content = f,
            o.canTap = !0,
            n(e).off(".fb.touch"),
            n(e).on(p ? "touchend.fb.touch touchcancel.fb.touch" : "mouseup.fb.touch mouseleave.fb.touch", n.proxy(o, "ontouchend")),
            n(e).on(p ? "touchmove.fb.touch" : "mousemove.fb.touch", n.proxy(o, "ontouchmove")),
            i.stopPropagation(),
            !u.current.opts.touch && !u.canPan() || !l.is(o.$stage) && !o.$stage.find(l).length)
                return void (l.is("img") && i.preventDefault());
            n.fancybox.isMobile && (c(o.$target) || c(o.$target.parent())) || i.preventDefault(),
            o.canvasWidth = Math.round(d.$slide[0].clientWidth),
            o.canvasHeight = Math.round(d.$slide[0].clientHeight),
            o.startTime = (new Date).getTime(),
            o.distanceX = o.distanceY = o.distance = 0,
            o.isPanning = !1,
            o.isSwiping = !1,
            o.isZooming = !1,
            o.sliderStartPos = o.sliderLastPos || {
                top: 0,
                left: 0
            },
            o.contentStartPos = n.fancybox.getTranslate(o.$content),
            o.contentLastPos = null,
            1 !== o.startPoints.length || o.isZooming || (o.canTap = !u.isSliding,
            "image" === d.type && (o.contentStartPos.width > o.canvasWidth + 1 || o.contentStartPos.height > o.canvasHeight + 1) ? (n.fancybox.stop(o.$content),
            o.$content.css("transition-duration", "0ms"),
            o.isPanning = !0) : o.isSwiping = !0,
            o.$container.addClass("fancybox-controls--isGrabbing")),
            2 !== o.startPoints.length || u.isAnimating || d.hasError || "image" !== d.type || !d.isLoaded && !d.$ghost || (o.isZooming = !0,
            o.isSwiping = !1,
            o.isPanning = !1,
            n.fancybox.stop(o.$content),
            o.$content.css("transition-duration", "0ms"),
            o.centerPointStartX = .5 * (o.startPoints[0].x + o.startPoints[1].x) - n(t).scrollLeft(),
            o.centerPointStartY = .5 * (o.startPoints[0].y + o.startPoints[1].y) - n(t).scrollTop(),
            o.percentageOfImageAtPinchPointX = (o.centerPointStartX - o.contentStartPos.left) / o.contentStartPos.width,
            o.percentageOfImageAtPinchPointY = (o.centerPointStartY - o.contentStartPos.top) / o.contentStartPos.height,
            o.startDistanceBetweenFingers = s(o.startPoints[0], o.startPoints[1]))
        }
    }
    ,
    u.prototype.ontouchmove = function(t) {
        var e = this;
        if (e.newPoints = r(t),
        n.fancybox.isMobile && (c(e.$target) || c(e.$target.parent())))
            return t.stopPropagation(),
            void (e.canTap = !1);
        if ((e.instance.current.opts.touch || e.instance.canPan()) && e.newPoints && e.newPoints.length && (e.distanceX = s(e.newPoints[0], e.startPoints[0], "x"),
        e.distanceY = s(e.newPoints[0], e.startPoints[0], "y"),
        e.distance = s(e.newPoints[0], e.startPoints[0]),
        e.distance > 0)) {
            if (!e.$target.is(e.$stage) && !e.$stage.find(e.$target).length)
                return;
            t.stopPropagation(),
            t.preventDefault(),
            e.isSwiping ? e.onSwipe() : e.isPanning ? e.onPan() : e.isZooming && e.onZoom()
        }
    }
    ,
    u.prototype.onSwipe = function() {
        var e, r = this, s = r.isSwiping, a = r.sliderStartPos.left || 0;
        !0 === s ? Math.abs(r.distance) > 10 && (r.canTap = !1,
        r.instance.group.length < 2 && r.instance.opts.touch.vertical ? r.isSwiping = "y" : r.instance.isSliding || !1 === r.instance.opts.touch.vertical || "auto" === r.instance.opts.touch.vertical && n(t).width() > 800 ? r.isSwiping = "x" : (e = Math.abs(180 * Math.atan2(r.distanceY, r.distanceX) / Math.PI),
        r.isSwiping = e > 45 && e < 135 ? "y" : "x"),
        r.instance.isSliding = r.isSwiping,
        r.startPoints = r.newPoints,
        n.each(r.instance.slides, function(t, e) {
            n.fancybox.stop(e.$slide),
            e.$slide.css("transition-duration", "0ms"),
            e.inTransition = !1,
            e.pos === r.instance.current.pos && (r.sliderStartPos.left = n.fancybox.getTranslate(e.$slide).left)
        }),
        r.instance.SlideShow && r.instance.SlideShow.isActive && r.instance.SlideShow.stop()) : ("x" == s && (r.distanceX > 0 && (r.instance.group.length < 2 || 0 === r.instance.current.index && !r.instance.current.opts.loop) ? a += Math.pow(r.distanceX, .8) : r.distanceX < 0 && (r.instance.group.length < 2 || r.instance.current.index === r.instance.group.length - 1 && !r.instance.current.opts.loop) ? a -= Math.pow(-r.distanceX, .8) : a += r.distanceX),
        r.sliderLastPos = {
            top: "x" == s ? 0 : r.sliderStartPos.top + r.distanceY,
            left: a
        },
        r.requestId && (o(r.requestId),
        r.requestId = null),
        r.requestId = i(function() {
            r.sliderLastPos && (n.each(r.instance.slides, function(t, e) {
                var i = e.pos - r.instance.currPos;
                n.fancybox.setTranslate(e.$slide, {
                    top: r.sliderLastPos.top,
                    left: r.sliderLastPos.left + i * r.canvasWidth + i * e.opts.gutter
                })
            }),
            r.$container.addClass("fancybox-is-sliding"))
        }))
    }
    ,
    u.prototype.onPan = function() {
        var t, e, r, s = this;
        s.canTap = !1,
        t = s.contentStartPos.width > s.canvasWidth ? s.contentStartPos.left + s.distanceX : s.contentStartPos.left,
        e = s.contentStartPos.top + s.distanceY,
        r = s.limitMovement(t, e, s.contentStartPos.width, s.contentStartPos.height),
        r.scaleX = s.contentStartPos.scaleX,
        r.scaleY = s.contentStartPos.scaleY,
        s.contentLastPos = r,
        s.requestId && (o(s.requestId),
        s.requestId = null),
        s.requestId = i(function() {
            n.fancybox.setTranslate(s.$content, s.contentLastPos)
        })
    }
    ,
    u.prototype.limitMovement = function(t, e, n, i) {
        var o, r, s, a, l = this, c = l.canvasWidth, u = l.canvasHeight, d = l.contentStartPos.left, f = l.contentStartPos.top, p = l.distanceX, h = l.distanceY;
        return o = Math.max(0, .5 * c - .5 * n),
        r = Math.max(0, .5 * u - .5 * i),
        s = Math.min(c - n, .5 * c - .5 * n),
        a = Math.min(u - i, .5 * u - .5 * i),
        n > c && (p > 0 && t > o && (t = o - 1 + Math.pow(-o + d + p, .8) || 0),
        p < 0 && t < s && (t = s + 1 - Math.pow(s - d - p, .8) || 0)),
        i > u && (h > 0 && e > r && (e = r - 1 + Math.pow(-r + f + h, .8) || 0),
        h < 0 && e < a && (e = a + 1 - Math.pow(a - f - h, .8) || 0)),
        {
            top: e,
            left: t
        }
    }
    ,
    u.prototype.limitPosition = function(t, e, n, i) {
        var o = this
          , r = o.canvasWidth
          , s = o.canvasHeight;
        return n > r ? (t = t > 0 ? 0 : t,
        t = t < r - n ? r - n : t) : t = Math.max(0, r / 2 - n / 2),
        i > s ? (e = e > 0 ? 0 : e,
        e = e < s - i ? s - i : e) : e = Math.max(0, s / 2 - i / 2),
        {
            top: e,
            left: t
        }
    }
    ,
    u.prototype.onZoom = function() {
        var e = this
          , r = e.contentStartPos.width
          , a = e.contentStartPos.height
          , l = e.contentStartPos.left
          , c = e.contentStartPos.top
          , u = s(e.newPoints[0], e.newPoints[1])
          , d = u / e.startDistanceBetweenFingers
          , f = Math.floor(r * d)
          , p = Math.floor(a * d)
          , h = (r - f) * e.percentageOfImageAtPinchPointX
          , g = (a - p) * e.percentageOfImageAtPinchPointY
          , v = (e.newPoints[0].x + e.newPoints[1].x) / 2 - n(t).scrollLeft()
          , m = (e.newPoints[0].y + e.newPoints[1].y) / 2 - n(t).scrollTop()
          , y = v - e.centerPointStartX
          , b = m - e.centerPointStartY
          , w = l + (h + y)
          , E = c + (g + b)
          , C = {
            top: E,
            left: w,
            scaleX: e.contentStartPos.scaleX * d,
            scaleY: e.contentStartPos.scaleY * d
        };
        e.canTap = !1,
        e.newWidth = f,
        e.newHeight = p,
        e.contentLastPos = C,
        e.requestId && (o(e.requestId),
        e.requestId = null),
        e.requestId = i(function() {
            n.fancybox.setTranslate(e.$content, e.contentLastPos)
        })
    }
    ,
    u.prototype.ontouchend = function(t) {
        var i = this
          , s = Math.max((new Date).getTime() - i.startTime, 1)
          , a = i.isSwiping
          , l = i.isPanning
          , c = i.isZooming;
        return i.endPoints = r(t),
        i.$container.removeClass("fancybox-controls--isGrabbing"),
        n(e).off(".fb.touch"),
        i.requestId && (o(i.requestId),
        i.requestId = null),
        i.isSwiping = !1,
        i.isPanning = !1,
        i.isZooming = !1,
        i.canTap ? i.onTap(t) : (i.speed = 366,
        i.velocityX = i.distanceX / s * .5,
        i.velocityY = i.distanceY / s * .5,
        i.speedX = Math.max(.5 * i.speed, Math.min(1.5 * i.speed, 1 / Math.abs(i.velocityX) * i.speed)),
        void (l ? i.endPanning() : c ? i.endZooming() : i.endSwiping(a)))
    }
    ,
    u.prototype.endSwiping = function(t) {
        var e = this
          , i = !1;
        e.instance.isSliding = !1,
        e.sliderLastPos = null,
        "y" == t && Math.abs(e.distanceY) > 50 ? (n.fancybox.animate(e.instance.current.$slide, {
            top: e.sliderStartPos.top + e.distanceY + 150 * e.velocityY,
            opacity: 0
        }, 150),
        i = e.instance.close(!0, 300)) : "x" == t && e.distanceX > 50 && e.instance.group.length > 1 ? i = e.instance.previous(e.speedX) : "x" == t && e.distanceX < -50 && e.instance.group.length > 1 && (i = e.instance.next(e.speedX)),
        !1 !== i || "x" != t && "y" != t || e.instance.jumpTo(e.instance.current.index, 150),
        e.$container.removeClass("fancybox-is-sliding")
    }
    ,
    u.prototype.endPanning = function() {
        var t, e, i, o = this;
        o.contentLastPos && (!1 === o.instance.current.opts.touch.momentum ? (t = o.contentLastPos.left,
        e = o.contentLastPos.top) : (t = o.contentLastPos.left + o.velocityX * o.speed,
        e = o.contentLastPos.top + o.velocityY * o.speed),
        i = o.limitPosition(t, e, o.contentStartPos.width, o.contentStartPos.height),
        i.width = o.contentStartPos.width,
        i.height = o.contentStartPos.height,
        n.fancybox.animate(o.$content, i, 330))
    }
    ,
    u.prototype.endZooming = function() {
        var t, e, i, o, r = this, s = r.instance.current, a = r.newWidth, l = r.newHeight;
        r.contentLastPos && (t = r.contentLastPos.left,
        e = r.contentLastPos.top,
        o = {
            top: e,
            left: t,
            width: a,
            height: l,
            scaleX: 1,
            scaleY: 1
        },
        n.fancybox.setTranslate(r.$content, o),
        a < r.canvasWidth && l < r.canvasHeight ? r.instance.scaleToFit(150) : a > s.width || l > s.height ? r.instance.scaleToActual(r.centerPointStartX, r.centerPointStartY, 150) : (i = r.limitPosition(t, e, a, l),
        n.fancybox.setTranslate(r.content, n.fancybox.getTranslate(r.$content)),
        n.fancybox.animate(r.$content, i, 150)))
    }
    ,
    u.prototype.onTap = function(t) {
        var e, i = this, o = n(t.target), s = i.instance, a = s.current, l = t && r(t) || i.startPoints, c = l[0] ? l[0].x - i.$stage.offset().left : 0, u = l[0] ? l[0].y - i.$stage.offset().top : 0, d = function(e) {
            var o = a.opts[e];
            if (n.isFunction(o) && (o = o.apply(s, [a, t])),
            o)
                switch (o) {
                case "close":
                    s.close(i.startEvent);
                    break;
                case "toggleControls":
                    s.toggleControls(!0);
                    break;
                case "next":
                    s.next();
                    break;
                case "nextOrClose":
                    s.group.length > 1 ? s.next() : s.close(i.startEvent);
                    break;
                case "zoom":
                    "image" == a.type && (a.isLoaded || a.$ghost) && (s.canPan() ? s.scaleToFit() : s.isScaledDown() ? s.scaleToActual(c, u) : s.group.length < 2 && s.close(i.startEvent))
                }
        };
        if (!(t.originalEvent && 2 == t.originalEvent.button || s.isSliding || c > o[0].clientWidth + o.offset().left)) {
            if (o.is(".fancybox-bg,.fancybox-inner,.fancybox-outer,.fancybox-container"))
                e = "Outside";
            else if (o.is(".fancybox-slide"))
                e = "Slide";
            else {
                if (!s.current.$content || !s.current.$content.has(t.target).length)
                    return;
                e = "Content"
            }
            if (i.tapped) {
                if (clearTimeout(i.tapped),
                i.tapped = null,
                Math.abs(c - i.tapX) > 50 || Math.abs(u - i.tapY) > 50 || s.isSliding)
                    return this;
                d("dblclick" + e)
            } else
                i.tapX = c,
                i.tapY = u,
                a.opts["dblclick" + e] && a.opts["dblclick" + e] !== a.opts["click" + e] ? i.tapped = setTimeout(function() {
                    i.tapped = null,
                    d("click" + e)
                }, 300) : d("click" + e);
            return this
        }
    }
    ,
    n(e).on("onActivate.fb", function(t, e) {
        e && !e.Guestures && (e.Guestures = new u(e))
    }),
    n(e).on("beforeClose.fb", function(t, e) {
        e && e.Guestures && e.Guestures.destroy()
    })
}(window, document, window.jQuery),
function(t, e) {
    "use strict";
    var n = function(t) {
        this.instance = t,
        this.init()
    };
    e.extend(n.prototype, {
        timer: null,
        isActive: !1,
        $button: null,
        speed: 3e3,
        init: function() {
            var t = this;
            t.$button = t.instance.$refs.toolbar.find("[data-fancybox-play]").on("click", function() {
                t.toggle()
            }),
            (t.instance.group.length < 2 || !t.instance.group[t.instance.currIndex].opts.slideShow) && t.$button.hide()
        },
        set: function() {
            var t = this;
            t.instance && t.instance.current && (t.instance.current.opts.loop || t.instance.currIndex < t.instance.group.length - 1) ? t.timer = setTimeout(function() {
                t.instance.next()
            }, t.instance.current.opts.slideShow.speed || t.speed) : (t.stop(),
            t.instance.idleSecondsCounter = 0,
            t.instance.showControls())
        },
        clear: function() {
            var t = this;
            clearTimeout(t.timer),
            t.timer = null
        },
        start: function() {
            var t = this
              , e = t.instance.current;
            t.instance && e && (e.opts.loop || e.index < t.instance.group.length - 1) && (t.isActive = !0,
            t.$button.attr("title", e.opts.i18n[e.opts.lang].PLAY_STOP).addClass("fancybox-button--pause"),
            e.isComplete && t.set())
        },
        stop: function() {
            var t = this
              , e = t.instance.current;
            t.clear(),
            t.$button.attr("title", e.opts.i18n[e.opts.lang].PLAY_START).removeClass("fancybox-button--pause"),
            t.isActive = !1
        },
        toggle: function() {
            var t = this;
            t.isActive ? t.stop() : t.start()
        }
    }),
    e(t).on({
        "onInit.fb": function(t, e) {
            e && !e.SlideShow && (e.SlideShow = new n(e))
        },
        "beforeShow.fb": function(t, e, n, i) {
            var o = e && e.SlideShow;
            i ? o && n.opts.slideShow.autoStart && o.start() : o && o.isActive && o.clear()
        },
        "afterShow.fb": function(t, e, n) {
            var i = e && e.SlideShow;
            i && i.isActive && i.set()
        },
        "afterKeydown.fb": function(n, i, o, r, s) {
            var a = i && i.SlideShow;
            !a || !o.opts.slideShow || 80 !== s && 32 !== s || e(t.activeElement).is("button,a,input") || (r.preventDefault(),
            a.toggle())
        },
        "beforeClose.fb onDeactivate.fb": function(t, e) {
            var n = e && e.SlideShow;
            n && n.stop()
        }
    }),
    e(t).on("visibilitychange", function() {
        var n = e.fancybox.getInstance()
          , i = n && n.SlideShow;
        i && i.isActive && (t.hidden ? i.clear() : i.set())
    })
}(document, window.jQuery),
function(t, e) {
    "use strict";
    var n = function() {
        var e, n, i, o = [["requestFullscreen", "exitFullscreen", "fullscreenElement", "fullscreenEnabled", "fullscreenchange", "fullscreenerror"], ["webkitRequestFullscreen", "webkitExitFullscreen", "webkitFullscreenElement", "webkitFullscreenEnabled", "webkitfullscreenchange", "webkitfullscreenerror"], ["webkitRequestFullScreen", "webkitCancelFullScreen", "webkitCurrentFullScreenElement", "webkitCancelFullScreen", "webkitfullscreenchange", "webkitfullscreenerror"], ["mozRequestFullScreen", "mozCancelFullScreen", "mozFullScreenElement", "mozFullScreenEnabled", "mozfullscreenchange", "mozfullscreenerror"], ["msRequestFullscreen", "msExitFullscreen", "msFullscreenElement", "msFullscreenEnabled", "MSFullscreenChange", "MSFullscreenError"]], r = {};
        for (n = 0; n < o.length; n++)
            if ((e = o[n]) && e[1]in t) {
                for (i = 0; i < e.length; i++)
                    r[o[0][i]] = e[i];
                return r
            }
        return !1
    }();
    if (!n)
        return void (e.fancybox.defaults.btnTpl.fullScreen = !1);
    var i = {
        request: function(e) {
            e = e || t.documentElement,
            e[n.requestFullscreen](e.ALLOW_KEYBOARD_INPUT)
        },
        exit: function() {
            t[n.exitFullscreen]()
        },
        toggle: function(e) {
            e = e || t.documentElement,
            this.isFullscreen() ? this.exit() : this.request(e)
        },
        isFullscreen: function() {
            return Boolean(t[n.fullscreenElement])
        },
        enabled: function() {
            return Boolean(t[n.fullscreenEnabled])
        }
    };
    e(t).on({
        "onInit.fb": function(t, e) {
            var n, o = e.$refs.toolbar.find("[data-fancybox-fullscreen]");
            e && !e.FullScreen && e.group[e.currIndex].opts.fullScreen ? (n = e.$refs.container,
            n.on("click.fb-fullscreen", "[data-fancybox-fullscreen]", function(t) {
                t.stopPropagation(),
                t.preventDefault(),
                i.toggle(n[0])
            }),
            e.opts.fullScreen && !0 === e.opts.fullScreen.autoStart && i.request(n[0]),
            e.FullScreen = i) : o.hide()
        },
        "afterKeydown.fb": function(t, e, n, i, o) {
            e && e.FullScreen && 70 === o && (i.preventDefault(),
            e.FullScreen.toggle(e.$refs.container[0]))
        },
        "beforeClose.fb": function(t) {
            t && t.FullScreen && i.exit()
        }
    }),
    e(t).on(n.fullscreenchange, function() {
        var t = e.fancybox.getInstance();
        t.current && "image" === t.current.type && t.isAnimating && (t.current.$content.css("transition", "none"),
        t.isAnimating = !1,
        t.update(!0, !0, 0))
    })
}(document, window.jQuery),
function(t, e) {
    "use strict";
    var n = function(t) {
        this.instance = t,
        this.init()
    };
    e.extend(n.prototype, {
        $button: null,
        $grid: null,
        $list: null,
        isVisible: !1,
        init: function() {
            var t = this
              , e = t.instance.group[0]
              , n = t.instance.group[1];
            t.$button = t.instance.$refs.toolbar.find("[data-fancybox-thumbs]"),
            t.instance.group.length > 1 && t.instance.group[t.instance.currIndex].opts.thumbs && ("image" == e.type || e.opts.thumb || e.opts.$thumb) && ("image" == n.type || n.opts.thumb || n.opts.$thumb) ? (t.$button.on("click", function() {
                t.toggle()
            }),
            t.isActive = !0) : (t.$button.hide(),
            t.isActive = !1)
        },
        create: function() {
            var t, n, i = this.instance;
            this.$grid = e('<div class="fancybox-thumbs"></div>').appendTo(i.$refs.container),
            t = "<ul>",
            e.each(i.group, function(e, i) {
                n = i.opts.thumb || (i.opts.$thumb ? i.opts.$thumb.attr("src") : null),
                n || "image" !== i.type || (n = i.src),
                n && n.length && (t += '<li data-index="' + e + '"  tabindex="0" class="fancybox-thumbs-loading"><img data-src="' + n + '" /></li>')
            }),
            t += "</ul>",
            this.$list = e(t).appendTo(this.$grid).on("click", "li", function() {
                i.jumpTo(e(this).data("index"))
            }),
            this.$list.find("img").hide().one("load", function() {
                var t, n, i, o, r = e(this).parent().removeClass("fancybox-thumbs-loading"), s = r.outerWidth(), a = r.outerHeight();
                t = this.naturalWidth || this.width,
                n = this.naturalHeight || this.height,
                i = t / s,
                o = n / a,
                i >= 1 && o >= 1 && (i > o ? (t /= o,
                n = a) : (t = s,
                n /= i)),
                e(this).css({
                    width: Math.floor(t),
                    height: Math.floor(n),
                    "margin-top": Math.min(0, Math.floor(.3 * a - .3 * n)),
                    "margin-left": Math.min(0, Math.floor(.5 * s - .5 * t))
                }).show()
            }).each(function() {
                this.src = e(this).data("src")
            })
        },
        focus: function() {
            this.instance.current && this.$list.children().removeClass("fancybox-thumbs-active").filter('[data-index="' + this.instance.current.index + '"]').addClass("fancybox-thumbs-active").focus()
        },
        close: function() {
            this.$grid.hide()
        },
        update: function() {
            this.instance.$refs.container.toggleClass("fancybox-show-thumbs", this.isVisible),
            this.isVisible ? (this.$grid || this.create(),
            this.instance.trigger("onThumbsShow"),
            this.focus()) : this.$grid && this.instance.trigger("onThumbsHide"),
            this.instance.update()
        },
        hide: function() {
            this.isVisible = !1,
            this.update()
        },
        show: function() {
            this.isVisible = !0,
            this.update()
        },
        toggle: function() {
            this.isVisible = !this.isVisible,
            this.update()
        }
    }),
    e(t).on({
        "onInit.fb": function(t, e) {
            e && !e.Thumbs && (e.Thumbs = new n(e))
        },
        "beforeShow.fb": function(t, e, n, i) {
            var o = e && e.Thumbs;
            if (o && o.isActive) {
                if (n.modal)
                    return o.$button.hide(),
                    void o.hide();
                i && !0 === e.opts.thumbs.autoStart && o.show(),
                o.isVisible && o.focus()
            }
        },
        "afterKeydown.fb": function(t, e, n, i, o) {
            var r = e && e.Thumbs;
            r && r.isActive && 71 === o && (i.preventDefault(),
            r.toggle())
        },
        "beforeClose.fb": function(t, e) {
            var n = e && e.Thumbs;
            n && n.isVisible && !1 !== e.opts.thumbs.hideOnClose && n.close()
        }
    })
}(document, window.jQuery),
function(t, e, n) {
    "use strict";
    function i() {
        var t = e.location.hash.substr(1)
          , n = t.split("-")
          , i = n.length > 1 && /^\+?\d+$/.test(n[n.length - 1]) ? parseInt(n.pop(-1), 10) || 1 : 1
          , o = n.join("-");
        return i < 1 && (i = 1),
        {
            hash: t,
            index: i,
            gallery: o
        }
    }
    function o(t) {
        var e;
        "" !== t.gallery && (e = n("[data-fancybox='" + n.escapeSelector(t.gallery) + "']").eq(t.index - 1),
        e.length ? e.trigger("click") : n("#" + n.escapeSelector(t.gallery)).trigger("click"))
    }
    function r(t) {
        var e;
        return !!t && (e = t.current ? t.current.opts : t.opts,
        e.$orig ? e.$orig.data("fancybox") : e.hash || "")
    }
    n.escapeSelector || (n.escapeSelector = function(t) {
        return (t + "").replace(/([\0-\x1f\x7f]|^-?\d)|^-$|[^\x80-\uFFFF\w-]/g, function(t, e) {
            return e ? "\0" === t ? "�" : t.slice(0, -1) + "\\" + t.charCodeAt(t.length - 1).toString(16) + " " : "\\" + t
        })
    }
    );
    var s = null
      , a = null;
    n(function() {
        setTimeout(function() {
            !1 !== n.fancybox.defaults.hash && (n(t).on({
                "onInit.fb": function(t, e) {
                    var n, o;
                    !1 !== e.group[e.currIndex].opts.hash && (n = i(),
                    (o = r(e)) && n.gallery && o == n.gallery && (e.currIndex = n.index - 1))
                },
                "beforeShow.fb": function(n, i, o, l) {
                    var c;
                    !1 !== o.opts.hash && (c = r(i)) && "" !== c && (e.location.hash.indexOf(c) < 0 && (i.opts.origHash = e.location.hash),
                    s = c + (i.group.length > 1 ? "-" + (o.index + 1) : ""),
                    "replaceState"in e.history ? (a && clearTimeout(a),
                    a = setTimeout(function() {
                        e.history[l ? "pushState" : "replaceState"]({}, t.title, e.location.pathname + e.location.search + "#" + s),
                        a = null
                    }, 300)) : e.location.hash = s)
                },
                "beforeClose.fb": function(i, o, l) {
                    var c, u;
                    a && clearTimeout(a),
                    !1 !== l.opts.hash && (c = r(o),
                    u = o && o.opts.origHash ? o.opts.origHash : "",
                    c && "" !== c && ("replaceState"in history ? e.history.replaceState({}, t.title, e.location.pathname + e.location.search + u) : (e.location.hash = u,
                    n(e).scrollTop(o.scrollTop).scrollLeft(o.scrollLeft))),
                    s = null)
                }
            }),
            n(e).on("hashchange.fb", function() {
                var t = i();
                n.fancybox.getInstance() ? !s || s === t.gallery + "-" + t.index || 1 === t.index && s == t.gallery || (s = null,
                n.fancybox.close()) : "" !== t.gallery && o(t)
            }),
            n(e).one("unload.fb popstate.fb", function() {
                n.fancybox.getInstance("close", !0, 0)
            }),
            o(i()))
        }, 50)
    })
}(document, window, window.jQuery),
function(t) {
    "use strict";
    "function" == typeof define && define.amd ? define(["jquery"], t) : "undefined" != typeof exports ? module.exports = t(require("jquery")) : t(jQuery)
}(function(t) {
    "use strict";
    var e = window.Slick || {};
    e = function() {
        function e(e, i) {
            var o, r = this;
            r.defaults = {
                accessibility: !0,
                adaptiveHeight: !1,
                appendArrows: t(e),
                appendDots: t(e),
                arrows: !0,
                asNavFor: null,
                prevArrow: '<button type="button" data-role="none" class="slick-prev" aria-label="Previous" tabindex="0" role="button">Previous</button>',
                nextArrow: '<button type="button" data-role="none" class="slick-next" aria-label="Next" tabindex="0" role="button">Next</button>',
                autoplay: !1,
                autoplaySpeed: 3e3,
                centerMode: !1,
                centerPadding: "50px",
                cssEase: "ease",
                customPaging: function(e, n) {
                    return t('<button type="button" data-role="none" role="button" tabindex="0" />').text(n + 1)
                },
                dots: !1,
                dotsClass: "slick-dots",
                draggable: !0,
                easing: "linear",
                edgeFriction: .35,
                fade: !1,
                focusOnSelect: !1,
                infinite: !0,
                initialSlide: 0,
                lazyLoad: "ondemand",
                mobileFirst: !1,
                pauseOnHover: !0,
                pauseOnFocus: !0,
                pauseOnDotsHover: !1,
                respondTo: "window",
                responsive: null,
                rows: 1,
                rtl: !1,
                slide: "",
                slidesPerRow: 1,
                slidesToShow: 1,
                slidesToScroll: 1,
                speed: 500,
                swipe: !0,
                swipeToSlide: !1,
                touchMove: !0,
                touchThreshold: 5,
                useCSS: !0,
                useTransform: !0,
                variableWidth: !1,
                vertical: !1,
                verticalSwiping: !1,
                waitForAnimate: !0,
                zIndex: 1e3
            },
            r.initials = {
                animating: !1,
                dragging: !1,
                autoPlayTimer: null,
                currentDirection: 0,
                currentLeft: null,
                currentSlide: 0,
                direction: 1,
                $dots: null,
                listWidth: null,
                listHeight: null,
                loadIndex: 0,
                $nextArrow: null,
                $prevArrow: null,
                slideCount: null,
                slideWidth: null,
                $slideTrack: null,
                $slides: null,
                sliding: !1,
                slideOffset: 0,
                swipeLeft: null,
                $list: null,
                touchObject: {},
                transformsEnabled: !1,
                unslicked: !1
            },
            t.extend(r, r.initials),
            r.activeBreakpoint = null,
            r.animType = null,
            r.animProp = null,
            r.breakpoints = [],
            r.breakpointSettings = [],
            r.cssTransitions = !1,
            r.focussed = !1,
            r.interrupted = !1,
            r.hidden = "hidden",
            r.paused = !0,
            r.positionProp = null,
            r.respondTo = null,
            r.rowCount = 1,
            r.shouldClick = !0,
            r.$slider = t(e),
            r.$slidesCache = null,
            r.transformType = null,
            r.transitionType = null,
            r.visibilityChange = "visibilitychange",
            r.windowWidth = 0,
            r.windowTimer = null,
            o = t(e).data("slick") || {},
            r.options = t.extend({}, r.defaults, i, o),
            r.currentSlide = r.options.initialSlide,
            r.originalSettings = r.options,
            void 0 !== document.mozHidden ? (r.hidden = "mozHidden",
            r.visibilityChange = "mozvisibilitychange") : void 0 !== document.webkitHidden && (r.hidden = "webkitHidden",
            r.visibilityChange = "webkitvisibilitychange"),
            r.autoPlay = t.proxy(r.autoPlay, r),
            r.autoPlayClear = t.proxy(r.autoPlayClear, r),
            r.autoPlayIterator = t.proxy(r.autoPlayIterator, r),
            r.changeSlide = t.proxy(r.changeSlide, r),
            r.clickHandler = t.proxy(r.clickHandler, r),
            r.selectHandler = t.proxy(r.selectHandler, r),
            r.setPosition = t.proxy(r.setPosition, r),
            r.swipeHandler = t.proxy(r.swipeHandler, r),
            r.dragHandler = t.proxy(r.dragHandler, r),
            r.keyHandler = t.proxy(r.keyHandler, r),
            r.instanceUid = n++,
            r.htmlExpr = /^(?:\s*(<[\w\W]+>)[^>]*)$/,
            r.registerBreakpoints(),
            r.init(!0)
        }
        var n = 0;
        return e
    }(),
    e.prototype.activateADA = function() {
        this.$slideTrack.find(".slick-active").attr({
            "aria-hidden": "false"
        }).find("a, input, button, select").attr({
            tabindex: "0"
        })
    }
    ,
    e.prototype.addSlide = e.prototype.slickAdd = function(e, n, i) {
        var o = this;
        if ("boolean" == typeof n)
            i = n,
            n = null;
        else if (n < 0 || n >= o.slideCount)
            return !1;
        o.unload(),
        "number" == typeof n ? 0 === n && 0 === o.$slides.length ? t(e).appendTo(o.$slideTrack) : i ? t(e).insertBefore(o.$slides.eq(n)) : t(e).insertAfter(o.$slides.eq(n)) : !0 === i ? t(e).prependTo(o.$slideTrack) : t(e).appendTo(o.$slideTrack),
        o.$slides = o.$slideTrack.children(this.options.slide),
        o.$slideTrack.children(this.options.slide).detach(),
        o.$slideTrack.append(o.$slides),
        o.$slides.each(function(e, n) {
            t(n).attr("data-slick-index", e)
        }),
        o.$slidesCache = o.$slides,
        o.reinit()
    }
    ,
    e.prototype.animateHeight = function() {
        var t = this;
        if (1 === t.options.slidesToShow && !0 === t.options.adaptiveHeight && !1 === t.options.vertical) {
            var e = t.$slides.eq(t.currentSlide).outerHeight(!0);
            t.$list.animate({
                height: e
            }, t.options.speed)
        }
    }
    ,
    e.prototype.animateSlide = function(e, n) {
        var i = {}
          , o = this;
        o.animateHeight(),
        !0 === o.options.rtl && !1 === o.options.vertical && (e = -e),
        !1 === o.transformsEnabled ? !1 === o.options.vertical ? o.$slideTrack.animate({
            left: e
        }, o.options.speed, o.options.easing, n) : o.$slideTrack.animate({
            top: e
        }, o.options.speed, o.options.easing, n) : !1 === o.cssTransitions ? (!0 === o.options.rtl && (o.currentLeft = -o.currentLeft),
        t({
            animStart: o.currentLeft
        }).animate({
            animStart: e
        }, {
            duration: o.options.speed,
            easing: o.options.easing,
            step: function(t) {
                t = Math.ceil(t),
                !1 === o.options.vertical ? (i[o.animType] = "translate(" + t + "px, 0px)",
                o.$slideTrack.css(i)) : (i[o.animType] = "translate(0px," + t + "px)",
                o.$slideTrack.css(i))
            },
            complete: function() {
                n && n.call()
            }
        })) : (o.applyTransition(),
        e = Math.ceil(e),
        !1 === o.options.vertical ? i[o.animType] = "translate3d(" + e + "px, 0px, 0px)" : i[o.animType] = "translate3d(0px," + e + "px, 0px)",
        o.$slideTrack.css(i),
        n && setTimeout(function() {
            o.disableTransition(),
            n.call()
        }, o.options.speed))
    }
    ,
    e.prototype.getNavTarget = function() {
        var e = this
          , n = e.options.asNavFor;
        return n && null !== n && (n = t(n).not(e.$slider)),
        n
    }
    ,
    e.prototype.asNavFor = function(e) {
        var n = this
          , i = n.getNavTarget();
        null !== i && "object" == typeof i && i.each(function() {
            var n = t(this).slick("getSlick");
            n.unslicked || n.slideHandler(e, !0)
        })
    }
    ,
    e.prototype.applyTransition = function(t) {
        var e = this
          , n = {};
        !1 === e.options.fade ? n[e.transitionType] = e.transformType + " " + e.options.speed + "ms " + e.options.cssEase : n[e.transitionType] = "opacity " + e.options.speed + "ms " + e.options.cssEase,
        !1 === e.options.fade ? e.$slideTrack.css(n) : e.$slides.eq(t).css(n)
    }
    ,
    e.prototype.autoPlay = function() {
        var t = this;
        t.autoPlayClear(),
        t.slideCount > t.options.slidesToShow && (t.autoPlayTimer = setInterval(t.autoPlayIterator, t.options.autoplaySpeed))
    }
    ,
    e.prototype.autoPlayClear = function() {
        var t = this;
        t.autoPlayTimer && clearInterval(t.autoPlayTimer)
    }
    ,
    e.prototype.autoPlayIterator = function() {
        var t = this
          , e = t.currentSlide + t.options.slidesToScroll;
        t.paused || t.interrupted || t.focussed || (!1 === t.options.infinite && (1 === t.direction && t.currentSlide + 1 === t.slideCount - 1 ? t.direction = 0 : 0 === t.direction && (e = t.currentSlide - t.options.slidesToScroll,
        t.currentSlide - 1 == 0 && (t.direction = 1))),
        t.slideHandler(e))
    }
    ,
    e.prototype.buildArrows = function() {
        var e = this;
        !0 === e.options.arrows && (e.$prevArrow = t(e.options.prevArrow).addClass("slick-arrow"),
        e.$nextArrow = t(e.options.nextArrow).addClass("slick-arrow"),
        e.slideCount > e.options.slidesToShow ? (e.$prevArrow.removeClass("slick-hidden").removeAttr("aria-hidden tabindex"),
        e.$nextArrow.removeClass("slick-hidden").removeAttr("aria-hidden tabindex"),
        e.htmlExpr.test(e.options.prevArrow) && e.$prevArrow.prependTo(e.options.appendArrows),
        e.htmlExpr.test(e.options.nextArrow) && e.$nextArrow.appendTo(e.options.appendArrows),
        !0 !== e.options.infinite && e.$prevArrow.addClass("slick-disabled").attr("aria-disabled", "true")) : e.$prevArrow.add(e.$nextArrow).addClass("slick-hidden").attr({
            "aria-disabled": "true",
            tabindex: "-1"
        }))
    }
    ,
    e.prototype.buildDots = function() {
        var e, n, i = this;
        if (!0 === i.options.dots && i.slideCount > i.options.slidesToShow) {
            for (i.$slider.addClass("slick-dotted"),
            n = t("<ul />").addClass(i.options.dotsClass),
            e = 0; e <= i.getDotCount(); e += 1)
                n.append(t("<li />").append(i.options.customPaging.call(this, i, e)));
            i.$dots = n.appendTo(i.options.appendDots),
            i.$dots.find("li").first().addClass("slick-active").attr("aria-hidden", "false")
        }
    }
    ,
    e.prototype.buildOut = function() {
        var e = this;
        e.$slides = e.$slider.children(e.options.slide + ":not(.slick-cloned)").addClass("slick-slide"),
        e.slideCount = e.$slides.length,
        e.$slides.each(function(e, n) {
            t(n).attr("data-slick-index", e).data("originalStyling", t(n).attr("style") || "")
        }),
        e.$slider.addClass("slick-slider"),
        e.$slideTrack = 0 === e.slideCount ? t('<div class="slick-track"/>').appendTo(e.$slider) : e.$slides.wrapAll('<div class="slick-track"/>').parent(),
        e.$list = e.$slideTrack.wrap('<div aria-live="polite" class="slick-list"/>').parent(),
        e.$slideTrack.css("opacity", 0),
        !0 !== e.options.centerMode && !0 !== e.options.swipeToSlide || (e.options.slidesToScroll = 1),
        t("img[data-lazy]", e.$slider).not("[src]").addClass("slick-loading"),
        e.setupInfinite(),
        e.buildArrows(),
        e.buildDots(),
        e.updateDots(),
        e.setSlideClasses("number" == typeof e.currentSlide ? e.currentSlide : 0),
        !0 === e.options.draggable && e.$list.addClass("draggable")
    }
    ,
    e.prototype.buildRows = function() {
        var t, e, n, i, o, r, s, a = this;
        if (i = document.createDocumentFragment(),
        r = a.$slider.children(),
        a.options.rows > 1) {
            for (s = a.options.slidesPerRow * a.options.rows,
            o = Math.ceil(r.length / s),
            t = 0; t < o; t++) {
                var l = document.createElement("div");
                for (e = 0; e < a.options.rows; e++) {
                    var c = document.createElement("div");
                    for (n = 0; n < a.options.slidesPerRow; n++) {
                        var u = t * s + (e * a.options.slidesPerRow + n);
                        r.get(u) && c.appendChild(r.get(u))
                    }
                    l.appendChild(c)
                }
                i.appendChild(l)
            }
            a.$slider.empty().append(i),
            a.$slider.children().children().children().css({
                width: 100 / a.options.slidesPerRow + "%",
                display: "inline-block"
            })
        }
    }
    ,
    e.prototype.checkResponsive = function(e, n) {
        var i, o, r, s = this, a = !1, l = s.$slider.width(), c = window.innerWidth || t(window).width();
        if ("window" === s.respondTo ? r = c : "slider" === s.respondTo ? r = l : "min" === s.respondTo && (r = Math.min(c, l)),
        s.options.responsive && s.options.responsive.length && null !== s.options.responsive) {
            o = null;
            for (i in s.breakpoints)
                s.breakpoints.hasOwnProperty(i) && (!1 === s.originalSettings.mobileFirst ? r < s.breakpoints[i] && (o = s.breakpoints[i]) : r > s.breakpoints[i] && (o = s.breakpoints[i]));
            null !== o ? null !== s.activeBreakpoint ? (o !== s.activeBreakpoint || n) && (s.activeBreakpoint = o,
            "unslick" === s.breakpointSettings[o] ? s.unslick(o) : (s.options = t.extend({}, s.originalSettings, s.breakpointSettings[o]),
            !0 === e && (s.currentSlide = s.options.initialSlide),
            s.refresh(e)),
            a = o) : (s.activeBreakpoint = o,
            "unslick" === s.breakpointSettings[o] ? s.unslick(o) : (s.options = t.extend({}, s.originalSettings, s.breakpointSettings[o]),
            !0 === e && (s.currentSlide = s.options.initialSlide),
            s.refresh(e)),
            a = o) : null !== s.activeBreakpoint && (s.activeBreakpoint = null,
            s.options = s.originalSettings,
            !0 === e && (s.currentSlide = s.options.initialSlide),
            s.refresh(e),
            a = o),
            e || !1 === a || s.$slider.trigger("breakpoint", [s, a])
        }
    }
    ,
    e.prototype.changeSlide = function(e, n) {
        var i, o, r, s = this, a = t(e.currentTarget);
        switch (a.is("a") && e.preventDefault(),
        a.is("li") || (a = a.closest("li")),
        r = s.slideCount % s.options.slidesToScroll != 0,
        i = r ? 0 : (s.slideCount - s.currentSlide) % s.options.slidesToScroll,
        e.data.message) {
        case "previous":
            o = 0 === i ? s.options.slidesToScroll : s.options.slidesToShow - i,
            s.slideCount > s.options.slidesToShow && s.slideHandler(s.currentSlide - o, !1, n);
            break;
        case "next":
            o = 0 === i ? s.options.slidesToScroll : i,
            s.slideCount > s.options.slidesToShow && s.slideHandler(s.currentSlide + o, !1, n);
            break;
        case "index":
            var l = 0 === e.data.index ? 0 : e.data.index || a.index() * s.options.slidesToScroll;
            s.slideHandler(s.checkNavigable(l), !1, n),
            a.children().trigger("focus");
            break;
        default:
            return
        }
    }
    ,
    e.prototype.checkNavigable = function(t) {
        var e, n, i = this;
        if (e = i.getNavigableIndexes(),
        n = 0,
        t > e[e.length - 1])
            t = e[e.length - 1];
        else
            for (var o in e) {
                if (t < e[o]) {
                    t = n;
                    break
                }
                n = e[o]
            }
        return t
    }
    ,
    e.prototype.cleanUpEvents = function() {
        var e = this;
        e.options.dots && null !== e.$dots && t("li", e.$dots).off("click.slick", e.changeSlide).off("mouseenter.slick", t.proxy(e.interrupt, e, !0)).off("mouseleave.slick", t.proxy(e.interrupt, e, !1)),
        e.$slider.off("focus.slick blur.slick"),
        !0 === e.options.arrows && e.slideCount > e.options.slidesToShow && (e.$prevArrow && e.$prevArrow.off("click.slick", e.changeSlide),
        e.$nextArrow && e.$nextArrow.off("click.slick", e.changeSlide)),
        e.$list.off("touchstart.slick mousedown.slick", e.swipeHandler),
        e.$list.off("touchmove.slick mousemove.slick", e.swipeHandler),
        e.$list.off("touchend.slick mouseup.slick", e.swipeHandler),
        e.$list.off("touchcancel.slick mouseleave.slick", e.swipeHandler),
        e.$list.off("click.slick", e.clickHandler),
        t(document).off(e.visibilityChange, e.visibility),
        e.cleanUpSlideEvents(),
        !0 === e.options.accessibility && e.$list.off("keydown.slick", e.keyHandler),
        !0 === e.options.focusOnSelect && t(e.$slideTrack).children().off("click.slick", e.selectHandler),
        t(window).off("orientationchange.slick.slick-" + e.instanceUid, e.orientationChange),
        t(window).off("resize.slick.slick-" + e.instanceUid, e.resize),
        t("[draggable!=true]", e.$slideTrack).off("dragstart", e.preventDefault),
        t(window).off("load.slick.slick-" + e.instanceUid, e.setPosition),
        t(document).off("ready.slick.slick-" + e.instanceUid, e.setPosition)
    }
    ,
    e.prototype.cleanUpSlideEvents = function() {
        var e = this;
        e.$list.off("mouseenter.slick", t.proxy(e.interrupt, e, !0)),
        e.$list.off("mouseleave.slick", t.proxy(e.interrupt, e, !1))
    }
    ,
    e.prototype.cleanUpRows = function() {
        var t, e = this;
        e.options.rows > 1 && (t = e.$slides.children().children(),
        t.removeAttr("style"),
        e.$slider.empty().append(t))
    }
    ,
    e.prototype.clickHandler = function(t) {
        !1 === this.shouldClick && (t.stopImmediatePropagation(),
        t.stopPropagation(),
        t.preventDefault())
    }
    ,
    e.prototype.destroy = function(e) {
        var n = this;
        n.autoPlayClear(),
        n.touchObject = {},
        n.cleanUpEvents(),
        t(".slick-cloned", n.$slider).detach(),
        n.$dots && n.$dots.remove(),
        n.$prevArrow && n.$prevArrow.length && (n.$prevArrow.removeClass("slick-disabled slick-arrow slick-hidden").removeAttr("aria-hidden aria-disabled tabindex").css("display", ""),
        n.htmlExpr.test(n.options.prevArrow) && n.$prevArrow.remove()),
        n.$nextArrow && n.$nextArrow.length && (n.$nextArrow.removeClass("slick-disabled slick-arrow slick-hidden").removeAttr("aria-hidden aria-disabled tabindex").css("display", ""),
        n.htmlExpr.test(n.options.nextArrow) && n.$nextArrow.remove()),
        n.$slides && (n.$slides.removeClass("slick-slide slick-active slick-center slick-visible slick-current").removeAttr("aria-hidden").removeAttr("data-slick-index").each(function() {
            t(this).attr("style", t(this).data("originalStyling"))
        }),
        n.$slideTrack.children(this.options.slide).detach(),
        n.$slideTrack.detach(),
        n.$list.detach(),
        n.$slider.append(n.$slides)),
        n.cleanUpRows(),
        n.$slider.removeClass("slick-slider"),
        n.$slider.removeClass("slick-initialized"),
        n.$slider.removeClass("slick-dotted"),
        n.unslicked = !0,
        e || n.$slider.trigger("destroy", [n])
    }
    ,
    e.prototype.disableTransition = function(t) {
        var e = this
          , n = {};
        n[e.transitionType] = "",
        !1 === e.options.fade ? e.$slideTrack.css(n) : e.$slides.eq(t).css(n)
    }
    ,
    e.prototype.fadeSlide = function(t, e) {
        var n = this;
        !1 === n.cssTransitions ? (n.$slides.eq(t).css({
            zIndex: n.options.zIndex
        }),
        n.$slides.eq(t).animate({
            opacity: 1
        }, n.options.speed, n.options.easing, e)) : (n.applyTransition(t),
        n.$slides.eq(t).css({
            opacity: 1,
            zIndex: n.options.zIndex
        }),
        e && setTimeout(function() {
            n.disableTransition(t),
            e.call()
        }, n.options.speed))
    }
    ,
    e.prototype.fadeSlideOut = function(t) {
        var e = this;
        !1 === e.cssTransitions ? e.$slides.eq(t).animate({
            opacity: 0,
            zIndex: e.options.zIndex - 2
        }, e.options.speed, e.options.easing) : (e.applyTransition(t),
        e.$slides.eq(t).css({
            opacity: 0,
            zIndex: e.options.zIndex - 2
        }))
    }
    ,
    e.prototype.filterSlides = e.prototype.slickFilter = function(t) {
        var e = this;
        null !== t && (e.$slidesCache = e.$slides,
        e.unload(),
        e.$slideTrack.children(this.options.slide).detach(),
        e.$slidesCache.filter(t).appendTo(e.$slideTrack),
        e.reinit())
    }
    ,
    e.prototype.focusHandler = function() {
        var e = this;
        e.$slider.off("focus.slick blur.slick").on("focus.slick blur.slick", "*:not(.slick-arrow)", function(n) {
            n.stopImmediatePropagation();
            var i = t(this);
            setTimeout(function() {
                e.options.pauseOnFocus && (e.focussed = i.is(":focus"),
                e.autoPlay())
            }, 0)
        })
    }
    ,
    e.prototype.getCurrent = e.prototype.slickCurrentSlide = function() {
        return this.currentSlide
    }
    ,
    e.prototype.getDotCount = function() {
        var t = this
          , e = 0
          , n = 0
          , i = 0;
        if (!0 === t.options.infinite)
            for (; e < t.slideCount; )
                ++i,
                e = n + t.options.slidesToScroll,
                n += t.options.slidesToScroll <= t.options.slidesToShow ? t.options.slidesToScroll : t.options.slidesToShow;
        else if (!0 === t.options.centerMode)
            i = t.slideCount;
        else if (t.options.asNavFor)
            for (; e < t.slideCount; )
                ++i,
                e = n + t.options.slidesToScroll,
                n += t.options.slidesToScroll <= t.options.slidesToShow ? t.options.slidesToScroll : t.options.slidesToShow;
        else
            i = 1 + Math.ceil((t.slideCount - t.options.slidesToShow) / t.options.slidesToScroll);
        return i - 1
    }
    ,
    e.prototype.getLeft = function(t) {
        var e, n, i, o = this, r = 0;
        return o.slideOffset = 0,
        n = o.$slides.first().outerHeight(!0),
        !0 === o.options.infinite ? (o.slideCount > o.options.slidesToShow && (o.slideOffset = o.slideWidth * o.options.slidesToShow * -1,
        r = n * o.options.slidesToShow * -1),
        o.slideCount % o.options.slidesToScroll != 0 && t + o.options.slidesToScroll > o.slideCount && o.slideCount > o.options.slidesToShow && (t > o.slideCount ? (o.slideOffset = (o.options.slidesToShow - (t - o.slideCount)) * o.slideWidth * -1,
        r = (o.options.slidesToShow - (t - o.slideCount)) * n * -1) : (o.slideOffset = o.slideCount % o.options.slidesToScroll * o.slideWidth * -1,
        r = o.slideCount % o.options.slidesToScroll * n * -1))) : t + o.options.slidesToShow > o.slideCount && (o.slideOffset = (t + o.options.slidesToShow - o.slideCount) * o.slideWidth,
        r = (t + o.options.slidesToShow - o.slideCount) * n),
        o.slideCount <= o.options.slidesToShow && (o.slideOffset = 0,
        r = 0),
        !0 === o.options.centerMode && !0 === o.options.infinite ? o.slideOffset += o.slideWidth * Math.floor(o.options.slidesToShow / 2) - o.slideWidth : !0 === o.options.centerMode && (o.slideOffset = 0,
        o.slideOffset += o.slideWidth * Math.floor(o.options.slidesToShow / 2)),
        e = !1 === o.options.vertical ? t * o.slideWidth * -1 + o.slideOffset : t * n * -1 + r,
        !0 === o.options.variableWidth && (i = o.slideCount <= o.options.slidesToShow || !1 === o.options.infinite ? o.$slideTrack.children(".slick-slide").eq(t) : o.$slideTrack.children(".slick-slide").eq(t + o.options.slidesToShow),
        e = !0 === o.options.rtl ? i[0] ? -1 * (o.$slideTrack.width() - i[0].offsetLeft - i.width()) : 0 : i[0] ? -1 * i[0].offsetLeft : 0,
        !0 === o.options.centerMode && (i = o.slideCount <= o.options.slidesToShow || !1 === o.options.infinite ? o.$slideTrack.children(".slick-slide").eq(t) : o.$slideTrack.children(".slick-slide").eq(t + o.options.slidesToShow + 1),
        e = !0 === o.options.rtl ? i[0] ? -1 * (o.$slideTrack.width() - i[0].offsetLeft - i.width()) : 0 : i[0] ? -1 * i[0].offsetLeft : 0,
        e += (o.$list.width() - i.outerWidth()) / 2)),
        e
    }
    ,
    e.prototype.getOption = e.prototype.slickGetOption = function(t) {
        return this.options[t]
    }
    ,
    e.prototype.getNavigableIndexes = function() {
        var t, e = this, n = 0, i = 0, o = [];
        for (!1 === e.options.infinite ? t = e.slideCount : (n = -1 * e.options.slidesToScroll,
        i = -1 * e.options.slidesToScroll,
        t = 2 * e.slideCount); n < t; )
            o.push(n),
            n = i + e.options.slidesToScroll,
            i += e.options.slidesToScroll <= e.options.slidesToShow ? e.options.slidesToScroll : e.options.slidesToShow;
        return o
    }
    ,
    e.prototype.getSlick = function() {
        return this
    }
    ,
    e.prototype.getSlideCount = function() {
        var e, n, i = this;
        return n = !0 === i.options.centerMode ? i.slideWidth * Math.floor(i.options.slidesToShow / 2) : 0,
        !0 === i.options.swipeToSlide ? (i.$slideTrack.find(".slick-slide").each(function(o, r) {
            if (r.offsetLeft - n + t(r).outerWidth() / 2 > -1 * i.swipeLeft)
                return e = r,
                !1
        }),
        Math.abs(t(e).attr("data-slick-index") - i.currentSlide) || 1) : i.options.slidesToScroll
    }
    ,
    e.prototype.goTo = e.prototype.slickGoTo = function(t, e) {
        this.changeSlide({
            data: {
                message: "index",
                index: parseInt(t)
            }
        }, e)
    }
    ,
    e.prototype.init = function(e) {
        var n = this;
        t(n.$slider).hasClass("slick-initialized") || (t(n.$slider).addClass("slick-initialized"),
        n.buildRows(),
        n.buildOut(),
        n.setProps(),
        n.startLoad(),
        n.loadSlider(),
        n.initializeEvents(),
        n.updateArrows(),
        n.updateDots(),
        n.checkResponsive(!0),
        n.focusHandler()),
        e && n.$slider.trigger("init", [n]),
        !0 === n.options.accessibility && n.initADA(),
        n.options.autoplay && (n.paused = !1,
        n.autoPlay())
    }
    ,
    e.prototype.initADA = function() {
        var e = this;
        e.$slides.add(e.$slideTrack.find(".slick-cloned")).attr({
            "aria-hidden": "true",
            tabindex: "-1"
        }).find("a, input, button, select").attr({
            tabindex: "-1"
        }),
        e.$slideTrack.attr("role", "listbox"),
        e.$slides.not(e.$slideTrack.find(".slick-cloned")).each(function(n) {
            t(this).attr({
                role: "option",
                "aria-describedby": "slick-slide" + e.instanceUid + n
            })
        }),
        null !== e.$dots && e.$dots.attr("role", "tablist").find("li").each(function(n) {
            t(this).attr({
                role: "presentation",
                "aria-selected": "false",
                "aria-controls": "navigation" + e.instanceUid + n,
                id: "slick-slide" + e.instanceUid + n
            })
        }).first().attr("aria-selected", "true").end().find("button").attr("role", "button").end().closest("div").attr("role", "toolbar"),
        e.activateADA()
    }
    ,
    e.prototype.initArrowEvents = function() {
        var t = this;
        !0 === t.options.arrows && t.slideCount > t.options.slidesToShow && (t.$prevArrow.off("click.slick").on("click.slick", {
            message: "previous"
        }, t.changeSlide),
        t.$nextArrow.off("click.slick").on("click.slick", {
            message: "next"
        }, t.changeSlide))
    }
    ,
    e.prototype.initDotEvents = function() {
        var e = this;
        !0 === e.options.dots && e.slideCount > e.options.slidesToShow && t("li", e.$dots).on("click.slick", {
            message: "index"
        }, e.changeSlide),
        !0 === e.options.dots && !0 === e.options.pauseOnDotsHover && t("li", e.$dots).on("mouseenter.slick", t.proxy(e.interrupt, e, !0)).on("mouseleave.slick", t.proxy(e.interrupt, e, !1))
    }
    ,
    e.prototype.initSlideEvents = function() {
        var e = this;
        e.options.pauseOnHover && (e.$list.on("mouseenter.slick", t.proxy(e.interrupt, e, !0)),
        e.$list.on("mouseleave.slick", t.proxy(e.interrupt, e, !1)))
    }
    ,
    e.prototype.initializeEvents = function() {
        var e = this;
        e.initArrowEvents(),
        e.initDotEvents(),
        e.initSlideEvents(),
        e.$list.on("touchstart.slick mousedown.slick", {
            action: "start"
        }, e.swipeHandler),
        e.$list.on("touchmove.slick mousemove.slick", {
            action: "move"
        }, e.swipeHandler),
        e.$list.on("touchend.slick mouseup.slick", {
            action: "end"
        }, e.swipeHandler),
        e.$list.on("touchcancel.slick mouseleave.slick", {
            action: "end"
        }, e.swipeHandler),
        e.$list.on("click.slick", e.clickHandler),
        t(document).on(e.visibilityChange, t.proxy(e.visibility, e)),
        !0 === e.options.accessibility && e.$list.on("keydown.slick", e.keyHandler),
        !0 === e.options.focusOnSelect && t(e.$slideTrack).children().on("click.slick", e.selectHandler),
        t(window).on("orientationchange.slick.slick-" + e.instanceUid, t.proxy(e.orientationChange, e)),
        t(window).on("resize.slick.slick-" + e.instanceUid, t.proxy(e.resize, e)),
        t("[draggable!=true]", e.$slideTrack).on("dragstart", e.preventDefault),
        t(window).on("load.slick.slick-" + e.instanceUid, e.setPosition),
        t(document).on("ready.slick.slick-" + e.instanceUid, e.setPosition)
    }
    ,
    e.prototype.initUI = function() {
        var t = this;
        !0 === t.options.arrows && t.slideCount > t.options.slidesToShow && (t.$prevArrow.show(),
        t.$nextArrow.show()),
        !0 === t.options.dots && t.slideCount > t.options.slidesToShow && t.$dots.show()
    }
    ,
    e.prototype.keyHandler = function(t) {
        var e = this;
        t.target.tagName.match("TEXTAREA|INPUT|SELECT") || (37 === t.keyCode && !0 === e.options.accessibility ? e.changeSlide({
            data: {
                message: !0 === e.options.rtl ? "next" : "previous"
            }
        }) : 39 === t.keyCode && !0 === e.options.accessibility && e.changeSlide({
            data: {
                message: !0 === e.options.rtl ? "previous" : "next"
            }
        }))
    }
    ,
    e.prototype.lazyLoad = function() {
        function e(e) {
            t("img[data-lazy]", e).each(function() {
                var e = t(this)
                  , n = t(this).attr("data-lazy")
                  , i = document.createElement("img");
                i.onload = function() {
                    e.animate({
                        opacity: 0
                    }, 100, function() {
                        e.attr("src", n).animate({
                            opacity: 1
                        }, 200, function() {
                            e.removeAttr("data-lazy").removeClass("slick-loading")
                        }),
                        s.$slider.trigger("lazyLoaded", [s, e, n])
                    })
                }
                ,
                i.onerror = function() {
                    e.removeAttr("data-lazy").removeClass("slick-loading").addClass("slick-lazyload-error"),
                    s.$slider.trigger("lazyLoadError", [s, e, n])
                }
                ,
                i.src = n
            })
        }
        var n, i, o, r, s = this;
        !0 === s.options.centerMode ? !0 === s.options.infinite ? (o = s.currentSlide + (s.options.slidesToShow / 2 + 1),
        r = o + s.options.slidesToShow + 2) : (o = Math.max(0, s.currentSlide - (s.options.slidesToShow / 2 + 1)),
        r = s.options.slidesToShow / 2 + 1 + 2 + s.currentSlide) : (o = s.options.infinite ? s.options.slidesToShow + s.currentSlide : s.currentSlide,
        r = Math.ceil(o + s.options.slidesToShow),
        !0 === s.options.fade && (o > 0 && o--,
        r <= s.slideCount && r++)),
        n = s.$slider.find(".slick-slide").slice(o, r),
        e(n),
        s.slideCount <= s.options.slidesToShow ? (i = s.$slider.find(".slick-slide"),
        e(i)) : s.currentSlide >= s.slideCount - s.options.slidesToShow ? (i = s.$slider.find(".slick-cloned").slice(0, s.options.slidesToShow),
        e(i)) : 0 === s.currentSlide && (i = s.$slider.find(".slick-cloned").slice(-1 * s.options.slidesToShow),
        e(i))
    }
    ,
    e.prototype.loadSlider = function() {
        var t = this;
        t.setPosition(),
        t.$slideTrack.css({
            opacity: 1
        }),
        t.$slider.removeClass("slick-loading"),
        t.initUI(),
        "progressive" === t.options.lazyLoad && t.progressiveLazyLoad()
    }
    ,
    e.prototype.next = e.prototype.slickNext = function() {
        this.changeSlide({
            data: {
                message: "next"
            }
        })
    }
    ,
    e.prototype.orientationChange = function() {
        var t = this;
        t.checkResponsive(),
        t.setPosition()
    }
    ,
    e.prototype.pause = e.prototype.slickPause = function() {
        var t = this;
        t.autoPlayClear(),
        t.paused = !0
    }
    ,
    e.prototype.play = e.prototype.slickPlay = function() {
        var t = this;
        t.autoPlay(),
        t.options.autoplay = !0,
        t.paused = !1,
        t.focussed = !1,
        t.interrupted = !1
    }
    ,
    e.prototype.postSlide = function(t) {
        var e = this;
        e.unslicked || (e.$slider.trigger("afterChange", [e, t]),
        e.animating = !1,
        e.setPosition(),
        e.swipeLeft = null,
        e.options.autoplay && e.autoPlay(),
        !0 === e.options.accessibility && e.initADA())
    }
    ,
    e.prototype.prev = e.prototype.slickPrev = function() {
        this.changeSlide({
            data: {
                message: "previous"
            }
        })
    }
    ,
    e.prototype.preventDefault = function(t) {
        t.preventDefault()
    }
    ,
    e.prototype.progressiveLazyLoad = function(e) {
        e = e || 1;
        var n, i, o, r = this, s = t("img[data-lazy]", r.$slider);
        s.length ? (n = s.first(),
        i = n.attr("data-lazy"),
        o = document.createElement("img"),
        o.onload = function() {
            n.attr("src", i).removeAttr("data-lazy").removeClass("slick-loading"),
            !0 === r.options.adaptiveHeight && r.setPosition(),
            r.$slider.trigger("lazyLoaded", [r, n, i]),
            r.progressiveLazyLoad()
        }
        ,
        o.onerror = function() {
            e < 3 ? setTimeout(function() {
                r.progressiveLazyLoad(e + 1)
            }, 500) : (n.removeAttr("data-lazy").removeClass("slick-loading").addClass("slick-lazyload-error"),
            r.$slider.trigger("lazyLoadError", [r, n, i]),
            r.progressiveLazyLoad())
        }
        ,
        o.src = i) : r.$slider.trigger("allImagesLoaded", [r])
    }
    ,
    e.prototype.refresh = function(e) {
        var n, i, o = this;
        i = o.slideCount - o.options.slidesToShow,
        !o.options.infinite && o.currentSlide > i && (o.currentSlide = i),
        o.slideCount <= o.options.slidesToShow && (o.currentSlide = 0),
        n = o.currentSlide,
        o.destroy(!0),
        t.extend(o, o.initials, {
            currentSlide: n
        }),
        o.init(),
        e || o.changeSlide({
            data: {
                message: "index",
                index: n
            }
        }, !1)
    }
    ,
    e.prototype.registerBreakpoints = function() {
        var e, n, i, o = this, r = o.options.responsive || null;
        if ("array" === t.type(r) && r.length) {
            o.respondTo = o.options.respondTo || "window";
            for (e in r)
                if (i = o.breakpoints.length - 1,
                n = r[e].breakpoint,
                r.hasOwnProperty(e)) {
                    for (; i >= 0; )
                        o.breakpoints[i] && o.breakpoints[i] === n && o.breakpoints.splice(i, 1),
                        i--;
                    o.breakpoints.push(n),
                    o.breakpointSettings[n] = r[e].settings
                }
            o.breakpoints.sort(function(t, e) {
                return o.options.mobileFirst ? t - e : e - t
            })
        }
    }
    ,
    e.prototype.reinit = function() {
        var e = this;
        e.$slides = e.$slideTrack.children(e.options.slide).addClass("slick-slide"),
        e.slideCount = e.$slides.length,
        e.currentSlide >= e.slideCount && 0 !== e.currentSlide && (e.currentSlide = e.currentSlide - e.options.slidesToScroll),
        e.slideCount <= e.options.slidesToShow && (e.currentSlide = 0),
        e.registerBreakpoints(),
        e.setProps(),
        e.setupInfinite(),
        e.buildArrows(),
        e.updateArrows(),
        e.initArrowEvents(),
        e.buildDots(),
        e.updateDots(),
        e.initDotEvents(),
        e.cleanUpSlideEvents(),
        e.initSlideEvents(),
        e.checkResponsive(!1, !0),
        !0 === e.options.focusOnSelect && t(e.$slideTrack).children().on("click.slick", e.selectHandler),
        e.setSlideClasses("number" == typeof e.currentSlide ? e.currentSlide : 0),
        e.setPosition(),
        e.focusHandler(),
        e.paused = !e.options.autoplay,
        e.autoPlay(),
        e.$slider.trigger("reInit", [e])
    }
    ,
    e.prototype.resize = function() {
        var e = this;
        t(window).width() !== e.windowWidth && (clearTimeout(e.windowDelay),
        e.windowDelay = window.setTimeout(function() {
            e.windowWidth = t(window).width(),
            e.checkResponsive(),
            e.unslicked || e.setPosition()
        }, 50))
    }
    ,
    e.prototype.removeSlide = e.prototype.slickRemove = function(t, e, n) {
        var i = this;
        if ("boolean" == typeof t ? (e = t,
        t = !0 === e ? 0 : i.slideCount - 1) : t = !0 === e ? --t : t,
        i.slideCount < 1 || t < 0 || t > i.slideCount - 1)
            return !1;
        i.unload(),
        !0 === n ? i.$slideTrack.children().remove() : i.$slideTrack.children(this.options.slide).eq(t).remove(),
        i.$slides = i.$slideTrack.children(this.options.slide),
        i.$slideTrack.children(this.options.slide).detach(),
        i.$slideTrack.append(i.$slides),
        i.$slidesCache = i.$slides,
        i.reinit()
    }
    ,
    e.prototype.setCSS = function(t) {
        var e, n, i = this, o = {};
        !0 === i.options.rtl && (t = -t),
        e = "left" == i.positionProp ? Math.ceil(t) + "px" : "0px",
        n = "top" == i.positionProp ? Math.ceil(t) + "px" : "0px",
        o[i.positionProp] = t,
        !1 === i.transformsEnabled ? i.$slideTrack.css(o) : (o = {},
        !1 === i.cssTransitions ? (o[i.animType] = "translate(" + e + ", " + n + ")",
        i.$slideTrack.css(o)) : (o[i.animType] = "translate3d(" + e + ", " + n + ", 0px)",
        i.$slideTrack.css(o)))
    }
    ,
    e.prototype.setDimensions = function() {
        var t = this;
        !1 === t.options.vertical ? !0 === t.options.centerMode && t.$list.css({
            padding: "0px " + t.options.centerPadding
        }) : (t.$list.height(t.$slides.first().outerHeight(!0) * t.options.slidesToShow),
        !0 === t.options.centerMode && t.$list.css({
            padding: t.options.centerPadding + " 0px"
        })),
        t.listWidth = t.$list.width(),
        t.listHeight = t.$list.height(),
        !1 === t.options.vertical && !1 === t.options.variableWidth ? (t.slideWidth = Math.ceil(t.listWidth / t.options.slidesToShow),
        t.$slideTrack.width(Math.ceil(t.slideWidth * t.$slideTrack.children(".slick-slide").length))) : !0 === t.options.variableWidth ? t.$slideTrack.width(5e3 * t.slideCount) : (t.slideWidth = Math.ceil(t.listWidth),
        t.$slideTrack.height(Math.ceil(t.$slides.first().outerHeight(!0) * t.$slideTrack.children(".slick-slide").length)));
        var e = t.$slides.first().outerWidth(!0) - t.$slides.first().width();
        !1 === t.options.variableWidth && t.$slideTrack.children(".slick-slide").width(t.slideWidth - e)
    }
    ,
    e.prototype.setFade = function() {
        var e, n = this;
        n.$slides.each(function(i, o) {
            e = n.slideWidth * i * -1,
            !0 === n.options.rtl ? t(o).css({
                position: "relative",
                right: e,
                top: 0,
                zIndex: n.options.zIndex - 2,
                opacity: 0
            }) : t(o).css({
                position: "relative",
                left: e,
                top: 0,
                zIndex: n.options.zIndex - 2,
                opacity: 0
            })
        }),
        n.$slides.eq(n.currentSlide).css({
            zIndex: n.options.zIndex - 1,
            opacity: 1
        })
    }
    ,
    e.prototype.setHeight = function() {
        var t = this;
        if (1 === t.options.slidesToShow && !0 === t.options.adaptiveHeight && !1 === t.options.vertical) {
            var e = t.$slides.eq(t.currentSlide).outerHeight(!0);
            t.$list.css("height", e)
        }
    }
    ,
    e.prototype.setOption = e.prototype.slickSetOption = function() {
        var e, n, i, o, r, s = this, a = !1;
        if ("object" === t.type(arguments[0]) ? (i = arguments[0],
        a = arguments[1],
        r = "multiple") : "string" === t.type(arguments[0]) && (i = arguments[0],
        o = arguments[1],
        a = arguments[2],
        "responsive" === arguments[0] && "array" === t.type(arguments[1]) ? r = "responsive" : void 0 !== arguments[1] && (r = "single")),
        "single" === r)
            s.options[i] = o;
        else if ("multiple" === r)
            t.each(i, function(t, e) {
                s.options[t] = e
            });
        else if ("responsive" === r)
            for (n in o)
                if ("array" !== t.type(s.options.responsive))
                    s.options.responsive = [o[n]];
                else {
                    for (e = s.options.responsive.length - 1; e >= 0; )
                        s.options.responsive[e].breakpoint === o[n].breakpoint && s.options.responsive.splice(e, 1),
                        e--;
                    s.options.responsive.push(o[n])
                }
        a && (s.unload(),
        s.reinit())
    }
    ,
    e.prototype.setPosition = function() {
        var t = this;
        t.setDimensions(),
        t.setHeight(),
        !1 === t.options.fade ? t.setCSS(t.getLeft(t.currentSlide)) : t.setFade(),
        t.$slider.trigger("setPosition", [t])
    }
    ,
    e.prototype.setProps = function() {
        var t = this
          , e = document.body.style;
        t.positionProp = !0 === t.options.vertical ? "top" : "left",
        "top" === t.positionProp ? t.$slider.addClass("slick-vertical") : t.$slider.removeClass("slick-vertical"),
        void 0 === e.WebkitTransition && void 0 === e.MozTransition && void 0 === e.msTransition || !0 === t.options.useCSS && (t.cssTransitions = !0),
        t.options.fade && ("number" == typeof t.options.zIndex ? t.options.zIndex < 3 && (t.options.zIndex = 3) : t.options.zIndex = t.defaults.zIndex),
        void 0 !== e.OTransform && (t.animType = "OTransform",
        t.transformType = "-o-transform",
        t.transitionType = "OTransition",
        void 0 === e.perspectiveProperty && void 0 === e.webkitPerspective && (t.animType = !1)),
        void 0 !== e.MozTransform && (t.animType = "MozTransform",
        t.transformType = "-moz-transform",
        t.transitionType = "MozTransition",
        void 0 === e.perspectiveProperty && void 0 === e.MozPerspective && (t.animType = !1)),
        void 0 !== e.webkitTransform && (t.animType = "webkitTransform",
        t.transformType = "-webkit-transform",
        t.transitionType = "webkitTransition",
        void 0 === e.perspectiveProperty && void 0 === e.webkitPerspective && (t.animType = !1)),
        void 0 !== e.msTransform && (t.animType = "msTransform",
        t.transformType = "-ms-transform",
        t.transitionType = "msTransition",
        void 0 === e.msTransform && (t.animType = !1)),
        void 0 !== e.transform && !1 !== t.animType && (t.animType = "transform",
        t.transformType = "transform",
        t.transitionType = "transition"),
        t.transformsEnabled = t.options.useTransform && null !== t.animType && !1 !== t.animType
    }
    ,
    e.prototype.setSlideClasses = function(t) {
        var e, n, i, o, r = this;
        n = r.$slider.find(".slick-slide").removeClass("slick-active slick-center slick-current").attr("aria-hidden", "true"),
        r.$slides.eq(t).addClass("slick-current"),
        !0 === r.options.centerMode ? (e = Math.floor(r.options.slidesToShow / 2),
        !0 === r.options.infinite && (t >= e && t <= r.slideCount - 1 - e ? r.$slides.slice(t - e, t + e + 1).addClass("slick-active").attr("aria-hidden", "false") : (i = r.options.slidesToShow + t,
        n.slice(i - e + 1, i + e + 2).addClass("slick-active").attr("aria-hidden", "false")),
        0 === t ? n.eq(n.length - 1 - r.options.slidesToShow).addClass("slick-center") : t === r.slideCount - 1 && n.eq(r.options.slidesToShow).addClass("slick-center")),
        r.$slides.eq(t).addClass("slick-center")) : t >= 0 && t <= r.slideCount - r.options.slidesToShow ? r.$slides.slice(t, t + r.options.slidesToShow).addClass("slick-active").attr("aria-hidden", "false") : n.length <= r.options.slidesToShow ? n.addClass("slick-active").attr("aria-hidden", "false") : (o = r.slideCount % r.options.slidesToShow,
        i = !0 === r.options.infinite ? r.options.slidesToShow + t : t,
        r.options.slidesToShow == r.options.slidesToScroll && r.slideCount - t < r.options.slidesToShow ? n.slice(i - (r.options.slidesToShow - o), i + o).addClass("slick-active").attr("aria-hidden", "false") : n.slice(i, i + r.options.slidesToShow).addClass("slick-active").attr("aria-hidden", "false")),
        "ondemand" === r.options.lazyLoad && r.lazyLoad()
    }
    ,
    e.prototype.setupInfinite = function() {
        var e, n, i, o = this;
        if (!0 === o.options.fade && (o.options.centerMode = !1),
        !0 === o.options.infinite && !1 === o.options.fade && (n = null,
        o.slideCount > o.options.slidesToShow)) {
            for (i = !0 === o.options.centerMode ? o.options.slidesToShow + 1 : o.options.slidesToShow,
            e = o.slideCount; e > o.slideCount - i; e -= 1)
                n = e - 1,
                t(o.$slides[n]).clone(!0).attr("id", "").attr("data-slick-index", n - o.slideCount).prependTo(o.$slideTrack).addClass("slick-cloned");
            for (e = 0; e < i; e += 1)
                n = e,
                t(o.$slides[n]).clone(!0).attr("id", "").attr("data-slick-index", n + o.slideCount).appendTo(o.$slideTrack).addClass("slick-cloned");
            o.$slideTrack.find(".slick-cloned").find("[id]").each(function() {
                t(this).attr("id", "")
            })
        }
    }
    ,
    e.prototype.interrupt = function(t) {
        var e = this;
        t || e.autoPlay(),
        e.interrupted = t
    }
    ,
    e.prototype.selectHandler = function(e) {
        var n = this
          , i = t(e.target).is(".slick-slide") ? t(e.target) : t(e.target).parents(".slick-slide")
          , o = parseInt(i.attr("data-slick-index"));
        if (o || (o = 0),
        n.slideCount <= n.options.slidesToShow)
            return n.setSlideClasses(o),
            void n.asNavFor(o);
        n.slideHandler(o)
    }
    ,
    e.prototype.slideHandler = function(t, e, n) {
        var i, o, r, s, a, l = null, c = this;
        if (e = e || !1,
        (!0 !== c.animating || !0 !== c.options.waitForAnimate) && !(!0 === c.options.fade && c.currentSlide === t || c.slideCount <= c.options.slidesToShow)) {
            if (!1 === e && c.asNavFor(t),
            i = t,
            l = c.getLeft(i),
            s = c.getLeft(c.currentSlide),
            c.currentLeft = null === c.swipeLeft ? s : c.swipeLeft,
            !1 === c.options.infinite && !1 === c.options.centerMode && (t < 0 || t > c.getDotCount() * c.options.slidesToScroll))
                return void (!1 === c.options.fade && (i = c.currentSlide,
                !0 !== n ? c.animateSlide(s, function() {
                    c.postSlide(i)
                }) : c.postSlide(i)));
            if (!1 === c.options.infinite && !0 === c.options.centerMode && (t < 0 || t > c.slideCount - c.options.slidesToScroll))
                return void (!1 === c.options.fade && (i = c.currentSlide,
                !0 !== n ? c.animateSlide(s, function() {
                    c.postSlide(i)
                }) : c.postSlide(i)));
            if (c.options.autoplay && clearInterval(c.autoPlayTimer),
            o = i < 0 ? c.slideCount % c.options.slidesToScroll != 0 ? c.slideCount - c.slideCount % c.options.slidesToScroll : c.slideCount + i : i >= c.slideCount ? c.slideCount % c.options.slidesToScroll != 0 ? 0 : i - c.slideCount : i,
            c.animating = !0,
            c.$slider.trigger("beforeChange", [c, c.currentSlide, o]),
            r = c.currentSlide,
            c.currentSlide = o,
            c.setSlideClasses(c.currentSlide),
            c.options.asNavFor && (a = c.getNavTarget(),
            a = a.slick("getSlick"),
            a.slideCount <= a.options.slidesToShow && a.setSlideClasses(c.currentSlide)),
            c.updateDots(),
            c.updateArrows(),
            !0 === c.options.fade)
                return !0 !== n ? (c.fadeSlideOut(r),
                c.fadeSlide(o, function() {
                    c.postSlide(o)
                })) : c.postSlide(o),
                void c.animateHeight();
            !0 !== n ? c.animateSlide(l, function() {
                c.postSlide(o)
            }) : c.postSlide(o)
        }
    }
    ,
    e.prototype.startLoad = function() {
        var t = this;
        !0 === t.options.arrows && t.slideCount > t.options.slidesToShow && (t.$prevArrow.hide(),
        t.$nextArrow.hide()),
        !0 === t.options.dots && t.slideCount > t.options.slidesToShow && t.$dots.hide(),
        t.$slider.addClass("slick-loading")
    }
    ,
    e.prototype.swipeDirection = function() {
        var t, e, n, i, o = this;
        return t = o.touchObject.startX - o.touchObject.curX,
        e = o.touchObject.startY - o.touchObject.curY,
        n = Math.atan2(e, t),
        i = Math.round(180 * n / Math.PI),
        i < 0 && (i = 360 - Math.abs(i)),
        i <= 45 && i >= 0 ? !1 === o.options.rtl ? "left" : "right" : i <= 360 && i >= 315 ? !1 === o.options.rtl ? "left" : "right" : i >= 135 && i <= 225 ? !1 === o.options.rtl ? "right" : "left" : !0 === o.options.verticalSwiping ? i >= 35 && i <= 135 ? "down" : "up" : "vertical"
    }
    ,
    e.prototype.swipeEnd = function(t) {
        var e, n, i = this;
        if (i.dragging = !1,
        i.interrupted = !1,
        i.shouldClick = !(i.touchObject.swipeLength > 10),
        void 0 === i.touchObject.curX)
            return !1;
        if (!0 === i.touchObject.edgeHit && i.$slider.trigger("edge", [i, i.swipeDirection()]),
        i.touchObject.swipeLength >= i.touchObject.minSwipe) {
            switch (n = i.swipeDirection()) {
            case "left":
            case "down":
                e = i.options.swipeToSlide ? i.checkNavigable(i.currentSlide + i.getSlideCount()) : i.currentSlide + i.getSlideCount(),
                i.currentDirection = 0;
                break;
            case "right":
            case "up":
                e = i.options.swipeToSlide ? i.checkNavigable(i.currentSlide - i.getSlideCount()) : i.currentSlide - i.getSlideCount(),
                i.currentDirection = 1
            }
            "vertical" != n && (i.slideHandler(e),
            i.touchObject = {},
            i.$slider.trigger("swipe", [i, n]))
        } else
            i.touchObject.startX !== i.touchObject.curX && (i.slideHandler(i.currentSlide),
            i.touchObject = {})
    }
    ,
    e.prototype.swipeHandler = function(t) {
        var e = this;
        if (!(!1 === e.options.swipe || "ontouchend"in document && !1 === e.options.swipe || !1 === e.options.draggable && -1 !== t.type.indexOf("mouse")))
            switch (e.touchObject.fingerCount = t.originalEvent && void 0 !== t.originalEvent.touches ? t.originalEvent.touches.length : 1,
            e.touchObject.minSwipe = e.listWidth / e.options.touchThreshold,
            !0 === e.options.verticalSwiping && (e.touchObject.minSwipe = e.listHeight / e.options.touchThreshold),
            t.data.action) {
            case "start":
                e.swipeStart(t);
                break;
            case "move":
                e.swipeMove(t);
                break;
            case "end":
                e.swipeEnd(t)
            }
    }
    ,
    e.prototype.swipeMove = function(t) {
        var e, n, i, o, r, s = this;
        return r = void 0 !== t.originalEvent ? t.originalEvent.touches : null,
        !(!s.dragging || r && 1 !== r.length) && (e = s.getLeft(s.currentSlide),
        s.touchObject.curX = void 0 !== r ? r[0].pageX : t.clientX,
        s.touchObject.curY = void 0 !== r ? r[0].pageY : t.clientY,
        s.touchObject.swipeLength = Math.round(Math.sqrt(Math.pow(s.touchObject.curX - s.touchObject.startX, 2))),
        !0 === s.options.verticalSwiping && (s.touchObject.swipeLength = Math.round(Math.sqrt(Math.pow(s.touchObject.curY - s.touchObject.startY, 2)))),
        "vertical" !== (n = s.swipeDirection()) ? (void 0 !== t.originalEvent && s.touchObject.swipeLength > 4 && t.preventDefault(),
        o = (!1 === s.options.rtl ? 1 : -1) * (s.touchObject.curX > s.touchObject.startX ? 1 : -1),
        !0 === s.options.verticalSwiping && (o = s.touchObject.curY > s.touchObject.startY ? 1 : -1),
        i = s.touchObject.swipeLength,
        s.touchObject.edgeHit = !1,
        !1 === s.options.infinite && (0 === s.currentSlide && "right" === n || s.currentSlide >= s.getDotCount() && "left" === n) && (i = s.touchObject.swipeLength * s.options.edgeFriction,
        s.touchObject.edgeHit = !0),
        !1 === s.options.vertical ? s.swipeLeft = e + i * o : s.swipeLeft = e + i * (s.$list.height() / s.listWidth) * o,
        !0 === s.options.verticalSwiping && (s.swipeLeft = e + i * o),
        !0 !== s.options.fade && !1 !== s.options.touchMove && (!0 === s.animating ? (s.swipeLeft = null,
        !1) : void s.setCSS(s.swipeLeft))) : void 0)
    }
    ,
    e.prototype.swipeStart = function(t) {
        var e, n = this;
        if (n.interrupted = !0,
        1 !== n.touchObject.fingerCount || n.slideCount <= n.options.slidesToShow)
            return n.touchObject = {},
            !1;
        void 0 !== t.originalEvent && void 0 !== t.originalEvent.touches && (e = t.originalEvent.touches[0]),
        n.touchObject.startX = n.touchObject.curX = void 0 !== e ? e.pageX : t.clientX,
        n.touchObject.startY = n.touchObject.curY = void 0 !== e ? e.pageY : t.clientY,
        n.dragging = !0
    }
    ,
    e.prototype.unfilterSlides = e.prototype.slickUnfilter = function() {
        var t = this;
        null !== t.$slidesCache && (t.unload(),
        t.$slideTrack.children(this.options.slide).detach(),
        t.$slidesCache.appendTo(t.$slideTrack),
        t.reinit())
    }
    ,
    e.prototype.unload = function() {
        var e = this;
        t(".slick-cloned", e.$slider).remove(),
        e.$dots && e.$dots.remove(),
        e.$prevArrow && e.htmlExpr.test(e.options.prevArrow) && e.$prevArrow.remove(),
        e.$nextArrow && e.htmlExpr.test(e.options.nextArrow) && e.$nextArrow.remove(),
        e.$slides.removeClass("slick-slide slick-active slick-visible slick-current").attr("aria-hidden", "true").css("width", "")
    }
    ,
    e.prototype.unslick = function(t) {
        var e = this;
        e.$slider.trigger("unslick", [e, t]),
        e.destroy()
    }
    ,
    e.prototype.updateArrows = function() {
        var t = this;
        Math.floor(t.options.slidesToShow / 2),
        !0 === t.options.arrows && t.slideCount > t.options.slidesToShow && !t.options.infinite && (t.$prevArrow.removeClass("slick-disabled").attr("aria-disabled", "false"),
        t.$nextArrow.removeClass("slick-disabled").attr("aria-disabled", "false"),
        0 === t.currentSlide ? (t.$prevArrow.addClass("slick-disabled").attr("aria-disabled", "true"),
        t.$nextArrow.removeClass("slick-disabled").attr("aria-disabled", "false")) : t.currentSlide >= t.slideCount - t.options.slidesToShow && !1 === t.options.centerMode ? (t.$nextArrow.addClass("slick-disabled").attr("aria-disabled", "true"),
        t.$prevArrow.removeClass("slick-disabled").attr("aria-disabled", "false")) : t.currentSlide >= t.slideCount - 1 && !0 === t.options.centerMode && (t.$nextArrow.addClass("slick-disabled").attr("aria-disabled", "true"),
        t.$prevArrow.removeClass("slick-disabled").attr("aria-disabled", "false")))
    }
    ,
    e.prototype.updateDots = function() {
        var t = this;
        null !== t.$dots && (t.$dots.find("li").removeClass("slick-active").attr("aria-hidden", "true"),
        t.$dots.find("li").eq(Math.floor(t.currentSlide / t.options.slidesToScroll)).addClass("slick-active").attr("aria-hidden", "false"))
    }
    ,
    e.prototype.visibility = function() {
        var t = this;
        t.options.autoplay && (document[t.hidden] ? t.interrupted = !0 : t.interrupted = !1)
    }
    ,
    t.fn.slick = function() {
        var t, n, i = this, o = arguments[0], r = Array.prototype.slice.call(arguments, 1), s = i.length;
        for (t = 0; t < s; t++)
            if ("object" == typeof o || void 0 === o ? i[t].slick = new e(i[t],o) : n = i[t].slick[o].apply(i[t].slick, r),
            void 0 !== n)
                return n;
        return i
    }
}),
"undefined" == typeof jQuery)
    throw new Error("Bootstrap's JavaScript requires jQuery. jQuery must be included before Bootstrap's JavaScript.");
if (function(t) {
    var e = t.fn.jquery.split(" ")[0].split(".");
    if (e[0] < 2 && e[1] < 9 || 1 == e[0] && 9 == e[1] && e[2] < 1 || e[0] >= 4)
        throw new Error("Bootstrap's JavaScript requires at least jQuery v1.9.1 but less than v4.0.0")
}(jQuery),
function() {
    function t(t, e) {
        if (!t)
            throw new ReferenceError("this hasn't been initialised - super() hasn't been called");
        return !e || "object" != typeof e && "function" != typeof e ? t : e
    }
    function e(t, e) {
        if ("function" != typeof e && null !== e)
            throw new TypeError("Super expression must either be null or a function, not " + typeof e);
        t.prototype = Object.create(e && e.prototype, {
            constructor: {
                value: t,
                enumerable: !1,
                writable: !0,
                configurable: !0
            }
        }),
        e && (Object.setPrototypeOf ? Object.setPrototypeOf(t, e) : t.__proto__ = e)
    }
    function n(t, e) {
        if (!(t instanceof e))
            throw new TypeError("Cannot call a class as a function")
    }
    var i = "function" == typeof Symbol && "symbol" == typeof Symbol.iterator ? function(t) {
        return typeof t
    }
    : function(t) {
        return t && "function" == typeof Symbol && t.constructor === Symbol && t !== Symbol.prototype ? "symbol" : typeof t
    }
      , o = function() {
        function t(t, e) {
            for (var n = 0; n < e.length; n++) {
                var i = e[n];
                i.enumerable = i.enumerable || !1,
                i.configurable = !0,
                "value"in i && (i.writable = !0),
                Object.defineProperty(t, i.key, i)
            }
        }
        return function(e, n, i) {
            return n && t(e.prototype, n),
            i && t(e, i),
            e
        }
    }()
      , r = function(t) {
        function e(t) {
            return {}.toString.call(t).match(/\s([a-zA-Z]+)/)[1].toLowerCase()
        }
        function n(t) {
            return (t[0] || t).nodeType
        }
        function i() {
            return {
                bindType: s.end,
                delegateType: s.end,
                handle: function(e) {
                    if (t(e.target).is(this))
                        return e.handleObj.handler.apply(this, arguments)
                }
            }
        }
        function o() {
            if (window.QUnit)
                return !1;
            var t = document.createElement("bootstrap");
            for (var e in a)
                if (void 0 !== t.style[e])
                    return {
                        end: a[e]
                    };
            return !1
        }
        function r(e) {
            var n = this
              , i = !1;
            return t(this).one(l.TRANSITION_END, function() {
                i = !0
            }),
            setTimeout(function() {
                i || l.triggerTransitionEnd(n)
            }, e),
            this
        }
        var s = !1
          , a = {
            WebkitTransition: "webkitTransitionEnd",
            MozTransition: "transitionend",
            OTransition: "oTransitionEnd otransitionend",
            transition: "transitionend"
        }
          , l = {
            TRANSITION_END: "bsTransitionEnd",
            getUID: function(t) {
                do {
                    t += ~~(1e6 * Math.random())
                } while (document.getElementById(t));return t
            },
            getSelectorFromElement: function(t) {
                var e = t.getAttribute("data-target");
                return e || (e = t.getAttribute("href") || "",
                e = /^#[a-z]/i.test(e) ? e : null),
                e
            },
            reflow: function(t) {
                return t.offsetHeight
            },
            triggerTransitionEnd: function(e) {
                t(e).trigger(s.end)
            },
            supportsTransitionEnd: function() {
                return Boolean(s)
            },
            typeCheckConfig: function(t, i, o) {
                for (var r in o)
                    if (o.hasOwnProperty(r)) {
                        var s = o[r]
                          , a = i[r]
                          , l = a && n(a) ? "element" : e(a);
                        if (!new RegExp(s).test(l))
                            throw new Error(t.toUpperCase() + ': Option "' + r + '" provided type "' + l + '" but expected type "' + s + '".')
                    }
            }
        };
        return function() {
            s = o(),
            t.fn.emulateTransitionEnd = r,
            l.supportsTransitionEnd() && (t.event.special[l.TRANSITION_END] = i())
        }(),
        l
    }(jQuery)
      , s = (function(t) {
        var e = "alert"
          , i = t.fn[e]
          , s = {
            DISMISS: '[data-dismiss="alert"]'
        }
          , a = {
            CLOSE: "close.bs.alert",
            CLOSED: "closed.bs.alert",
            CLICK_DATA_API: "click.bs.alert.data-api"
        }
          , l = {
            ALERT: "alert",
            FADE: "fade",
            SHOW: "show"
        }
          , c = function() {
            function e(t) {
                n(this, e),
                this._element = t
            }
            return e.prototype.close = function(t) {
                t = t || this._element;
                var e = this._getRootElement(t);
                this._triggerCloseEvent(e).isDefaultPrevented() || this._removeElement(e)
            }
            ,
            e.prototype.dispose = function() {
                t.removeData(this._element, "bs.alert"),
                this._element = null
            }
            ,
            e.prototype._getRootElement = function(e) {
                var n = r.getSelectorFromElement(e)
                  , i = !1;
                return n && (i = t(n)[0]),
                i || (i = t(e).closest("." + l.ALERT)[0]),
                i
            }
            ,
            e.prototype._triggerCloseEvent = function(e) {
                var n = t.Event(a.CLOSE);
                return t(e).trigger(n),
                n
            }
            ,
            e.prototype._removeElement = function(e) {
                var n = this;
                if (t(e).removeClass(l.SHOW),
                !r.supportsTransitionEnd() || !t(e).hasClass(l.FADE))
                    return void this._destroyElement(e);
                t(e).one(r.TRANSITION_END, function(t) {
                    return n._destroyElement(e, t)
                }).emulateTransitionEnd(150)
            }
            ,
            e.prototype._destroyElement = function(e) {
                t(e).detach().trigger(a.CLOSED).remove()
            }
            ,
            e._jQueryInterface = function(n) {
                return this.each(function() {
                    var i = t(this)
                      , o = i.data("bs.alert");
                    o || (o = new e(this),
                    i.data("bs.alert", o)),
                    "close" === n && o[n](this)
                })
            }
            ,
            e._handleDismiss = function(t) {
                return function(e) {
                    e && e.preventDefault(),
                    t.close(this)
                }
            }
            ,
            o(e, null, [{
                key: "VERSION",
                get: function() {
                    return "4.0.0-alpha.6"
                }
            }]),
            e
        }();
        t(document).on(a.CLICK_DATA_API, s.DISMISS, c._handleDismiss(new c)),
        t.fn[e] = c._jQueryInterface,
        t.fn[e].Constructor = c,
        t.fn[e].noConflict = function() {
            return t.fn[e] = i,
            c._jQueryInterface
        }
    }(jQuery),
    function(t) {
        var e = "button"
          , i = t.fn[e]
          , r = {
            ACTIVE: "active",
            BUTTON: "btn",
            FOCUS: "focus"
        }
          , s = {
            DATA_TOGGLE_CARROT: '[data-toggle^="button"]',
            DATA_TOGGLE: '[data-toggle="buttons"]',
            INPUT: "input",
            ACTIVE: ".active",
            BUTTON: ".btn"
        }
          , a = {
            CLICK_DATA_API: "click.bs.button.data-api",
            FOCUS_BLUR_DATA_API: "focus.bs.button.data-api blur.bs.button.data-api"
        }
          , l = function() {
            function e(t) {
                n(this, e),
                this._element = t
            }
            return e.prototype.toggle = function() {
                var e = !0
                  , n = t(this._element).closest(s.DATA_TOGGLE)[0];
                if (n) {
                    var i = t(this._element).find(s.INPUT)[0];
                    if (i) {
                        if ("radio" === i.type)
                            if (i.checked && t(this._element).hasClass(r.ACTIVE))
                                e = !1;
                            else {
                                var o = t(n).find(s.ACTIVE)[0];
                                o && t(o).removeClass(r.ACTIVE)
                            }
                        e && (i.checked = !t(this._element).hasClass(r.ACTIVE),
                        t(i).trigger("change")),
                        i.focus()
                    }
                }
                this._element.setAttribute("aria-pressed", !t(this._element).hasClass(r.ACTIVE)),
                e && t(this._element).toggleClass(r.ACTIVE)
            }
            ,
            e.prototype.dispose = function() {
                t.removeData(this._element, "bs.button"),
                this._element = null
            }
            ,
            e._jQueryInterface = function(n) {
                return this.each(function() {
                    var i = t(this).data("bs.button");
                    i || (i = new e(this),
                    t(this).data("bs.button", i)),
                    "toggle" === n && i[n]()
                })
            }
            ,
            o(e, null, [{
                key: "VERSION",
                get: function() {
                    return "4.0.0-alpha.6"
                }
            }]),
            e
        }();
        t(document).on(a.CLICK_DATA_API, s.DATA_TOGGLE_CARROT, function(e) {
            e.preventDefault();
            var n = e.target;
            t(n).hasClass(r.BUTTON) || (n = t(n).closest(s.BUTTON)),
            l._jQueryInterface.call(t(n), "toggle")
        }).on(a.FOCUS_BLUR_DATA_API, s.DATA_TOGGLE_CARROT, function(e) {
            var n = t(e.target).closest(s.BUTTON)[0];
            t(n).toggleClass(r.FOCUS, /^focus(in)?$/.test(e.type))
        }),
        t.fn[e] = l._jQueryInterface,
        t.fn[e].Constructor = l,
        t.fn[e].noConflict = function() {
            return t.fn[e] = i,
            l._jQueryInterface
        }
    }(jQuery),
    function(t) {
        var e = "carousel"
          , s = "bs.carousel"
          , a = "." + s
          , l = t.fn[e]
          , c = {
            interval: 5e3,
            keyboard: !0,
            slide: !1,
            pause: "hover",
            wrap: !0
        }
          , u = {
            interval: "(number|boolean)",
            keyboard: "boolean",
            slide: "(boolean|string)",
            pause: "(string|boolean)",
            wrap: "boolean"
        }
          , d = {
            NEXT: "next",
            PREV: "prev",
            LEFT: "left",
            RIGHT: "right"
        }
          , f = {
            SLIDE: "slide" + a,
            SLID: "slid" + a,
            KEYDOWN: "keydown" + a,
            MOUSEENTER: "mouseenter" + a,
            MOUSELEAVE: "mouseleave" + a,
            LOAD_DATA_API: "load.bs.carousel.data-api",
            CLICK_DATA_API: "click.bs.carousel.data-api"
        }
          , p = {
            CAROUSEL: "carousel",
            ACTIVE: "active",
            SLIDE: "slide",
            RIGHT: "carousel-item-right",
            LEFT: "carousel-item-left",
            NEXT: "carousel-item-next",
            PREV: "carousel-item-prev",
            ITEM: "carousel-item"
        }
          , h = {
            ACTIVE: ".active",
            ACTIVE_ITEM: ".active.carousel-item",
            ITEM: ".carousel-item",
            NEXT_PREV: ".carousel-item-next, .carousel-item-prev",
            INDICATORS: ".carousel-indicators",
            DATA_SLIDE: "[data-slide], [data-slide-to]",
            DATA_RIDE: '[data-ride="carousel"]'
        }
          , g = function() {
            function l(e, i) {
                n(this, l),
                this._items = null,
                this._interval = null,
                this._activeElement = null,
                this._isPaused = !1,
                this._isSliding = !1,
                this._config = this._getConfig(i),
                this._element = t(e)[0],
                this._indicatorsElement = t(this._element).find(h.INDICATORS)[0],
                this._addEventListeners()
            }
            return l.prototype.next = function() {
                if (this._isSliding)
                    throw new Error("Carousel is sliding");
                this._slide(d.NEXT)
            }
            ,
            l.prototype.nextWhenVisible = function() {
                document.hidden || this.next()
            }
            ,
            l.prototype.prev = function() {
                if (this._isSliding)
                    throw new Error("Carousel is sliding");
                this._slide(d.PREVIOUS)
            }
            ,
            l.prototype.pause = function(e) {
                e || (this._isPaused = !0),
                t(this._element).find(h.NEXT_PREV)[0] && r.supportsTransitionEnd() && (r.triggerTransitionEnd(this._element),
                this.cycle(!0)),
                clearInterval(this._interval),
                this._interval = null
            }
            ,
            l.prototype.cycle = function(t) {
                t || (this._isPaused = !1),
                this._interval && (clearInterval(this._interval),
                this._interval = null),
                this._config.interval && !this._isPaused && (this._interval = setInterval((document.visibilityState ? this.nextWhenVisible : this.next).bind(this), this._config.interval))
            }
            ,
            l.prototype.to = function(e) {
                var n = this;
                this._activeElement = t(this._element).find(h.ACTIVE_ITEM)[0];
                var i = this._getItemIndex(this._activeElement);
                if (!(e > this._items.length - 1 || e < 0)) {
                    if (this._isSliding)
                        return void t(this._element).one(f.SLID, function() {
                            return n.to(e)
                        });
                    if (i === e)
                        return this.pause(),
                        void this.cycle();
                    var o = e > i ? d.NEXT : d.PREVIOUS;
                    this._slide(o, this._items[e])
                }
            }
            ,
            l.prototype.dispose = function() {
                t(this._element).off(a),
                t.removeData(this._element, s),
                this._items = null,
                this._config = null,
                this._element = null,
                this._interval = null,
                this._isPaused = null,
                this._isSliding = null,
                this._activeElement = null,
                this._indicatorsElement = null
            }
            ,
            l.prototype._getConfig = function(n) {
                return n = t.extend({}, c, n),
                r.typeCheckConfig(e, n, u),
                n
            }
            ,
            l.prototype._addEventListeners = function() {
                var e = this;
                this._config.keyboard && t(this._element).on(f.KEYDOWN, function(t) {
                    return e._keydown(t)
                }),
                "hover" !== this._config.pause || "ontouchstart"in document.documentElement || t(this._element).on(f.MOUSEENTER, function(t) {
                    return e.pause(t)
                }).on(f.MOUSELEAVE, function(t) {
                    return e.cycle(t)
                })
            }
            ,
            l.prototype._keydown = function(t) {
                if (!/input|textarea/i.test(t.target.tagName))
                    switch (t.which) {
                    case 37:
                        t.preventDefault(),
                        this.prev();
                        break;
                    case 39:
                        t.preventDefault(),
                        this.next();
                        break;
                    default:
                        return
                    }
            }
            ,
            l.prototype._getItemIndex = function(e) {
                return this._items = t.makeArray(t(e).parent().find(h.ITEM)),
                this._items.indexOf(e)
            }
            ,
            l.prototype._getItemByDirection = function(t, e) {
                var n = t === d.NEXT
                  , i = t === d.PREVIOUS
                  , o = this._getItemIndex(e)
                  , r = this._items.length - 1;
                if ((i && 0 === o || n && o === r) && !this._config.wrap)
                    return e;
                var s = t === d.PREVIOUS ? -1 : 1
                  , a = (o + s) % this._items.length;
                return -1 === a ? this._items[this._items.length - 1] : this._items[a]
            }
            ,
            l.prototype._triggerSlideEvent = function(e, n) {
                var i = t.Event(f.SLIDE, {
                    relatedTarget: e,
                    direction: n
                });
                return t(this._element).trigger(i),
                i
            }
            ,
            l.prototype._setActiveIndicatorElement = function(e) {
                if (this._indicatorsElement) {
                    t(this._indicatorsElement).find(h.ACTIVE).removeClass(p.ACTIVE);
                    var n = this._indicatorsElement.children[this._getItemIndex(e)];
                    n && t(n).addClass(p.ACTIVE)
                }
            }
            ,
            l.prototype._slide = function(e, n) {
                var i = this
                  , o = t(this._element).find(h.ACTIVE_ITEM)[0]
                  , s = n || o && this._getItemByDirection(e, o)
                  , a = Boolean(this._interval)
                  , l = void 0
                  , c = void 0
                  , u = void 0;
                if (e === d.NEXT ? (l = p.LEFT,
                c = p.NEXT,
                u = d.LEFT) : (l = p.RIGHT,
                c = p.PREV,
                u = d.RIGHT),
                s && t(s).hasClass(p.ACTIVE))
                    return void (this._isSliding = !1);
                if (!this._triggerSlideEvent(s, u).isDefaultPrevented() && o && s) {
                    this._isSliding = !0,
                    a && this.pause(),
                    this._setActiveIndicatorElement(s);
                    var g = t.Event(f.SLID, {
                        relatedTarget: s,
                        direction: u
                    });
                    r.supportsTransitionEnd() && t(this._element).hasClass(p.SLIDE) ? (t(s).addClass(c),
                    r.reflow(s),
                    t(o).addClass(l),
                    t(s).addClass(l),
                    t(o).one(r.TRANSITION_END, function() {
                        t(s).removeClass(l + " " + c).addClass(p.ACTIVE),
                        t(o).removeClass(p.ACTIVE + " " + c + " " + l),
                        i._isSliding = !1,
                        setTimeout(function() {
                            return t(i._element).trigger(g)
                        }, 0)
                    }).emulateTransitionEnd(600)) : (t(o).removeClass(p.ACTIVE),
                    t(s).addClass(p.ACTIVE),
                    this._isSliding = !1,
                    t(this._element).trigger(g)),
                    a && this.cycle()
                }
            }
            ,
            l._jQueryInterface = function(e) {
                return this.each(function() {
                    var n = t(this).data(s)
                      , o = t.extend({}, c, t(this).data());
                    "object" === (void 0 === e ? "undefined" : i(e)) && t.extend(o, e);
                    var r = "string" == typeof e ? e : o.slide;
                    if (n || (n = new l(this,o),
                    t(this).data(s, n)),
                    "number" == typeof e)
                        n.to(e);
                    else if ("string" == typeof r) {
                        if (void 0 === n[r])
                            throw new Error('No method named "' + r + '"');
                        n[r]()
                    } else
                        o.interval && (n.pause(),
                        n.cycle())
                })
            }
            ,
            l._dataApiClickHandler = function(e) {
                var n = r.getSelectorFromElement(this);
                if (n) {
                    var i = t(n)[0];
                    if (i && t(i).hasClass(p.CAROUSEL)) {
                        var o = t.extend({}, t(i).data(), t(this).data())
                          , a = this.getAttribute("data-slide-to");
                        a && (o.interval = !1),
                        l._jQueryInterface.call(t(i), o),
                        a && t(i).data(s).to(a),
                        e.preventDefault()
                    }
                }
            }
            ,
            o(l, null, [{
                key: "VERSION",
                get: function() {
                    return "4.0.0-alpha.6"
                }
            }, {
                key: "Default",
                get: function() {
                    return c
                }
            }]),
            l
        }();
        t(document).on(f.CLICK_DATA_API, h.DATA_SLIDE, g._dataApiClickHandler),
        t(window).on(f.LOAD_DATA_API, function() {
            t(h.DATA_RIDE).each(function() {
                var e = t(this);
                g._jQueryInterface.call(e, e.data())
            })
        }),
        t.fn[e] = g._jQueryInterface,
        t.fn[e].Constructor = g,
        t.fn[e].noConflict = function() {
            return t.fn[e] = l,
            g._jQueryInterface
        }
    }(jQuery),
    function(t) {
        var e = "collapse"
          , s = "bs.collapse"
          , a = t.fn[e]
          , l = {
            toggle: !0,
            parent: ""
        }
          , c = {
            toggle: "boolean",
            parent: "string"
        }
          , u = {
            SHOW: "show.bs.collapse",
            SHOWN: "shown.bs.collapse",
            HIDE: "hide.bs.collapse",
            HIDDEN: "hidden.bs.collapse",
            CLICK_DATA_API: "click.bs.collapse.data-api"
        }
          , d = {
            SHOW: "show",
            COLLAPSE: "collapse",
            COLLAPSING: "collapsing",
            COLLAPSED: "collapsed"
        }
          , f = {
            WIDTH: "width",
            HEIGHT: "height"
        }
          , p = {
            ACTIVES: ".card > .show, .card > .collapsing",
            DATA_TOGGLE: '[data-toggle="collapse"]'
        }
          , h = function() {
            function a(e, i) {
                n(this, a),
                this._isTransitioning = !1,
                this._element = e,
                this._config = this._getConfig(i),
                this._triggerArray = t.makeArray(t('[data-toggle="collapse"][href="#' + e.id + '"],[data-toggle="collapse"][data-target="#' + e.id + '"]')),
                this._parent = this._config.parent ? this._getParent() : null,
                this._config.parent || this._addAriaAndCollapsedClass(this._element, this._triggerArray),
                this._config.toggle && this.toggle()
            }
            return a.prototype.toggle = function() {
                t(this._element).hasClass(d.SHOW) ? this.hide() : this.show()
            }
            ,
            a.prototype.show = function() {
                var e = this;
                if (this._isTransitioning)
                    throw new Error("Collapse is transitioning");
                if (!t(this._element).hasClass(d.SHOW)) {
                    var n = void 0
                      , i = void 0;
                    if (this._parent && (n = t.makeArray(t(this._parent).find(p.ACTIVES)),
                    n.length || (n = null)),
                    !(n && (i = t(n).data(s)) && i._isTransitioning)) {
                        var o = t.Event(u.SHOW);
                        if (t(this._element).trigger(o),
                        !o.isDefaultPrevented()) {
                            n && (a._jQueryInterface.call(t(n), "hide"),
                            i || t(n).data(s, null));
                            var l = this._getDimension();
                            t(this._element).removeClass(d.COLLAPSE).addClass(d.COLLAPSING),
                            this._element.style[l] = 0,
                            this._element.setAttribute("aria-expanded", !0),
                            this._triggerArray.length && t(this._triggerArray).removeClass(d.COLLAPSED).attr("aria-expanded", !0),
                            this.setTransitioning(!0);
                            var c = function() {
                                t(e._element).removeClass(d.COLLAPSING).addClass(d.COLLAPSE).addClass(d.SHOW),
                                e._element.style[l] = "",
                                e.setTransitioning(!1),
                                t(e._element).trigger(u.SHOWN)
                            };
                            if (!r.supportsTransitionEnd())
                                return void c();
                            var f = l[0].toUpperCase() + l.slice(1)
                              , h = "scroll" + f;
                            t(this._element).one(r.TRANSITION_END, c).emulateTransitionEnd(600),
                            this._element.style[l] = this._element[h] + "px"
                        }
                    }
                }
            }
            ,
            a.prototype.hide = function() {
                var e = this;
                if (this._isTransitioning)
                    throw new Error("Collapse is transitioning");
                if (t(this._element).hasClass(d.SHOW)) {
                    var n = t.Event(u.HIDE);
                    if (t(this._element).trigger(n),
                    !n.isDefaultPrevented()) {
                        var i = this._getDimension()
                          , o = i === f.WIDTH ? "offsetWidth" : "offsetHeight";
                        this._element.style[i] = this._element[o] + "px",
                        r.reflow(this._element),
                        t(this._element).addClass(d.COLLAPSING).removeClass(d.COLLAPSE).removeClass(d.SHOW),
                        this._element.setAttribute("aria-expanded", !1),
                        this._triggerArray.length && t(this._triggerArray).addClass(d.COLLAPSED).attr("aria-expanded", !1),
                        this.setTransitioning(!0);
                        var s = function() {
                            e.setTransitioning(!1),
                            t(e._element).removeClass(d.COLLAPSING).addClass(d.COLLAPSE).trigger(u.HIDDEN)
                        };
                        if (this._element.style[i] = "",
                        !r.supportsTransitionEnd())
                            return void s();
                        t(this._element).one(r.TRANSITION_END, s).emulateTransitionEnd(600)
                    }
                }
            }
            ,
            a.prototype.setTransitioning = function(t) {
                this._isTransitioning = t
            }
            ,
            a.prototype.dispose = function() {
                t.removeData(this._element, s),
                this._config = null,
                this._parent = null,
                this._element = null,
                this._triggerArray = null,
                this._isTransitioning = null
            }
            ,
            a.prototype._getConfig = function(n) {
                return n = t.extend({}, l, n),
                n.toggle = Boolean(n.toggle),
                r.typeCheckConfig(e, n, c),
                n
            }
            ,
            a.prototype._getDimension = function() {
                return t(this._element).hasClass(f.WIDTH) ? f.WIDTH : f.HEIGHT
            }
            ,
            a.prototype._getParent = function() {
                var e = this
                  , n = t(this._config.parent)[0]
                  , i = '[data-toggle="collapse"][data-parent="' + this._config.parent + '"]';
                return t(n).find(i).each(function(t, n) {
                    e._addAriaAndCollapsedClass(a._getTargetFromElement(n), [n])
                }),
                n
            }
            ,
            a.prototype._addAriaAndCollapsedClass = function(e, n) {
                if (e) {
                    var i = t(e).hasClass(d.SHOW);
                    e.setAttribute("aria-expanded", i),
                    n.length && t(n).toggleClass(d.COLLAPSED, !i).attr("aria-expanded", i)
                }
            }
            ,
            a._getTargetFromElement = function(e) {
                var n = r.getSelectorFromElement(e);
                return n ? t(n)[0] : null
            }
            ,
            a._jQueryInterface = function(e) {
                return this.each(function() {
                    var n = t(this)
                      , o = n.data(s)
                      , r = t.extend({}, l, n.data(), "object" === (void 0 === e ? "undefined" : i(e)) && e);
                    if (!o && r.toggle && /show|hide/.test(e) && (r.toggle = !1),
                    o || (o = new a(this,r),
                    n.data(s, o)),
                    "string" == typeof e) {
                        if (void 0 === o[e])
                            throw new Error('No method named "' + e + '"');
                        o[e]()
                    }
                })
            }
            ,
            o(a, null, [{
                key: "VERSION",
                get: function() {
                    return "4.0.0-alpha.6"
                }
            }, {
                key: "Default",
                get: function() {
                    return l
                }
            }]),
            a
        }();
        t(document).on(u.CLICK_DATA_API, p.DATA_TOGGLE, function(e) {
            e.preventDefault();
            var n = h._getTargetFromElement(this)
              , i = t(n).data(s)
              , o = i ? "toggle" : t(this).data();
            h._jQueryInterface.call(t(n), o)
        }),
        t.fn[e] = h._jQueryInterface,
        t.fn[e].Constructor = h,
        t.fn[e].noConflict = function() {
            return t.fn[e] = a,
            h._jQueryInterface
        }
    }(jQuery),
    function(t) {
        var e = "dropdown"
          , i = ".bs.dropdown"
          , s = t.fn[e]
          , a = {
            HIDE: "hide" + i,
            HIDDEN: "hidden" + i,
            SHOW: "show" + i,
            SHOWN: "shown" + i,
            CLICK: "click" + i,
            CLICK_DATA_API: "click.bs.dropdown.data-api",
            FOCUSIN_DATA_API: "focusin.bs.dropdown.data-api",
            KEYDOWN_DATA_API: "keydown.bs.dropdown.data-api"
        }
          , l = {
            BACKDROP: "dropdown-backdrop",
            DISABLED: "disabled",
            SHOW: "show"
        }
          , c = {
            BACKDROP: ".dropdown-backdrop",
            DATA_TOGGLE: '[data-toggle="dropdown"]',
            FORM_CHILD: ".dropdown form",
            ROLE_MENU: '[role="menu"]',
            ROLE_LISTBOX: '[role="listbox"]',
            NAVBAR_NAV: ".navbar-nav",
            VISIBLE_ITEMS: '[role="menu"] li:not(.disabled) a, [role="listbox"] li:not(.disabled) a'
        }
          , u = function() {
            function e(t) {
                n(this, e),
                this._element = t,
                this._addEventListeners()
            }
            return e.prototype.toggle = function() {
                if (this.disabled || t(this).hasClass(l.DISABLED))
                    return !1;
                var n = e._getParentFromElement(this)
                  , i = t(n).hasClass(l.SHOW);
                if (e._clearMenus(),
                i)
                    return !1;
                if ("ontouchstart"in document.documentElement && !t(n).closest(c.NAVBAR_NAV).length) {
                    var o = document.createElement("div");
                    o.className = l.BACKDROP,
                    t(o).insertBefore(this),
                    t(o).on("click", e._clearMenus)
                }
                var r = {
                    relatedTarget: this
                }
                  , s = t.Event(a.SHOW, r);
                return t(n).trigger(s),
                !s.isDefaultPrevented() && (this.focus(),
                this.setAttribute("aria-expanded", !0),
                t(n).toggleClass(l.SHOW),
                t(n).trigger(t.Event(a.SHOWN, r)),
                !1)
            }
            ,
            e.prototype.dispose = function() {
                t.removeData(this._element, "bs.dropdown"),
                t(this._element).off(i),
                this._element = null
            }
            ,
            e.prototype._addEventListeners = function() {
                t(this._element).on(a.CLICK, this.toggle)
            }
            ,
            e._jQueryInterface = function(n) {
                return this.each(function() {
                    var i = t(this).data("bs.dropdown");
                    if (i || (i = new e(this),
                    t(this).data("bs.dropdown", i)),
                    "string" == typeof n) {
                        if (void 0 === i[n])
                            throw new Error('No method named "' + n + '"');
                        i[n].call(this)
                    }
                })
            }
            ,
            e._clearMenus = function(n) {
                if (!n || 3 !== n.which) {
                    var i = t(c.BACKDROP)[0];
                    i && i.parentNode.removeChild(i);
                    for (var o = t.makeArray(t(c.DATA_TOGGLE)), r = 0; r < o.length; r++) {
                        var s = e._getParentFromElement(o[r])
                          , u = {
                            relatedTarget: o[r]
                        };
                        if (t(s).hasClass(l.SHOW) && !(n && ("click" === n.type && /input|textarea/i.test(n.target.tagName) || "focusin" === n.type) && t.contains(s, n.target))) {
                            var d = t.Event(a.HIDE, u);
                            t(s).trigger(d),
                            d.isDefaultPrevented() || (o[r].setAttribute("aria-expanded", "false"),
                            t(s).removeClass(l.SHOW).trigger(t.Event(a.HIDDEN, u)))
                        }
                    }
                }
            }
            ,
            e._getParentFromElement = function(e) {
                var n = void 0
                  , i = r.getSelectorFromElement(e);
                return i && (n = t(i)[0]),
                n || e.parentNode
            }
            ,
            e._dataApiKeydownHandler = function(n) {
                if (/(38|40|27|32)/.test(n.which) && !/input|textarea/i.test(n.target.tagName) && (n.preventDefault(),
                n.stopPropagation(),
                !this.disabled && !t(this).hasClass(l.DISABLED))) {
                    var i = e._getParentFromElement(this)
                      , o = t(i).hasClass(l.SHOW);
                    if (!o && 27 !== n.which || o && 27 === n.which) {
                        if (27 === n.which) {
                            var r = t(i).find(c.DATA_TOGGLE)[0];
                            t(r).trigger("focus")
                        }
                        return void t(this).trigger("click")
                    }
                    var s = t(i).find(c.VISIBLE_ITEMS).get();
                    if (s.length) {
                        var a = s.indexOf(n.target);
                        38 === n.which && a > 0 && a--,
                        40 === n.which && a < s.length - 1 && a++,
                        a < 0 && (a = 0),
                        s[a].focus()
                    }
                }
            }
            ,
            o(e, null, [{
                key: "VERSION",
                get: function() {
                    return "4.0.0-alpha.6"
                }
            }]),
            e
        }();
        t(document).on(a.KEYDOWN_DATA_API, c.DATA_TOGGLE, u._dataApiKeydownHandler).on(a.KEYDOWN_DATA_API, c.ROLE_MENU, u._dataApiKeydownHandler).on(a.KEYDOWN_DATA_API, c.ROLE_LISTBOX, u._dataApiKeydownHandler).on(a.CLICK_DATA_API + " " + a.FOCUSIN_DATA_API, u._clearMenus).on(a.CLICK_DATA_API, c.DATA_TOGGLE, u.prototype.toggle).on(a.CLICK_DATA_API, c.FORM_CHILD, function(t) {
            t.stopPropagation()
        }),
        t.fn[e] = u._jQueryInterface,
        t.fn[e].Constructor = u,
        t.fn[e].noConflict = function() {
            return t.fn[e] = s,
            u._jQueryInterface
        }
    }(jQuery),
    function(t) {
        var e = "modal"
          , s = ".bs.modal"
          , a = t.fn[e]
          , l = {
            backdrop: !0,
            keyboard: !0,
            focus: !0,
            show: !0
        }
          , c = {
            backdrop: "(boolean|string)",
            keyboard: "boolean",
            focus: "boolean",
            show: "boolean"
        }
          , u = {
            HIDE: "hide.bs.modal",
            HIDDEN: "hidden.bs.modal",
            SHOW: "show.bs.modal",
            SHOWN: "shown.bs.modal",
            FOCUSIN: "focusin.bs.modal",
            RESIZE: "resize.bs.modal",
            CLICK_DISMISS: "click.dismiss.bs.modal",
            KEYDOWN_DISMISS: "keydown.dismiss.bs.modal",
            MOUSEUP_DISMISS: "mouseup.dismiss.bs.modal",
            MOUSEDOWN_DISMISS: "mousedown.dismiss.bs.modal",
            CLICK_DATA_API: "click.bs.modal.data-api"
        }
          , d = {
            SCROLLBAR_MEASURER: "modal-scrollbar-measure",
            BACKDROP: "modal-backdrop",
            OPEN: "modal-open",
            FADE: "fade",
            SHOW: "show"
        }
          , f = {
            DIALOG: ".modal-dialog",
            DATA_TOGGLE: '[data-toggle="modal"]',
            DATA_DISMISS: '[data-dismiss="modal"]',
            FIXED_CONTENT: ".fixed-top, .fixed-bottom, .is-fixed, .sticky-top"
        }
          , p = function() {
            function a(e, i) {
                n(this, a),
                this._config = this._getConfig(i),
                this._element = e,
                this._dialog = t(e).find(f.DIALOG)[0],
                this._backdrop = null,
                this._isShown = !1,
                this._isBodyOverflowing = !1,
                this._ignoreBackdropClick = !1,
                this._isTransitioning = !1,
                this._originalBodyPadding = 0,
                this._scrollbarWidth = 0
            }
            return a.prototype.toggle = function(t) {
                return this._isShown ? this.hide() : this.show(t)
            }
            ,
            a.prototype.show = function(e) {
                var n = this;
                if (this._isTransitioning)
                    throw new Error("Modal is transitioning");
                r.supportsTransitionEnd() && t(this._element).hasClass(d.FADE) && (this._isTransitioning = !0);
                var i = t.Event(u.SHOW, {
                    relatedTarget: e
                });
                t(this._element).trigger(i),
                this._isShown || i.isDefaultPrevented() || (this._isShown = !0,
                this._checkScrollbar(),
                this._setScrollbar(),
                t(document.body).addClass(d.OPEN),
                this._setEscapeEvent(),
                this._setResizeEvent(),
                t(this._element).on(u.CLICK_DISMISS, f.DATA_DISMISS, function(t) {
                    return n.hide(t)
                }),
                t(this._dialog).on(u.MOUSEDOWN_DISMISS, function() {
                    t(n._element).one(u.MOUSEUP_DISMISS, function(e) {
                        t(e.target).is(n._element) && (n._ignoreBackdropClick = !0)
                    })
                }),
                this._showBackdrop(function() {
                    return n._showElement(e)
                }))
            }
            ,
            a.prototype.hide = function(e) {
                var n = this;
                if (e && e.preventDefault(),
                this._isTransitioning)
                    throw new Error("Modal is transitioning");
                var i = r.supportsTransitionEnd() && t(this._element).hasClass(d.FADE);
                i && (this._isTransitioning = !0);
                var o = t.Event(u.HIDE);
                t(this._element).trigger(o),
                this._isShown && !o.isDefaultPrevented() && (this._isShown = !1,
                this._setEscapeEvent(),
                this._setResizeEvent(),
                t(document).off(u.FOCUSIN),
                t(this._element).removeClass(d.SHOW),
                t(this._element).off(u.CLICK_DISMISS),
                t(this._dialog).off(u.MOUSEDOWN_DISMISS),
                i ? t(this._element).one(r.TRANSITION_END, function(t) {
                    return n._hideModal(t)
                }).emulateTransitionEnd(300) : this._hideModal())
            }
            ,
            a.prototype.dispose = function() {
                t.removeData(this._element, "bs.modal"),
                t(window, document, this._element, this._backdrop).off(s),
                this._config = null,
                this._element = null,
                this._dialog = null,
                this._backdrop = null,
                this._isShown = null,
                this._isBodyOverflowing = null,
                this._ignoreBackdropClick = null,
                this._originalBodyPadding = null,
                this._scrollbarWidth = null
            }
            ,
            a.prototype._getConfig = function(n) {
                return n = t.extend({}, l, n),
                r.typeCheckConfig(e, n, c),
                n
            }
            ,
            a.prototype._showElement = function(e) {
                var n = this
                  , i = r.supportsTransitionEnd() && t(this._element).hasClass(d.FADE);
                this._element.parentNode && this._element.parentNode.nodeType === Node.ELEMENT_NODE || document.body.appendChild(this._element),
                this._element.style.display = "block",
                this._element.removeAttribute("aria-hidden"),
                this._element.scrollTop = 0,
                i && r.reflow(this._element),
                t(this._element).addClass(d.SHOW),
                this._config.focus && this._enforceFocus();
                var o = t.Event(u.SHOWN, {
                    relatedTarget: e
                })
                  , s = function() {
                    n._config.focus && n._element.focus(),
                    n._isTransitioning = !1,
                    t(n._element).trigger(o)
                };
                i ? t(this._dialog).one(r.TRANSITION_END, s).emulateTransitionEnd(300) : s()
            }
            ,
            a.prototype._enforceFocus = function() {
                var e = this;
                t(document).off(u.FOCUSIN).on(u.FOCUSIN, function(n) {
                    document === n.target || e._element === n.target || t(e._element).has(n.target).length || e._element.focus()
                })
            }
            ,
            a.prototype._setEscapeEvent = function() {
                var e = this;
                this._isShown && this._config.keyboard ? t(this._element).on(u.KEYDOWN_DISMISS, function(t) {
                    27 === t.which && e.hide()
                }) : this._isShown || t(this._element).off(u.KEYDOWN_DISMISS)
            }
            ,
            a.prototype._setResizeEvent = function() {
                var e = this;
                this._isShown ? t(window).on(u.RESIZE, function(t) {
                    return e._handleUpdate(t)
                }) : t(window).off(u.RESIZE)
            }
            ,
            a.prototype._hideModal = function() {
                var e = this;
                this._element.style.display = "none",
                this._element.setAttribute("aria-hidden", "true"),
                this._isTransitioning = !1,
                this._showBackdrop(function() {
                    t(document.body).removeClass(d.OPEN),
                    e._resetAdjustments(),
                    e._resetScrollbar(),
                    t(e._element).trigger(u.HIDDEN)
                })
            }
            ,
            a.prototype._removeBackdrop = function() {
                this._backdrop && (t(this._backdrop).remove(),
                this._backdrop = null)
            }
            ,
            a.prototype._showBackdrop = function(e) {
                var n = this
                  , i = t(this._element).hasClass(d.FADE) ? d.FADE : "";
                if (this._isShown && this._config.backdrop) {
                    var o = r.supportsTransitionEnd() && i;
                    if (this._backdrop = document.createElement("div"),
                    this._backdrop.className = d.BACKDROP,
                    i && t(this._backdrop).addClass(i),
                    t(this._backdrop).appendTo(document.body),
                    t(this._element).on(u.CLICK_DISMISS, function(t) {
                        if (n._ignoreBackdropClick)
                            return void (n._ignoreBackdropClick = !1);
                        t.target === t.currentTarget && ("static" === n._config.backdrop ? n._element.focus() : n.hide())
                    }),
                    o && r.reflow(this._backdrop),
                    t(this._backdrop).addClass(d.SHOW),
                    !e)
                        return;
                    if (!o)
                        return void e();
                    t(this._backdrop).one(r.TRANSITION_END, e).emulateTransitionEnd(150)
                } else if (!this._isShown && this._backdrop) {
                    t(this._backdrop).removeClass(d.SHOW);
                    var s = function() {
                        n._removeBackdrop(),
                        e && e()
                    };
                    r.supportsTransitionEnd() && t(this._element).hasClass(d.FADE) ? t(this._backdrop).one(r.TRANSITION_END, s).emulateTransitionEnd(150) : s()
                } else
                    e && e()
            }
            ,
            a.prototype._handleUpdate = function() {
                this._adjustDialog()
            }
            ,
            a.prototype._adjustDialog = function() {
                var t = this._element.scrollHeight > document.documentElement.clientHeight;
                !this._isBodyOverflowing && t && (this._element.style.paddingLeft = this._scrollbarWidth + "px"),
                this._isBodyOverflowing && !t && (this._element.style.paddingRight = this._scrollbarWidth + "px")
            }
            ,
            a.prototype._resetAdjustments = function() {
                this._element.style.paddingLeft = "",
                this._element.style.paddingRight = ""
            }
            ,
            a.prototype._checkScrollbar = function() {
                this._isBodyOverflowing = document.body.clientWidth < window.innerWidth,
                this._scrollbarWidth = this._getScrollbarWidth()
            }
            ,
            a.prototype._setScrollbar = function() {
                var e = parseInt(t(f.FIXED_CONTENT).css("padding-right") || 0, 10);
                this._originalBodyPadding = document.body.style.paddingRight || "",
                this._isBodyOverflowing && (document.body.style.paddingRight = e + this._scrollbarWidth + "px")
            }
            ,
            a.prototype._resetScrollbar = function() {
                document.body.style.paddingRight = this._originalBodyPadding
            }
            ,
            a.prototype._getScrollbarWidth = function() {
                var t = document.createElement("div");
                t.className = d.SCROLLBAR_MEASURER,
                document.body.appendChild(t);
                var e = t.offsetWidth - t.clientWidth;
                return document.body.removeChild(t),
                e
            }
            ,
            a._jQueryInterface = function(e, n) {
                return this.each(function() {
                    var o = t(this).data("bs.modal")
                      , r = t.extend({}, a.Default, t(this).data(), "object" === (void 0 === e ? "undefined" : i(e)) && e);
                    if (o || (o = new a(this,r),
                    t(this).data("bs.modal", o)),
                    "string" == typeof e) {
                        if (void 0 === o[e])
                            throw new Error('No method named "' + e + '"');
                        o[e](n)
                    } else
                        r.show && o.show(n)
                })
            }
            ,
            o(a, null, [{
                key: "VERSION",
                get: function() {
                    return "4.0.0-alpha.6"
                }
            }, {
                key: "Default",
                get: function() {
                    return l
                }
            }]),
            a
        }();
        t(document).on(u.CLICK_DATA_API, f.DATA_TOGGLE, function(e) {
            var n = this
              , i = void 0
              , o = r.getSelectorFromElement(this);
            o && (i = t(o)[0]);
            var s = t(i).data("bs.modal") ? "toggle" : t.extend({}, t(i).data(), t(this).data());
            "A" !== this.tagName && "AREA" !== this.tagName || e.preventDefault();
            var a = t(i).one(u.SHOW, function(e) {
                e.isDefaultPrevented() || a.one(u.HIDDEN, function() {
                    t(n).is(":visible") && n.focus()
                })
            });
            p._jQueryInterface.call(t(i), s, this)
        }),
        t.fn[e] = p._jQueryInterface,
        t.fn[e].Constructor = p,
        t.fn[e].noConflict = function() {
            return t.fn[e] = a,
            p._jQueryInterface
        }
    }(jQuery),
    function(t) {
        var e = "scrollspy"
          , s = t.fn[e]
          , a = {
            offset: 10,
            method: "auto",
            target: ""
        }
          , l = {
            offset: "number",
            method: "string",
            target: "(string|element)"
        }
          , c = {
            ACTIVATE: "activate.bs.scrollspy",
            SCROLL: "scroll.bs.scrollspy",
            LOAD_DATA_API: "load.bs.scrollspy.data-api"
        }
          , u = {
            DROPDOWN_ITEM: "dropdown-item",
            DROPDOWN_MENU: "dropdown-menu",
            NAV_LINK: "nav-link",
            NAV: "nav",
            ACTIVE: "active"
        }
          , d = {
            DATA_SPY: '[data-spy="scroll"]',
            ACTIVE: ".active",
            LIST_ITEM: ".list-item",
            LI: "li",
            LI_DROPDOWN: "li.dropdown",
            NAV_LINKS: ".nav-link",
            DROPDOWN: ".dropdown",
            DROPDOWN_ITEMS: ".dropdown-item",
            DROPDOWN_TOGGLE: ".dropdown-toggle"
        }
          , f = {
            OFFSET: "offset",
            POSITION: "position"
        }
          , p = function() {
            function s(e, i) {
                var o = this;
                n(this, s),
                this._element = e,
                this._scrollElement = "BODY" === e.tagName ? window : e,
                this._config = this._getConfig(i),
                this._selector = this._config.target + " " + d.NAV_LINKS + "," + this._config.target + " " + d.DROPDOWN_ITEMS,
                this._offsets = [],
                this._targets = [],
                this._activeTarget = null,
                this._scrollHeight = 0,
                t(this._scrollElement).on(c.SCROLL, function(t) {
                    return o._process(t)
                }),
                this.refresh(),
                this._process()
            }
            return s.prototype.refresh = function() {
                var e = this
                  , n = this._scrollElement !== this._scrollElement.window ? f.POSITION : f.OFFSET
                  , i = "auto" === this._config.method ? n : this._config.method
                  , o = i === f.POSITION ? this._getScrollTop() : 0;
                this._offsets = [],
                this._targets = [],
                this._scrollHeight = this._getScrollHeight(),
                t.makeArray(t(this._selector)).map(function(e) {
                    var n = void 0
                      , s = r.getSelectorFromElement(e);
                    return s && (n = t(s)[0]),
                    n && (n.offsetWidth || n.offsetHeight) ? [t(n)[i]().top + o, s] : null
                }).filter(function(t) {
                    return t
                }).sort(function(t, e) {
                    return t[0] - e[0]
                }).forEach(function(t) {
                    e._offsets.push(t[0]),
                    e._targets.push(t[1])
                })
            }
            ,
            s.prototype.dispose = function() {
                t.removeData(this._element, "bs.scrollspy"),
                t(this._scrollElement).off(".bs.scrollspy"),
                this._element = null,
                this._scrollElement = null,
                this._config = null,
                this._selector = null,
                this._offsets = null,
                this._targets = null,
                this._activeTarget = null,
                this._scrollHeight = null
            }
            ,
            s.prototype._getConfig = function(n) {
                if (n = t.extend({}, a, n),
                "string" != typeof n.target) {
                    var i = t(n.target).attr("id");
                    i || (i = r.getUID(e),
                    t(n.target).attr("id", i)),
                    n.target = "#" + i
                }
                return r.typeCheckConfig(e, n, l),
                n
            }
            ,
            s.prototype._getScrollTop = function() {
                return this._scrollElement === window ? this._scrollElement.pageYOffset : this._scrollElement.scrollTop
            }
            ,
            s.prototype._getScrollHeight = function() {
                return this._scrollElement.scrollHeight || Math.max(document.body.scrollHeight, document.documentElement.scrollHeight)
            }
            ,
            s.prototype._getOffsetHeight = function() {
                return this._scrollElement === window ? window.innerHeight : this._scrollElement.offsetHeight
            }
            ,
            s.prototype._process = function() {
                var t = this._getScrollTop() + this._config.offset
                  , e = this._getScrollHeight()
                  , n = this._config.offset + e - this._getOffsetHeight();
                if (this._scrollHeight !== e && this.refresh(),
                t >= n) {
                    var i = this._targets[this._targets.length - 1];
                    return void (this._activeTarget !== i && this._activate(i))
                }
                if (this._activeTarget && t < this._offsets[0] && this._offsets[0] > 0)
                    return this._activeTarget = null,
                    void this._clear();
                for (var o = this._offsets.length; o--; ) {
                    this._activeTarget !== this._targets[o] && t >= this._offsets[o] && (void 0 === this._offsets[o + 1] || t < this._offsets[o + 1]) && this._activate(this._targets[o])
                }
            }
            ,
            s.prototype._activate = function(e) {
                this._activeTarget = e,
                this._clear();
                var n = this._selector.split(",");
                n = n.map(function(t) {
                    return t + '[data-target="' + e + '"],' + t + '[href="' + e + '"]'
                });
                var i = t(n.join(","));
                i.hasClass(u.DROPDOWN_ITEM) ? (i.closest(d.DROPDOWN).find(d.DROPDOWN_TOGGLE).addClass(u.ACTIVE),
                i.addClass(u.ACTIVE)) : i.parents(d.LI).find("> " + d.NAV_LINKS).addClass(u.ACTIVE),
                t(this._scrollElement).trigger(c.ACTIVATE, {
                    relatedTarget: e
                })
            }
            ,
            s.prototype._clear = function() {
                t(this._selector).filter(d.ACTIVE).removeClass(u.ACTIVE)
            }
            ,
            s._jQueryInterface = function(e) {
                return this.each(function() {
                    var n = t(this).data("bs.scrollspy")
                      , o = "object" === (void 0 === e ? "undefined" : i(e)) && e;
                    if (n || (n = new s(this,o),
                    t(this).data("bs.scrollspy", n)),
                    "string" == typeof e) {
                        if (void 0 === n[e])
                            throw new Error('No method named "' + e + '"');
                        n[e]()
                    }
                })
            }
            ,
            o(s, null, [{
                key: "VERSION",
                get: function() {
                    return "4.0.0-alpha.6"
                }
            }, {
                key: "Default",
                get: function() {
                    return a
                }
            }]),
            s
        }();
        t(window).on(c.LOAD_DATA_API, function() {
            for (var e = t.makeArray(t(d.DATA_SPY)), n = e.length; n--; ) {
                var i = t(e[n]);
                p._jQueryInterface.call(i, i.data())
            }
        }),
        t.fn[e] = p._jQueryInterface,
        t.fn[e].Constructor = p,
        t.fn[e].noConflict = function() {
            return t.fn[e] = s,
            p._jQueryInterface
        }
    }(jQuery),
    function(t) {
        var e = t.fn.tab
          , i = {
            HIDE: "hide.bs.tab",
            HIDDEN: "hidden.bs.tab",
            SHOW: "show.bs.tab",
            SHOWN: "shown.bs.tab",
            CLICK_DATA_API: "click.bs.tab.data-api"
        }
          , s = {
            DROPDOWN_MENU: "dropdown-menu",
            ACTIVE: "active",
            DISABLED: "disabled",
            FADE: "fade",
            SHOW: "show"
        }
          , a = {
            A: "a",
            LI: "li",
            DROPDOWN: ".dropdown",
            LIST: "ul:not(.dropdown-menu), ol:not(.dropdown-menu), nav:not(.dropdown-menu)",
            FADE_CHILD: "> .nav-item .fade, > .fade",
            ACTIVE: ".active",
            ACTIVE_CHILD: "> .nav-item > .active, > .active",
            DATA_TOGGLE: '[data-toggle="tab"], [data-toggle="pill"]',
            DROPDOWN_TOGGLE: ".dropdown-toggle",
            DROPDOWN_ACTIVE_CHILD: "> .dropdown-menu .active"
        }
          , l = function() {
            function e(t) {
                n(this, e),
                this._element = t
            }
            return e.prototype.show = function() {
                var e = this;
                if (!(this._element.parentNode && this._element.parentNode.nodeType === Node.ELEMENT_NODE && t(this._element).hasClass(s.ACTIVE) || t(this._element).hasClass(s.DISABLED))) {
                    var n = void 0
                      , o = void 0
                      , l = t(this._element).closest(a.LIST)[0]
                      , c = r.getSelectorFromElement(this._element);
                    l && (o = t.makeArray(t(l).find(a.ACTIVE)),
                    o = o[o.length - 1]);
                    var u = t.Event(i.HIDE, {
                        relatedTarget: this._element
                    })
                      , d = t.Event(i.SHOW, {
                        relatedTarget: o
                    });
                    if (o && t(o).trigger(u),
                    t(this._element).trigger(d),
                    !d.isDefaultPrevented() && !u.isDefaultPrevented()) {
                        c && (n = t(c)[0]),
                        this._activate(this._element, l);
                        var f = function() {
                            var n = t.Event(i.HIDDEN, {
                                relatedTarget: e._element
                            })
                              , r = t.Event(i.SHOWN, {
                                relatedTarget: o
                            });
                            t(o).trigger(n),
                            t(e._element).trigger(r)
                        };
                        n ? this._activate(n, n.parentNode, f) : f()
                    }
                }
            }
            ,
            e.prototype.dispose = function() {
                t.removeClass(this._element, "bs.tab"),
                this._element = null
            }
            ,
            e.prototype._activate = function(e, n, i) {
                var o = this
                  , l = t(n).find(a.ACTIVE_CHILD)[0]
                  , c = i && r.supportsTransitionEnd() && (l && t(l).hasClass(s.FADE) || Boolean(t(n).find(a.FADE_CHILD)[0]))
                  , u = function() {
                    return o._transitionComplete(e, l, c, i)
                };
                l && c ? t(l).one(r.TRANSITION_END, u).emulateTransitionEnd(150) : u(),
                l && t(l).removeClass(s.SHOW)
            }
            ,
            e.prototype._transitionComplete = function(e, n, i, o) {
                if (n) {
                    t(n).removeClass(s.ACTIVE);
                    var l = t(n.parentNode).find(a.DROPDOWN_ACTIVE_CHILD)[0];
                    l && t(l).removeClass(s.ACTIVE),
                    n.setAttribute("aria-expanded", !1)
                }
                if (t(e).addClass(s.ACTIVE),
                e.setAttribute("aria-expanded", !0),
                i ? (r.reflow(e),
                t(e).addClass(s.SHOW)) : t(e).removeClass(s.FADE),
                e.parentNode && t(e.parentNode).hasClass(s.DROPDOWN_MENU)) {
                    var c = t(e).closest(a.DROPDOWN)[0];
                    c && t(c).find(a.DROPDOWN_TOGGLE).addClass(s.ACTIVE),
                    e.setAttribute("aria-expanded", !0)
                }
                o && o()
            }
            ,
            e._jQueryInterface = function(n) {
                return this.each(function() {
                    var i = t(this)
                      , o = i.data("bs.tab");
                    if (o || (o = new e(this),
                    i.data("bs.tab", o)),
                    "string" == typeof n) {
                        if (void 0 === o[n])
                            throw new Error('No method named "' + n + '"');
                        o[n]()
                    }
                })
            }
            ,
            o(e, null, [{
                key: "VERSION",
                get: function() {
                    return "4.0.0-alpha.6"
                }
            }]),
            e
        }();
        t(document).on(i.CLICK_DATA_API, a.DATA_TOGGLE, function(e) {
            e.preventDefault(),
            l._jQueryInterface.call(t(this), "show")
        }),
        t.fn.tab = l._jQueryInterface,
        t.fn.tab.Constructor = l,
        t.fn.tab.noConflict = function() {
            return t.fn.tab = e,
            l._jQueryInterface
        }
    }(jQuery),
    function(t) {
        if ("undefined" == typeof Tether)
            throw new Error("Bootstrap tooltips require Tether (http://tether.io/)");
        var e = "tooltip"
          , s = ".bs.tooltip"
          , a = t.fn[e]
          , l = {
            animation: !0,
            template: '<div class="tooltip" role="tooltip"><div class="tooltip-inner"></div></div>',
            trigger: "hover focus",
            title: "",
            delay: 0,
            html: !1,
            selector: !1,
            placement: "top",
            offset: "0 0",
            constraints: [],
            container: !1
        }
          , c = {
            animation: "boolean",
            template: "string",
            title: "(string|element|function)",
            trigger: "string",
            delay: "(number|object)",
            html: "boolean",
            selector: "(string|boolean)",
            placement: "(string|function)",
            offset: "string",
            constraints: "array",
            container: "(string|element|boolean)"
        }
          , u = {
            TOP: "bottom center",
            RIGHT: "middle left",
            BOTTOM: "top center",
            LEFT: "middle right"
        }
          , d = {
            SHOW: "show",
            OUT: "out"
        }
          , f = {
            HIDE: "hide" + s,
            HIDDEN: "hidden" + s,
            SHOW: "show" + s,
            SHOWN: "shown" + s,
            INSERTED: "inserted" + s,
            CLICK: "click" + s,
            FOCUSIN: "focusin" + s,
            FOCUSOUT: "focusout" + s,
            MOUSEENTER: "mouseenter" + s,
            MOUSELEAVE: "mouseleave" + s
        }
          , p = {
            FADE: "fade",
            SHOW: "show"
        }
          , h = {
            TOOLTIP: ".tooltip",
            TOOLTIP_INNER: ".tooltip-inner"
        }
          , g = {
            element: !1,
            enabled: !1
        }
          , v = {
            HOVER: "hover",
            FOCUS: "focus",
            CLICK: "click",
            MANUAL: "manual"
        }
          , m = function() {
            function a(t, e) {
                n(this, a),
                this._isEnabled = !0,
                this._timeout = 0,
                this._hoverState = "",
                this._activeTrigger = {},
                this._isTransitioning = !1,
                this._tether = null,
                this.element = t,
                this.config = this._getConfig(e),
                this.tip = null,
                this._setListeners()
            }
            return a.prototype.enable = function() {
                this._isEnabled = !0
            }
            ,
            a.prototype.disable = function() {
                this._isEnabled = !1
            }
            ,
            a.prototype.toggleEnabled = function() {
                this._isEnabled = !this._isEnabled
            }
            ,
            a.prototype.toggle = function(e) {
                if (e) {
                    var n = this.constructor.DATA_KEY
                      , i = t(e.currentTarget).data(n);
                    i || (i = new this.constructor(e.currentTarget,this._getDelegateConfig()),
                    t(e.currentTarget).data(n, i)),
                    i._activeTrigger.click = !i._activeTrigger.click,
                    i._isWithActiveTrigger() ? i._enter(null, i) : i._leave(null, i)
                } else {
                    if (t(this.getTipElement()).hasClass(p.SHOW))
                        return void this._leave(null, this);
                    this._enter(null, this)
                }
            }
            ,
            a.prototype.dispose = function() {
                clearTimeout(this._timeout),
                this.cleanupTether(),
                t.removeData(this.element, this.constructor.DATA_KEY),
                t(this.element).off(this.constructor.EVENT_KEY),
                t(this.element).closest(".modal").off("hide.bs.modal"),
                this.tip && t(this.tip).remove(),
                this._isEnabled = null,
                this._timeout = null,
                this._hoverState = null,
                this._activeTrigger = null,
                this._tether = null,
                this.element = null,
                this.config = null,
                this.tip = null
            }
            ,
            a.prototype.show = function() {
                var e = this;
                if ("none" === t(this.element).css("display"))
                    throw new Error("Please use show on visible elements");
                var n = t.Event(this.constructor.Event.SHOW);
                if (this.isWithContent() && this._isEnabled) {
                    if (this._isTransitioning)
                        throw new Error("Tooltip is transitioning");
                    t(this.element).trigger(n);
                    var i = t.contains(this.element.ownerDocument.documentElement, this.element);
                    if (n.isDefaultPrevented() || !i)
                        return;
                    var o = this.getTipElement()
                      , s = r.getUID(this.constructor.NAME);
                    o.setAttribute("id", s),
                    this.element.setAttribute("aria-describedby", s),
                    this.setContent(),
                    this.config.animation && t(o).addClass(p.FADE);
                    var l = "function" == typeof this.config.placement ? this.config.placement.call(this, o, this.element) : this.config.placement
                      , c = this._getAttachment(l)
                      , u = !1 === this.config.container ? document.body : t(this.config.container);
                    t(o).data(this.constructor.DATA_KEY, this).appendTo(u),
                    t(this.element).trigger(this.constructor.Event.INSERTED),
                    this._tether = new Tether({
                        attachment: c,
                        element: o,
                        target: this.element,
                        classes: g,
                        classPrefix: "bs-tether",
                        offset: this.config.offset,
                        constraints: this.config.constraints,
                        addTargetClasses: !1
                    }),
                    r.reflow(o),
                    this._tether.position(),
                    t(o).addClass(p.SHOW);
                    var f = function() {
                        var n = e._hoverState;
                        e._hoverState = null,
                        e._isTransitioning = !1,
                        t(e.element).trigger(e.constructor.Event.SHOWN),
                        n === d.OUT && e._leave(null, e)
                    };
                    if (r.supportsTransitionEnd() && t(this.tip).hasClass(p.FADE))
                        return this._isTransitioning = !0,
                        void t(this.tip).one(r.TRANSITION_END, f).emulateTransitionEnd(a._TRANSITION_DURATION);
                    f()
                }
            }
            ,
            a.prototype.hide = function(e) {
                var n = this
                  , i = this.getTipElement()
                  , o = t.Event(this.constructor.Event.HIDE);
                if (this._isTransitioning)
                    throw new Error("Tooltip is transitioning");
                var s = function() {
                    n._hoverState !== d.SHOW && i.parentNode && i.parentNode.removeChild(i),
                    n.element.removeAttribute("aria-describedby"),
                    t(n.element).trigger(n.constructor.Event.HIDDEN),
                    n._isTransitioning = !1,
                    n.cleanupTether(),
                    e && e()
                };
                t(this.element).trigger(o),
                o.isDefaultPrevented() || (t(i).removeClass(p.SHOW),
                this._activeTrigger[v.CLICK] = !1,
                this._activeTrigger[v.FOCUS] = !1,
                this._activeTrigger[v.HOVER] = !1,
                r.supportsTransitionEnd() && t(this.tip).hasClass(p.FADE) ? (this._isTransitioning = !0,
                t(i).one(r.TRANSITION_END, s).emulateTransitionEnd(150)) : s(),
                this._hoverState = "")
            }
            ,
            a.prototype.isWithContent = function() {
                return Boolean(this.getTitle())
            }
            ,
            a.prototype.getTipElement = function() {
                return this.tip = this.tip || t(this.config.template)[0]
            }
            ,
            a.prototype.setContent = function() {
                var e = t(this.getTipElement());
                this.setElementContent(e.find(h.TOOLTIP_INNER), this.getTitle()),
                e.removeClass(p.FADE + " " + p.SHOW),
                this.cleanupTether()
            }
            ,
            a.prototype.setElementContent = function(e, n) {
                var o = this.config.html;
                "object" === (void 0 === n ? "undefined" : i(n)) && (n.nodeType || n.jquery) ? o ? t(n).parent().is(e) || e.empty().append(n) : e.text(t(n).text()) : e[o ? "html" : "text"](n)
            }
            ,
            a.prototype.getTitle = function() {
                var t = this.element.getAttribute("data-original-title");
                return t || (t = "function" == typeof this.config.title ? this.config.title.call(this.element) : this.config.title),
                t
            }
            ,
            a.prototype.cleanupTether = function() {
                this._tether && this._tether.destroy()
            }
            ,
            a.prototype._getAttachment = function(t) {
                return u[t.toUpperCase()]
            }
            ,
            a.prototype._setListeners = function() {
                var e = this;
                this.config.trigger.split(" ").forEach(function(n) {
                    if ("click" === n)
                        t(e.element).on(e.constructor.Event.CLICK, e.config.selector, function(t) {
                            return e.toggle(t)
                        });
                    else if (n !== v.MANUAL) {
                        var i = n === v.HOVER ? e.constructor.Event.MOUSEENTER : e.constructor.Event.FOCUSIN
                          , o = n === v.HOVER ? e.constructor.Event.MOUSELEAVE : e.constructor.Event.FOCUSOUT;
                        t(e.element).on(i, e.config.selector, function(t) {
                            return e._enter(t)
                        }).on(o, e.config.selector, function(t) {
                            return e._leave(t)
                        })
                    }
                    t(e.element).closest(".modal").on("hide.bs.modal", function() {
                        return e.hide()
                    })
                }),
                this.config.selector ? this.config = t.extend({}, this.config, {
                    trigger: "manual",
                    selector: ""
                }) : this._fixTitle()
            }
            ,
            a.prototype._fixTitle = function() {
                var t = i(this.element.getAttribute("data-original-title"));
                (this.element.getAttribute("title") || "string" !== t) && (this.element.setAttribute("data-original-title", this.element.getAttribute("title") || ""),
                this.element.setAttribute("title", ""))
            }
            ,
            a.prototype._enter = function(e, n) {
                var i = this.constructor.DATA_KEY;
                return n = n || t(e.currentTarget).data(i),
                n || (n = new this.constructor(e.currentTarget,this._getDelegateConfig()),
                t(e.currentTarget).data(i, n)),
                e && (n._activeTrigger["focusin" === e.type ? v.FOCUS : v.HOVER] = !0),
                t(n.getTipElement()).hasClass(p.SHOW) || n._hoverState === d.SHOW ? void (n._hoverState = d.SHOW) : (clearTimeout(n._timeout),
                n._hoverState = d.SHOW,
                n.config.delay && n.config.delay.show ? void (n._timeout = setTimeout(function() {
                    n._hoverState === d.SHOW && n.show()
                }, n.config.delay.show)) : void n.show())
            }
            ,
            a.prototype._leave = function(e, n) {
                var i = this.constructor.DATA_KEY;
                if (n = n || t(e.currentTarget).data(i),
                n || (n = new this.constructor(e.currentTarget,this._getDelegateConfig()),
                t(e.currentTarget).data(i, n)),
                e && (n._activeTrigger["focusout" === e.type ? v.FOCUS : v.HOVER] = !1),
                !n._isWithActiveTrigger()) {
                    if (clearTimeout(n._timeout),
                    n._hoverState = d.OUT,
                    !n.config.delay || !n.config.delay.hide)
                        return void n.hide();
                    n._timeout = setTimeout(function() {
                        n._hoverState === d.OUT && n.hide()
                    }, n.config.delay.hide)
                }
            }
            ,
            a.prototype._isWithActiveTrigger = function() {
                for (var t in this._activeTrigger)
                    if (this._activeTrigger[t])
                        return !0;
                return !1
            }
            ,
            a.prototype._getConfig = function(n) {
                return n = t.extend({}, this.constructor.Default, t(this.element).data(), n),
                n.delay && "number" == typeof n.delay && (n.delay = {
                    show: n.delay,
                    hide: n.delay
                }),
                r.typeCheckConfig(e, n, this.constructor.DefaultType),
                n
            }
            ,
            a.prototype._getDelegateConfig = function() {
                var t = {};
                if (this.config)
                    for (var e in this.config)
                        this.constructor.Default[e] !== this.config[e] && (t[e] = this.config[e]);
                return t
            }
            ,
            a._jQueryInterface = function(e) {
                return this.each(function() {
                    var n = t(this).data("bs.tooltip")
                      , o = "object" === (void 0 === e ? "undefined" : i(e)) && e;
                    if ((n || !/dispose|hide/.test(e)) && (n || (n = new a(this,o),
                    t(this).data("bs.tooltip", n)),
                    "string" == typeof e)) {
                        if (void 0 === n[e])
                            throw new Error('No method named "' + e + '"');
                        n[e]()
                    }
                })
            }
            ,
            o(a, null, [{
                key: "VERSION",
                get: function() {
                    return "4.0.0-alpha.6"
                }
            }, {
                key: "Default",
                get: function() {
                    return l
                }
            }, {
                key: "NAME",
                get: function() {
                    return e
                }
            }, {
                key: "DATA_KEY",
                get: function() {
                    return "bs.tooltip"
                }
            }, {
                key: "Event",
                get: function() {
                    return f
                }
            }, {
                key: "EVENT_KEY",
                get: function() {
                    return s
                }
            }, {
                key: "DefaultType",
                get: function() {
                    return c
                }
            }]),
            a
        }();
        return t.fn[e] = m._jQueryInterface,
        t.fn[e].Constructor = m,
        t.fn[e].noConflict = function() {
            return t.fn[e] = a,
            m._jQueryInterface
        }
        ,
        m
    }(jQuery));
    !function(r) {
        var a = "popover"
          , l = ".bs.popover"
          , c = r.fn[a]
          , u = r.extend({}, s.Default, {
            placement: "right",
            trigger: "click",
            content: "",
            template: '<div class="popover" role="tooltip"><h3 class="popover-title"></h3><div class="popover-content"></div></div>'
        })
          , d = r.extend({}, s.DefaultType, {
            content: "(string|element|function)"
        })
          , f = {
            FADE: "fade",
            SHOW: "show"
        }
          , p = {
            TITLE: ".popover-title",
            CONTENT: ".popover-content"
        }
          , h = {
            HIDE: "hide" + l,
            HIDDEN: "hidden" + l,
            SHOW: "show" + l,
            SHOWN: "shown" + l,
            INSERTED: "inserted" + l,
            CLICK: "click" + l,
            FOCUSIN: "focusin" + l,
            FOCUSOUT: "focusout" + l,
            MOUSEENTER: "mouseenter" + l,
            MOUSELEAVE: "mouseleave" + l
        }
          , g = function(s) {
            function c() {
                return n(this, c),
                t(this, s.apply(this, arguments))
            }
            return e(c, s),
            c.prototype.isWithContent = function() {
                return this.getTitle() || this._getContent()
            }
            ,
            c.prototype.getTipElement = function() {
                return this.tip = this.tip || r(this.config.template)[0]
            }
            ,
            c.prototype.setContent = function() {
                var t = r(this.getTipElement());
                this.setElementContent(t.find(p.TITLE), this.getTitle()),
                this.setElementContent(t.find(p.CONTENT), this._getContent()),
                t.removeClass(f.FADE + " " + f.SHOW),
                this.cleanupTether()
            }
            ,
            c.prototype._getContent = function() {
                return this.element.getAttribute("data-content") || ("function" == typeof this.config.content ? this.config.content.call(this.element) : this.config.content)
            }
            ,
            c._jQueryInterface = function(t) {
                return this.each(function() {
                    var e = r(this).data("bs.popover")
                      , n = "object" === (void 0 === t ? "undefined" : i(t)) ? t : null;
                    if ((e || !/destroy|hide/.test(t)) && (e || (e = new c(this,n),
                    r(this).data("bs.popover", e)),
                    "string" == typeof t)) {
                        if (void 0 === e[t])
                            throw new Error('No method named "' + t + '"');
                        e[t]()
                    }
                })
            }
            ,
            o(c, null, [{
                key: "VERSION",
                get: function() {
                    return "4.0.0-alpha.6"
                }
            }, {
                key: "Default",
                get: function() {
                    return u
                }
            }, {
                key: "NAME",
                get: function() {
                    return a
                }
            }, {
                key: "DATA_KEY",
                get: function() {
                    return "bs.popover"
                }
            }, {
                key: "Event",
                get: function() {
                    return h
                }
            }, {
                key: "EVENT_KEY",
                get: function() {
                    return l
                }
            }, {
                key: "DefaultType",
                get: function() {
                    return d
                }
            }]),
            c
        }(s);
        r.fn[a] = g._jQueryInterface,
        r.fn[a].Constructor = g,
        r.fn[a].noConflict = function() {
            return r.fn[a] = c,
            g._jQueryInterface
        }
    }(jQuery)
}(),
"undefined" == typeof jQuery)
    throw new Error("Jasny Bootstrap's JavaScript requires jQuery");




jQuery(function(){
    jQuery("a.hamburger-toggle").click(function(e){
      e.preventDefault();
      if (jQuery("#menu1").hasClass("hidden-sm")) { //the menu is open
        jQuery("#menu1").removeClass("hidden-sm").addClass("hidden-xs");
      }else{ //menu is not open
        jQuery("#menu1").removeClass("hidden-xs").addClass("hidden-sm");
      }
    });
    var items = jQuery(".gift-card-image");
    setInterval(function(e) {
        var x = items[Math.floor(Math.random()*items.length)];
        var y = items[Math.floor(Math.random()*items.length)];
        jQuery(x).addClass('active');
        jQuery(y).addClass('active');
        setTimeout(function() {
            jQuery(x).removeClass('active');
            jQuery(y).removeClass('active');
        },1.5*1000);
    }, 1*1000);
  });
