import { Component, OnInit } from '@angular/core';
declare var jquery: any;
declare var $: any;

@Component({
  selector: 'app-join',
  templateUrl: './join.component.html',
  styleUrls: ['./join.component.css']
})
export class JoinComponent implements OnInit {

  constructor() { }

  ngOnInit() {
    $.getScript('assets/javascripts/scripts.js');
    $.getScript('assets/LivIconsEvo/js/LivIconsEvo.Tools.js');
    $.getScript('assets/LivIconsEvo/js/LivIconsEvo.defaults.js');
    $.getScript('assets/LivIconsEvo/js/LivIconsEvo.min.js');
  }

}
