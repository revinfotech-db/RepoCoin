import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { AlertModule, AlertService } from 'ngx-alerts';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { AppConfig } from '../../app.config';
import { UserService } from '../../services/user.service';
import { CustomValidators } from '../../CustomValidators';
declare var $: any;

@Component({
  selector: 'app-car-apply',
  templateUrl: './car-apply.component.html',
  styleUrls: ['./car-apply.component.css'],
})
export class CarApplyComponent implements OnInit {
  myForm: FormGroup;
  submitted = false;
  countries;
  youtube: any;
  typeImage: any;
  picturldata: any;
  country: any;
  selectedCountry = false;
  picture_required = false;
  picture_error = false;
  private base64textString: String = '';
  emailRegex: any = new RegExp(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/);
  public loading = false;
  NameRegex: any = new RegExp(/^[a-zA-Z ]+$/);
  constructor(private fb: FormBuilder, private alertService: AlertService,
    private http: Http, private appConfig: AppConfig, private userService: UserService) {
      this.myForm = this.fb.group({
        email: ['', Validators.compose([Validators.required, Validators.pattern(this.emailRegex)])],
        ownavehicle: ['', Validators.required],
        howyougetaround: ['', Validators.required],
        havedriverlicense: ['', Validators.required],
        havechildren: ['', Validators.required],
        cartype: ['', Validators.required],
        youtubelink: ['', [Validators.required, CustomValidators.ValidateUrl]],
        essay: ['', Validators.required],
        firstname: ['', Validators.required],
        lastname: ['', Validators.required],
        phone:  ['', Validators.compose([Validators.required, Validators.minLength(10), Validators.maxLength(12)])],
        picture: [''],
        firstline: ['', Validators.required],
        secondline: ['', Validators.required],
        city: ['', Validators.required],
        state: ['', Validators.required],
        zipcode: ['', Validators.required],
        acceptapplication: ['', Validators.required]
     });
  }
 // acceptapplication: ['', Validators.required],
  ngOnInit() {
    $.getScript('assets/javascripts/validator.min.js');
    $.getScript('assets/javascripts/jquery.easyWizard.js');
    $.getScript('assets/javascripts/apply.js');
    $.getScript('assets/javascripts/scripts.js');
    this.userService.getcountries().subscribe(data => {
      this.countries = data;
      console.log(data);
    });
  }

  onSubmit() {
      this.submitted = true;
      if (this.myForm.valid) {
        this.loading = true;
        console.log(this.myForm.value);
        if (this.myForm.value.youtubelink) {
          const fulllink = this.myForm.value.youtubelink;
          this.youtube = fulllink.replace('https://www.youtube.com/watch?v=', '');
        }
        const application_records = {
          'application': {
            'ownavehicle': this.myForm.value.ownavehicle,
            'howyougetaround': this.myForm.value.howyougetaround,
            'havedriverlicense': this.myForm.value.havedriverlicense,
            'havechildren': this.myForm.value.havechildren,
            'cartype': this.myForm.value.cartype,
            'youtubelink': this.youtube,
            'essay': this.myForm.value.essay
          },
          'personalinfo': {
            'firstname': this.myForm.value.firstname,
            'lastname': this.myForm.value.lastname,
            'email': this.myForm.value.email,
            'phone': this.myForm.value.phone,
            'address': {
              'firstline': this.myForm.value.firstline,
              'secondline': this.myForm.value.secondline,
              'city': this.myForm.value.city,
              'state': this.country,
              'zipcode': this.myForm.value.zipcode,
            }
          },
          'wallet': {
            'key': '',
            'secret': ''
          },
          'picture': 'data:'.concat(this.typeImage, ';base64,', this.picturldata),
          'donation': '',
          'acceptapplication': 'false',
          'votingstart': '',
          'votingends': '',
          'winnerannounced': ''
        };
        console.log(application_records);
        this.userService.ApplicationFormService(application_records).subscribe(
          (data) => {
            this.alertService.success(data.json().message);
            this.loading = false;
            this.myForm.reset();
            this.submitted = false;
          });
        //  this.myForm.reset();
      } else {
        console.log(this.myForm);
        if (this.picturldata) {
          this.picture_required = false;
        } else {
          this.picture_required = true;
        }
      this.alertService.warning('form is invalid');
    }
  }

 /* handleFileSelect(evt) {
    const files = evt.target.files;
    const file = files[0];
    console.log(file);
     this.typeImage = files[0].type;
   // console.log(typeImage);
    if (files && file) {
    const reader = new FileReader();
    reader.onload = this._handleReaderLoaded.bind(this);
      reader.readAsBinaryString(file);
    }
  }*/

  handleFileSelect(evt) {
    const files = evt.target.files;
    const file = files[0];
    if (!file) {
     this.picture_required = true;
     return;
    } else {
     this.picture_required = false;
    }
    this.typeImage = files[0].type;
    if (this.typeImage.search('jpeg') > 0
      || this.typeImage.search('jpg') > 0 || this.typeImage.search('png') > 0 || this.typeImage.search('bmp') > 0) {
     this.picture_error = false;
    } else {
     this.picture_error = true;
    }
    if (files && file) {
     const reader = new FileReader();
     reader.onload = this._handleReaderLoaded.bind(this);
     reader.readAsBinaryString(file);
    }
   }

_handleReaderLoaded(readerEvt) {
   const binaryString = readerEvt.target.result;
  this.base64textString = btoa(binaryString);
  this.picturldata = this.base64textString;
 // console.log(this.picturldata);
  }
  onlyNumberKey(event) {
    return (event.charCode === 8 || event.charCode === 0) ? null : event.charCode >= 48 && event.charCode <= 57;
}

selectCountry(country) {
  this.country = country.target.value;
  this.selectedCountry = true;
  }

}
