import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { CarApplyComponent } from './car-apply.component';

describe('CarApplyComponent', () => {
  let component: CarApplyComponent;
  let fixture: ComponentFixture<CarApplyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ CarApplyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(CarApplyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
