import { Component, OnInit } from '@angular/core';
declare var $: any;

@Component({
  selector: 'app-terms',
  templateUrl: './terms.component.html',
  styleUrls: ['./terms.component.css']
})
export class TermsComponent implements OnInit {

  constructor() {
    $.getScript('assets/javascripts/scripts.js');
    $.getScript('assets/LivIconsEvo/js/LivIconsEvo.Tools.js');
    $.getScript('assets/LivIconsEvo/js/LivIconsEvo.defaults.js');
    $.getScript('assets/LivIconsEvo/js/LivIconsEvo.min.js');
   }

  ngOnInit() {
    $('#menu1').removeClass('bar--absolute bar--transparent');
    $('.logo.logo-dark').attr('src', '/assets/img/logo-dark.png');
  }

}
