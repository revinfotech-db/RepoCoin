import { Component, OnInit } from '@angular/core';
import { AlertModule, AlertService } from 'ngx-alerts';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { AppConfig } from '../../app.config';
import { UserService } from '../../services/user.service';
import { Subject } from 'rxjs/Subject';
import { ActivatedRoute, Router } from '@angular/router';
declare var jquery: any;
declare var $: any;

@Component({
  selector: 'app-videos',
  templateUrl: './videos.component.html',
  styleUrls: ['./videos.component.css']
})
export class VideosComponent implements OnInit {
  data: any;
  records: any;
  constructor(private http: Http, private appConfig: AppConfig, private userService: UserService, public router: Router) { }

  ngOnInit() {
    $.getScript('assets/javascripts/scripts.js');
    $.getScript('assets/LivIconsEvo/js/LivIconsEvo.Tools.js');
    $.getScript('assets/LivIconsEvo/js/LivIconsEvo.defaults.js');
    $.getScript('assets/LivIconsEvo/js/LivIconsEvo.min.js');
    this.getVideoUsers();
  }


  getVideoUsers() {
    const application_records = {
      'acceptapplication': true
    };
    this.userService.VideoSearchService(application_records).map(
      (response) => response.json()).subscribe(response => {
        if (response.length) {
          this.data = response;
          console.log(this.data);
             } else {
              this.data = [];
             }
      });
  }
  ViewCoinRoute(objectId) {
    this.router.navigate(['/viewCoin', { '_id': objectId }]);
  }

}
