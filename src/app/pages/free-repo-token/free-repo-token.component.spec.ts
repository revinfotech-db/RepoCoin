import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FreeRepoTokenComponent } from './free-repo-token.component';

describe('FreeRepoTokenComponent', () => {
  let component: FreeRepoTokenComponent;
  let fixture: ComponentFixture<FreeRepoTokenComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FreeRepoTokenComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FreeRepoTokenComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
