import { Component, OnInit } from '@angular/core';
import { Title } from '@angular/platform-browser';
import { AlertModule, AlertService } from 'ngx-alerts';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { AppConfig } from '../../app.config';
import { UserService } from '../../services/user.service';
import { Subject } from 'rxjs/Subject';
import { ActivatedRoute, Router } from '@angular/router';
declare var jquery: any;
declare var $: any;

@Component({
  selector: 'app-rewards',
  templateUrl: './rewards.component.html',
  styleUrls: ['./rewards.component.css',
  '../../../assets/rewards/stylesheet/bootstrap.min.css',
    '../../../assets/rewards/stylesheet/main.css',
    '../../../assets/rewards/stylesheet/header.css',
    '../../../assets/rewards/stylesheet/footer.css'
  ]
})
export class RewardsComponent implements OnInit {
  productFeatured: any;
  featued_desc: any;
  featured_name: any;
  featured_image: any;
  featued_price: any;
  not_Featured: any;
  constructor(private router: Router,
    private titleService: Title,
    private http: Http,
    private appConfig: AppConfig,
    private userService: UserService) {
    $.getScript('assets/javascripts/custom.js');

    this.getIsFeatued();
    this.getNotFeatued();
  }

  ngOnInit() {
    this.titleService.setTitle('Best App For Free Gift Cards | Shopkick');
    $.getScript('assets/rewards/javascripts/bubble.min.js');
  }
  getIsFeatued() {
    const records = {
      'isFeatured': 'true'
    };
    this.userService.getFeatuedProduct(records).map(
      (response) => response.json()).subscribe(response => {
        if (response.length) {
          this.productFeatured = response[0];
          this.featured_name = response[0].name;
          this.featued_desc = response[0].description;
          this.featured_image = response[0].picture;
          this.featued_price = response[0].value;
             } else {
              this.productFeatured = [];
             }
      });
  }
  getNotFeatued() {
    const records = {
      'isFeatured': 'false'
    };
    this.userService.getNotFeatuedProduct(records).map(
      (response) => response.json()).subscribe(response => {
        if (response.length) {
          this.not_Featured = response;
       //   console.log(this.not_Featured);
             } else {
              this.not_Featured = [];
             }
      });
  }

}
