import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { AlertModule, AlertService } from 'ngx-alerts';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { AppConfig } from '../../app.config';
import { UserService } from '../../services/user.service';
declare var $: any;

@Component({
  selector: 'app-contact-us',
  templateUrl: './contact-us.component.html',
  styleUrls: ['./contact-us.component.css']
})
export class ContactUsComponent implements OnInit {

  myForm: FormGroup;
  submitted = false;
  emailRegex: any = new RegExp(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/);
  public loading = false;
  NameRegex: any = new RegExp(/^[a-zA-Z ]+$/);
 constructor(private fb: FormBuilder, private alertService: AlertService,
  private appConfig: AppConfig, private http: Http, private userService: UserService) {
    $.getScript('assets/javascripts/scripts.js');
    $.getScript('assets/LivIconsEvo/js/LivIconsEvo.Tools.js');
    $.getScript('assets/LivIconsEvo/js/LivIconsEvo.defaults.js');
   $.getScript('assets/LivIconsEvo/js/LivIconsEvo.min.js');

   this.myForm = this.fb.group({
     email: ['', Validators.compose([Validators.required, Validators.pattern(this.emailRegex)])],
     name: ['', Validators.compose([Validators.required, Validators.minLength(1),  Validators.pattern(this.NameRegex)])],
     companyname: ['', Validators.compose([Validators.required, Validators.minLength(4)])],
     workphone: ['', Validators.compose([Validators.required, Validators.minLength(10), Validators.maxLength(10)])],
     message: ['', Validators.required]
  });
 }

  ngOnInit() {
    $('#menu1').removeClass('bar--absolute bar--transparent');
    $('.logo.logo-dark').attr('src', 'assets/img/logo-dark.png');
  }

  onSubmit() {
    this.submitted = true;
    if (this.myForm.valid) {
      this.loading = true;
      console.log(this.myForm.value);
      const contact_records = {
        'email': this.myForm.value.email,
        'name': this.myForm.value.name,
        'companyname': this.myForm.value.companyname,
        'message': this.myForm.value.message,
        'workphone': this.myForm.value.workphone
      };
      this.userService.contactFormService(contact_records).subscribe(
        (data) => {
         this.alertService.success(data.json().message);
         this.loading = false;
         this.myForm.reset();
         this.submitted = false;
        });
    } else {
      this.loading = false;
      this.alertService.warning('Please check your form details again !!');
    }
  }
  onlyNumberKey(event) {
    return (event.charCode === 8 || event.charCode === 0) ? null : event.charCode >= 48 && event.charCode <= 57;
}


}
