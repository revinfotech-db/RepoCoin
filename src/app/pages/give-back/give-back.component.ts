import { Component, OnInit } from '@angular/core';
declare var jquery: any;
declare var $: any;

@Component({
  selector: 'app-give-back',
  templateUrl: './give-back.component.html',
  styleUrls: ['./give-back.component.css']
})
export class GiveBackComponent implements OnInit {
  constructor() {}

  ngOnInit() {
    $.getScript('assets/javascripts/particles.min.js');
   $.getScript('assets/javascripts/scripts.js');
    $.getScript('assets/LivIconsEvo/js/LivIconsEvo.Tools.js');
    $.getScript('assets/LivIconsEvo/js/LivIconsEvo.defaults.js');
    $.getScript('assets/LivIconsEvo/js/LivIconsEvo.min.js');
  }

}
