import { Component, OnInit } from '@angular/core';
declare var $: any;

@Component({
  selector: 'app-give-way-rule',
  templateUrl: './give-way-rule.component.html',
  styleUrls: ['./give-way-rule.component.css']
})
export class GiveWayRuleComponent implements OnInit {

  constructor() {
    $.getScript('assets/javascripts/scripts.js');
    $.getScript('assets/LivIconsEvo/js/LivIconsEvo.Tools.js');
    $.getScript('assets/LivIconsEvo/js/LivIconsEvo.defaults.js');
    $.getScript('assets/LivIconsEvo/js/LivIconsEvo.min.js');
   }

  ngOnInit() {
    $('#menu1').removeClass('bar--absolute bar--transparent');
   $('.logo.logo-dark').attr('src', '/assets/img/logo-dark.png');
  }

}
