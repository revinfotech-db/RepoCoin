import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GiveWayRuleComponent } from './give-way-rule.component';

describe('GiveWayRuleComponent', () => {
  let component: GiveWayRuleComponent;
  let fixture: ComponentFixture<GiveWayRuleComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GiveWayRuleComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GiveWayRuleComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
