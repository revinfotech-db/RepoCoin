import { Component, OnInit } from '@angular/core';
declare var jquery: any;
declare var $: any;

@Component({
  selector: 'app-launch',
  templateUrl: './launch.component.html',
  styleUrls: ['./launch.component.css']
})
export class LaunchComponent implements OnInit {
  substring = null;
  constructor() { this.substring = null; }
  ngOnInit() {
     $.getScript('assets/javascripts/flickity.min.js');
     $.getScript('assets/javascripts/scripts.js');
     $.getScript('assets/LivIconsEvo/js/LivIconsEvo.Tools.js');
     $.getScript('assets/LivIconsEvo/js/LivIconsEvo.defaults.js');
     $.getScript('assets/LivIconsEvo/js/LivIconsEvo.min.js');
  }
}
