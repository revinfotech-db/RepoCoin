import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { AlertModule, AlertService } from 'ngx-alerts';
import { AppConfig } from '../../app.config';
import { UserService } from '../../services/user.service';
import { Subject } from 'rxjs/Subject';
import { DatePipe } from '@angular/common';
declare var jquery: any;
declare var $: any;

@Component({
  selector: 'app-vote-coin',
  templateUrl: './vote-coin.component.html',
  styleUrls: ['./vote-coin.component.css']
})
export class VoteCoinComponent implements OnInit {

  data_id: any;
  user_data: any;
  firstname: any;
  city: any;
  state: any;
  essay: any;
  youLink = '';
  picture: any;
  votingstart: any;
  votingends: any;
  winnerannounced: any;
  walletKey: any;
  endDate: any;
  eventExpires;
  EventStartDays;
  deadline: any;
  leftdays: any;
  eventEnd: any;
  public loading = false;
  constructor(private route: ActivatedRoute, private http: Http,
    private appConfig: AppConfig, private userService: UserService, public router: Router,
    private alertService: AlertService, private datePipe: DatePipe) {
    this.route.params.subscribe(params => {
      this.data_id = params._id;
    });


  }

  ngOnInit() {
    var eventStart;
    var eventEnd;
    this.getUserDetails();
    this.loading = true;
    setTimeout(() => {
      $.getScript('assets/javascripts/plyr.js');
      $.getScript('assets/javascripts/playerSetup.js');
      $.getScript('assets/javascripts/video-player.js');
      beginCountdown('countdown-vote', localStorage.getItem('eventEnd'));
    }, 3000);
    $.getScript('assets/javascripts/countdown.min.js');
    $.getScript('assets/javascripts/scripts.js');
    $.getScript('assets/LivIconsEvo/js/LivIconsEvo.Tools.js');
    $.getScript('assets/LivIconsEvo/js/LivIconsEvo.defaults.js');
    $.getScript('assets/LivIconsEvo/js/LivIconsEvo.min.js');
    function timeToEvent(eventStart) {
      var timeRemaining = Date.parse(localStorage.getItem('eventEnd')) - Date.parse(Date());
      var days = Math.floor(timeRemaining/(1000*60*60*24));
      var hours = Math.floor((timeRemaining/(1000*60*60))%24);
      var minutes = Math.floor((timeRemaining/ 1000/60)%60);
      var seconds = Math.floor((timeRemaining / 1000) % 60);

      return {
        'totalTime': timeRemaining,
        'days': days,
        'hours': hours,
        'minutes': minutes,
        'seconds': seconds
      };
    }
    function beginCountdown(id, eventStart) {
      var counter = document.getElementById(id);
      var daysElement = counter.querySelector('.days');
      var hoursElement = counter.querySelector('.hours');
      var minutesElement = counter.querySelector('.minutes');
      var secondsElement = counter.querySelector('.seconds');
      function updateCountdown() {
        var timeRemaining = timeToEvent(eventStart);
        // slice = limits number of digits to two so you don't get 023 hours...
        daysElement.innerHTML = ('' + timeRemaining.days).slice(-2);
        hoursElement.innerHTML = ('0' + timeRemaining.hours).slice(-2);
        minutesElement.innerHTML = ('0' + timeRemaining.minutes).slice(-2);
        secondsElement.innerHTML = ('0' + timeRemaining.seconds).slice(-2);
        // stops countdown at less than or zero
        if (timeRemaining.totalTime <= 0) {
          // clearInterval(timeinterval);
          document.getElementById('countdown-vote').innerHTML = 'Voting Closed. Results to be annouced at' + this.winnerannounced;
          document.getElementById('countdown-vote').style.fontSize = '2em';
          document.getElementById('countdown-vote').style.padding = '2rem 0rem';
        }
      }
      updateCountdown();
      var timeinterval = setInterval(updateCountdown, 1000);  // 1000 = 1sec
    }
  }
  getUserDetails() {
    var eventStart;
    var eventEnd;
    const user_find = {
      '_id': this.data_id
    };
    this.userService.UserDetailsService(user_find).map(
      (response) => response.json()).subscribe(response => {
        if (response.length) {
          this.user_data = response[0];
          console.log(this.user_data);
          this.firstname = this.user_data.personalinfo.firstname;
          this.city = this.user_data.personalinfo.address.city;
          this.state = this.user_data.personalinfo.address.state;
          this.essay = this.user_data.application.essay;
          this.youLink = this.user_data.application.youtubelink;
          $('#dataVideo').attr('data-video-id', this.youLink);
          this.picture = this.user_data.picture;
          this.votingstart = this.user_data.votingstart;
          this.votingends = this.user_data.votingends;
          this.winnerannounced = this.user_data.winnerannounced;
          this.walletKey = this.user_data.wallet.key;
          var endDate = this.user_data.votingends;
          var dateCheck = new Date(endDate);
          this.deadline = dateCheck;
          var nowDate = new Date();
          var onlyDay = nowDate.getDate();
          this.leftdays = dateCheck.getDate() - onlyDay;
          this.eventEnd = dateCheck;
          localStorage.setItem('eventEnd', this.eventEnd);

         } else {
          this.user_data = [];
          this.firstname = [];
          this.city = [];
          this.state = [];
          this.essay = [];
          this.youLink = '';
          this.picture = [];
          this.votingstart = [];
          this.votingends = [];
          this.winnerannounced = [];
          this.walletKey = [];
        }
        this.loading = false;
      });
  }
  copyToClipboard() {
    this.alertService.success('token address has copied to your clip board');
  }
  PageTransfer() {
    this.router.navigate(['/car-apply']);
  }
  toTimestamp(strDate) {
    const datum = Date.parse(strDate);
    return datum / 1000;
   }
}
