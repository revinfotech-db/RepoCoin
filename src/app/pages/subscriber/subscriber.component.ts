import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, FormControl, Validators } from '@angular/forms';
import { AlertModule, AlertService } from 'ngx-alerts';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { AppConfig } from '../../app.config';
import { UserService } from '../../services/user.service';

@Component({
  selector: 'app-subscriber',
  templateUrl: './subscriber.component.html',
  styleUrls: ['./subscriber.component.css']
})
export class SubscriberComponent implements OnInit {
  substring = null;
  subscribeForm: FormGroup;
  submitted = false;
  emailRegex: any = new RegExp(/^\w+([\.-]?\w+)*@\w+([\.-]?\w+)*(\.\w{2,3})+$/);
  public loading = false;

  constructor(private fb: FormBuilder, private alertService: AlertService,
    private appConfig: AppConfig, private http: Http, private userService: UserService) {
    this.subscribeForm = this.fb.group({
      email: ['', Validators.compose([Validators.required, Validators.pattern(this.emailRegex)])]
    });
    this.substring = null;
  }

  ngOnInit() {
  }

  onSubmit() {
    this.submitted = true;
    if (this.subscribeForm.valid) {
      this.loading = true;
      const emaildata = {
        'email': this.subscribeForm.value.email
      };
       this.userService.createSubscriber(emaildata).subscribe(
         (data) => {
          this.alertService.success(data.json().message);
          this.loading = false;
          this.subscribeForm.reset();
          this.submitted = false;
         });
   }
  }


}
