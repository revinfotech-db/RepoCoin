import { Component, OnInit } from '@angular/core';
declare var jquery: any;
declare var $: any;

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css']
})
export class HomeComponent implements OnInit {
  constructor() {  }

  ngOnInit() {
    $.getScript('assets/javascripts/flickity.min.js');
    $.getScript('assets/javascripts/drawchart.js');
    $.getScript('assets/javascripts/scripts.js');
    $.getScript('assets/LivIconsEvo/js/LivIconsEvo.Tools.js');
    $.getScript('assets/LivIconsEvo/js/LivIconsEvo.defaults.js');
    $.getScript('assets/LivIconsEvo/js/LivIconsEvo.min.js');

    const eventStart = new Date(Date.parse('03/10/2018 00:00 am'));

    function timeToEvent (eventStart: any) {
      var timeRemaining = Date.parse(eventStart) - Date.parse(Date());
      var days = Math.floor(timeRemaining/(1000*60*60*24));
      var hours = Math.floor((timeRemaining/(1000*60*60))%24);
      var minutes = Math.floor((timeRemaining/ 1000/60)%60);
      var seconds = Math.floor((timeRemaining/1000)%60);
      return {
        'totalTime': timeRemaining,
        'days': days,
        'hours': hours,
        'minutes': minutes,
        'seconds': seconds
      };
    }
    /*
    beginCountdown = function name
    id = id in CSS
    eventStart = countdown date
    counter = div with id that will hold countdown clock
    daysElement, hoursElement, minutesElement, secondsElement = html elements that will hold parts of the countdown based on querySelector
    */

    function beginCountdown(id, eventStart) {
      var counter = document.getElementById(id);
      var daysElement = counter.querySelector('.days');
      var hoursElement = counter.querySelector('.hours');
      var minutesElement = counter.querySelector('.minutes');
      var secondsElement = counter.querySelector('.seconds');


      function updateCountdown() {
        var timeRemaining = timeToEvent(eventStart);
        // slice = limits number of digits to two so you don't get 023 hours...
        daysElement.innerHTML = ('' + timeRemaining.days).slice(-2);
        hoursElement.innerHTML = ('0' + timeRemaining.hours).slice(-2);
        minutesElement.innerHTML = ('0' + timeRemaining.minutes).slice(-2);
        secondsElement.innerHTML = ('0' + timeRemaining.seconds).slice(-2);
        // stops countdown at less than or zero
        if (timeRemaining.totalTime <= 0) {
          // clearInterval(timeinterval);
          document.getElementById('countdown-sale').innerHTML = 'Sale Ended';
          document.getElementById('countdown-sale').style.fontSize = '2em';
          document.getElementById('countdown-sale').style.padding = '2rem 0rem';
        }
      }
      updateCountdown();
      var timeinterval = setInterval(updateCountdown, 1000);  // 1000 = 1sec
    }
    beginCountdown('countdown-sale', eventStart);
    }
    scroll(el) {
    el.scrollIntoView();
}

  }

