import { Injectable } from '@angular/core';
import { AppConfig } from '../app.config';
import { Http, Headers, RequestOptions, Response } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';
import 'rxjs/add/operator/map';

@Injectable()
export class UserService {

  constructor(public http: Http, private config: AppConfig) { }

  createSubscriber(email: any) {
    return this.http.post(this.config.apiUrl + 'subscriber/create', email);
  }

  contactFormService(data: any) {
    return this.http.post(this.config.apiUrl + 'contact/create', data);
  }
  ApplicationFormService(data: any) {
    return this.http.post(this.config.apiUrl + 'application/create', data);
  }

  VideoSearchService(data: any) {
    return this.http.post(this.config.apiUrl + 'application/search', data);
  }
  UserDetailsService(data: any) {
    return this.http.post(this.config.apiUrl + 'application/search', data);
  }
  getcountries() {
    return this.http.get('https://gist.githubusercontent.com/mshafrir/2646763/raw/8b0dbb93521f5d6889502305335104218454c2bf/states_titlecase.json')
      .map((response: Response) => response.json())
  }

  getFeatuedProduct(data: any) {
    return this.http.post(this.config.apiUrl + 'product/search', data);
  }
  getNotFeatuedProduct(data: any) {
    return this.http.post(this.config.apiUrl + 'product/search', data);
  }

}
