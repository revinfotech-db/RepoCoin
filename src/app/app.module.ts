import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { FormsModule, FormControl, ReactiveFormsModule, AbstractControl } from '@angular/forms';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { HttpModule } from '@angular/http';
import { DatePipe } from '@angular/common';


import { AppComponent } from './app.component';
import { HeaderComponent } from './layout/header/header.component';
import { FooterComponent } from './layout/footer/footer.component';
import { HomeComponent } from './home/home.component';
import { FaqComponent } from './pages/faq/faq.component';
import { JoinComponent } from './pages/join/join.component';
import { LaunchComponent } from './pages/launch/launch.component';
import { StarComponent } from './pages/star/star.component';
import { CrowdsaleComponent } from './pages/crowdsale/crowdsale.component';
import { TokenSaleComponent } from './pages/token-sale/token-sale.component';
import { SubscriberComponent } from './pages/subscriber/subscriber.component';

// Import ngx library
import { AlertModule } from 'ngx-alerts';
import { LoadingModule } from 'ngx-loading';
import { ClipboardModule } from 'ngx-clipboard';
import { HeaderNavComponent } from './layout/header-nav/header-nav.component';
import { FooterNavComponent } from './layout/footer-nav/footer-nav.component';
import { ComingSoonComponent } from './pages/coming-soon/coming-soon.component';
import { GiveBackComponent } from './pages/give-back/give-back.component';
import { VideosComponent } from './pages/videos/videos.component';
import { VoteCoinComponent } from './pages/vote-coin/vote-coin.component';
import { RewardHeaderComponent } from './layout/reward-header/reward-header.component';
import { RewardsComponent } from './pages/rewards/rewards.component';
import { RewardFooterComponent } from './layout/reward-footer/reward-footer.component';
import { MeetTeamComponent } from './pages/meet-team/meet-team.component';
import { ContactUsComponent } from './pages/contact-us/contact-us.component';
import { CarApplyComponent } from './pages/car-apply/car-apply.component';
import { TermsComponent } from './pages/terms/terms.component';
import { BountyComponent } from './pages/bounty/bounty.component';
import { FreeRepoTokenComponent } from './pages/free-repo-token/free-repo-token.component';
import { AppConfig } from './app.config';
import { UserService } from './services/user.service';
import { GiveWayRuleComponent } from './pages/give-way-rule/give-way-rule.component';
import { PrivacyPolicyComponent } from './pages/privacy-policy/privacy-policy.component';
import { TeamMemberComponent } from './pages/team-member/team-member.component';
import { NotFoundComponent } from './pages/not-found/not-found.component';

const appRoutes: Routes = [
  { path: '', redirectTo: '/', pathMatch: 'full'},
  { path: '', component: ComingSoonComponent},
  { path: 'home', component: HomeComponent},
  { path: 'faq', component: FaqComponent},
  { path: 'join', component: JoinComponent},
  { path: 'launch', component: LaunchComponent},
  { path: 'star', component: StarComponent},
  { path: 'crowdsale', component: CrowdsaleComponent },
  { path: 'giveBack', component: GiveBackComponent },
  { path: 'videos', component: VideosComponent },
  { path: 'viewCoin', component: VoteCoinComponent },
  { path: 'rewards', component: RewardsComponent },
  { path: 'contactUs', component: ContactUsComponent },
  { path: 'terms-Condition', component: TermsComponent },
  { path: 'meetTeam', component: MeetTeamComponent },
  { path: 'car-apply', component: CarApplyComponent },
  { path: 'bounty', component: BountyComponent },
  { path: 'Freetoken', component: FreeRepoTokenComponent },
  { path: 'givewayrules', component: GiveWayRuleComponent },
  { path: 'privacy', component: PrivacyPolicyComponent },
  { path: 'team', component: TeamMemberComponent },
  { path: '**', component: NotFoundComponent }
];
@NgModule({
  declarations: [
    AppComponent,
    HeaderComponent,
    FooterComponent,
    HomeComponent,
    FaqComponent,
    JoinComponent,
    LaunchComponent,
    StarComponent,
    CrowdsaleComponent,
    TokenSaleComponent,
    SubscriberComponent,
    HeaderNavComponent,
    FooterNavComponent,
    ComingSoonComponent,
    GiveBackComponent,
    VideosComponent,
    VoteCoinComponent,
    RewardHeaderComponent,
    RewardsComponent,
    RewardFooterComponent,
    MeetTeamComponent,
    ContactUsComponent,
    CarApplyComponent,
    TermsComponent,
    BountyComponent,
    FreeRepoTokenComponent,
    GiveWayRuleComponent,
    PrivacyPolicyComponent,
    TeamMemberComponent,
    NotFoundComponent,
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpModule,
    LoadingModule,
    ClipboardModule,
    ReactiveFormsModule,
     RouterModule.forRoot(
      appRoutes,
      { enableTracing: false } // <-- debugging purposes only
    ),
    AlertModule.forRoot(),
    BrowserAnimationsModule,
  ],
  providers: [AppConfig, UserService, DatePipe],
  bootstrap: [AppComponent]
})
export class AppModule { }
