import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RewardHeaderComponent } from './reward-header.component';

describe('RewardHeaderComponent', () => {
  let component: RewardHeaderComponent;
  let fixture: ComponentFixture<RewardHeaderComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RewardHeaderComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RewardHeaderComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
