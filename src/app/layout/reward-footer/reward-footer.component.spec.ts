import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RewardFooterComponent } from './reward-footer.component';

describe('RewardFooterComponent', () => {
  let component: RewardFooterComponent;
  let fixture: ComponentFixture<RewardFooterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RewardFooterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RewardFooterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
